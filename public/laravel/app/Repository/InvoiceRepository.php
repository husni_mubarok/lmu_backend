<?php

namespace App\Repository;
use File;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use App\Model\Invoice;

class InvoiceRepository 
{ 
    public function update_image($id, $image)
    {
        $invoice = Invoice::FindOrFail($id);

        $original_directory = "uploads/invoice/".$invoice->invoice_desc."/";
        
        if(!File::exists($original_directory))
        {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
        }
        $file_extension = $image->getClientOriginalExtension();
        $invoice->invoice_attachment = Carbon::now()->format("d-m-Y h-i-s").$image->getClientOriginalName();
        $image->move($original_directory, $invoice->invoice_attachment);

        $thumbnail_directory = $original_directory."thumbnail/";
        if(!File::exists($thumbnail_directory))
        {
            File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
        }
        $thumbnail = Image::make($original_directory.$invoice->invoice_attachment);
        $thumbnail->fit(300,300)->save($thumbnail_directory.$invoice->invoice_attachment);

        $invoice->save();
        return $invoice;
    }

    public function delete_image($id)
    {
        $invoice = Invoice::FindOrFail($id);
        $invoice->invoice_attachment = null;
        $invoice->save();
    }

}