<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Background extends Model
{
    use SoftDeletes;

    protected $table = 'background';
    protected $dates = ['deleted_at'];
 
}
