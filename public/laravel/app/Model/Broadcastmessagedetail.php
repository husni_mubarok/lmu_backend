<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Broadcastmessagedetail extends Model
{
    use SoftDeletes;

    protected $table = 'broadcast_message_detail';
    protected $dates = ['deleted_at']; 
}
