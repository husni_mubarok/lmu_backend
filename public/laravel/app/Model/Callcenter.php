<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Callcenter extends Model
{
    use SoftDeletes;

    protected $table = 'call_center';
    protected $dates = ['deleted_at']; 
}
