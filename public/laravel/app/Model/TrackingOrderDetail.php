<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrackingOrderDetail extends Model
{
    use SoftDeletes;

    protected $table = 'view_order_detail';
    protected $dates = ['deleted_at']; 
}
