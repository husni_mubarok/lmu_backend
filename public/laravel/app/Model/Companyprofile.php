<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Companyprofile extends Model
{
    use SoftDeletes;

    protected $table = 'company_profile';
    protected $dates = ['deleted_at']; 
}
