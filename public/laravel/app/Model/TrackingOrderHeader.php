<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrackingOrderHeader extends Model
{
    use SoftDeletes;

    protected $table = 'view_sales_summary';
    protected $dates = ['deleted_at']; 

    public function tracking_order_detail()
    {
        return $this->hasMany('App\Model\TrackingOrderDetail', 'id_header', 'id');
    }
}

