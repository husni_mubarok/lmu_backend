<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Broadcastmessage extends Model
{
    use SoftDeletes;

    protected $table = 'broadcast_message';
    protected $dates = ['deleted_at']; 
}
