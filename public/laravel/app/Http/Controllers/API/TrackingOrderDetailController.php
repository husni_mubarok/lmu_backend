<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\TrackingOrderDetail;

class TrackingOrderDetailController extends Controller
{
   
  public function index($id)
  {
    $tracking_order_header = TrackingOrderDetail::where('id_delivery_order', $id)->get();  
    for($i = 0; $i < $tracking_order_header->count(); $i++)
    {
      $json[$i] = 
      [ 
      'material' => $tracking_order_header[$i]->material,  
      'unit' => $tracking_order_header[$i]->unit,  
      'quantity' => $tracking_order_header[$i]->quantity,  
      ];
    }
    return response()->json($json);
  } 
}
