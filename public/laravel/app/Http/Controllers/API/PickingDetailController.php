<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\TrackingOrderDetail;
use DB;


class PickingDetailController extends Controller
{

  public function index($id)
  {
    $sql1 = 'SELECT
    sales_order.id,
    sales_order.sales_no,
    sales_order.sales_date,
    sales_order.po_no,
    marketing.sname,
    sales_order.currency,
    sales_order.so_status,
    sales_order.id_customer
    FROM
    sales_order
    INNER JOIN marketing ON sales_order.id_marketing = marketing.id
    INNER JOIN sales_order_detail ON sales_order.id = sales_order_detail.id_sales_order
    INNER JOIN material ON sales_order_detail.id_material = material.id
    INNER JOIN picking_order_detail ON sales_order_detail.id = picking_order_detail.id_sales_order_detail
    INNER JOIN picking_order ON picking_order_detail.id_picking = picking_order.id
    WHERE
    so_status = 0 AND sales_order.id_customer = '.$id.'
    GROUP BY
    sales_order.id,
    sales_order.sales_no,
    sales_order.sales_date,
    sales_order.po_no,
    marketing.sname,
    sales_order.currency,
    sales_order.so_status,
    sales_order.id_customer';
    $data_header = DB::table(DB::raw("(" . $sql1 . ") as rs_sql"))->get(); 

    if(count($data_header) > 0)
      {
    foreach ($data_header as $h_key => $data_headers) {
        $sql2 = 'SELECT
                    sales_order_detail.id,
                    sales_order_detail.id_sales_order,
                    material.matnr AS item_code,
                    material.maktx AS item_name,
                    sales_order_detail.unit,
                    picking_order_detail.quantity,
                    sales_order_detail.price,
                    picking_order.picking_no,
                    picking_order.picking_date
                FROM
                    sales_order_detail
                INNER JOIN material ON sales_order_detail.id_material = material.id
                INNER JOIN picking_order_detail ON sales_order_detail.id = picking_order_detail.id_sales_order_detail
                INNER JOIN picking_order ON picking_order_detail.id_picking = picking_order.id';

        $data_detail = DB::table(DB::raw("(" . $sql2 . ") as rs_sql"))
        ->where('id_sales_order', '=', $data_headers->id)
        ->get(); 

        $json[$h_key]['id'] = $data_headers->id;
        $json[$h_key]['sales_no'] = $data_headers->sales_no;
        $json[$h_key]['sales_date'] = $data_headers->sales_date;
        $json[$h_key]['po_no'] = $data_headers->po_no;
        $json[$h_key]['sname'] = $data_headers->sname;
        $json[$h_key]['currency'] = $data_headers->currency;
        $json[$h_key]['id_customer'] = $data_headers->id_customer;

        foreach($data_detail as $d_key => $data_details)
        {
            $json[$h_key]['detail'][$d_key]['picking_no'] = $data_details->picking_no;
            $json[$h_key]['detail'][$d_key]['picking_date'] = $data_details->picking_date;
            $json[$h_key]['detail'][$d_key]['item_code'] = $data_details->item_code;
            $json[$h_key]['detail'][$d_key]['item_name'] = $data_details->item_name;
            $json[$h_key]['detail'][$d_key]['unit'] = $data_details->unit;
            $json[$h_key]['detail'][$d_key]['quantity'] = $data_details->quantity;
            $json[$h_key]['detail'][$d_key]['price'] = $data_details->price;  
        } 

    } 

     }else{
    $json[] = 
      [
        'message' => 'Tidak ada data!',  
      ];

  };  
    return response()->json($json);
}
}

