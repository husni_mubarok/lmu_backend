<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Rating;
use App\Model\Customer;
use App\Model\User;
use DB;

class RatingController extends Controller
{

  //change status to confirm
  public function store(Request $request)
  {
    // dd($request['customer_id']);
    $auth = new Rating;
    $auth->customer_id = $request['customer_id'];
    $auth->shipment_id = $request['shipment_id']; 
    $auth->rate = $request['rate']; 
    $auth->comment = $request['comment']; 
    $auth->save();
 
    return $auth;
  } 
  
}
