<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Callcenter;

class CallcenterController extends Controller
{
   
  public function index()
  {
    $callcenter = Callcenter::all(); 
    for($i = 0; $i < $callcenter->count(); $i++)
    {
      $json[$i] = 
      [
      'id' => $callcenter[$i]->id, 
      'name' => $callcenter[$i]->name,
      'phone_number' => $callcenter[$i]->phone_number,
      'image' => $callcenter[$i]->image, 
      ];
    }
    return response()->json($json);
  } 
}
