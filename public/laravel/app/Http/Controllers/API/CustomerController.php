<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Customer;
use DB;

class CustomerController extends Controller
{
   
  public function index($id)
  {
    $customer = Customer::where('id', $id)->get(); 

     $sql1 = 'SELECT
                customer.id,
                customer.mandt,
                customer.kunnr,
                customer.name1,
                customer.name2,
                `user`.filename,
                customer.stras,
                customer.telf1,
                user.first_name,
                user.last_name
              FROM
                customer
              INNER JOIN `user` ON customer.id = `user`.customer_id AND customer.id = '.$id.'';
    $customer = DB::table(DB::raw("(" . $sql1 . ") as rs_sql"))->get(); 

    for($i = 0; $i < $customer->count(); $i++)
    {
      $json[$i] = 
      [
      'id_customer' => $customer[$i]->id, 
      'customer_code' => $customer[$i]->kunnr,
      'customer_name' => $customer[$i]->name1, 
      'address' => $customer[$i]->stras,
      'telp' => $customer[$i]->telf1,
      'first_name' => $customer[$i]->first_name,
      'last_name' => $customer[$i]->last_name,
      'filename' => $customer[$i]->filename
      
      ];
    }
    return response()->json($json);
  } 

   public function getbyphone($id)
  {
    $customer = Customer::where('telf1', $id)->get(); 

    for($i = 0; $i < $customer->count(); $i++)
    {
      $json[$i] = 
      [
      'id_customer' => $customer[$i]->id, 
      'customer_code' => $customer[$i]->kunnr,
      'customer_name' => $customer[$i]->name1, 
      'address' => $customer[$i]->stras
      ];
    }
    return response()->json($json);
  } 

   public function update($id)
  {
    $customer = Customer::where('id', $id)->get(); 

    for($i = 0; $i < $customer->count(); $i++)
    {
      $json[$i] = 
      [
      'id_customer' => $customer[$i]->id, 
      'customer_code' => $customer[$i]->kunnr,
      'customer_name' => $customer[$i]->name1, 
      'address' => $customer[$i]->stras,
      'telp' => $customer[$i]->telf1
      ];
    }
    return response()->json($json);
  } 
}
