<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Customer;

class CustomerStatusController extends Controller
{

  public function index($id)
  {
    $customer = Customer::where('telf1', $id)->first(); 

    if(isset($customer)) {
      $status = true;
    }else{
      $status = false;
    };
    
    $json[] = 
    [
      'status' => $status,  
    ];
    
    return response()->json($json);
  } 
}
