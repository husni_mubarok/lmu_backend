<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Companyprofile;

class CompanyprofileController extends Controller
{
   
  public function index()
  {
    $companyprofile = Companyprofile::first(); 
    
      $json[] = 
      [
      'id' => $companyprofile->id, 
      'name' => $companyprofile->name,
      'address' => $companyprofile->address,
      'tlp' => $companyprofile->tlp,
      'fax' => $companyprofile->fax,
      'email' => $companyprofile->email,
      'description' => $companyprofile->description, 
      'image' => $companyprofile->image, 
      ];
   
    return response()->json($json);
  } 
}
