<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Registration;
use App\Model\Customer;
use App\Model\User;
use DB;

class RegistrationController extends Controller
{

  //change status to confirm
  public function store(Request $request)
  {
    $auth = new Registration;
    $auth->customer_id = $request['customer_id'];
    $auth->username = $request['username'];
    $auth->password = md5($request['password']);
    $auth->save();

    // return $auth; 
    // $auth = Registration::Where('username', $request['username'])->Where('password', $request['password'])->first();
    return $auth;
  }

   public function storeuser(Request $request)
    {
      $ver_code = Registration::Where('ver_code', $request->ver_code)->first();

      // dd($ver_code);

      if(isset($ver_code))
      {
        $auth = new User;
        $auth->customer_id = $ver_code->customer_id;
        $auth->username = $ver_code->username;
        $auth->password = $ver_code->password;
        $auth->save();
        
        $ver_code->delete();
        return $auth;
      }else{
        $json[] = 
          [
            'status' => false,  
          ];

        return response()->json($json);
      };
      
    }

  public function numbercheck(Request $request)
  { 
    $numbercheck = Customer::Where('telf1', $request['telp'])->first();

    //dd($numbercheck);
    
    if(isset($numbercheck)) {
      $status = true;
    }else{
      $status = false;
    };
    
    $json[] = 
    [
      'status' => $status,  
    ];
    
    return response()->json($json);
  }

  
}
