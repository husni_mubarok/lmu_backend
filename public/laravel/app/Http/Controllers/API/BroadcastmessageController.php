<?php

namespace App\Http\Controllers\API;
use Auth;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Broadcastmessage;
use App\Model\Broadcastmessagedetail;

class BroadcastmessageController extends Controller
{
   
  public function index($id)
  {
    // $broadcastmessage = Broadcastmessage::all(); 

    $sql1 = 'SELECT
              broadcast_message_detail.id,
              broadcast_message.`subject`,
              broadcast_message.content,
              broadcast_message_detail.customer_id,
              DATE_FORMAT(broadcast_message.created_at, "%d-%m-%Y %H:%i") AS created_at,
              broadcast_message_detail.`status`
            FROM
              broadcast_message
            INNER JOIN broadcast_message_detail ON broadcast_message.id = broadcast_message_detail.broadcast_message_id
            WHERE broadcast_message_detail.customer_id = '.$id.'
            ORDER BY broadcast_message.created_at DESC';
    $broadcastmessage = DB::table(DB::raw("(" . $sql1 . ") as rs_sql"))->orderBy('created_at', 'DESC')->get(); 

 if(count($broadcastmessage) > 0)
      {
    for($i = 0; $i < $broadcastmessage->count(); $i++)
    {
      $json[$i] = 
      [
      'id' => $broadcastmessage[$i]->id, 
      'subject' => $broadcastmessage[$i]->subject, 
      'content' => $broadcastmessage[$i]->content, 
      'customer_id' => $broadcastmessage[$i]->customer_id, 
      'status' => $broadcastmessage[$i]->status, 
      'created_at' => $broadcastmessage[$i]->created_at, 
      ];
    }
     }else{
    $json[] = 
      [
        'message' => 'Data not available!',  
      ];
    }
    return response()->json($json);
  } 

  //change status to confirm
  public function update(Request $request)
  {
    // $id_message = $request['id_message'];
    // dd($request['id_message']);
    $bm = Broadcastmessagedetail::where('id', $request['id_message'])->first();
    $bm->status = 1; 
    $bm->save();
 
    return $bm;
  } 


  public function count($id)
  {
    // $customer = Customer::where('telf1', $id)->first(); 

    $sql1 = 'SELECT
                broadcast_message_detail.customer_id,
                COUNT(
                  broadcast_message_detail.`status`
                ) AS count
              FROM
                broadcast_message_detail
              WHERE broadcast_message_detail.`status` = 0 AND  broadcast_message_detail.customer_id = '.$id.'
              GROUP BY
              broadcast_message_detail.customer_id';
    $unread = DB::table(DB::raw("(" . $sql1 . ") as rs_sql"))->first(); 

    if(isset($unread)) {
      $count = $unread->count;
    }else{
      $count = 0;
    };

    $json[] = 
    [
      'status' => $count,  
    ];
    
    return response()->json($json);
  } 

}
