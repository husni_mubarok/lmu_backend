<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\TrackingOrderHeader;
use DB;


class OrderCompleteController extends Controller
{
   
  public function index($id)
  {
     $sql1 = 'SELECT
                    sales_order.id,
                    sales_order.sales_no,
                    sales_order.sales_date,
                    customer.name1 AS customer,
                    sales_order.po_no,
                    marketing.sname,
                    sales_order.currency,
                    NULL AS deleted_at,
                    sales_order.so_status,
                    customer.id AS id_customer
                FROM
                    sales_order
                INNER JOIN customer ON sales_order.id_customer = customer.id
                INNER JOIN marketing ON sales_order.id_marketing = marketing.id';

        
    $data_header = DB::table(DB::raw("(" . $sql1 . ") as rs_sql"))
                   ->where('so_status', 1)->where('id_customer', $id)->get();

                   // dd($data_header);   
    
    if(count($data_header)>0)
    {
 
    foreach ($data_header as $h_key => $data_headers) {

     $sql2 = 'SELECT
                    sales_order_detail.id,
                    sales_order_detail.id_sales_order,
                    material.matnr AS item_code,
                    material.maktx AS item_name,
                    sales_order_detail.unit,
                    sales_order_detail.quantity,
                    sales_order_detail.price 
                FROM
                    sales_order_detail
                INNER JOIN material ON sales_order_detail.id_material = material.id
                INNER JOIN picking_order_detail ON sales_order_detail.id = picking_order_detail.id_sales_order_detail';

        $data_detail = DB::table(DB::raw("(" . $sql2 . ") as rs_sql"))
        ->where('id_sales_order', '=', $data_headers->id)
        ->get();  

        $json[$h_key]['id'] = $data_headers->id;
        $json[$h_key]['sales_no'] = $data_headers->sales_no;
        $json[$h_key]['sales_date'] = $data_headers->sales_date;
        $json[$h_key]['po_no'] = $data_headers->po_no;
        $json[$h_key]['sname'] = $data_headers->sname;
        $json[$h_key]['currency'] = $data_headers->currency;
        $json[$h_key]['id_customer'] = $data_headers->id_customer; 

        foreach($data_detail as $d_key => $data_details)
        { 
            $json[$h_key]['detail'][$d_key]['item_code'] = $data_details->item_code;
            $json[$h_key]['detail'][$d_key]['item_name'] = $data_details->item_name;
            $json[$h_key]['detail'][$d_key]['unit'] = $data_details->unit;
            $json[$h_key]['detail'][$d_key]['quantity'] = $data_details->quantity;
            $json[$h_key]['detail'][$d_key]['price'] = $data_details->price;  
        } 
        }
    }else{
    $json[] = 
      [
        'message' => 'Data not available!', 
      ]; 

    }
    return response()->json($json);
  } 

  public function count($id)
  {
  // $data_header = TrackingOrderHeader::where('so_status', 0)->where('customer_id', $id)->get();  

   $sql = 'SELECT
                COUNT(view_sales_summary.id) AS count
            FROM
                view_sales_summary 
            WHERE
            so_status = 0 AND view_sales_summary.customer_id = '.$id.'
            GROUP BY view_sales_summary.customer_id';
    $data_active = DB::table(DB::raw("(" . $sql . ") as rs_sql"))->first();

   $sql1 = 'SELECT
                COUNT(view_sales_summary.id) AS count
            FROM
                view_sales_summary 
            WHERE
            so_status = 1 AND view_sales_summary.customer_id = '.$id.'
            GROUP BY view_sales_summary.customer_id';
    $data_complete = DB::table(DB::raw("(" . $sql1 . ") as rs_sql"))->first();

   if(isset($data_active)) 
   {
    $active = $data_active->count;
   }else{
    $active = 0;
   }

   if(isset($complete))
   {
    $complete = $data_complete->count;
   }else{
    $complete = 0;
   }
    
    $json[] = 
          [
            'active' => $active,
            'complete' => $complete,
          ]; 
    return response()->json($json);
  } 
}
