<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\TrackingOrderDetail;
use DB;

class TrackingOrderSOController extends Controller
{
   
  public function index($id)
  {
    $sql = 'SELECT
            view_order_detail.sales_no,
            view_order_detail.sales_date,
            view_order_detail.po_no
            FROM
              view_order_detail
            WHERE view_order_detail.id_customer =  '.$id.'
            GROUP BY
              view_order_detail.sales_no,
              view_order_detail.sales_date,
            view_order_detail.po_no';
            $tracking_order_header = DB::table(DB::raw("(" . $sql . ") as rs_sql"))->get(); 
    for($i = 0; $i < $tracking_order_header->count(); $i++)
    {
      $json[$i] = 
      [
      'sales_no' => $tracking_order_header[$i]->sales_no,  
      'sales_date' => $tracking_order_header[$i]->sales_date,  
      'po_no' => $tracking_order_header[$i]->po_no,    
      ];
    }
    return response()->json($json);
  } 
}
