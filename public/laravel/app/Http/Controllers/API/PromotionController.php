<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Promotion;
use DB;




class PromotionController extends Controller
{
   
  public function index()
  {
    $promotion = Promotion::all(); 
    for($i = 0; $i < $promotion->count(); $i++)
    {
      $json[$i] = 
      [
      'id' => $promotion[$i]->id, 
      'title' => $promotion[$i]->title,
      'content' => $promotion[$i]->content,
      'periodfrom' => $promotion[$i]->periodfrom,
      'periodto' => $promotion[$i]->periodto,
      'image' => $promotion[$i]->image, 
      ];
    }
    return response()->json($json);
  } 


  public function popup()
  {
     $sql1 = 'SELECT
                promotion.id,
                promotion.title,
                promotion.periodfrom,
                promotion.periodto,
                promotion.content,
                promotion.image
              FROM
                promotion
              WHERE NOW() BETWEEN promotion.periodfrom AND promotion.periodto';
    $promotion = DB::table(DB::raw("(" . $sql1 . ") as rs_sql"))->first();

    if(isset($promotion))
    {
      $json[] = 
      [
      'id' => $promotion->id, 
      'title' => $promotion->title,
      'content' => $promotion->content,
      'periodfrom' => $promotion->periodfrom,
      'periodto' => $promotion->periodto,
      'image' => $promotion->image, 
      ];

       }else{
          $json[] = 
            [
              'message' => 'Tidak ada promosi',  
            ]; 
        };
    
    return response()->json($json);
  } 
}
