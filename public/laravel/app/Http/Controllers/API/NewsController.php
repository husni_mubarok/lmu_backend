<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\News;

class NewsController extends Controller
{
   
  public function index()
  {
    $news = News::all(); 
    for($i = 0; $i < $news->count(); $i++)
    {
      $json[$i] = 
      [
      'id' => $news[$i]->id, 
      'title' => $news[$i]->title,
      'content' => $news[$i]->content,
      'date' => $news[$i]->created_at,
      'image' => $news[$i]->image, 
      ];
    }
    return response()->json($json);
  } 
}
