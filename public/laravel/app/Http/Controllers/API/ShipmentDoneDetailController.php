<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\TrackingOrderDetail;
use DB;


class ShipmentDoneDetailController extends Controller
{

 public function index($id)
  {
    $sql1 = 'SELECT
    sales_order.id,
    sales_order.sales_no,
    sales_order.sales_date,
    sales_order.po_no,
    marketing.sname,
    sales_order.currency,
    sales_order.so_status,
    sales_order.id_customer
    FROM
    sales_order
    INNER JOIN marketing ON sales_order.id_marketing = marketing.id
    WHERE
    so_status = 0 AND sales_order.id_customer = '.$id.'';
    $data_header = DB::table(DB::raw("(" . $sql1 . ") as rs_sql"))->get(); 


    foreach ($data_header as $h_key => $data_headers) {
        $sql2 = 'SELECT
                    shipment.id,
                    sales_order_detail.id_sales_order,
                    shipment.shipment_no,
                    shipment.transport_plan,
                    shipment.truck_no,
                    shipment.driver_name,
                    shipment.driver_hp,
                    shipment.plan_delivery_date,
                    shipment.loading_date,
                    shipment.loading_strart_time,
                    shipment.loading_end_time,
                    shipment.delivery_start_date,
                    shipment.delivery_start_time,
                    shipment.delivery_end_date,
                    shipment.delivery_end_time,
                    shipment.delivered_status
                FROM
                    sales_order_detail
                INNER JOIN shipment_detail ON sales_order_detail.id = shipment_detail.id_sales_order_detail
                INNER JOIN shipment ON shipment_detail.id_shipment = shipment.id
                WHERE
                    shipment.delivered_status = 1
                GROUP BY
                    shipment.shipment_no,
                    shipment.transport_plan,
                    shipment.truck_no,
                    shipment.driver_name,
                    shipment.driver_hp,
                    shipment.plan_delivery_date,
                    shipment.loading_date,
                    shipment.loading_strart_time,
                    shipment.loading_end_time,
                    shipment.delivery_start_date,
                    shipment.delivery_start_time,
                    shipment.delivery_end_date,
                    shipment.delivery_end_time,
                    shipment.delivered_status';

        $data_detail = DB::table(DB::raw("(" . $sql2 . ") as rs_sql"))
        ->where('id_sales_order', '=', $data_headers->id)
        ->get(); 

        $json[$h_key]['id'] = $data_headers->id;
        $json[$h_key]['sales_no'] = $data_headers->sales_no;
        $json[$h_key]['sales_date'] = $data_headers->sales_date;
        $json[$h_key]['po_no'] = $data_headers->po_no;
        $json[$h_key]['sname'] = $data_headers->sname;
        $json[$h_key]['currency'] = $data_headers->currency;
        $json[$h_key]['id_customer'] = $data_headers->id_customer;

        foreach($data_detail as $d_key => $data_details)
        {

       

                $json[$h_key]['detail'][$d_key]['shipment_no'] = $data_details->shipment_no;
                $json[$h_key]['detail'][$d_key]['loading_date'] = $data_details->loading_date;
                $json[$h_key]['detail'][$d_key]['truck_no'] = $data_details->truck_no;
                $json[$h_key]['detail'][$d_key]['driver_name'] = $data_details->driver_name;
                $json[$h_key]['detail'][$d_key]['driver_hp'] = $data_details->driver_hp;
                $json[$h_key]['detail'][$d_key]['loading_strart_time'] = $data_details->loading_strart_time;
                $json[$h_key]['detail'][$d_key]['loading_end_time'] = $data_details->loading_end_time;
                $json[$h_key]['detail'][$d_key]['delivery_start_date'] = $data_details->delivery_start_date;
                $json[$h_key]['detail'][$d_key]['delivery_start_time'] = $data_details->delivery_start_time;
                $json[$h_key]['detail'][$d_key]['delivery_end_date'] = $data_details->delivery_end_date;
                $json[$h_key]['detail'][$d_key]['delivery_end_time'] = $data_details->delivery_end_time; 
     
               $sql3 = 'SELECT
                            shipment_detail.id_shipment,
                            sales_order_detail.id,
                            sales_order_detail.id_sales_order,
                            material.matnr AS item_code,
                            material.maktx AS item_name,
                            sales_order_detail.unit,
                            sales_order_detail.quantity,
                            sales_order_detail.price
                        FROM
                            sales_order_detail
                        INNER JOIN material ON sales_order_detail.id_material = material.id
                        INNER JOIN shipment_detail ON sales_order_detail.id = shipment_detail.id_sales_order_detail 
                        INNER JOIN shipment ON shipment_detail.id_shipment = shipment.id
                        WHERE
                            shipment.delivered_status=1';

                $data_subdetail = DB::table(DB::raw("(" . $sql3 . ") as rs_sql"))
                ->where('id_shipment', '=', $data_details->id)
                ->get();  

                 foreach($data_subdetail as $sd_key => $data_subdetails)
                {
                    $json[$h_key]['detail'][$d_key]['subdetail'][$sd_key]['item_code'] = $data_subdetails->item_code;
                    $json[$h_key]['detail'][$d_key]['subdetail'][$sd_key]['item_name'] = $data_subdetails->item_name;
                    $json[$h_key]['detail'][$d_key]['subdetail'][$sd_key]['unit'] = $data_subdetails->unit;
                    $json[$h_key]['detail'][$d_key]['subdetail'][$sd_key]['quantity'] = $data_subdetails->quantity;
                    $json[$h_key]['detail'][$d_key]['subdetail'][$sd_key]['price'] = $data_subdetails->price; 
                } 
        } 

    } 
    return response()->json($json);
}
}

