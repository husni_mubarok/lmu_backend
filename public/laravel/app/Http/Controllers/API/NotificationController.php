<?php

// Required if your environment does not handle autoloading
//require 'public/laravel/vendor/autoload.php';
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Registration;
use App\Model\Customer;
use DB;

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

class NotificationController extends Controller
{
 
    public function sendsms(Request $request)
    {

        $today = date('dHi');
        $startDate = date('dHi', strtotime('-10 days'));
        $range = $today - $startDate;
        $rand = rand(0, $range);
        $ver_code = $startDate + $rand;

        $numbercheck = Customer::Where('telf1', $request['telp'])->first();


        // dd($numbercheck);

        if(isset($numbercheck)) {

        $customer_id = Registration::Where('customer_id', $numbercheck->id)->first();

        if(isset($customer_id)) 
        {
            //dd($customer_id);
            $customer_id->delete(); 
        };

        $client = new Registration;
        $client->customer_id = $numbercheck->id;
        $client->username = $request['username'];
        $client->password = md5($request['password']);
        $client->ver_code = $ver_code;
        $client->save();

        $to = $numbercheck->telf1;
        // https://www.twilio.com/docs/libraries/php
        //https://www.twilio.com/docs/api/rest/test-credentials
        //husni.mubarok@outlook.com 1sQuarepants123
        //Your Account SID and Auth Token from twilio.com/console
        $sid = 'AC2afe2dadabfe4922f51c2ee3f16e3e0e';
        //$apiKey = 'P310ARNZQ7rqUsP4hnrWWpjwtsBhpWAf';
        $token = '4910b81177e11a389fbe7e2b8f3d4e39';
        $client = new Client($sid, $token);

        // Use the client to do fun stuff like send text messages!
        $client->messages->create(
            // the number you'd like to send the message to
            ''.$to.'',
            array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => '+16232320940',
                // the body of the text message you'd like to send
                'body' => 'Your LMU Mobile verification code is: '.$ver_code.''
            )
        );

        return $client;

        }else{
          $json[] = 
            [
              'status' => false,  
            ];

          return response()->json($json);
        };
    }
}