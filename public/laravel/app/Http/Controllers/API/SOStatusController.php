<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\TrackingOrderHeader;
use DB;

class SOStatusController extends Controller
{
   
  public function index($id)
  {
    $sql = 'SELECT
              qty_order.id_customer,
              customer.kunnr AS customer_code,
              customer.name1 AS customer_name,
              qty_order.qty_order,
              qty_process.qty_process,
              qty_load.qty_load,
              qty_ship.qty_ship,
              qty_done.qty_done
            FROM
              (
                SELECT
                  sales_order.id_customer,
                  COUNT(sales_order.id) AS qty_order
                FROM
                  sales_order
                GROUP BY
                  sales_order.id_customer
              ) AS qty_order
            LEFT JOIN (
              SELECT
                sales_order.id_customer,
                COUNT(sales_order.id) qty_process
              FROM
                sales_order
              INNER JOIN (
                SELECT
                  sales_order_detail.id_sales_order
                FROM
                  sales_order_detail
                WHERE
                  sales_order_detail.id IN (
                    SELECT
                      id_sales_order_detail
                    FROM
                      picking_order_detail
                  )
                GROUP BY
                  sales_order_detail.id_sales_order
              ) sales_order_detail ON sales_order_detail.id_sales_order = sales_order.id
              GROUP BY
                sales_order.id_customer
            ) AS qty_process ON qty_order.id_customer = qty_process.id_customer
            LEFT JOIN (
              SELECT
                sales_order.id_customer,
                COUNT(sales_order.id) qty_load
              FROM
                sales_order
              INNER JOIN (
                SELECT
                  sales_order_detail.id_sales_order
                FROM
                  sales_order_detail
                WHERE
                  sales_order_detail.id IN (
                    SELECT
                      id_sales_order_detail
                    FROM
                      delivery_order_detail
                  )
                GROUP BY
                  sales_order_detail.id_sales_order
              ) sales_order_detail ON sales_order_detail.id_sales_order = sales_order.id
              GROUP BY
                sales_order.id_customer
            ) AS qty_load ON qty_order.id_customer = qty_load.id_customer
            LEFT JOIN (
              SELECT
                sales_order.id_customer,
                COUNT(sales_order.id) qty_ship
              FROM
                sales_order
              INNER JOIN (
                SELECT
                  sales_order_detail.id_sales_order
                FROM
                  sales_order_detail
                WHERE
                  sales_order_detail.id IN (
                    SELECT
                      shipment_detail.id_sales_order_detail
                    FROM
                      shipment_detail
                    INNER JOIN shipment ON shipment_detail.id_shipment = shipment.id
                    WHERE
                      shipment.delivered_status IS NULL
                  )
                GROUP BY
                  sales_order_detail.id_sales_order
              ) sales_order_detail ON sales_order_detail.id_sales_order = sales_order.id
              GROUP BY
                sales_order.id_customer
            ) AS qty_ship ON qty_order.id_customer = qty_ship.id_customer
            LEFT JOIN (
              SELECT
                sales_order.id_customer,
                COUNT(sales_order.id) qty_done
              FROM
                sales_order
              INNER JOIN (
                SELECT
                  sales_order_detail.id_sales_order
                FROM
                  sales_order_detail
                WHERE
                  sales_order_detail.id IN (
                    SELECT
                      shipment_detail.id_sales_order_detail
                    FROM
                      shipment_detail
                    INNER JOIN shipment ON shipment_detail.id_shipment = shipment.id
                    WHERE
                      shipment.delivered_status = 1
                  )
                GROUP BY
                  sales_order_detail.id_sales_order
              ) sales_order_detail ON sales_order_detail.id_sales_order = sales_order.id
              GROUP BY
                sales_order.id_customer
            ) AS qty_done ON qty_order.id_customer = qty_done.id_customer
            INNER JOIN customer ON qty_order.id_customer = customer.id
            WHERE
              qty_order.id_customer = '.$id.'';
            $so_status = DB::table(DB::raw("(" . $sql . ") as rs_sql"))->get(); 

    for($i = 0; $i < $so_status->count(); $i++)
    {
      $json[$i] = 
      [
      'id_customer' => $so_status[$i]->id_customer, 
      'customer_code' => $so_status[$i]->customer_code,
      'customer_name' => $so_status[$i]->customer_name, 
      'qty_order' => $so_status[$i]->qty_order,
      'qty_process' => $so_status[$i]->qty_process,
      'qty_load' => $so_status[$i]->qty_load,
      'qty_ship' => $so_status[$i]->qty_ship,
      'qty_done' => $so_status[$i]->qty_done
      ];
    }
    return response()->json($json);
  } 
}
