<?php

namespace App\Http\Controllers\API;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Background;

class BackgroundController extends Controller
{
   
  public function index()
  {
    $banner = Background::all(); 
    for($i = 0; $i < $banner->count(); $i++)
    {
      $json[$i] = 
      [
      'id' => $banner[$i]->id, 
      'title' => $banner[$i]->title, 
      'image' => str_replace(" ", "_", $banner[$i]->image), 
      ];
    }
    return response()->json($json);
  } 
}
