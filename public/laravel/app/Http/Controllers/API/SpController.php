<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Auth;
use DB;

class SpController extends Controller
{

  //change status to confirm
  public function spadddatesent(Request $request, $id)
  {
    
    $auth = DB::statement('call spAddDateSent(?)',["$id"]);
    return response()->json($auth);
  }

   public function spshipmentback(Request $request, $id)
  {
    
    $auth = DB::statement('call spShipmentBack(?)',["$id"]);
    return response()->json($auth);
  }

   public function spshipmentsent(Request $request, $id)
  {
    
    $auth = DB::statement('call spShipmentSent(?)',["$id"]);
    return response()->json($auth);
  }

}
