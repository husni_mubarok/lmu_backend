<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\TrackingOrderDetail;
use DB;

class TrackingOrderDOController extends Controller
{
   
  public function index($id)
  {
    $sql = 'SELECT
              view_order_detail.id_delivery_order,
              view_order_detail.delivery_no,
              view_order_detail.delivery_date,
              view_order_detail.shipment_no,
              view_order_detail.transport_plan,
              view_order_detail.truck_no,
              view_order_detail.driver_name,
              view_order_detail.driver_hp,
              view_order_detail.plan_delivery_date,
              view_order_detail.loading_date,
              view_order_detail.loading_strart_time,
              view_order_detail.loading_end_time,
              view_order_detail.delivery_start_date,
              view_order_detail.delivery_start_time,
              view_order_detail.delivery_end_date,
              view_order_detail.delivery_end_time
            FROM
              view_order_detail
            WHERE view_order_detail.id_sales_order =  '.$id.'
            GROUP BY
              view_order_detail.delivery_no,
              view_order_detail.delivery_date,
              view_order_detail.shipment_no,
              view_order_detail.transport_plan,
              view_order_detail.truck_no,
              view_order_detail.driver_name,
              view_order_detail.driver_hp,
              view_order_detail.plan_delivery_date,
              view_order_detail.loading_date,
              view_order_detail.loading_strart_time,
              view_order_detail.loading_end_time,
              view_order_detail.delivery_start_date,
              view_order_detail.delivery_start_time,
              view_order_detail.delivery_end_date,
              view_order_detail.delivery_end_time';
            $tracking_order_header = DB::table(DB::raw("(" . $sql . ") as rs_sql"))->get(); 
    for($i = 0; $i < $tracking_order_header->count(); $i++)
    {
      $json[$i] = 
      [
         'id_delivery_order' => $tracking_order_header[$i]->id_delivery_order,  
      'delivery_no' => $tracking_order_header[$i]->delivery_no,  
      'delivery_date' => $tracking_order_header[$i]->delivery_date,  
      'shipment_no' => $tracking_order_header[$i]->shipment_no,  
      'transport_plan' => $tracking_order_header[$i]->transport_plan,  
      'truck_no' => $tracking_order_header[$i]->truck_no,  
      'driver_name' => $tracking_order_header[$i]->driver_name,  
      'driver_hp' => $tracking_order_header[$i]->driver_hp,  
      'plan_delivery_date' => $tracking_order_header[$i]->plan_delivery_date,  
      'loading_date' => $tracking_order_header[$i]->loading_date,  
      'loading_strart_time' => $tracking_order_header[$i]->loading_strart_time,  
      'loading_end_time' => $tracking_order_header[$i]->loading_end_time,  
      'delivery_start_date' => $tracking_order_header[$i]->delivery_start_date,  
      'delivery_start_time' => $tracking_order_header[$i]->delivery_start_time,  
      'delivery_end_date' => $tracking_order_header[$i]->delivery_end_date,  
      'delivery_end_time' => $tracking_order_header[$i]->delivery_end_time     
      ];
    }
    return response()->json($json);
  } 
}
