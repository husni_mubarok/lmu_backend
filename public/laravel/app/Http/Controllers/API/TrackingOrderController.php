<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\TrackingOrderHeader;

class TrackingOrderController extends Controller
{
   
  public function index($id)
  {
    $tracking_order_header = TrackingOrderHeader::all();  
    for($i = 0; $i < $tracking_order_header->count(); $i++)
    {
      $json[$i] = 
      [
      'id' => $tracking_order_header[$i]->id, 
      'sales_no' => $tracking_order_header[$i]->sales_no,
      'sales_date' => $tracking_order_header[$i]->sales_date,
      'customer' => $tracking_order_header[$i]->customer,
      'po_no' => $tracking_order_header[$i]->po_no,
      'markrting' => $tracking_order_header[$i]->markrting,
      'currency' => $tracking_order_header[$i]->currency,
      'received' => $tracking_order_header[$i]->received, 
      'process' => $tracking_order_header[$i]->process, 
      'sending' => $tracking_order_header[$i]->sending, 
      'delivered' => $tracking_order_header[$i]->delivered, 
      ];
    }
    return response()->json($json);
  } 
}
