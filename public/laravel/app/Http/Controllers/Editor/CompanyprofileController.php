<?php

namespace App\Http\Controllers\Editor;

use File;
use Session;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Http\Requests\CompanyprofileRequest;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Model\Companyprofile;

class CompanyprofileController extends Controller
{
    public function index()
    {
    	if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 
        $companyprofiles = Companyprofile::paginate(15);
    	return view ('editor.companyprofile.index', compact('companyprofiles'))->with('number',$no);
    }

    public function create()
    {
    	return view ('editor.companyprofile.form');
    }

    public function store(CompanyprofileRequest $request)
    {
        //dd("sdsada");
    	$companyprofile = new Companyprofile;
    	$companyprofile->name = $request->input('name');
    	$companyprofile->address = $request->input('address'); 
        $companyprofile->tlp = $request->input('tlp'); 
        $companyprofile->fax = $request->input('fax'); 
        $companyprofile->email = $request->input('email'); 
        $companyprofile->description = $request->input('description');  
        $companyprofile->save();

        if($request->image)
        {
            $companyprofile = Companyprofile::FindOrFail($companyprofile->id);

            $original_directory = "uploads/companyprofile/";

            if(!File::exists($original_directory))
            {
                File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

            $file_extension = $request->image->getClientOriginalExtension();
            $companyprofile->image = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
            $request->image->move($original_directory, $companyprofile->image);

            $thumbnail_directory = $original_directory."thumbnail/";
            if(!File::exists($thumbnail_directory))
            {
             File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
            }
            $thumbnail = Image::make($original_directory.$companyprofile->image);
            $thumbnail->fit(30,30)->save($thumbnail_directory.$companyprofile->image);

            $companyprofile->save(); 
        }
 
    	return redirect()->action('Editor\CompanyprofileController@index');
    }

    public function edit($id)
    {
    	$companyprofile = Companyprofile::find($id);
    	return view ('editor.companyprofile.form', compact('companyprofile'));
    }

    public function update($id, CompanyprofileRequest $request)
    {

    	$companyprofile = Companyprofile::find($id);
    	$companyprofile->name = $request->input('name');
        $companyprofile->address = $request->input('address'); 
        $companyprofile->tlp = $request->input('tlp'); 
        $companyprofile->fax = $request->input('fax'); 
        $companyprofile->email = $request->input('email'); 
        $companyprofile->description = $request->input('description');  
        $companyprofile->save();

        if($request->image)
        {
            $companyprofile = Companyprofile::FindOrFail($companyprofile->id);

            $original_directory = "uploads/companyprofile/";

            if(!File::exists($original_directory))
            {
                File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

            $file_extension = $request->image->getClientOriginalExtension();
            $companyprofile->image = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
            $request->image->move($original_directory, $companyprofile->image);

            $thumbnail_directory = $original_directory."thumbnail/";
            if(!File::exists($thumbnail_directory))
            {
             File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
            }
            $thumbnail = Image::make($original_directory.$companyprofile->image);
            $thumbnail->fit(30,30)->save($thumbnail_directory.$companyprofile->image);

            $companyprofile->save(); 
        }
    	return redirect()->action('Editor\CompanyprofileController@index');
    }

    public function delete($id)
    {
    	Companyprofile::find($id)->delete();
    	return redirect()->action('Editor\CompanyprofileController@index');
    }
}
