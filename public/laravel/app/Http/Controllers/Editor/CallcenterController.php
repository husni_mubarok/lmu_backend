<?php

namespace App\Http\Controllers\Editor;

use File;
use Session;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Http\Requests\CallcenterRequest;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Model\Callcenter;

class CallcenterController extends Controller
{
    public function index()
    {
    	if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 
        $callcenters = Callcenter::paginate(15);
    	return view ('editor.callcenter.index', compact('callcenters'))->with('number',$no);
    }

    public function create()
    {
    	return view ('editor.callcenter.form');
    }

    public function store(CallcenterRequest $request)
    {
        //dd("sdsada");
    	$callcenter = new Callcenter;
    	$callcenter->name = $request->input('name');
    	$callcenter->phone_number = $request->input('phone_number'); 
        $callcenter->save();

        if($request->image)
        {
            $callcenter = Callcenter::FindOrFail($callcenter->id);

            $original_directory = "uploads/callcenter/";

            if(!File::exists($original_directory))
            {
                File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

            $file_extension = $request->image->getClientOriginalExtension();
            $callcenter->image = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
            $request->image->move($original_directory, $callcenter->image);

            $thumbnail_directory = $original_directory."thumbnail/";
            if(!File::exists($thumbnail_directory))
            {
             File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
            }
            $thumbnail = Image::make($original_directory.$callcenter->image);
            $thumbnail->fit(30,30)->save($thumbnail_directory.$callcenter->image);

            $callcenter->save(); 
        }
 
    	return redirect()->action('Editor\CallcenterController@index');
    }

    public function edit($id)
    {
    	$callcenter = Callcenter::find($id);
    	return view ('editor.callcenter.form', compact('callcenter'));
    }

    public function update($id, CallcenterRequest $request)
    {

    	$callcenter = Callcenter::find($id);
    	$callcenter->name = $request->input('name');
        $callcenter->phone_number = $request->input('phone_number'); 
        $callcenter->save();

        if($request->image)
        {
            $callcenter = Callcenter::FindOrFail($callcenter->id);

            $original_directory = "uploads/callcenter/";

            if(!File::exists($original_directory))
            {
                File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

            $file_extension = $request->image->getClientOriginalExtension();
            $callcenter->image = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
            $request->image->move($original_directory, $callcenter->image);

            $thumbnail_directory = $original_directory."thumbnail/";
            if(!File::exists($thumbnail_directory))
            {
             File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
            }
            $thumbnail = Image::make($original_directory.$callcenter->image);
            $thumbnail->fit(30,30)->save($thumbnail_directory.$callcenter->image);

            $callcenter->save(); 
        }
    	return redirect()->action('Editor\CallcenterController@index');
    }

    public function delete($id)
    {
    	Callcenter::find($id)->delete();
    	return redirect()->action('Editor\CallcenterController@index');
    }
}
