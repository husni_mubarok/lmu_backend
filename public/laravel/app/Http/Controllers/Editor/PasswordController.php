<?php

namespace App\Http\Controllers\Editor;

use DB;
use Auth;
use Validator;
use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Repository\UserRepository;

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

class PasswordController extends Controller
{
    //
    protected $UserRepository;

    public function __construct(UserRepository $user_repository)
    {
    	$this->UserRepository = $user_repository;
    }
 
    public function edit_password($id)
    {
        $sql = 'SELECT
                  `user`.*, customer.telf1
                FROM
                  customer
                INNER JOIN `user` ON customer.id = `user`.customer_id
                WHERE customer.id = "'.$id.'"';

        $user = DB::table(DB::raw("(" . $sql . ") as rs_sql"))->first(); 

        //dd($user);

        return view ('editor.password.password', compact('user'));
    }

    public function update_password(Request $request, $id)
    {

        $data = array(
            'password_current' => $request->input('password_current'), 
            'password_new' => $request->input('password_new'),
            'password_new_confirmation' => $request->input('password_new_confirmation'), 
            );

        $rules = [
            'password_current' => 'required',
            'password_new' => 'required|confirmed',
            'password_new_confirmation' => 'required',
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {   
            return redirect()->action('Editor\PasswordController@edit_password', $id)->withInput()->withErrors(['New password confirmation failed!']);
        } else { 

            $user = $this->UserRepository->get_cust($id);
 
            if(Hash::check($request->input('password_current'), $user->password))
            {
                $this->UserRepository->change_password_cust($id, $request->input('password_new'));

                return redirect()->action('Editor\PasswordController@edit_password', $id)->with('message', 'Password has been changed!');
 
            } else {
                 return redirect()->action('Editor\PasswordController@edit_password', $id)->withInput()->withErrors(['Current password mismatch!']);
            }
        }
    }

    public function reset_password()
    { 
        return view ('editor.password.reset');
    }

    public function store_password(Request $request)
    {
         $sql1 = 'SELECT
                    customer.telf1,
                    customer.id
                FROM
                    customer
                WHERE
                REPLACE(customer.telf1,"+62","") =  "'.$request['telf1'].'"';
        $cust = DB::table(DB::raw("(" . $sql1 . ") as rs_sql"))->first();

        // dd($cust);

        if(isset($cust))
        {
            $today = date('dHi');
            $startDate = date('dHi', strtotime('-10 days'));
            $range = $today - $startDate;
            $rand = rand(0, $range);
            $ver_code = $startDate + $rand;

            $sid = 'AC2afe2dadabfe4922f51c2ee3f16e3e0e';
            //$apiKey = 'P310ARNZQ7rqUsP4hnrWWpjwtsBhpWAf';
            $token = '4910b81177e11a389fbe7e2b8f3d4e39';
            $client = new Client($sid, $token);

            // Use the client to do fun stuff like send text messages!
            $client->messages->create(
                // the number you'd like to send the message to
                ''.$cust->telf1.'',
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => '+16232320940',
                    // the body of the text message you'd like to send
                    'body' => 'Your LMU Mobile reset password code is: '.$ver_code.''
                )
            );

            $user = User::where('customer_id', $cust->id)->first();
            $user->ver_code = $ver_code;
            $user->save();

            // return back()->with('error', 'Success!'); 
            return redirect()->action('Editor\PasswordController@reset_vercode', $cust->id); 

        }else{
            return back()->with('error', 'Your phone number is not valid!');
        }   
    }


    public function reset_vercode($id)
    { 
        $sql1 = 'SELECT
                    customer.telf1,
                    customer.id,
                    `user`.ver_code
                FROM
                    customer
                INNER JOIN `user` ON customer.id = `user`.customer_id
                WHERE
                customer.id='.$id.'';
        $cust = DB::table(DB::raw("(" . $sql1 . ") as rs_sql"))->first();

        return view ('editor.password.vercode', compact('cust'));
    }


    public function store_vercode(Request $request, $id)
    {
         $sql1 = 'SELECT
                    customer.telf1,
                    customer.id,
                    `user`.ver_code
                FROM
                    customer
                INNER JOIN `user` ON customer.id = `user`.customer_id
                WHERE
                customer.id='.$id.' AND `user`.ver_code = "'.$request['ver_code'].'"';
        $cust = DB::table(DB::raw("(" . $sql1 . ") as rs_sql"))->first();



         // dd($cust);

        if(isset($cust))
        { 

            $user = User::where('customer_id', $cust->id)->first();
            $user->ver_code = '';
            $user->save();

            // return back()->with('error', 'Success!'); 
            return redirect()->action('Editor\PasswordController@reset_newpass', $id); 

        }else{
            return back()->with('error', 'Your verification code is not valid!');
        }   
    }


    public function reset_newpass($id)
    { 
        $sql1 = 'SELECT
                    customer.telf1,
                    customer.id,
                    `user`.ver_code
                FROM
                    customer
                INNER JOIN `user` ON customer.id = `user`.customer_id
                WHERE
                customer.id='.$id.'';
        $cust = DB::table(DB::raw("(" . $sql1 . ") as rs_sql"))->first();

        return view ('editor.password.newpass', compact('cust'));
    }


    public function store_newpass(Request $request, $id)
    { 

            $user = User::where('customer_id', $id)->first();
            $user->password = md5($request['newpass']);
            $user->save();

            return redirect()->action('Editor\PasswordController@success_newpass', $id); 
        
    }

     public function success_newpass($id)
    {  

        return view ('editor.password.successnewpass');
    }


}
