<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\PasswordRequest;
use App\Http\Controllers\Controller; 
use App\User;

class UserController extends Controller
{
    public function index()
    {
        if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 
        //$users = User::paginate(15);
        $users = DB::table('user') 
            ->select('user.id', 
                'user.username',
                'user.email',
                'user.first_name',
                'user.last_name', 
                'user.created_at',
                'user.updated_at') 
            ->whereNull('user.deleted_at')
            ->paginate(15);

            //dd($users);

        return view ('editor.user.index', compact('users'))->with('number',$no);
    }

    public function create()
    {
        //$list_employee = Employee::pluck('emp_name', 'id');
        //dd($list_employee);

        // $list_employee = DB::table('employee')->whereNotIn('id', function($q){
        //     $q->select('employee_id')->from('user');
        // })->pluck('emp_name', 'id');

         //dd($list_employee);


    	return view ('editor.user.form');
    }

    public function store(RegisterRequest $request)
    {
        $user = new User;
        $user->username = $request->input('username'); 
        $user->password = bcrypt($request->input('password'));
        $user->save();

        return redirect()->action('Editor\UserController@index');
    }

    public function show($id)
    {
    	$user = User::find($id);
    	return view ('editor.user.detail', compact('user'));
    }

    public function edit($id)
    {
        $user = User::find($id); 
        return view ('editor.user.form', compact('user'));
    }

    public function update($id, PasswordRequest $request)
    {
        $user = User::find($id); 
        $user->password = bcrypt($request->input('password'));
        $user->save();

        return redirect()->action('Editor\UserController@index');
    }

    public function delete($id)
    {
        User::find($id)->delete();
        return redirect()->action('Editor\UserController@index');
    }
}
