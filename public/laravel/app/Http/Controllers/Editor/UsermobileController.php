<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\PasswordRequest;
use App\Http\Controllers\Controller; 
use App\Http\Controller\Customer;
use App\Model\Mstustomer;
use App\User;

class UsermobileController extends Controller
{
    public function index()
    {
        if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 
        //$users = User::paginate(15);
        $users = DB::table('user') 
            ->select('user.id', 
                'user.username',
                'user.email',
                'user.first_name',
                'user.last_name', 
                'user.created_at',
                'customer.name1 AS customer_name',
                'user.updated_at') 
            ->join('customer', 'user.customer_id', '=', 'customer.id')
            ->whereNull('user.deleted_at')
            ->paginate(15);

            //dd($users);

        return view ('editor.usermobile.index', compact('users'))->with('number',$no);
    }

    public function create()
    {
        $customer_list = Mstustomer::all()->pluck('name1', 'id');

    	return view ('editor.usermobile.form', compact('customer_list'));
    }

    public function store(RegisterRequest $request)
    {
        $user = new User;
        $user->username = $request->input('username'); 
        $user->password = md5($request->input('password'));
        $user->customer_id = $request->input('customer_id');
        $user->save();

        return redirect()->action('Editor\UsermobileController@index');
    }

    public function show($id)
    {
        $customer_list = Mstustomer::all()->pluck('name1', 'id'); 
        $user = User::find($id);
    	return view ('editor.usermobile.detail', compact('user', 'customer_list'));
    }

    public function edit($id)
    {
        $user = User::find($id); 
        $customer_list = Mstustomer::all()->pluck('name1', 'id'); 
        return view ('editor.usermobile.form', compact('user', 'customer_list'));
    }

    public function update($id, PasswordRequest $request)
    {
        $user = User::find($id); 
        $user->password = md5($request->input('password'));
        $user->customer_id = $request->input('customer_id'); 
        $user->save();

        return redirect()->action('Editor\UsermobileController@index');
    }

    public function delete($id)
    {
        User::find($id)->delete();
        return redirect()->action('Editor\UsermobileController@index');
    }
}
