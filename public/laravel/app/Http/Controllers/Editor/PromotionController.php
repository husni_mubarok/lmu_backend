<?php

namespace App\Http\Controllers\Editor;

use File;
use Session;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Http\Requests\PromotionRequest;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Model\Promotion;

class PromotionController extends Controller
{
    public function index()
    {
    	if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 
        $promotions = Promotion::paginate(15);
    	return view ('editor.promotion.index', compact('promotions'))->with('number',$no);
    }

    public function create()
    {
    	return view ('editor.promotion.form');
    }

    public function store(PromotionRequest $request)
    {
        //dd("sdsada");
    	$promotion = new Promotion;
    	$promotion->title = $request->input('title');
    	$promotion->content = $request->input('content'); 
        $promotion->periodfrom = $request->input('periodfrom'); 
        $promotion->periodto = $request->input('periodto'); 
        $promotion->save();

        if($request->image)
        {
            $promotion = Promotion::FindOrFail($promotion->id);

            $original_directory = "uploads/promotion/";

            if(!File::exists($original_directory))
            {
                File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

            $file_extension = $request->image->getClientOriginalExtension();
            $promotion->image = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
            $request->image->move($original_directory, $promotion->image);

            $thumbnail_directory = $original_directory."thumbnail/";
            if(!File::exists($thumbnail_directory))
            {
             File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
            }
            $thumbnail = Image::make($original_directory.$promotion->image);
            $thumbnail->fit(30,30)->save($thumbnail_directory.$promotion->image);

            $promotion->save(); 
        }
 
    	return redirect()->action('Editor\PromotionController@index');
    }

    public function edit($id)
    {
    	$promotion = Promotion::find($id);
    	return view ('editor.promotion.form', compact('promotion'));
    }

    public function update($id, PromotionRequest $request)
    {

    	$promotion = Promotion::find($id);
    	$promotion->title = $request->input('title');
        $promotion->content = $request->input('content'); 
        $promotion->periodfrom = $request->input('periodfrom'); 
        $promotion->periodto = $request->input('periodto'); 
        $promotion->save();

        if($request->image)
        {
            $promotion = Promotion::FindOrFail($promotion->id);

            $original_directory = "uploads/promotion/";

            if(!File::exists($original_directory))
            {
                File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

            $file_extension = $request->image->getClientOriginalExtension();
            $promotion->image = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
            $request->image->move($original_directory, $promotion->image);

            $thumbnail_directory = $original_directory."thumbnail/";
            if(!File::exists($thumbnail_directory))
            {
             File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
            }
            $thumbnail = Image::make($original_directory.$promotion->image);
            $thumbnail->fit(30,30)->save($thumbnail_directory.$promotion->image);

            $promotion->save(); 
        }
    	return redirect()->action('Editor\PromotionController@index');
    }

    public function delete($id)
    {
    	Promotion::find($id)->delete();
    	return redirect()->action('Editor\PromotionController@index');
    }
}
