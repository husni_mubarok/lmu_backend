<?php

namespace App\Http\Controllers\Editor;

use File;
use Session;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Http\Requests\NewsRequest;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Model\News;

class NewsController extends Controller
{
    public function index()
    {
    	if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 
        $newss = News::paginate(15);
    	return view ('editor.news.index', compact('newss'))->with('number',$no);
    }

    public function create()
    {
    	return view ('editor.news.form');
    }

    public function store(NewsRequest $request)
    {
        //dd("sdsada");
    	$news = new News;
    	$news->title = $request->input('title');
    	$news->content = $request->input('content'); 
        $news->save();

        if($request->image)
        {
            $news = News::FindOrFail($news->id);

            $original_directory = "uploads/news/";

            if(!File::exists($original_directory))
            {
                File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

            $file_extension = $request->image->getClientOriginalExtension();
            $news->image = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
            $request->image->move($original_directory, $news->image);

            $thumbnail_directory = $original_directory."thumbnail/";
            if(!File::exists($thumbnail_directory))
            {
             File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
            }
            $thumbnail = Image::make($original_directory.$news->image);
            $thumbnail->fit(30,30)->save($thumbnail_directory.$news->image);

            $news->save(); 
        }
 
    	return redirect()->action('Editor\NewsController@index');
    }

    public function edit($id)
    {
    	$news = News::find($id);
    	return view ('editor.news.form', compact('news'));
    }

    public function update($id, NewsRequest $request)
    {

    	$news = News::find($id);
    	$news->title = $request->input('title');
        $news->content = $request->input('content'); 
        $news->save();

        if($request->image)
        {
            $news = News::FindOrFail($news->id);

            $original_directory = "uploads/news/";

            if(!File::exists($original_directory))
            {
                File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

            $file_extension = $request->image->getClientOriginalExtension();
            $news->image = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
            $request->image->move($original_directory, $news->image);

            $thumbnail_directory = $original_directory."thumbnail/";
            if(!File::exists($thumbnail_directory))
            {
             File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
            }
            $thumbnail = Image::make($original_directory.$news->image);
            $thumbnail->fit(30,30)->save($thumbnail_directory.$news->image);

            $news->save(); 
        }
    	return redirect()->action('Editor\NewsController@index');
    }

    public function delete($id)
    {
    	News::find($id)->delete();
    	return redirect()->action('Editor\NewsController@index');
    }
}
