<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller; 

class EditorController extends Controller
{
    public function index()
    {
    	return view ('editor.index');
    }
}
