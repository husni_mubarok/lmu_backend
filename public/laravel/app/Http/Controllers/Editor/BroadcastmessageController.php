<?php

namespace App\Http\Controllers\Editor;

use File;
use Session;
use DB;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Http\Requests\BroadcastmessageRequest;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Model\Broadcastmessage;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

use LaravelFCM\Message\Topics;

class BroadcastmessageController extends Controller
{
    public function index()
    {
    	if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 
        $broadcastmessages = Broadcastmessage::paginate(15);
    	return view ('editor.broadcastmessage.index', compact('broadcastmessages'))->with('number',$no);
    }

    public function create()
    {
    	return view ('editor.broadcastmessage.form');
    }

    public function store(BroadcastmessageRequest $request)
    {
        //dd("sdsada");
    	$broadcastmessage = new Broadcastmessage;
    	$broadcastmessage->subject = $request->input('subject');
    	$broadcastmessage->content = $request->input('content'); 
        $broadcastmessage->save();

        DB::statement('INSERT INTO broadcast_message_detail (
                            customer_id,
                            broadcast_message_id,
                            status
                        ) SELECT
                            customer.id,
                            '.$broadcastmessage->id.',
                            0
                        FROM
                            customer');

        $title = $broadcastmessage->subject;
        $body = $broadcastmessage->content;


        //FCM
        // https://packagist.org/packages/brozot/laravel-fcm 

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('LMU Mobile');
        $notificationBuilder->setBody(''.$title.'')
                            ->setSound('default');
                            
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'my_data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = "AAAAK5jVGDo:APA91bG8a_-0fY-C-FxoOEZZuyUhOqJff8H70Sm328GIaG-3GsdvvxED-3-UgCVE1WO5Qar2kX5RMqY_rGyaGjN_pDKAuiVPQf6O7Fx8lMB7si9t3hN4Fa0FFGjaZd4fy3HSmnKJg8Zk";

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete(); 

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify(); 

        //return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();

        // return Array (key:token, value:errror) - in production you should remove from your database the tokens

        // dd($downstreamResponse);

    	return redirect()->action('Editor\BroadcastmessageController@index');
    }

    public function edit($id)
    {
    	$broadcastmessage = Broadcastmessage::find($id);
    	return view ('editor.broadcastmessage.form', compact('broadcastmessage'));
    }

    public function update($id, BroadcastmessageRequest $request)
    {

    	$broadcastmessage = Broadcastmessage::find($id);
    	$broadcastmessage->subject = $request->input('subject');
        $broadcastmessage->content = $request->input('content'); 
        $broadcastmessage->save();

        
    	return redirect()->action('Editor\BroadcastmessageController@index');
    }

    public function delete($id)
    {
    	Broadcastmessage::find($id)->delete();
    	return redirect()->action('Editor\BroadcastmessageController@index');
    }
}
