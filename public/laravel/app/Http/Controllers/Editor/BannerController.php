<?php

namespace App\Http\Controllers\Editor;

use File;
use Session;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Http\Requests\BannerRequest;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Model\Banner;

class BannerController extends Controller
{
    public function index()
    {
    	if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 
        $banners = Banner::paginate(15);
    	return view ('editor.banner.index', compact('banners'))->with('number',$no);
    }

    public function create()
    {
        //dd('sad');
    	return view ('editor.banner.form');
    }

    public function store(BannerRequest $request)
    { 
    	$banner = new Banner;
    	$banner->title = $request->input('title'); 
    	$banner->save();

        //dd($banner);

         if($request->image)
        {
            $banner = Banner::FindOrFail($banner->id);

            $original_directory = "uploads/banner/";

            if(!File::exists($original_directory))
            {
                File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

            $file_extension = $request->image->getClientOriginalExtension();
            $banner->image = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
            $request->image->move($original_directory, $banner->image);

            $thumbnail_directory = $original_directory."thumbnail/";
            if(!File::exists($thumbnail_directory))
            {
             File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
            }
            $thumbnail = Image::make($original_directory.$banner->image);
            $thumbnail->fit(30,30)->save($thumbnail_directory.$banner->image);

            $banner->save(); 
        }

    	return redirect()->action('Editor\BannerController@index');
    }

    public function edit($id)
    {
    	$banner = Banner::find($id);
    	return view ('editor.banner.form', compact('banner'));
    }

    public function update($id, Request $request)
    {
    	$banner = Banner::find($id);
    	$banner->title = $request->input('title');
    	$banner->save();

         if($request->image)
        {
            $banner = Banner::FindOrFail($banner->id);

            $original_directory = "uploads/banner/";

            if(!File::exists($original_directory))
            {
                File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

            $file_extension = $request->image->getClientOriginalExtension();
            $banner->image = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
            $request->image->move($original_directory, $banner->image);

            $thumbnail_directory = $original_directory."thumbnail/";
            if(!File::exists($thumbnail_directory))
            {
             File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
            }
            $thumbnail = Image::make($original_directory.$banner->image);
            $thumbnail->fit(30,30)->save($thumbnail_directory.$banner->image);

            $banner->save(); 
        }

    	return redirect()->action('Editor\BannerController@index');
    }

    public function delete($id)
    {
    	Banner::find($id)->delete();
    	return redirect()->action('Editor\BannerController@index');
    }
}
