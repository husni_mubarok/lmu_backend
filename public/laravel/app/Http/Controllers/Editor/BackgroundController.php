<?php

namespace App\Http\Controllers\Editor;

use File;
use Session;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Http\Requests\BackgroundRequest;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Model\Background;

class BackgroundController extends Controller
{
    public function index()
    {
    	if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 
        $backgrounds = Background::paginate(15);
    	return view ('editor.background.index', compact('backgrounds'))->with('number',$no);
    }

    public function create()
    {
        //dd('sad');
    	return view ('editor.background.form');
    }

    public function store(BackgroundRequest $request)
    { 
    	$background = new Background;
    	$background->title = $request->input('title'); 
    	$background->save();

        //dd($background);

         if($request->image)
        {
            $background = Background::FindOrFail($background->id);

            $original_directory = "uploads/background/";

            if(!File::exists($original_directory))
            {
                File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

            $file_extension = $request->image->getClientOriginalExtension();
            $background->image = str_replace(" ", "_", Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName()); 
            $request->image->move($original_directory, $background->image);

            $thumbnail_directory = $original_directory."thumbnail/";
            if(!File::exists($thumbnail_directory))
            {
             File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
            }
            $thumbnail = Image::make($original_directory.$background->image);
            $thumbnail->fit(30,30)->save($thumbnail_directory.$background->image);

            $background->save(); 
        }

    	return redirect()->action('Editor\BackgroundController@index');
    }

    public function edit($id)
    {
    	$background = Background::find($id);
    	return view ('editor.background.form', compact('background'));
    }

    public function update($id, Request $request)
    {
    	$background = Background::find($id);
    	$background->title = $request->input('title');
    	$background->save();

         if($request->image)
        {
            $background = Background::FindOrFail($background->id);

            $original_directory = "uploads/background/";

            if(!File::exists($original_directory))
            {
                File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

            $file_extension = $request->image->getClientOriginalExtension();
            $background->image = str_replace(" ", "_", Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName());
            $request->image->move($original_directory, $background->image);

            $thumbnail_directory = $original_directory."thumbnail/";
            if(!File::exists($thumbnail_directory))
            {
             File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
            }
            $thumbnail = Image::make($original_directory.$background->image);
            $thumbnail->fit(30,30)->save($thumbnail_directory.$background->image);

            $background->save(); 
        }

    	return redirect()->action('Editor\BackgroundController@index');
    }

    public function delete($id)
    {
    	Background::find($id)->delete();
    	return redirect()->action('Editor\BackgroundController@index');
    }
}
