<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar" style="min-height: 100px;border: 1px solid rgba(0,0,0,0)"> 
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" style="margin-top:45px">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="{{url('/')}}/editor">
            <i class="fa fa-home"></i> 
            <span>Home</span>
          </a>
        </li> 
 
         <li class="treeview">
          <a href="{{ URL::route('editor.news.index') }}">
            <i class="fa fa-newspaper-o"></i> 
            <span>News</span>
          </a>
        </li>

        <li class="treeview">
          <a href="{{ URL::route('editor.banner.index') }}">
            <i class="fa fa-file-picture-o"></i> 
            <span>Banner</span>
          </a>
        </li> 

        <li class="treeview">
          <a href="{{ URL::route('editor.background.index') }}">
            <i class="fa fa-image"></i> 
            <span>Background</span>
          </a>
        </li> 

        <li class="treeview">
          <a href="{{ URL::route('editor.companyprofile.index') }}">
            <i class="fa fa-user-circle"></i> 
            <span>Profile</span>
          </a>
        </li> 
 

        <li class="treeview">
          <a href="{{ URL::route('editor.callcenter.index') }}">
            <i class="fa fa-phone"></i> 
            <span>Call Center</span>
          </a>
        </li> 

        <li class="treeview">
          <a href="{{ URL::route('editor.broadcastmessage.index') }}">
            <i class="fa fa-envelope"></i> 
            <span>Broadcast Message</span>
          </a>
        </li> 

         <li class="treeview">
          <a href="{{ URL::route('editor.promotion.index') }}">
            <i class="fa fa-gift"></i> 
            <span>Promotion</span>
          </a>
        </li> 
       <li><a href="{{ URL::route('editor.usermobile.index') }}"><i class="fa fa-user-circle"></i>&nbsp;&nbsp;User Registration</a></li>


         <!-- <li class="treeview">
          <a href="{{url('/')}}/editor">
            <i class="fa fa-star"></i> 
            <span>View Rating</span>
          </a>
        </li>  -->
 <!-- 
         <li class="treeview">
          <a href="{{url('/')}}/editor">
            <i class="fa fa-flask"></i> 
            <span>Order Status</span>
          </a>
        </li> 

         <li class="treeview">
          <a href="{{url('/')}}/editor">
            <i class="fa fa-cube"></i> 
            <span>Product Report</span>
          </a>
        </li>  -->

       <!--  <li class="treeview">
          <a href="{{url('/')}}/editor">
            <i class="fa fa-envelope"></i> 
            <span>Support Ticketing</span>
          </a>
        </li> 

        <li class="treeview">
          <a href="{{url('/')}}/editor">
            <i class="fa fa-users"></i> 
            <span>Member Management</span>
          </a>
        </li> 
 -->
       <!--  <li class="treeview">
          <a href="{{url('/')}}/editor">
            <i class="fa fa-television"></i> 
            <span>Content Management</span>
          </a>
        </li>   -->
 
      <li class="treeview">
        <a href="#">
          <i class="fa fa-user"></i>
          <span>Admin</span>

          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
       @actionStart('user', 'read')
       <li><a href="{{ URL::route('editor.user.index') }}"><i class="fa fa-user-circle"></i>&nbsp;&nbsp;User List</a></li>
       @actionEnd

       @actionStart('module', 'read')
       <li><a href="{{ URL::route('editor.module.index') }}"><i class="fa fa-cog"></i>&nbsp;&nbsp;Module List</a></li>
       @actionEnd

       @actionStart('action', 'read')
       <li><a href="{{ URL::route('editor.action.index') }}"><i class="fa fa-wrench"></i>&nbsp;&nbsp;Action List</a></li>
       @actionEnd

       @actionStart('privilege', 'read')
       <li><a href="{{ URL::route('editor.privilege.index') }}"><i class="fa fa-vcard"></i>&nbsp;&nbsp;Privilege List</a></li>
       @actionEnd 
 </ul>
</li>   
</section>
<!-- /.sidebar -->
</aside>