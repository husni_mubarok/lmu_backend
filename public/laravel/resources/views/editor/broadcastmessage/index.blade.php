@extends('layouts.editor.template')
@section('content')
<section class="content-header hidden-xs">
	<h1 style="margin-top: -20px">
    	CMS
    	<small>Content Management System</small>
  	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><i class="fa fa-envelope"></i> Broadcast Message List</li>
	</ol>
</section> 
<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12"> 
				<div class="x_panel">
					<h2>
						<i class="fa fa-envelope"></i> Broadcast Message List
						<a href="{{ URL::route('editor.broadcastmessage.create') }}" class="btn btn-success btn-lg pull-right btn-flat"><i class="fa fa-plus"></i> Add</a>
					</h2>
					<hr>
					<div class="x_phone_number">
						<table id="table_index" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="5%">#</th>
									<th>Subject</th>
									<th>Content</th> 
									<th width="10%">Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($broadcastmessages as $key => $broadcastmessage)
								<tr>
									<td>{{$number++}}</td>
									<td>{{$broadcastmessage->subject}}</td>
									@php
									$content = strip_tags($broadcastmessage->content);

									if (strlen($broadcastmessage->content) > 150) {

									    // truncate content
									    $contentCut = substr($broadcastmessage->content, 0, 150);

									    // make sure it ends in a word so assassinate doesn't become ass...
									    $content = substr($contentCut, 0, strrpos($contentCut, ' ')).'...'; 
									}
									@endphp 
									<td>@php echo $content @endphp</td> 
									<td align="center">
										<div class="act_tb"> 
											<div>
												<a href="{{ URL::route('editor.broadcastmessage.edit', [$broadcastmessage->id]) }}" class="btn btn-default btn-sm btn-flat"><i class="fa fa-pencil"></i></a>
											</div>  
											<div>
												{!! Form::open(array('route' => ['editor.broadcastmessage.delete', $broadcastmessage->id], 'method' => 'delete', 'class'=>'delete'))!!}
												{{ csrf_field() }}	                    				
												<button type="submit" class="btn btn-default btn-sm btn-flat"><i class="fa fa-trash"></i></a></button>
												{!! Form::close() !!}
											</div>  
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div> 
					<div class="pull-right">{{ $broadcastmessages->links() }}</div> 
				</div> 
			</div>
		</div>
	</section>
</section> 
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#table_index").DataTable(
		{
			"bPaginate": false,
		});
	});
</script>


<!-- Add fancyBox -->
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
</script>
<script>
	$(".delete").on("submit", function(){
		return confirm("Delete this data?");
	});
</script> 
@stop