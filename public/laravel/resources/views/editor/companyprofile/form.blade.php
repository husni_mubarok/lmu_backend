@extends('layouts.editor.template')

@section('content')
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<section class="content-header hidden-xs">
  	<h1 style="margin-top: -20px">
    	CMS
    	<small>Content Management System</small>
  	</h1>
  	<ol class="breadcrumb">
    	<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    	<li><a href="{{ URL::route('editor.companyprofile.index') }}"><i class="fa fa-user-circle"></i> Profile List</a></li>
    	<li class="active">
    		@if(isset($companyprofile))
    		<i class="fa fa-pencil"></i> Edit
    		@else
    		<i class="fa fa-plus"></i> Create
    		@endif
		</li>
  	</ol>
</section>
@actionStart('companyprofile', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-11 col-sm-11 col-xs-11"> 
		        <div class="x_panel">
	                <h2>
	                	@if(isset($companyprofile))
	                	<i class="fa fa-pencil"></i>
	                	@else
	                	<i class="fa fa-plus"></i>
	                	@endif
	                	&nbsp;Profile
                	</h2>
	                <hr>
		            <div class="x_content">
		                @include('errors.error')

		                @if(isset($companyprofile))
		                {!! Form::model($companyprofile, array('route' => ['editor.companyprofile.update', $companyprofile->id], 'method' => 'PUT', 'files' => 'true'))!!}
	                    @else
	                    {!! Form::open(array('route' => 'editor.companyprofile.store', 'files' => 'true'))!!}
	                    @endif
	                    {{ csrf_field() }}
	                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                    	{{ Form::label('name', 'Name') }} 
	                    	{{ Form::text('name', old('name'), ['class' => 'form-control']) }} 
	                    	<br> 
	                    	{{ Form::label('address', 'Address') }} 
	                    	{{ Form::text('address', old('address'), ['class' => 'form-control']) }} 
	                    	<br> 
	                    	{{ Form::label('tlp', 'Tlp') }} 
	                    	{{ Form::text('tlp', old('tlp'), ['class' => 'form-control']) }} 
	                    	<br> 
	                    	{{ Form::label('fax', 'Fax') }} 
	                    	{{ Form::text('fax', old('fax'), ['class' => 'form-control']) }} 
	                    	<br> 
	                    	{{ Form::label('email', 'Email') }} 
	                    	{{ Form::text('email', old('email'), ['class' => 'form-control']) }} 
	                    	<br> 
	                    	{{ Form::label('description', 'Description') }} 
	                    	{{ Form::text('description', old('description'), ['class' => 'form-control']) }} 
	                    	<br> 
	                    	 
                            {{ Form::label('image', 'Image') }}
							{{ Form::file('image') }}<br/>
	                    	<br>
                            <button type="submit" class="btn btn-success pull-right btn-flat btn-lg"><i class="fa fa-check"></i> Save</button>
	                    	<a href="{{ URL::route('editor.companyprofile.index') }}" class="btn btn-default pull-right btn-flat btn-lg" style="margin-right: 5px;"><i class="fa fa-close"></i> Close</a>
                            <br>
                    	</div>  
                        {!! Form::close() !!}
		            </div> 
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
 
@stop

@section('scripts')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{Config::get('constants.path.plugin')}}/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>  
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('content');
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
  });
</script>
@stop