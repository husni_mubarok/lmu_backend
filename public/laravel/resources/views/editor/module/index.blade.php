@extends('layouts.editor.template')
@section('content')
<section class="content-header hidden-xs">
  	<h1 style="margin-top: -20px">
    	CMS
    	<small>Content Management System</small>
  	</h1>
  	<ol class="breadcrumb">
    	<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    	<li class="active"><i class="fa fa-gear"></i> Module List</li>
  	</ol>
</section>
@actionStart('module', 'read')
<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12"> 
		        <div class="x_panel">
	                <h2>
	                	<i class="fa fa-gear"></i> Module List
	                	<a href="{{ URL::route('editor.module.create') }}" class="btn btn-success btn-lg pull-right btn-flat"><i class="fa fa-plus"></i> Add</a>
                	</h2>
	                <hr>
		            <div class="x_content">
		                <table id="table_index" class="table table-bordered table-hover">
						  	<thead>
						  	  	<tr>
							      	<th width="5%">#</th>
							      	<th>Name</th>
							      	<th>Description</th>
							      	<th width="10%">Action</th>
						    	</tr>
						  	</thead>
						  	<tbody>
						    @foreach($modules as $key => $module)
						    	<tr>
						      		<td data-th="#">{{$number++}}</td>
							      	<td data-th="Name">{{$module->name}}</td>
							      	<td data-th="Description">{{$module->description}}</td>
							      	<td align="center">
							      		<div class="act_tb">
							      		
							      			@actionStart('module', 'update')
							      			<div>
							      				<a href="{{ URL::route('editor.module.edit', [$module->id]) }}" class="btn btn-default btn-sm btn-flat"><i class="fa fa-pencil"></i></a>
							      			</div>
							      			@actionEnd

							      			@actionStart('module', 'delete')
						      				<div>
								      			{!! Form::open(array('route' => ['editor.module.delete', $module->id], 'method' => 'delete'))!!}
		                    					{{ csrf_field() }}	                    				
								      			<button type="submit" class="btn btn-default btn-sm btn-flat"><i class="fa fa-trash"></i></a></button>
								      			{!! Form::close() !!}
						      				</div>
						      				@actionEnd
							      		
							      		</div>
						      		</td>
							    </tr>
						    @endforeach
							</tbody>
						</table>
		            </div> 
					{{ $modules->links() }} 
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#table_index").DataTable();
	});
</script>


<!-- Add fancyBox -->
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
</script>
<script>
	$(".delete").on("submit", function(){
		return confirm("Delete this data?");
	});
</script> 
@stop