@extends('layouts.editor.template')
@section('content')
<section class="content-header hidden-xs">
  	<h1 style="margin-top: -20px">
    	CMS
    	<small>Content Management System</small>
  	</h1>
  	<ol class="breadcrumb">
    	<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    	<li><a href="{{ URL::route('editor.banner.index') }}"><i class="fa fa-file-picture-o"></i> Banner Form</a></li>
    	<li class="active">
    		@if(isset($banner))
    		<i class="fa fa-pencil"></i> Edit
    		@else
    		<i class="fa fa-plus"></i> Create
    		@endif
		</li>
  	</ol>
</section>
@actionStart('banner', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12"> 
		        <div class="x_panel">
	                <h2>
	                	@if(isset($banner))
	                	<i class="fa fa-pencil"></i>
	                	@else
	                	<i class="fa fa-plus"></i>
	                	@endif
	                	&nbsp;Banner
                	</h2>
	                <hr>
		            <div class="x_content">
		                @include('errors.error')

		                @if(isset($banner))
		                {!! Form::model($banner, array('route' => ['editor.banner.update', $banner->id], 'method' => 'PUT', 'files' => 'true'))!!}
	                    @else
	                    {!! Form::open(array('route' => 'editor.banner.store', 'files' => 'true'))!!}
	                    @endif
	                    {{ csrf_field() }}
	                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                    	{{ Form::label('title', 'Title') }} 
	                    	{{ Form::text('title', old('title'), ['class' => 'form-control']) }} 
	                    	<br>

	                    	{{ Form::label('image', 'Image') }}
							{{ Form::file('image') }}<br/>
	                    	<br>

                            <button type="submit" class="btn btn-success btn-lg btn-flat pull-right"><i class="fa fa-check"></i> Save</button>

	                    	<a href="{{ URL::route('editor.banner.index') }}" class="btn btn-default btn-lg btn-flat pull-right" style="margin-right: 5px;"><i class="fa fa-close"></i> Close</a>
                    	</div>
                        {!! Form::close() !!}
		            </div>
		        </div> 
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
