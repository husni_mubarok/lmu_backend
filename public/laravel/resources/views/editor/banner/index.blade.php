@extends('layouts.editor.template')
@section('content')
<section class="content-header">
	<h1 style="margin-top: -20px">
    	CMS
    	<small>Content Management System</small>
  	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active"><a href="#"><i class="fa fa-file-picture-o"></i> Banner List</a></li> 
	</ol> 
</section> 
<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12"> 
				<div class="x_panel">
					<h2>
						<i class="fa fa-file-picture-o"></i> Banner List
						<a href="{{ URL::route('editor.banner.create') }}" class="btn btn-success btn-lg pull-right btn-flat"><i class="fa fa-plus"></i> Add</a>
					</h2> 
					<hr/>
					<div class="x_content">
						<table id="table_index" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="5%">#</th>
									<th>Name</th>
									<th>Image</th>
									<th width="10%">Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($banners as $key => $banner)
								<tr>
									<td>{{$number++}}</td>
									@php
										$title = strip_tags($banner->title);

										if (strlen($banner->title) > 70) {

										    // truncate title
										    $titleCut = substr($banner->title, 0, 70);

										    // make sure it ends in a word so assassinate doesn't become ass...
										    $title = substr($titleCut, 0, strrpos($titleCut, ' ')).'...'; 
										}
									@endphp 
									<td>@php echo $title @endphp</td>
									<td>
										@if(isset($banner->image))
										<a class="fancybox" rel="group" href="{{Config::get('constants.path.uploads')}}/banner/{{$banner->image}}"><img src="{{Config::get('constants.path.uploads')}}/banner/thumbnail/{{$banner->image}}" class="img-thumbnail img-responsive" /></a>
										@else
										NO IMAGE
										@endif 
									</td>
									<td align="center">
										<div class="act_tb"> 
											<div>
												<a href="{{ URL::route('editor.banner.edit', [$banner->id]) }}" class="btn btn-default btn-sm btn-flat"><i class="fa fa-pencil"></i></a>
											</div>  
											<div>
												{!! Form::open(array('route' => ['editor.banner.delete', $banner->id], 'method' => 'delete', 'class'=>'delete'))!!}
												{{ csrf_field() }}	                    				
												<button type="submit" class="btn btn-default btn-sm btn-flat"><i class="fa fa-trash"></i></a></button>
												{!! Form::close() !!}
											</div>  
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div> 
					<div class="pull-right">{{ $banners->links() }}</div>  
				</div> 
			</div>
		</div>
	</section>
</section> 
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#table_index").DataTable(
		{
			"bPaginate": false,
		});
	});
</script>


<!-- Add fancyBox -->
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
</script>
<script>
	$(".delete").on("submit", function(){
		return confirm("Delete this data?");
	});
</script> 
@stop