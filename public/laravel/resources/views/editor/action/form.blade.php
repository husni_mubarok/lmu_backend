@extends('layouts.editor.template')
@section('content')
<section class="content-header hidden-xs">
  	<h1 style="margin-top: -20px">
    	CMS
    	<small>Content Management System</small>
  	</h1>
  	<ol class="breadcrumb">
    	<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    	<li><a href="{{ URL::route('editor.action.index') }}"><i class="fa fa-wrench"></i> Action List</a></li>
    	<li class="active">
    		@if(isset($action))
    		<i class="fa fa-pencil"></i> Edit
    		@else
    		<i class="fa fa-plus"></i> Create
    		@endif
		</li>
  	</ol>
</section>
@actionStart('action', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-6 col-sm-6 col-xs-6"> 
		        <div class="x_panel">
	                <h2>
	                	@if(isset($action))
	                	<i class="fa fa-pencil"></i>
	                	@else
	                	<i class="fa fa-plus"></i>
	                	@endif
	                	&nbsp;Action
                	</h2>
	                <hr>
		            <div class="x_content">
		                @include('errors.error')

		                @if(isset($action))
		                {!! Form::model($action, array('route' => ['editor.action.update', $action->id], 'method' => 'PUT', 'files' => 'true'))!!}
	                    @else
	                    {!! Form::open(array('route' => 'editor.action.store', 'files' => 'true'))!!}
	                    @endif
	                    {{ csrf_field() }}
	                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                    	{{ Form::label('name', 'Name') }}
	                    	@if(isset($action))
	                    	{{ Form::text('name', old('name'), ['class' => 'form-control', 'disabled' => 'true']) }}
	                    	@else
	                    	{{ Form::text('name', old('name'), ['class' => 'form-control']) }}
	                    	@endif
	                    	<br>

	                    	{{ Form::label('description', 'Description') }}
	                    	{{ Form::text('description', old('description'), ['class' => 'form-control']) }}
	                    	<br>

                            <button type="submit" class="btn btn-success pull-right btn-lg btn-flat"><i class="fa fa-check"></i> Save</button>
                            
	                    	<a href="{{ URL::route('editor.action.index') }}" class="btn btn-default pull-right btn-flat btn-lg" style="margin-right: 5px;"><i class="fa fa-close"></i> Close</a>
                    	</div>
                        {!! Form::close() !!}
		            </div> 
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
