@extends('layouts.editor.template')
@section('content')
<section class="content-header hidden-xs">
  	<h1 style="margin-top: -20px">
    	CMS
    	<small>Content Management System</small>
  	</h1>
  	<ol class="breadcrumb">
    	<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    	<li class="active"><i class="fa fa-wrench"></i> Action List</li>
  	</ol>
</section>
@actionStart('action', 'read')
<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12"> 
		        <div class="x_panel">
	                <h2>
	                	<i class="fa fa-wrench"></i> Action List
	                	<a href="{{ URL::route('editor.action.create') }}" class="btn btn-success btn-lg pull-right btn-flat"><i class="fa fa-plus"></i> Add</a>
                	</h2>
	                <hr>
		            <div class="x_content">
		                <table id="table_index" class="table table-bordered table-hover">
						  	<thead>
						  	  	<tr>
							      	<th width="5%">#</th>
							      	<th>Name</th>
							      	<th>Description</th>
							      	<th width="10%">Action</th>
						    	</tr>
						  	</thead>
						  	<tbody>
						    @foreach($actions as $key => $action)
						    	<tr>
						      		<td data-th="#">{{$key+1}}</td>
							      	<td data-th="Name">{{$action->name}}</td>
							      	<td data-th="Description">{{$action->description}}</td>
							      	<td align="center">
							      		<div class="act_tb">
							      		
							      			@actionStart('action', 'update')
							      			<div>
							      				<a href="{{ URL::route('editor.action.edit', [$action->id]) }}" class="btn btn-default btn-sm btn-flat"><i class="fa fa-pencil"></i></a>
							      			</div>
							      			@actionEnd

							      			@actionStart('action', 'delete')
						      				<div>
								      			{!! Form::open(array('route' => ['editor.action.delete', $action->id], 'method' => 'delete'))!!}
		                    					{{ csrf_field() }}	                    				
								      			<button type="submit" class="btn btn-default btn-sm btn-flat"><i class="fa fa-trash"></i></a></button>
								      			{!! Form::close() !!}
						      				</div>
						      				@actionEnd
							      		
							      		</div>
						      		</td>
							    </tr>
						    @endforeach
							</tbody>
						</table>
						{{ $actions->links() }}
		            </div> 
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#table_index").DataTable();
    });
</script>
@stop