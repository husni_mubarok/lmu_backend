@extends('layouts.editor.template')
@section('content')
<section class="content-header">
	<h1 style="margin-top: -20px">
    	CMS
    	<small>Content Management System</small>
  	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active"><a href="#"><i class="fa fa-newspaper-o"></i> News List</a></li> 
	</ol> 
</section> 
<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">  
				<div class="x_panel">
					<h2>
						<i class="fa fa-newspaper-o"></i> News List
						<a href="{{ URL::route('editor.news.create') }}" class="btn btn-success btn-lg pull-right btn-flat"><i class="fa fa-plus"></i> Add</a>
					</h2>
					<hr>
					<div class="x_content">
						<table id="table_index" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="5%">#</th>
									<th>Title</th>
									<th>Content</th>
									<th>Image</th>
									<th width="10%">Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($newss as $key => $news)
								<tr>
									<td>{{$number++}}</td>
										@php
										$title = strip_tags($news->title);

										if (strlen($news->title) > 70) {

										    // truncate title
										    $titleCut = substr($news->title, 0, 70);

										    // make sure it ends in a word so assassinate doesn't become ass...
										    $title = substr($titleCut, 0, strrpos($titleCut, ' ')).'...'; 
										}
										@endphp 
									<td>@php echo $title @endphp</td>
										@php
										$content = strip_tags($news->content);

										if (strlen($news->content) > 150) {

										    // truncate content
										    $contentCut = substr($news->content, 0, 150);

										    // make sure it ends in a word so assassinate doesn't become ass...
										    $content = substr($contentCut, 0, strrpos($contentCut, ' ')).'...'; 
										}
										@endphp 
									<td>@php echo $content @endphp</td>
									<td>
										@if(isset($news->image))
										<a class="fancybox" rel="group" href="{{Config::get('constants.path.uploads')}}/news/{{$news->image}}"><img src="{{Config::get('constants.path.uploads')}}/news/thumbnail/{{$news->image}}" class="img-thumbnail img-responsive" /></a> 
										@else
										NO IMAGE
										@endif
									</td>
									<td align="center">
										<div class="act_tb"> 
											<div>
												<a href="{{ URL::route('editor.news.edit', [$news->id]) }}" class="btn btn-default btn-sm btn-flat"><i class="fa fa-pencil"></i></a>
											</div>  
											<div>
												{!! Form::open(array('route' => ['editor.news.delete', $news->id], 'method' => 'delete', 'class'=>'delete'))!!}
												{{ csrf_field() }}	                    				
												<button type="submit" class="btn btn-default btn-sm btn-flat"><i class="fa fa-trash"></i></a></button>
												{!! Form::close() !!}
											</div>  
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div> 
					<div class="pull-right">{{ $newss->links() }}</div> 
				</div> 
			</div>
		</div>
	</section>
</section> 
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#table_index").DataTable(
		{
			"bPaginate": false,
		});
	});
</script>


<!-- Add fancyBox -->
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
</script>
<script>
	$(".delete").on("submit", function(){
		return confirm("Delete this data?");
	});
</script> 
@stop