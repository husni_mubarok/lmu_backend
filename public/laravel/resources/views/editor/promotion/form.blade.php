@extends('layouts.editor.template')

@section('content')
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<section class="content-header hidden-xs">
  	<h1 style="margin-top: -20px">
    	CMS
    	<small>Content Management System</small>
  	</h1>
  	<ol class="breadcrumb">
    	<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    	<li><a href="{{ URL::route('editor.promotion.index') }}"><i class="fa fa-gift"></i> Promotion Form</a></li>
    	<li class="active">
    		@if(isset($promotion))
    		<i class="fa fa-pencil"></i> Edit
    		@else
    		<i class="fa fa-plus"></i> Create
    		@endif
		</li>
  	</ol>
</section>
@actionStart('promotion', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12"> 
		    	<div class="col-md-12">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($promotion))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i>
		                	@endif
		                	&nbsp;Promotion
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')

			                @if(isset($promotion))
			                {!! Form::model($promotion, array('route' => ['editor.promotion.update', $promotion->id], 'method' => 'PUT', 'files' => 'true'))!!}
		                    @else
		                    {!! Form::open(array('route' => 'editor.promotion.store', 'files' => 'true'))!!}
		                    @endif
		                    {{ csrf_field() }}
		                    <div class="col-md-10 col-sm-10 col-xs-10 form-group">
		                    	{{ Form::label('title', 'Title') }} 
		                    	{{ Form::text('title', old('title'), ['class' => 'form-control']) }} 
		                    	<br> 
		                    	{{ Form::label('periodfrom', 'Period From') }} 
		                    	{{ Form::text('periodfrom', old('periodfrom'), ['class' => 'form-control']) }} 
		                    	<br> 
		                    	{{ Form::label('periodto', 'Period To') }} 
		                    	{{ Form::text('periodto', old('periodto'), ['class' => 'form-control']) }} 
		                    	<br> 
		                    	{{ Form::label('content', 'Content') }} 
		                    	 <textarea id="content" name="content" rows="10" cols="80"> 
		                    	 	@if(isset($promotion))
		                    	 	{{$promotion->content}}
		                    	 	 @endif 
		                    	 </textarea>  
		                    	<br> 
	                            {{ Form::label('image', 'Image') }}
								{{ Form::file('image') }}<br/>
		                    	<br>
	                            <button type="submit" class="btn btn-success pull-right btn-flat btn-lg"><i class="fa fa-check"></i> Save</button>
		                    	<a href="{{ URL::route('editor.promotion.index') }}" class="btn btn-default pull-right btn-flat btn-lg" style="margin-right: 5px;"><i class="fa fa-close"></i> Close</a>
	                            <br>
	                    	</div>  
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
 
@stop

@section('scripts')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{Config::get('constants.path.plugin')}}/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>  
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('content');
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
  });
</script>
@stop