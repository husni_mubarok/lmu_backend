@extends('layouts.editor.template')
@section('content')
<section class="content-header hidden-xs">
  	<h1 style="margin-top: -20px">
    	CMS
    	<small>Content Management System</small>
  	</h1>
  	<ol class="breadcrumb">
    	<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    	<li><a href="{{ URL::route('editor.privilege.index') }}"><i class="fa fa-vcard"></i> User List</a></li>
    	<li class="active">
    		@if(isset($privilege))
    		<i class="fa fa-pencil"></i> Edit
    		@else
    		<i class="fa fa-plus"></i> Create
    		@endif
		</li>
  	</ol>
</section>
{{-- @actionStart('user', 'create|update') --}}
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12"> 
		        <div class="x_panel">
	                <h2>
	                	@if(isset($user))
	                	<i class="fa fa-pencil"></i>
	                	@else
	                	<i class="fa fa-plus"></i>
	                	@endif
	                	&nbsp;Privilege
                	</h2>
	                <hr>
		            <div class="x_content">
		                @include('errors.error')

		                @if(isset($user))
		                {!! Form::model($user, array('route' => ['editor.privilege.update', $user->id], 'method' => 'PUT', 'files' => 'true'))!!}
	                    @else
	                    {!! Form::open(array('route' => 'editor.privilege.store', 'files' => 'true'))!!}
	                    @endif
	                    {{ csrf_field() }}
	                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                    	{{ Form::label('user_id', 'Username') }}
	                    	@if(isset($user))
	                    	{{ Form::text('user_id', $user->username, ['class' => 'form-control', 'disabled' => 'true']) }}
	                    	@else
	                    	{{ Form::select('user_id', $username_list, old('user_id'), ['class' => 'form-control']) }}
	                    	@endif
	                    	<br>

	                    	<table id="table_index" class="table table-bordered table-hover">
	                    	<thead>
	                    		<tr>
	                    			<th><i class="fa fa-gear"></i>|<i class="fa fa-wrench"></i></th>
	                    			@foreach($action_list as $action_key => $action)
	                    			<th>{{$action}}</th>
	                    			@endforeach
	                    		</tr>
	                    	</thead>
	                    	<tbody>
	                    		@foreach($module_list as $module_key => $module)
	                    		<tr>
	                    			<td>{{$module}}</td>
	                    			@foreach($action_list as $action_key => $action)
	                    			<td>{{ Form::checkbox('privilege['.$module_key.']['.$action_key.']', 1, null, ['id' => 'privilege_'.$module_key.'_'.$action_key]) }}</td>
	                    			@endforeach
	                    		</tr>
	                    		@endforeach
	                    	</tbody>
	                    	</table>

                            <button type="submit" class="btn btn-success pull-right btn-lg btn-flat"><i class="fa fa-check"></i> Save</button>
                            <a href="{{ URL::route('editor.privilege.index') }}" class="btn btn-default pull-right btn-flat btn-lg" style="margin-right: 5px;"><i class="fa fa-close"></i> Close</a>
                    	</div>
                        {!! Form::close() !!}
		            </div> 
		        </div>
		    </div>
		</div>
	</section>
</section>
{{-- @actionEnd --}}
@stop

@section('scripts')
@if(isset($user))
<script>
jQuery.each({!! $user->privilege !!}, function(key, value)
{
	$('#privilege_'+value['module_id']+'_'+value['action_id']).attr('checked', true);
});
</script>
@endif

<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#table_indexx").DataTable();
    });
</script>
@stop
