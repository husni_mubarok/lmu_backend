@extends('layouts.mobileauth.template')
@section('content')

<section class="content box box-solid">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12"> 
            <div class="col-md-5">
                <div class="x_panel">
                    <h2>
                        <i class="fa fa-user"></i> <i class="fa fa-pencil"></i> Reset Password
                    </h2>
                    <hr>   
                    <div class="x_content"> 
                        {!! Form::model($cust, array('route' => ['editor.password.store_newpass', $cust->id], 'method' => 'PUT', 'files' => 'true', 'class' => 'form-horizontal'))!!}
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">New Password</label>
                            <div class="col-md-6"> 
                                  <input type="password" name="newpass" class="form-control" data-inputmask="&quot;mask&quot;: &quot;(999) 999-9999&quot;" data-mask=""> 
                                <strong class="pull-right" style="color:red"> {!! session()->get('error') !!}</strong>    
                            </div>
                        </div>  

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary btn-flat pull-right">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
