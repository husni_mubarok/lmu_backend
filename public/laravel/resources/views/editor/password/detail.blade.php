@extends('layouts.editor.template')
@section('content')

<section class="content box box-solid">
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-md-1"></div>
	    	<div class="col-md-5">
		        <div class="x_panel">
	                <h2>
	                	<i class="fa fa-user"></i> Your Profile
	                	<a href="{{ URL::route('editor.profile.edit') }}" class="btn btn-default btn-sm pull-right"><i class="fa fa-pencil"></i> Edit</a>
                	</h2>
	                <hr>
		            <div class="x_content">
	                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                    	<table class="table">
		                    	<tr>
		                    		<th>Name</th>
		                    		<td>{{Auth::user()->username}}</td>
		                    	</tr>
		                    	<tr>
		                    		<th>Email</th>
		                    		<td>{{Auth::user()->email}}</td>
		                    	</tr>
		                    	<tr>
		                    		<th>Name</th>
		                    		<td>{{Auth::user()->first_name}} {{Auth::user()->last_name}}</td>
		                    	</tr>
		                    	<tr>
		                    		<th>Register</th>
		                    		<td>{{date("d-m-Y", strtotime(Auth::user()->created_at))}}</td>
		                    	</tr>
		                    	<tr>
		                    		<th>Profile Picture</th>
		                    		<td>
		                    			@if(Auth::user()->filename == null)
		        							<br/><a class="fancybox" rel="group" href="{{Config::get('constants.path.uploads')}}/user/placeholder.png"><img src="{{Config::get('constants.path.uploads')}}/user/thumbnail/placeholder.png" class="img-thumbnail img-responsive" /></a><br/>
		        						@else
				        					<br/><a class="fancybox" rel="group" href="{{Config::get('constants.path.uploads')}}/user/{{Auth::user()->username}}/{{Auth::user()->filename}}"><img src="{{Config::get('constants.path.uploads')}}/user/{{Auth::user()->username}}/thumbnail/{{Auth::user()->filename}}" class="img-thumbnail img-responsive" /></a>
				        					{!! Form::open(array('route' => 'editor.profile.delete_image', 'method' => 'PUT'))!!}
				                    			<button type="submit"><i class="fa fa-trash"></i>	
				                    		{!! Form::close() !!}
				        					<br/>
				        				@endif
			        				</td>
		                    	</tr>
	                    	</table>
                    	</div>
		            </div>
		        </div>
	        </div>
	    </div>
	</div>
</section>
@stop

@section('scripts')
<!-- Add fancyBox -->
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
</script>
@stop