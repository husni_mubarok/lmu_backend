@extends('layouts.mobileauth.template')
@section('content')

<section class="content box box-solid">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12"> 
            <div class="col-md-5">
                <div class="x_panel">
                    <h2>
                        <i class="fa fa-user"></i> <i class="fa fa-pencil"></i> Reset Password
                    </h2>
                    <hr>   
                    <div class="x_content">  
                        <div class="alert alert-success">
                          <strong>Reset Password Success,</strong> Please login with new password.
                        </div>  
                </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
