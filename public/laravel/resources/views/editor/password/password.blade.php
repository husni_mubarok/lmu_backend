@extends('layouts.mobileauth.template')
@section('content')


<section class="content box box-solid">
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-md-1"></div>
	    	<div class="col-md-12">
		        <div class="x_panel">
	                <h2>
	                	<i class="fa fa-user"></i> <i class="fa fa-pencil"></i> Change Password
                	</h2>
	                <hr>
		            <div class="x_content">
						@include('errors.error') 
						 {!! Form::model($user, array('route' => ['editor.password.update_password', $user->customer_id], 'method' => 'PUT', 'files' => 'true', 'class' => 'form-horizontal'))!!}
						{{ csrf_field() }}
						<div class="x_content">
							<div class="col-md-12 col-sm-12 col-xs-12">
								@if(session()->has('message'))
								    <div class="alert alert-success">
								        {{ session()->get('message') }}
								    </div>
								@endif
								<label for="password_current">Current Password</label>
								<input type="password" class="form-control" name="password_current" id="password_current" required><br>

								<label for="password_new">New Password</label>
								<input type="password" class="form-control" name="password_new" id="password_new" required><br>

								<label for="password_new_confirmation">Confirm New Password</label>
								<input type="password" class="form-control" name="password_new_confirmation" id="password_new_confirmation" required><br>

								<button type="submit" class="btn btn-success pull-right btn-flat"><i class="fa fa-check"></i> Save</button>
							</div>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
			</div>
		</div>
	</div> 
</section>
@stop