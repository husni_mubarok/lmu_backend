<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
//API
Route::group(['prefix' => 'api', 'namespace' => 'API'], function()
{ 
	//get news
	Route::get('/news/index', ['middleware' => 'cors', 'as' => 'api.news.index', 'uses' => 'NewsController@index']);
	//promotion
	Route::get('/promotion/index', ['middleware' => 'cors', 'as' => 'api.promotion.index', 'uses' => 'PromotionController@index']); 
	Route::get('/promotion/popup', ['middleware' => 'cors', 'as' => 'api.promotion.popup', 'uses' => 'PromotionController@popup']); 
	//get banner
	Route::get('/banner/index', ['middleware' => 'cors', 'as' => 'api.banner.index', 'uses' => 'BannerController@index']); 
	//get background
	Route::get('/background/index', ['middleware' => 'cors', 'as' => 'api.background.index', 'uses' => 'BackgroundController@index']); 
	//get company profile
	Route::get('/companyprofile/index', ['middleware' => 'cors', 'as' => 'api.companyprofile.index', 'uses' => 'CompanyprofileController@index']); 
	//get broadcast message
	Route::get('/broadcastmessage/index/{id}', ['middleware' => 'cors', 'as' => 'api.broadcastmessage.index', 'uses' => 'BroadcastmessageController@index']);  
	Route::get('/broadcastmessage/count/{id}', ['middleware' => 'cors', 'as' => 'api.broadcastmessage.count', 'uses' => 'BroadcastmessageController@count']);  
	//post broadcast message
	Route::post('/broadcastmessage/update', ['middleware' => 'cors', 'as' => 'api.broadcastmessage.update', 'uses' => 'BroadcastmessageController@update']);
	//customer status
	Route::get('/customerstatus/index/{id}', ['middleware' => 'cors', 'as' => 'api.customerstatus.index', 'uses' => 'CustomerStatusController@index']); 
	//customer 
	Route::get('/customer/index/{id}', ['middleware' => 'cors', 'as' => 'api.customer.index', 'uses' => 'CustomerController@index']);
	Route::get('/customer/getbyphone/{id}', ['middleware' => 'cors', 'as' => 'api.customer.getbyphone', 'uses' => 'CustomerController@getbyphone']);
	//customer 
	Route::put('/customer/update/{id}', ['middleware' => 'cors', 'as' => 'api.customer.update', 'uses' => 'CustomerController@update']);

	//get call center
	Route::get('/callcenter/index', ['middleware' => 'cors', 'as' => 'api.callcenter.index', 'uses' => 'CallcenterController@index']); 
	//get so status
	Route::get('/sostatus/index/{id}', ['middleware' => 'cors', 'as' => 'api.sostatus.index', 'uses' => 'SOStatusController@index']); 

	//order
	//order active
	Route::get('/orderactive/index/{id}', ['middleware' => 'cors', 'as' => 'api.orderactive.index', 'uses' => 'OrderActiveController@index']); 
	//order active count
	Route::get('/orderactive/count/{id}', ['middleware' => 'cors', 'as' => 'api.orderactive.count', 'uses' => 'OrderActiveController@count']); 

	//order complete
	Route::get('/ordercomplete/index/{id}', ['middleware' => 'cors', 'as' => 'api.ordercomplete.index', 'uses' => 'OrderCompleteController@index']); 
	//order complete count
	Route::get('/salesorder/count/{id}', ['middleware' => 'cors', 'as' => 'api.salesorder.count', 'uses' => 'OrderCompleteController@count']); 

	//order detail
	Route::get('/orderdetail/index/{id}', ['middleware' => 'cors', 'as' => 'api.orderdetail.index', 'uses' => 'OrderDetailController@index']); 
	//picking detail
	Route::get('/pickingdetail/index/{id}', ['middleware' => 'cors', 'as' => 'api.pickingdetail.index', 'uses' => 'PickingDetailController@index']); 
	//loading detail
	Route::get('/loadingdetail/index/{id}', ['middleware' => 'cors', 'as' => 'api.loadingdetail.index', 'uses' => 'LoadingDetailController@index']); 
	//shipment detail
	Route::get('/shipmentdetail/index/{id}', ['middleware' => 'cors', 'as' => 'api.shipmentdetail.index', 'uses' => 'ShipmentDetailController@index']); 
	//shipment done detail
	Route::get('/shipmentdonedetail/index/{id}', ['middleware' => 'cors', 'as' => 'api.shipmentdonedetail.index', 'uses' => 'ShipmentDoneDetailController@index']); 
	//done detail
	Route::get('/donedetail/index/{id}', ['middleware' => 'cors', 'as' => 'api.donedetail.index', 'uses' => 'DoneDetailController@index']); 

	//registration
	Route::post('/registration/store', ['middleware' => 'cors', 'as' => 'api.registration.store', 'uses' => 'RegistrationController@store']);

	//rating
	Route::post('/rating/store', ['middleware' => 'cors', 'as' => 'api.rating.store', 'uses' => 'RatingController@store']);

	//registration user
	Route::post('/registration/storeuser', ['middleware' => 'cors', 'as' => 'api.registration.storeuser', 'uses' => 'RegistrationController@storeuser']);

	//numbercheck
	Route::post('/registration/numbercheck', ['middleware' => 'cors', 'as' => 'api.registration.numbercheck', 'uses' => 'RegistrationController@numbercheck']);
	
	//get tracking order
	Route::get('/trackingorder/index/{id}', ['middleware' => 'cors', 'as' => 'api.trackingorder.index', 'uses' => 'TrackingOrderController@index']); 
	//order detail
	Route::get('/trackingorderdetail/index/{id}', ['middleware' => 'cors', 'as' => 'api.trackingorderdetail.index', 'uses' => 'TrackingOrderDetailController@index']); 
 

	//order so
	Route::get('/trackingorderso/index/{id}', ['middleware' => 'cors', 'as' => 'api.trackingorderso.index', 'uses' => 'TrackingOrderSOController@index']); 
	//order do
	Route::get('/trackingorderdo/index/{id}', ['middleware' => 'cors', 'as' => 'api.trackingorderdo.index', 'uses' => 'TrackingOrderDOController@index']); 
	//auth
	Route::post('/auth/store', ['middleware' => 'cors', 'as' => 'api.auth.store', 'uses' => 'AuthController@store']);

	//execute store procedure
	Route::post('/exsp/spadddatesent/{id}', ['middleware' => 'cors', 'as' => 'api.auth.exsp', 'uses' => 'SpController@spadddatesent']);
	Route::post('/exsp/spshipmentback/{id}', ['middleware' => 'cors', 'as' => 'api.auth.exsp', 'uses' => 'SpController@spshipmentback']);
	Route::post('/exsp/spshipmentsent/{id}', ['middleware' => 'cors', 'as' => 'api.auth.exsp', 'uses' => 'SpController@spshipmentsent']);

	//send SMS
	Route::post('/notification/sendsms', ['middleware' => 'cors', 'as' => 'api.notification.sendsms', 'uses' => 'NotificationController@sendsms']); 
});

Auth::routes();

//User Management
Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
	//Home
	Route::get('/', ['as' => 'editor.index', 'uses' => 'EditorController@index']);

	//Profile
		//detail
	Route::get('/profile', ['as' => 'editor.profile.show', 'uses' => 'ProfileController@show']);
		//edit
	Route::get('/profile/edit', ['as' => 'editor.profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('/profile/edit', ['as' => 'editor.profile.update', 'uses' => 'ProfileController@update']);
		//edit password
	Route::get('/profile/password', ['as' => 'editor.profile.edit_password', 'uses' => 'ProfileController@edit_password']);
	Route::put('/profile/password', ['as' => 'editor.profile.update_password', 'uses' => 'ProfileController@update_password']); 

	//User
		//index
	Route::get('/user', ['middleware' => ['role:user|read'], 'as' => 'editor.user.index', 'uses' => 'UserController@index']);
		//create 
	Route::get('/user/create', ['middleware' => ['role:user|create'], 'as' => 'editor.user.create', 'uses' => 'UserController@create']);
	Route::post('/user/create', ['middleware' => ['role:user|create'], 'as' => 'editor.user.store', 'uses' => 'UserController@store']);
		//edit  
	Route::get('/user/{id}/edit', ['middleware' => ['role:user|update'], 'as' => 'editor.user.edit', 'uses' => 'UserController@edit']);
	Route::put('/user/{id}/edit', ['middleware' => ['role:user|update'], 'as' => 'editor.user.update', 'uses' => 'UserController@update']);
		//delete
	Route::delete('/user/{id}/delete', ['middleware' => ['role:user|delete'], 'as' => 'editor.user.delete', 'uses' => 'UserController@delete']);

	//User Mobile
		//index
	Route::get('/usermobile', ['as' => 'editor.usermobile.index', 'uses' => 'UsermobileController@index']);
		//create 
	Route::get('/usermobile/create', ['as' => 'editor.usermobile.create', 'uses' => 'UsermobileController@create']);
	Route::post('/usermobile/create', ['as' => 'editor.usermobile.store', 'uses' => 'UsermobileController@store']);
		//edit  
	Route::get('/usermobile/{id}/edit', ['as' => 'editor.usermobile.edit', 'uses' => 'UsermobileController@edit']);
	Route::put('/usermobile/{id}/edit', ['as' => 'editor.usermobile.update', 'uses' => 'UsermobileController@update']);
		//delete
	Route::delete('/usermobile/{id}/delete', ['as' => 'editor.usermobile.delete', 'uses' => 'UsermobileController@delete']);

	//Module
		//index
	Route::get('/module', ['middleware' => ['role:module|read'], 'as' => 'editor.module.index', 'uses' => 'ModuleController@index']);
		//create
	Route::get('/module/create', ['middleware' => ['role:module|create'], 'as' => 'editor.module.create', 'uses' => 'ModuleController@create']);
	Route::post('/module/create', ['middleware' => ['role:module|create'], 'as' => 'editor.module.store', 'uses' => 'ModuleController@store']);
		//edit
	Route::get('/module/{id}/edit', ['middleware' => ['role:module|update'], 'as' => 'editor.module.edit', 'uses' => 'ModuleController@edit']);
	Route::put('/module/{id}/edit', ['middleware' => ['role:module|update'], 'as' => 'editor.module.update', 'uses' => 'ModuleController@update']);
		//delete
	Route::delete('/module/{id}/delete', ['middleware' => ['role:module|delete'], 'as' => 'editor.module.delete', 'uses' => 'ModuleController@delete']);

	//Action
		//index
	Route::get('/action', ['middleware' => ['role:action|read'], 'as' => 'editor.action.index', 'uses' => 'ActionController@index']);
		//create
	Route::get('/action/create', ['middleware' => ['role:action|create'], 'as' => 'editor.action.create', 'uses' => 'ActionController@create']);
	Route::post('/action/create', ['middleware' => ['role:action|create'], 'as' => 'editor.action.store', 'uses' => 'ActionController@store']);
		//edit
	Route::get('/action/{id}/edit', ['middleware' => ['role:action|update'], 'as' => 'editor.action.edit', 'uses' => 'ActionController@edit']);
	Route::put('/action/{id}/edit', ['middleware' => ['role:action|update'], 'as' => 'editor.action.update', 'uses' => 'ActionController@update']);
		//delete
	Route::delete('/action/{id}/delete', ['middleware' => ['role:action|delete'], 'as' => 'editor.action.delete', 'uses' => 'ActionController@delete']);

	//Privilege
		//index
	Route::get('/privilege', ['middleware' => ['role:privilege|read'], 'as' => 'editor.privilege.index', 'uses' => 'PrivilegeController@index']);
		//create
	Route::get('/privilege/create', ['middleware' => ['role:privilege|create'], 'as' => 'editor.privilege.create', 'uses' => 'PrivilegeController@create']);
	Route::post('/privilege/create', ['middleware' => ['role:privilege|create'], 'as' => 'editor.privilege.store', 'uses' => 'PrivilegeController@store']);
		//edit
	Route::get('/privilege/{id}/edit', ['middleware' => ['role:privilege|update'], 'as' => 'editor.privilege.edit', 'uses' => 'PrivilegeController@edit']);
	Route::put('/privilege/{id}/edit', ['middleware' => ['role:privilege|update'], 'as' => 'editor.privilege.update', 'uses' => 'PrivilegeController@update']);
		//delete
	Route::delete('/privilege/{id}/delete', ['middleware' => ['role:privilege|delete'], 'as' => 'editor.privilege.delete', 'uses' => 'PrivilegeController@delete']);
});


//Change password
Route::group(['prefix' => 'editor', 'namespace' => 'Editor'], function()
{ 
		//edit password
	Route::get('/password/edit/{id}', ['as' => 'editor.password.edit_password', 'uses' => 'PasswordController@edit_password']);
	Route::put('/password/edit/{id}', ['as' => 'editor.password.update_password', 'uses' => 'PasswordController@update_password']);

	//reset password
	Route::get('/password/reset', ['as' => 'editor.password.reset_password', 'uses' => 'PasswordController@reset_password']);
	Route::post('/password/reset', ['as' => 'editor.password.ver_password', 'uses' => 'PasswordController@store_password']);

	//ver no
	Route::get('/password/reset/vercode/{id}', ['as' => 'editor.password.vercode', 'uses' => 'PasswordController@reset_vercode']);
	Route::put('/password/reset/vercode/{id}', ['as' => 'editor.password.store_vercode', 'uses' => 'PasswordController@store_vercode']);

	//new Password
	Route::get('/password/reset/newpass/{id}', ['as' => 'editor.password.newpass', 'uses' => 'PasswordController@reset_newpass']);
	Route::put('/password/reset/newpass/{id}', ['as' => 'editor.password.store_newpass', 'uses' => 'PasswordController@store_newpass']);
	Route::get('/password/reset/successnewpass/{id}', ['as' => 'editor.password.successnewpass', 'uses' => 'PasswordController@success_newpass']);


	//edit
	Route::get('/profile/editmobile/{id}', ['as' => 'editor.profile.editmobile', 'uses' => 'ProfileController@editmobile']);
	Route::put('/profile/editmobile/{id}', ['as' => 'editor.profile.updatemobile', 'uses' => 'ProfileController@updatemobile']);
 
 
});



Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
	//News
		//index
	Route::get('/news', ['as' => 'editor.news.index', 'uses' => 'NewsController@index']);
		//create
	Route::get('/news/create', ['as' => 'editor.news.create', 'uses' => 'NewsController@create']);
	Route::post('/news/create', ['as' => 'editor.news.store', 'uses' => 'NewsController@store']);
		//edit
	Route::get('/news/{id}/edit', ['as' => 'editor.news.edit', 'uses' => 'NewsController@edit']);
	Route::put('/news/{id}/edit', ['as' => 'editor.news.update', 'uses' => 'NewsController@update']);
		//delete
	Route::delete('/news/{id}/delete', ['as' => 'editor.news.delete', 'uses' => 'NewsController@delete']);

	//Call center
		//index
	Route::get('/callcenter', ['as' => 'editor.callcenter.index', 'uses' => 'CallcenterController@index']);
		//create
	Route::get('/callcenter/create', ['as' => 'editor.callcenter.create', 'uses' => 'CallcenterController@create']);
	Route::post('/callcenter/create', ['as' => 'editor.callcenter.store', 'uses' => 'CallcenterController@store']);
		//edit
	Route::get('/callcenter/{id}/edit', ['as' => 'editor.callcenter.edit', 'uses' => 'CallcenterController@edit']);
	Route::put('/callcenter/{id}/edit', ['as' => 'editor.callcenter.update', 'uses' => 'CallcenterController@update']);
		//delete
	Route::delete('/callcenter/{id}/delete', ['as' => 'editor.callcenter.delete', 'uses' => 'CallcenterController@delete']);

	//Broadcast message
		//index
	Route::get('/broadcastmessage', ['as' => 'editor.broadcastmessage.index', 'uses' => 'BroadcastmessageController@index']);
		//create
	Route::get('/broadcastmessage/create', ['as' => 'editor.broadcastmessage.create', 'uses' => 'BroadcastmessageController@create']);
	Route::post('/broadcastmessage/create', ['as' => 'editor.broadcastmessage.store', 'uses' => 'BroadcastmessageController@store']);
		//edit
	Route::get('/broadcastmessage/{id}/edit', ['as' => 'editor.broadcastmessage.edit', 'uses' => 'BroadcastmessageController@edit']);
	Route::put('/broadcastmessage/{id}/edit', ['as' => 'editor.broadcastmessage.update', 'uses' => 'BroadcastmessageController@update']);
		//delete
	Route::delete('/broadcastmessage/{id}/delete', ['as' => 'editor.broadcastmessage.delete', 'uses' => 'BroadcastmessageController@delete']);

	//Banner
		//index
	Route::get('/banner', ['as' => 'editor.banner.index', 'uses' => 'BannerController@index']);
		//create
	Route::get('/banner/create', ['as' => 'editor.banner.create', 'uses' => 'BannerController@create']);
	Route::post('/banner/create', ['as' => 'editor.banner.store', 'uses' => 'BannerController@store']);
		//edit
	Route::get('/banner/{id}/edit', ['as' => 'editor.banner.edit', 'uses' => 'BannerController@edit']);
	Route::put('/banner/{id}/edit', ['as' => 'editor.banner.update', 'uses' => 'BannerController@update']);
		//delete
	Route::delete('/banner/{id}/delete', ['as' => 'editor.banner.delete', 'uses' => 'BannerController@delete']);

	//Background
		//index
	Route::get('/background', ['as' => 'editor.background.index', 'uses' => 'BackgroundController@index']);
		//create
	Route::get('/background/create', ['as' => 'editor.background.create', 'uses' => 'BackgroundController@create']);
	Route::post('/background/create', ['as' => 'editor.background.store', 'uses' => 'BackgroundController@store']);
		//edit
	Route::get('/background/{id}/edit', ['as' => 'editor.background.edit', 'uses' => 'BackgroundController@edit']);
	Route::put('/background/{id}/edit', ['as' => 'editor.background.update', 'uses' => 'BackgroundController@update']);
		//delete
	Route::delete('/background/{id}/delete', ['as' => 'editor.background.delete', 'uses' => 'BackgroundController@delete']);

	//Company Profile
		//index
	Route::get('/companyprofile', ['as' => 'editor.companyprofile.index', 'uses' => 'CompanyprofileController@index']);
		//create
	Route::get('/companyprofile/create', ['as' => 'editor.companyprofile.create', 'uses' => 'CompanyprofileController@create']);
	Route::post('/companyprofile/create', ['as' => 'editor.companyprofile.store', 'uses' => 'CompanyprofileController@store']);
		//edit
	Route::get('/companyprofile/{id}/edit', ['as' => 'editor.companyprofile.edit', 'uses' => 'CompanyprofileController@edit']);
	Route::put('/companyprofile/{id}/edit', ['as' => 'editor.companyprofile.update', 'uses' => 'CompanyprofileController@update']);
		//delete
	Route::delete('/companyprofile/{id}/delete', ['as' => 'editor.companyprofile.delete', 'uses' => 'CompanyprofileController@delete']);

	//Promotion
		//index
	Route::get('/promotion', ['as' => 'editor.promotion.index', 'uses' => 'PromotionController@index']);
		//create
	Route::get('/promotion/create', ['as' => 'editor.promotion.create', 'uses' => 'PromotionController@create']);
	Route::post('/promotion/create', ['as' => 'editor.promotion.store', 'uses' => 'PromotionController@store']);
		//edit
	Route::get('/promotion/{id}/edit', ['as' => 'editor.promotion.edit', 'uses' => 'PromotionController@edit']);
	Route::put('/promotion/{id}/edit', ['as' => 'editor.promotion.update', 'uses' => 'PromotionController@update']);
		//delete
	Route::delete('/promotion/{id}/delete', ['as' => 'editor.promotion.delete', 'uses' => 'PromotionController@delete']);

});
