<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Directory Paths
    |--------------------------------------------------------------------------
    |
    | This value determines the directory path for the assets you use such as
    | CSS, js, plug-ins, etc.
    | 
    */

    'path' => [
        'uploads' => '/lmu/public/uploads',
        'bootstrap' => '/lmu/public/laravel/bootstrap',
        'css' => '/lmu/public/assets/css',
        'scss' => '/lmu/public/assets/lte_sass/build/scss',
        'img' => '/lmu/public/assets/img',
        'js' => '/lmu/public/assets/js',
        'plugin' => '/lmu/public/assets/plugins',
    ],

];