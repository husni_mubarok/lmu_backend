-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2017 at 08:37 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ims_asset`
--
CREATE DATABASE IF NOT EXISTS `ims_asset` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims_asset`;

-- --------------------------------------------------------

--
-- Table structure for table `asset_twr`
--

CREATE TABLE `asset_twr` (
  `id` int(11) NOT NULL,
  `id_category` int(11) DEFAULT NULL,
  `id_location` int(11) DEFAULT NULL,
  `item_code` text,
  `item_name` text,
  `reg_date` date DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `thumb` text,
  `status` text,
  `item_pic` text,
  `spek` text,
  `kondisi` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asset_twr_cat`
--

CREATE TABLE `asset_twr_cat` (
  `id` int(11) NOT NULL,
  `cat_name` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asset_twr_loc`
--

CREATE TABLE `asset_twr_loc` (
  `id` int(11) NOT NULL,
  `location` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) NOT NULL,
  `cat_name` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `consumption`
--

CREATE TABLE `consumption` (
  `id_consumption` int(11) NOT NULL,
  `title` text,
  `usage_id` int(11) DEFAULT NULL,
  `amount` decimal(15,2) DEFAULT NULL,
  `type_consume` varchar(200) DEFAULT NULL,
  `input_date` date DEFAULT NULL,
  `input_by` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `consumption_type`
--

CREATE TABLE `consumption_type` (
  `id` int(11) NOT NULL,
  `code_name` varchar(200) NOT NULL,
  `UoM` varchar(50) DEFAULT NULL,
  `keterangan` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `consumption_usage`
--

CREATE TABLE `consumption_usage` (
  `usage_id` int(11) NOT NULL,
  `usage_name` varchar(200) DEFAULT NULL,
  `usage_for` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE `division` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `do`
--

CREATE TABLE `do` (
  `id` int(11) NOT NULL,
  `no_do` varchar(20) DEFAULT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `deliver_date` datetime NOT NULL,
  `deliver_by` varchar(32) NOT NULL,
  `project` varchar(10) DEFAULT NULL,
  `notes` text NOT NULL,
  `gr_no` varchar(32) DEFAULT NULL,
  `approved_by` varchar(40) DEFAULT NULL,
  `approved_time` date DEFAULT NULL,
  `division` varchar(20) NOT NULL DEFAULT 'Asset'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `do`
--

INSERT INTO `do` (`id`, `no_do`, `from_id`, `to_id`, `deliver_date`, `deliver_by`, `project`, `notes`, `gr_no`, `approved_by`, `approved_time`, `division`) VALUES
(1, '1/GT/DO/IV/17', 0, 0, '2017-04-25 13:05:00', 'superuser', NULL, '<p>fdgdf</p>', NULL, 'superuser', '2017-04-25', ''),
(2, '2/GT/DO/IV/17', 0, 0, '2017-04-21 00:00:00', 'superuser', NULL, '<p>sad</p>', NULL, NULL, NULL, 'GA'),
(3, '3/GT/DO/I/70', 0, 0, '1970-01-01 07:00:00', 'superuser', NULL, '', 'dzfdf', 'superuser', '2017-05-12', ''),
(4, '4/GT/DO/IV/17', 0, 0, '2017-04-20 15:15:00', 'superuser', NULL, '', 'husni', NULL, NULL, ''),
(5, '5/GT/DO/IV/17', 0, 0, '2017-04-19 08:10:00', 'superuser', NULL, '<p>asxax</p>', NULL, NULL, NULL, 'Production'),
(6, '6/GT/DO/V/17', 0, 0, '2017-05-12 16:25:00', 'superuser', NULL, '<p>sadas saddasdsad</p>', NULL, 'superuser', '2017-05-12', 'GA');

-- --------------------------------------------------------

--
-- Table structure for table `do_detail`
--

CREATE TABLE `do_detail` (
  `id` int(11) NOT NULL,
  `do_id` int(11) DEFAULT NULL,
  `item_id` varchar(20) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `do_detail`
--

INSERT INTO `do_detail` (`id`, `do_id`, `item_id`, `qty`, `type`) VALUES
(1, 1, 'sdf', 2, NULL),
(2, 1, 'sdf', 3, NULL),
(3, 2, 'sdf', 2, NULL),
(4, 5, 'sdf', 2, NULL),
(5, 6, 'sdf', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `doc_counter`
--

CREATE TABLE `doc_counter` (
  `doc_code` varchar(10) NOT NULL,
  `last_num` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='menyimpan informasi nomor dokumen terakhir';

-- --------------------------------------------------------

--
-- Table structure for table `equipment`
--

CREATE TABLE `equipment` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fr`
--

CREATE TABLE `fr` (
  `id` int(10) UNSIGNED NOT NULL,
  `do_id` int(10) UNSIGNED DEFAULT NULL,
  `fr_num` varchar(35) DEFAULT NULL,
  `request_by` varchar(35) DEFAULT NULL,
  `request_time` datetime DEFAULT NULL,
  `wh_ori` int(11) DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `project` char(5) DEFAULT NULL,
  `division` varchar(25) DEFAULT NULL,
  `fr_type` char(3) DEFAULT NULL,
  `approved_by` varchar(35) DEFAULT NULL,
  `approved_time` datetime DEFAULT NULL,
  `approved_notes` text,
  `rejected_by` varchar(50) DEFAULT NULL,
  `rejected_time` datetime DEFAULT NULL,
  `rejected_notes` text,
  `update_by` varchar(50) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `asset_rejected_by` varchar(50) DEFAULT NULL,
  `asset_rejected_time` datetime DEFAULT NULL,
  `fr_note` text,
  `keynumber` int(6) DEFAULT NULL,
  `justification` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fr`
--

INSERT INTO `fr` (`id`, `do_id`, `fr_num`, `request_by`, `request_time`, `wh_ori`, `due_date`, `project`, `division`, `fr_type`, `approved_by`, `approved_time`, `approved_notes`, `rejected_by`, `rejected_time`, `rejected_notes`, `update_by`, `update_time`, `asset_rejected_by`, `asset_rejected_time`, `fr_note`, `keynumber`, `justification`) VALUES
(1, NULL, '000/GT/RQ/V/17', 'superuser', '2017-05-22 00:00:00', NULL, '2017-05-22 00:00:00', NULL, 'Asset', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 104222, NULL),
(2, NULL, '000/GT/RQ/V/17', 'superuser|bd', '2017-05-23 00:00:00', NULL, '2017-05-23 00:00:00', NULL, 'HRD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 131446, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fr_detail`
--

CREATE TABLE `fr_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `fr_id` int(11) DEFAULT NULL,
  `item_id` varchar(20) DEFAULT NULL COMMENT 'kode barang',
  `qty` float DEFAULT NULL COMMENT 'qty barang yg diminta',
  `delivered` float DEFAULT NULL COMMENT 'qty barang yg diberikan',
  `qty_buy` float NOT NULL,
  `qty_deliver` float NOT NULL,
  `wh` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fr_detail`
--

INSERT INTO `fr_detail` (`id`, `fr_id`, `item_id`, `qty`, `delivered`, `qty_buy`, `qty_deliver`, `wh`) VALUES
(1, 1, '', 1, NULL, 0, 0, 0),
(2, 2, '', 12, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ga_fr`
--

CREATE TABLE `ga_fr` (
  `fr_num` varchar(35) DEFAULT NULL,
  `request_by` varchar(35) DEFAULT NULL,
  `request_time` datetime DEFAULT NULL,
  `wh_ori` int(11) DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `project` char(5) DEFAULT NULL,
  `division` varchar(25) DEFAULT NULL,
  `fr_type` char(3) DEFAULT NULL,
  `approved_by` varchar(35) DEFAULT NULL,
  `approved_time` datetime DEFAULT NULL,
  `approved_notes` text,
  `update_by` varchar(100) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `fr_note` text,
  `keynumber` int(6) DEFAULT NULL,
  `justification` varchar(200) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ga_fr_detail`
--

CREATE TABLE `ga_fr_detail` (
  `fr_id` int(11) DEFAULT NULL,
  `item_id` varchar(20) DEFAULT NULL COMMENT 'kode barang',
  `qty` float DEFAULT NULL COMMENT 'qty barang yg diminta',
  `delivered` float DEFAULT NULL COMMENT 'qty barang yg diberikan',
  `qty_buy` float NOT NULL,
  `qty_deliver` float NOT NULL,
  `wh` int(11) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ga_gr`
--

CREATE TABLE `ga_gr` (
  `po_num` varchar(35) DEFAULT NULL,
  `gr_num` varchar(35) DEFAULT NULL,
  `gr_date` datetime DEFAULT NULL,
  `wh_id` int(11) DEFAULT NULL COMMENT 'kode warehouse',
  `create_time` datetime DEFAULT NULL,
  `gr_by` varchar(35) DEFAULT NULL,
  `notes` text,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ga_gr_detail`
--

CREATE TABLE `ga_gr_detail` (
  `item_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `gr_id` int(11) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ga_items`
--

CREATE TABLE `ga_items` (
  `item_code` varchar(32) NOT NULL COMMENT 'berisi kode barang utk barcode',
  `name` varchar(50) NOT NULL COMMENT 'nama barang',
  `category_id` int(11) DEFAULT NULL COMMENT 'isi kategori ada di file categories.php',
  `uom` varchar(12) NOT NULL COMMENT 'unit of measurement. misal: kg, unit, pcs, galon, box dsb.',
  `price` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `minimal_stock` int(11) NOT NULL,
  `created_by` varchar(32) NOT NULL COMMENT 'utk nyimpan info username pembuat record',
  `created_time` datetime NOT NULL COMMENT 'menyimpan info waktu simpan record',
  `notes` text NOT NULL,
  `specification` text NOT NULL,
  `picture` text NOT NULL,
  `last_wh` int(11) DEFAULT NULL COMMENT 'lokasi warehouse terakhir',
  `id_main` int(11) NOT NULL,
  `serial_number` varchar(50) NOT NULL,
  `id` int(11) NOT NULL,
  `location` varchar(10) NOT NULL,
  `purchase_date` date NOT NULL,
  `bast` varchar(250) NOT NULL,
  `holder` varchar(100) NOT NULL,
  `condition` varchar(10) NOT NULL,
  `penalty` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='data item barang';

-- --------------------------------------------------------

--
-- Table structure for table `ga_items_main`
--

CREATE TABLE `ga_items_main` (
  `id` int(11) NOT NULL,
  `item_code` varchar(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `category_id` int(4) NOT NULL,
  `uom` varchar(20) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `created_time` datetime NOT NULL,
  `notes` text NOT NULL,
  `spesification` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ga_organization`
--

CREATE TABLE `ga_organization` (
  `category` varchar(20) COLLATE latin1_general_ci NOT NULL COMMENT 'customer | supplier',
  `name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `address` text COLLATE latin1_general_ci,
  `telephone` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `fax` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `web` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `bank_acct` text COLLATE latin1_general_ci,
  `currency` char(3) COLLATE latin1_general_ci DEFAULT NULL COMMENT '3 chars currency CODE',
  `pic` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `pic_email` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `created_by` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `created_time` datetime NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ga_projects`
--

CREATE TABLE `ga_projects` (
  `prj_code` char(5) DEFAULT NULL,
  `prj_name` varchar(50) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `category` varchar(40) NOT NULL DEFAULT 'internal',
  `transport` decimal(15,2) NOT NULL DEFAULT '0.00',
  `taxi` decimal(15,2) NOT NULL DEFAULT '0.00',
  `rent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `airtax` decimal(15,2) NOT NULL DEFAULT '0.00',
  `status` varchar(50) DEFAULT NULL,
  `n_sim` int(11) DEFAULT NULL,
  `start_time` date DEFAULT NULL,
  `end_time` date DEFAULT NULL,
  `ops_remark` text,
  `ops_progress` text,
  `ops_pic` varchar(200) DEFAULT NULL,
  `mar_remark` text,
  `mar_progress` text,
  `mar_pic` varchar(200) DEFAULT NULL,
  `ops_link` varchar(400) DEFAULT NULL,
  `mar_link` varchar(400) DEFAULT NULL,
  `to_category` varchar(5) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ga_qty_wh`
--

CREATE TABLE `ga_qty_wh` (
  `qty` int(11) DEFAULT NULL,
  `wh_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ga_warehouses`
--

CREATE TABLE `ga_warehouses` (
  `id` int(11) NOT NULL,
  `name` varchar(35) NOT NULL,
  `address` mediumtext NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `pic` varchar(35) DEFAULT NULL COMMENT 'person in charge',
  `is_delete` char(1) NOT NULL COMMENT 'y - yes, n - no',
  `internal` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ga_warehouses`
--

INSERT INTO `ga_warehouses` (`id`, `name`, `address`, `telephone`, `pic`, `is_delete`, `internal`) VALUES
(1, 'asddas', '', '', NULL, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gr`
--

CREATE TABLE `gr` (
  `po_num` varchar(35) DEFAULT NULL,
  `gr_num` varchar(35) DEFAULT NULL,
  `gr_date` datetime DEFAULT NULL,
  `wh_id` int(11) DEFAULT NULL COMMENT 'kode warehouse',
  `create_time` datetime DEFAULT NULL,
  `gr_by` varchar(35) DEFAULT NULL,
  `notes` text,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gr_detail`
--

CREATE TABLE `gr_detail` (
  `item_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `gr_id` int(11) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `username` varchar(30) NOT NULL,
  `row_id` int(11) NOT NULL,
  `table_name` varchar(30) NOT NULL,
  `field_name` varchar(30) NOT NULL,
  `old_value` varchar(30) DEFAULT NULL,
  `new_value` varchar(30) NOT NULL,
  `log_time` datetime NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `iprice`
--

CREATE TABLE `iprice` (
  `id` int(10) NOT NULL,
  `item` varchar(30) DEFAULT NULL,
  `cat` varchar(2) DEFAULT NULL,
  `price` decimal(30,2) DEFAULT NULL,
  `kurs` varchar(10) DEFAULT NULL,
  `uom` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `iprice_cat`
--

CREATE TABLE `iprice_cat` (
  `id` int(10) NOT NULL,
  `cat_name` varchar(30) DEFAULT NULL,
  `user` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `id_main` int(11) DEFAULT NULL,
  `item_code` varchar(32) NOT NULL COMMENT 'berisi kode barang utk barcode',
  `name` varchar(500) NOT NULL COMMENT 'nama barang',
  `category_id` int(11) DEFAULT NULL COMMENT 'isi kategori ada di file categories.php',
  `type_id` int(11) DEFAULT NULL,
  `uom` varchar(12) NOT NULL COMMENT 'unit of measurement. misal: kg, unit, pcs, galon, box dsb.',
  `price` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `minimal_stock` int(11) NOT NULL,
  `created_by` varchar(32) NOT NULL COMMENT 'utk nyimpan info username pembuat record',
  `created_time` datetime NOT NULL COMMENT 'menyimpan info waktu simpan record',
  `notes` text NOT NULL,
  `specification` text NOT NULL,
  `picture` text NOT NULL,
  `last_wh` int(11) DEFAULT NULL COMMENT 'lokasi warehouse terakhir',
  `serial_number` varchar(50) DEFAULT NULL,
  `location` varchar(10) DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `bast` varchar(250) DEFAULT NULL,
  `holder` varchar(100) DEFAULT NULL,
  `condition` varchar(10) DEFAULT NULL,
  `penalty` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='data item barang';

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `id_main`, `item_code`, `name`, `category_id`, `type_id`, `uom`, `price`, `supplier`, `minimal_stock`, `created_by`, `created_time`, `notes`, `specification`, `picture`, `last_wh`, `serial_number`, `location`, `purchase_date`, `bast`, `holder`, `condition`, `penalty`) VALUES
(1, 1, 'sdf', 'sdf', 2, 2, 'dsdas', 123, 1, 0, '', '0000-00-00 00:00:00', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `items_main`
--

CREATE TABLE `items_main` (
  `id` int(11) NOT NULL,
  `item_code` varchar(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `category_id` int(10) NOT NULL,
  `type_id` int(10) DEFAULT NULL,
  `uom` varchar(20) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `created_time` datetime NOT NULL,
  `notes` text NOT NULL,
  `spesification` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items_main`
--

INSERT INTO `items_main` (`id`, `item_code`, `name`, `category_id`, `type_id`, `uom`, `created_by`, `created_time`, `notes`, `spesification`) VALUES
(1, 'dsf', 'sdffd', 2, 2, '', '', '0000-00-00 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `name` varchar(32) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `name` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='lokasi gudang tempat tujuan barang dipindahkan';

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(6) NOT NULL,
  `position` int(3) NOT NULL,
  `stage` int(2) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `href` varchar(200) DEFAULT NULL,
  `level` int(3) NOT NULL,
  `division` varchar(200) DEFAULT NULL,
  `lock` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `module_id` int(10) NOT NULL,
  `module_name` varchar(100) DEFAULT NULL,
  `module_url` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mr_main`
--

CREATE TABLE `mr_main` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `isi_pesan` text NOT NULL,
  `tanggal_pesan` datetime NOT NULL,
  `keynumber` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `new_category`
--

CREATE TABLE `new_category` (
  `id` int(11) NOT NULL,
  `id_parent` int(11) NOT NULL DEFAULT '0',
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `new_category`
--

INSERT INTO `new_category` (`id`, `id_parent`, `name`) VALUES
(2, 0, 'Apparel, Textiles & Accessories'),
(3, 0, 'Auto & Transportation'),
(4, 0, 'Bags, Shoes & Accessories'),
(5, 0, 'Electronics'),
(6, 0, 'Electrical Equipment, Components & Telecoms'),
(7, 0, 'Gift, Sports & Toys'),
(8, 0, 'Health & Beauty'),
(9, 0, 'Home, Lights & Construction'),
(10, 0, 'Machinery, Industrial Parts & Tools'),
(11, 0, 'Metallurgy, Chemicals, Rubber & Plastics'),
(12, 0, 'Packaging, Advertising & Office'),
(20, 3, 'Auto & Transportation (child)'),
(21, 20, 'Auto child'),
(22, 2, 'Baju Kerah'),
(23, 22, 'testing');

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE `organization` (
  `category` varchar(20) COLLATE latin1_general_ci NOT NULL COMMENT 'customer | supplier',
  `name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `address` text COLLATE latin1_general_ci,
  `telephone` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `fax` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `web` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `bank_acct` text COLLATE latin1_general_ci,
  `currency` char(3) COLLATE latin1_general_ci DEFAULT NULL COMMENT '3 chars currency CODE',
  `pic` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `pic_email` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `created_by` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `created_time` datetime NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`category`, `name`, `address`, `telephone`, `fax`, `web`, `bank_acct`, `currency`, `pic`, `pic_email`, `created_by`, `created_time`, `id`) VALUES
('black market', 'wed', 'wedwed', 'ewd', 'wed', 'wed', 'wed', NULL, 'wed', 'husni@jndj.com', 'superuser', '2017-04-21 08:14:26', 1),
('supplier', 'sdf', 'sdf', 'sdf', 'sdf', 'sdf', 'sdf', NULL, 'sdf', 'sdf@ddsad.com', 'superuser', '2017-04-21 08:15:02', 2);

-- --------------------------------------------------------

--
-- Table structure for table `paper_special`
--

CREATE TABLE `paper_special` (
  `id` int(11) NOT NULL,
  `vendorname` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'special',
  `category` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pev`
--

CREATE TABLE `pev` (
  `id` int(10) UNSIGNED NOT NULL,
  `pre_num` varchar(50) NOT NULL,
  `pre_date` date NOT NULL,
  `pev_num` varchar(50) DEFAULT NULL,
  `pev_date` datetime DEFAULT NULL,
  `division` varchar(50) DEFAULT NULL,
  `project` char(5) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_time` datetime NOT NULL,
  `pc_by` varchar(20) DEFAULT NULL,
  `pc_time` datetime DEFAULT NULL,
  `div_by` varchar(20) DEFAULT NULL,
  `div_time` datetime DEFAULT NULL,
  `div_rejected_by` varchar(20) DEFAULT NULL,
  `div_rejected_time` datetime DEFAULT NULL,
  `approved_by` varchar(50) DEFAULT NULL,
  `approved_time` datetime DEFAULT NULL,
  `approved_notes` text,
  `rejected_by` varchar(20) DEFAULT NULL,
  `rejected_time` datetime DEFAULT NULL,
  `rejected_notes` text,
  `suppliers` varchar(50) DEFAULT NULL COMMENT 'array kandidat supplier',
  `selected_supplier` int(11) DEFAULT NULL,
  `ppns` varchar(30) DEFAULT NULL COMMENT 'array ppn',
  `dps` varchar(50) DEFAULT NULL COMMENT 'array dp',
  `discs` varchar(50) DEFAULT NULL COMMENT 'array discount',
  `tops` text COMMENT 'array terms of payment',
  `notes` text COMMENT 'array notes',
  `currencies` varchar(35) DEFAULT NULL,
  `delivers` text,
  `deliver_times` text,
  `terms` text,
  `keynumber` int(6) DEFAULT NULL,
  `justification` varchar(200) DEFAULT NULL,
  `fr_note` text,
  `attach1` varchar(200) DEFAULT NULL,
  `attach2` varchar(200) DEFAULT NULL,
  `attach3` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pev_app`
--

CREATE TABLE `pev_app` (
  `id` int(11) NOT NULL,
  `redir_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pev_detail`
--

CREATE TABLE `pev_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `pev_num` int(11) NOT NULL,
  `item_id` varchar(15) NOT NULL,
  `qty_appr` float DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  `supp_idx` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `po`
--

CREATE TABLE `po` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `alt_suppliers` varchar(250) DEFAULT NULL,
  `po_num` varchar(50) DEFAULT NULL,
  `po_date` date NOT NULL,
  `project` varchar(10) DEFAULT NULL COMMENT 'fk ke tabel projects',
  `division` varchar(50) DEFAULT NULL,
  `reference` varchar(30) NOT NULL,
  `deliver_to` varchar(250) DEFAULT NULL,
  `deliver_time` varchar(250) DEFAULT NULL,
  `currency` char(3) DEFAULT NULL,
  `discount` decimal(15,2) DEFAULT NULL,
  `dp` decimal(15,0) DEFAULT NULL,
  `ppn` varchar(250) DEFAULT NULL,
  `payment_term` varchar(500) DEFAULT NULL,
  `terms` text,
  `notes` text,
  `created_by` varchar(50) NOT NULL,
  `created_time` datetime NOT NULL,
  `approved_by` varchar(30) DEFAULT NULL,
  `approved_time` datetime DEFAULT NULL,
  `appr_notes` text,
  `rejected_by` varchar(50) DEFAULT NULL,
  `rejected_time` datetime DEFAULT NULL,
  `rejected_notes` text,
  `sent_time` datetime DEFAULT NULL,
  `msg_subject` text,
  `msg_body` text,
  `keynumber` int(6) DEFAULT NULL,
  `justification` varchar(200) DEFAULT NULL,
  `fr_note` text,
  `gr_date` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='tabel ini berisi PO dan 3 alternatif vendor, disc dan ppn.';

-- --------------------------------------------------------

--
-- Table structure for table `po_copy`
--

CREATE TABLE `po_copy` (
  `supplier_id` int(11) NOT NULL,
  `alt_suppliers` varchar(250) DEFAULT NULL,
  `po_num` varchar(50) DEFAULT NULL,
  `po_date` date NOT NULL,
  `project` varchar(10) DEFAULT NULL COMMENT 'fk ke tabel projects',
  `division` varchar(15) DEFAULT NULL,
  `reference` varchar(30) NOT NULL,
  `deliver_to` varchar(250) DEFAULT NULL,
  `deliver_time` varchar(250) DEFAULT NULL,
  `currency` char(3) DEFAULT NULL,
  `discount` decimal(15,0) DEFAULT NULL COMMENT 'berisi 3 discount dr masing2 vendor',
  `dp` decimal(15,0) DEFAULT NULL,
  `ppn` varchar(250) DEFAULT NULL COMMENT 'berisi 3 option ppn',
  `payment_term` varchar(25) DEFAULT NULL,
  `terms` text,
  `notes` text,
  `appr_notes` text,
  `approved_by` varchar(30) DEFAULT NULL,
  `approved_time` datetime DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_time` datetime NOT NULL,
  `sent_time` datetime DEFAULT NULL,
  `msg_subject` text,
  `msg_body` text,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `po_detail`
--

CREATE TABLE `po_detail` (
  `po_num` int(11) NOT NULL,
  `item_id` varchar(25) NOT NULL,
  `qty` float NOT NULL,
  `v1` int(11) NOT NULL,
  `v2` int(11) DEFAULT NULL,
  `v3` int(11) DEFAULT NULL,
  `price` decimal(15,2) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `po_detail_new`
--

CREATE TABLE `po_detail_new` (
  `po_num` int(11) NOT NULL,
  `item_id` varchar(25) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `po_new`
--

CREATE TABLE `po_new` (
  `supplier_id` int(11) NOT NULL,
  `po_num` varchar(50) DEFAULT NULL,
  `po_date` date NOT NULL,
  `project` varchar(10) DEFAULT NULL,
  `division` varchar(15) DEFAULT NULL,
  `reference` varchar(30) NOT NULL,
  `deliver_to` varchar(250) DEFAULT NULL,
  `deliver_time` varchar(250) DEFAULT NULL,
  `currency` char(3) DEFAULT NULL,
  `discount` varchar(250) DEFAULT NULL,
  `dp` decimal(15,0) DEFAULT NULL,
  `ppn` varchar(250) DEFAULT NULL,
  `payment_term` varchar(25) DEFAULT NULL,
  `terms` text,
  `notes` text,
  `appr_notes` text,
  `approved_by` varchar(30) DEFAULT NULL,
  `approved_time` datetime DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_time` datetime NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `po_old`
--

CREATE TABLE `po_old` (
  `supplier_id` int(11) NOT NULL,
  `alt_suppliers` varchar(250) DEFAULT NULL,
  `po_num` varchar(50) DEFAULT NULL,
  `po_date` date NOT NULL,
  `project` varchar(10) DEFAULT NULL COMMENT 'fk ke tabel projects',
  `division` varchar(15) DEFAULT NULL,
  `reference` varchar(30) NOT NULL,
  `deliver_to` varchar(250) DEFAULT NULL,
  `deliver_time` varchar(250) DEFAULT NULL,
  `currency` char(3) DEFAULT NULL,
  `alt_discount` varchar(250) DEFAULT NULL COMMENT 'berisi 3 discount dr masing2 vendor',
  `dp` decimal(15,0) DEFAULT NULL,
  `alt_ppn` varchar(250) DEFAULT NULL COMMENT 'berisi 3 option ppn',
  `payment_term` varchar(25) DEFAULT NULL,
  `terms` text,
  `notes` text,
  `appr_notes` text,
  `approved_by` varchar(30) DEFAULT NULL,
  `approved_time` datetime DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_time` datetime NOT NULL,
  `sent_time` datetime DEFAULT NULL,
  `msg_subject` text,
  `msg_body` text,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `position_id` int(10) NOT NULL,
  `name` varchar(100) DEFAULT NULL COMMENT 'position code',
  `hierarchy` int(10) DEFAULT NULL,
  `superior_id` int(10) DEFAULT NULL COMMENT 'work supervisor',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pre`
--

CREATE TABLE `pre` (
  `id` int(10) UNSIGNED NOT NULL,
  `fr_num` varchar(50) DEFAULT NULL,
  `fr_date` datetime DEFAULT NULL,
  `fr_type` varchar(50) DEFAULT NULL,
  `project` char(5) DEFAULT NULL,
  `pre_num` varchar(50) NOT NULL,
  `pre_date` date NOT NULL,
  `division` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `approved_by` varchar(50) DEFAULT NULL,
  `approved_time` datetime DEFAULT NULL,
  `approved_notes` text,
  `rejected_by` varchar(50) DEFAULT NULL,
  `rejected_time` datetime DEFAULT NULL,
  `rejected_notes` text,
  `created_by` varchar(50) NOT NULL,
  `created_time` datetime NOT NULL,
  `alt_suppliers` varchar(50) DEFAULT NULL COMMENT 'alternatif supplier',
  `sent_time` datetime DEFAULT NULL COMMENT 'tgl kirim quotation request',
  `msg_subject` text,
  `msg_body` text,
  `keynumber` int(6) DEFAULT NULL,
  `justification` varchar(200) DEFAULT NULL,
  `fr_note` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pre_detail`
--

CREATE TABLE `pre_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `pre_num` int(11) NOT NULL,
  `item_id` varchar(15) NOT NULL,
  `qoh` float DEFAULT NULL,
  `qty_req` float DEFAULT NULL,
  `qty_appr` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `prj_code` char(5) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `agreement_number` varchar(50) DEFAULT NULL,
  `agreement_title` varchar(200) DEFAULT NULL,
  `prj_name` varchar(200) DEFAULT NULL,
  `prefix` varchar(50) DEFAULT NULL,
  `category` varchar(40) NOT NULL DEFAULT 'internal',
  `currency` varchar(10) DEFAULT NULL,
  `address` text,
  `transport` decimal(15,2) NOT NULL DEFAULT '0.00',
  `taxi` decimal(15,2) NOT NULL DEFAULT '0.00',
  `rent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `airtax` decimal(15,2) NOT NULL DEFAULT '0.00',
  `status` varchar(50) DEFAULT NULL,
  `n_sim` int(11) DEFAULT NULL,
  `start_time` date DEFAULT NULL,
  `end_time` date DEFAULT NULL,
  `ops_remark` text,
  `ops_progress` text,
  `ops_pic` varchar(200) DEFAULT NULL,
  `mar_remark` text,
  `mar_progress` text,
  `mar_pic` varchar(200) DEFAULT NULL,
  `ops_link` varchar(400) DEFAULT NULL,
  `mar_link` varchar(400) DEFAULT NULL,
  `to_category` varchar(5) DEFAULT NULL,
  `view` varchar(10) DEFAULT NULL,
  `view_subcost` varchar(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `prj_code`, `value`, `agreement_number`, `agreement_title`, `prj_name`, `prefix`, `category`, `currency`, `address`, `transport`, `taxi`, `rent`, `airtax`, `status`, `n_sim`, `start_time`, `end_time`, `ops_remark`, `ops_progress`, `ops_pic`, `mar_remark`, `mar_progress`, `mar_pic`, `ops_link`, `mar_link`, `to_category`, `view`, `view_subcost`) VALUES
(1, '342', 324, '4', '423', '234', '234', 'internal', 'qwd', 'qwd', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `projects_save`
--

CREATE TABLE `projects_save` (
  `prj_code` char(5) DEFAULT NULL,
  `prj_name` varchar(50) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `category` varchar(40) NOT NULL DEFAULT 'internal',
  `transport` decimal(15,2) NOT NULL DEFAULT '0.00',
  `taxi` decimal(15,2) NOT NULL DEFAULT '0.00',
  `rent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `airtax` decimal(15,2) NOT NULL DEFAULT '0.00',
  `status` varchar(50) DEFAULT NULL,
  `n_sim` int(11) DEFAULT NULL,
  `start_time` date DEFAULT NULL,
  `end_time` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qo`
--

CREATE TABLE `qo` (
  `id` int(10) NOT NULL,
  `product` varchar(50) DEFAULT NULL,
  `quot_num` varchar(50) DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `to_name` varchar(50) DEFAULT NULL,
  `to_company` varchar(50) DEFAULT NULL,
  `to_address` text,
  `terms` text,
  `vat` varchar(2) DEFAULT NULL,
  `currency` int(5) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qo_detail`
--

CREATE TABLE `qo_detail` (
  `id` int(11) NOT NULL,
  `qo_id` int(11) DEFAULT NULL,
  `qty` varchar(50) DEFAULT NULL,
  `description` text,
  `currency` varchar(10) DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  `discount` varchar(50) DEFAULT NULL,
  `line_total` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qty_wh`
--

CREATE TABLE `qty_wh` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `wh_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `updater` varchar(10) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `qty_wh`
--

INSERT INTO `qty_wh` (`id`, `item_id`, `wh_id`, `qty`, `updater`) VALUES
(1, 0, 0, 2, NULL),
(2, 0, 0, 2, NULL),
(3, 0, 0, 3, NULL),
(4, 0, 0, 3, NULL),
(5, 0, 0, 1, NULL),
(6, 0, 0, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `id` int(10) UNSIGNED NOT NULL,
  `rpt_wh` char(20) DEFAULT NULL,
  `rpt_subject` varchar(255) DEFAULT NULL,
  `rpt_text` text,
  `rpt_time` datetime DEFAULT NULL,
  `approve_by` varchar(35) DEFAULT NULL,
  `approve_time` datetime DEFAULT NULL,
  `approve_notes` text,
  `create_by` varchar(35) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `created_by_level` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report`
--

INSERT INTO `report` (`id`, `rpt_wh`, `rpt_subject`, `rpt_text`, `rpt_time`, `approve_by`, `approve_time`, `approve_notes`, `create_by`, `create_time`, `created_by_level`) VALUES
(1, 'admin', 'sdc', 'a:6:{i:0;s:10:\"<p>dsc</p>\";i:1;s:10:\"<p>dcs</p>\";i:2;s:10:\"<p>dsc</p>\";i:3;s:15:\"<p>dc sdsad</p>\";i:4;s:0:\"\";i:5;s:0:\"\";}', '2017-04-15 00:00:00', 'superuser', '2017-04-21 16:32:25', '<p>SA</p>', 'superuser', '2017-04-21 13:59:14', '1-0-0'),
(2, 'admin', 'xz', 'a:6:{i:0;s:10:\"<p>zxc</p>\";i:1;s:9:\"<p>xz</p>\";i:2;s:9:\"<p>xc</p>\";i:3;s:9:\"<p>xc</p>\";i:4;s:0:\"\";i:5;s:0:\"\";}', '2017-04-21 00:00:00', NULL, NULL, NULL, 'superuser', '2017-04-21 14:38:50', '1-0-0');

-- --------------------------------------------------------

--
-- Table structure for table `report_attach`
--

CREATE TABLE `report_attach` (
  `id` int(11) NOT NULL,
  `id_report` int(11) DEFAULT NULL,
  `filename` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_so`
--

CREATE TABLE `rfq_so` (
  `id` int(10) UNSIGNED NOT NULL,
  `so_num` varchar(35) DEFAULT NULL,
  `so_id` int(11) DEFAULT NULL,
  `rfq_so_num` varchar(35) DEFAULT NULL,
  `rfq_so_date` datetime DEFAULT NULL,
  `project` varchar(35) DEFAULT NULL,
  `division` varchar(35) DEFAULT NULL,
  `sent_by` varchar(35) DEFAULT NULL,
  `alt_supplier` varchar(50) DEFAULT NULL,
  `sent_time` datetime DEFAULT NULL,
  `msg_subject` text,
  `msg_body` text,
  `approved_by` varchar(35) DEFAULT NULL,
  `approved_time` datetime DEFAULT NULL,
  `approved_notes` text NOT NULL,
  `rejected_by` varchar(35) DEFAULT NULL,
  `rejected_time` datetime DEFAULT NULL,
  `rejected_notes` text NOT NULL,
  `keynumber` int(6) NOT NULL,
  `so_note` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(10) NOT NULL,
  `role_name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE `role_permission` (
  `role_permission_id` int(10) NOT NULL,
  `role_id` int(10) DEFAULT NULL,
  `module_id` int(10) DEFAULT NULL,
  `permission` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rq`
--

CREATE TABLE `rq` (
  `id` int(10) UNSIGNED NOT NULL,
  `fr_num` varchar(35) DEFAULT NULL,
  `requested_by` varchar(35) DEFAULT NULL,
  `requested_at` datetime DEFAULT NULL,
  `wh_ori` int(11) DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `project_id` char(5) DEFAULT NULL,
  `division_id` varchar(25) DEFAULT NULL,
  `fr_type` char(3) DEFAULT NULL,
  `approved_by` varchar(35) DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `approved_notes` text,
  `update_by` varchar(100) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `fr_note` text,
  `keynumber` int(6) DEFAULT NULL,
  `justification` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `rq_detail`
--

CREATE TABLE `rq_detail` (
  `fr_id` int(11) DEFAULT NULL,
  `item_id` varchar(20) DEFAULT NULL COMMENT 'kode barang',
  `qty` float DEFAULT NULL COMMENT 'qty barang yg diminta',
  `delivered` float DEFAULT NULL COMMENT 'qty barang yg diberikan',
  `qty_buy` float NOT NULL,
  `qty_deliver` float NOT NULL,
  `wh` int(11) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `se`
--

CREATE TABLE `se` (
  `id` int(10) UNSIGNED NOT NULL,
  `rfq_so_num` varchar(50) NOT NULL DEFAULT '',
  `rfq_so_date` date NOT NULL,
  `se_num` varchar(50) DEFAULT NULL,
  `se_date` datetime DEFAULT NULL,
  `project` char(10) DEFAULT NULL,
  `division` varchar(35) DEFAULT NULL,
  `approved_by` varchar(50) DEFAULT NULL,
  `approved_time` datetime DEFAULT NULL,
  `approved_notes` text,
  `rejected_by` varchar(50) DEFAULT NULL,
  `rejected_time` datetime DEFAULT NULL,
  `rejected_notes` text,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_time` datetime NOT NULL,
  `suppliers` varchar(50) DEFAULT NULL COMMENT 'array kandidat supplier',
  `selected_supplier` int(11) DEFAULT NULL,
  `ppns` varchar(30) DEFAULT NULL COMMENT 'array ppn',
  `dps` varchar(50) DEFAULT NULL COMMENT 'array dp',
  `discs` varchar(50) DEFAULT NULL COMMENT 'array discount',
  `tops` text COMMENT 'array terms of payment',
  `notes` text COMMENT 'array notes',
  `currencies` varchar(35) DEFAULT NULL,
  `delivers` text,
  `deliver_times` text,
  `terms` text,
  `keynumber` int(6) NOT NULL,
  `ack_by` varchar(50) DEFAULT NULL,
  `ack_time` datetime DEFAULT NULL,
  `so_note` text,
  `attach1` varchar(300) DEFAULT NULL,
  `attach2` varchar(300) DEFAULT NULL,
  `attach3` varchar(300) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `se_detail`
--

CREATE TABLE `se_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `se_num` varchar(50) NOT NULL DEFAULT '',
  `item_id` varchar(15) NOT NULL,
  `job_desc` text,
  `qty_appr` float DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  `supp_idx` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `so`
--

CREATE TABLE `so` (
  `id` int(10) UNSIGNED NOT NULL,
  `so_type` varchar(35) DEFAULT NULL,
  `so_num` varchar(35) DEFAULT NULL,
  `division` varchar(35) DEFAULT NULL,
  `so_date` datetime DEFAULT NULL,
  `project` varchar(35) DEFAULT NULL,
  `notes` text,
  `reference` varchar(35) DEFAULT NULL,
  `deliver_to` text,
  `deliver_time` text,
  `create_by` varchar(35) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `approved_by` varchar(35) DEFAULT NULL,
  `approved_notes` text,
  `approved_time` datetime DEFAULT NULL,
  `rejected_by` varchar(10) DEFAULT NULL,
  `rejected_time` datetime DEFAULT NULL,
  `rejected_notes` text,
  `keynumber` int(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `so_detail`
--

CREATE TABLE `so_detail` (
  `so_id` int(11) NOT NULL,
  `job_desc` text,
  `qty` decimal(11,2) DEFAULT NULL,
  `unit_price` decimal(15,2) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `su`
--

CREATE TABLE `su` (
  `reason` varchar(15) COLLATE latin1_general_ci DEFAULT NULL COMMENT 'gr | correction',
  `from_id` int(11) DEFAULT NULL,
  `to_wh` int(11) DEFAULT NULL COMMENT 'wh id',
  `notes` text COLLATE latin1_general_ci,
  `created_by` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `su_detail`
--

CREATE TABLE `su_detail` (
  `item_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `su_id` int(11) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `realname` varchar(32) NOT NULL,
  `level` varchar(15) NOT NULL COMMENT 'link ke tabel levels',
  `division` varchar(15) NOT NULL COMMENT 'link ke tabel divisions',
  `last_login` datetime NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `warehouses`
--

CREATE TABLE `warehouses` (
  `id` int(11) NOT NULL,
  `name` varchar(35) NOT NULL,
  `address` mediumtext NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `pic` varchar(35) DEFAULT NULL COMMENT 'person in charge',
  `is_delete` char(1) NOT NULL COMMENT 'y - yes, n - no',
  `internal` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warehouses`
--

INSERT INTO `warehouses` (`id`, `name`, `address`, `telephone`, `pic`, `is_delete`, `internal`) VALUES
(1, 'wr;lsa,dlad', '', '', NULL, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wo`
--

CREATE TABLE `wo` (
  `id` int(10) UNSIGNED NOT NULL,
  `wo_type` varchar(35) DEFAULT NULL,
  `wo_num` varchar(35) DEFAULT NULL,
  `division` varchar(35) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `currency` char(3) DEFAULT NULL,
  `req_date` datetime DEFAULT NULL,
  `project` varchar(35) DEFAULT NULL,
  `content` text,
  `notes` text,
  `terms` text,
  `terms_payment` text,
  `reference` varchar(35) DEFAULT NULL,
  `deliver_to` text,
  `deliver_time` text,
  `discount` decimal(15,2) DEFAULT NULL,
  `dp` decimal(15,2) DEFAULT NULL,
  `ppn` char(3) DEFAULT NULL,
  `create_by` varchar(35) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `approved_by` varchar(35) DEFAULT NULL,
  `approved_time` datetime DEFAULT NULL,
  `rejected_by` varchar(35) DEFAULT NULL,
  `rejected_time` datetime DEFAULT NULL,
  `keynumber` int(6) NOT NULL,
  `so_note` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wo_detail`
--

CREATE TABLE `wo_detail` (
  `wo_id` int(11) DEFAULT NULL,
  `job_desc` text,
  `qty` decimal(20,2) DEFAULT NULL,
  `unit_price` decimal(15,2) DEFAULT NULL,
  `solar` int(11) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wo_track`
--

CREATE TABLE `wo_track` (
  `so_id` int(11) DEFAULT NULL,
  `rfqso_id` int(11) DEFAULT NULL,
  `se_id` int(11) DEFAULT NULL,
  `wo_id` int(11) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `x_gr`
--

CREATE TABLE `x_gr` (
  `reason` varchar(15) COLLATE latin1_general_ci DEFAULT NULL COMMENT 'gr | correction',
  `from_id` int(11) DEFAULT NULL,
  `to_wh` int(11) DEFAULT NULL COMMENT 'wh id',
  `notes` text COLLATE latin1_general_ci,
  `created_by` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset_twr`
--
ALTER TABLE `asset_twr`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_twr_cat`
--
ALTER TABLE `asset_twr_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_twr_loc`
--
ALTER TABLE `asset_twr_loc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consumption`
--
ALTER TABLE `consumption`
  ADD PRIMARY KEY (`id_consumption`);

--
-- Indexes for table `consumption_type`
--
ALTER TABLE `consumption_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consumption_usage`
--
ALTER TABLE `consumption_usage`
  ADD PRIMARY KEY (`usage_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `do`
--
ALTER TABLE `do`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deliver_by` (`deliver_by`);

--
-- Indexes for table `do_detail`
--
ALTER TABLE `do_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `do_id` (`do_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `doc_counter`
--
ALTER TABLE `doc_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fr`
--
ALTER TABLE `fr`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fr_detail`
--
ALTER TABLE `fr_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `fr_id` (`fr_id`);

--
-- Indexes for table `ga_fr`
--
ALTER TABLE `ga_fr`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ga_fr_detail`
--
ALTER TABLE `ga_fr_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `fr_id` (`fr_id`);

--
-- Indexes for table `ga_gr`
--
ALTER TABLE `ga_gr`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ga_gr_detail`
--
ALTER TABLE `ga_gr_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ga_items`
--
ALTER TABLE `ga_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item_code` (`item_code`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `ga_items_main`
--
ALTER TABLE `ga_items_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ga_organization`
--
ALTER TABLE `ga_organization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ga_projects`
--
ALTER TABLE `ga_projects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `prj_code` (`prj_code`);

--
-- Indexes for table `ga_qty_wh`
--
ALTER TABLE `ga_qty_wh`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item_wh` (`wh_id`,`item_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `wh_id` (`wh_id`);

--
-- Indexes for table `ga_warehouses`
--
ALTER TABLE `ga_warehouses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gr`
--
ALTER TABLE `gr`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gr_detail`
--
ALTER TABLE `gr_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iprice`
--
ALTER TABLE `iprice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iprice_cat`
--
ALTER TABLE `iprice_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item_code` (`item_code`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `items_main`
--
ALTER TABLE `items_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `mr_main`
--
ALTER TABLE `mr_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `new_category`
--
ALTER TABLE `new_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paper_special`
--
ALTER TABLE `paper_special`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pev`
--
ALTER TABLE `pev`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pev_app`
--
ALTER TABLE `pev_app`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pev_detail`
--
ALTER TABLE `pev_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `po`
--
ALTER TABLE `po`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `po_copy`
--
ALTER TABLE `po_copy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `po_detail`
--
ALTER TABLE `po_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `po_detail_new`
--
ALTER TABLE `po_detail_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `po_new`
--
ALTER TABLE `po_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `po_old`
--
ALTER TABLE `po_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`position_id`);

--
-- Indexes for table `pre`
--
ALTER TABLE `pre`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pre_detail`
--
ALTER TABLE `pre_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `prj_code` (`prj_code`);

--
-- Indexes for table `projects_save`
--
ALTER TABLE `projects_save`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `prj_code` (`prj_code`);

--
-- Indexes for table `qo`
--
ALTER TABLE `qo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qo_detail`
--
ALTER TABLE `qo_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qty_wh`
--
ALTER TABLE `qty_wh`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_attach`
--
ALTER TABLE `report_attach`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rfq_so`
--
ALTER TABLE `rfq_so`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `rq`
--
ALTER TABLE `rq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rq_detail`
--
ALTER TABLE `rq_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `fr_id` (`fr_id`);

--
-- Indexes for table `se`
--
ALTER TABLE `se`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `se_detail`
--
ALTER TABLE `se_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `so`
--
ALTER TABLE `so`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `so_detail`
--
ALTER TABLE `so_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `su`
--
ALTER TABLE `su`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `su_detail`
--
ALTER TABLE `su_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `warehouses`
--
ALTER TABLE `warehouses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wo`
--
ALTER TABLE `wo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wo_detail`
--
ALTER TABLE `wo_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wo_track`
--
ALTER TABLE `wo_track`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `x_gr`
--
ALTER TABLE `x_gr`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset_twr`
--
ALTER TABLE `asset_twr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `asset_twr_cat`
--
ALTER TABLE `asset_twr_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `asset_twr_loc`
--
ALTER TABLE `asset_twr_loc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `consumption`
--
ALTER TABLE `consumption`
  MODIFY `id_consumption` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `consumption_type`
--
ALTER TABLE `consumption_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `consumption_usage`
--
ALTER TABLE `consumption_usage`
  MODIFY `usage_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `division`
--
ALTER TABLE `division`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `do`
--
ALTER TABLE `do`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `do_detail`
--
ALTER TABLE `do_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `doc_counter`
--
ALTER TABLE `doc_counter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `equipment`
--
ALTER TABLE `equipment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fr`
--
ALTER TABLE `fr`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fr_detail`
--
ALTER TABLE `fr_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ga_fr`
--
ALTER TABLE `ga_fr`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ga_fr_detail`
--
ALTER TABLE `ga_fr_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ga_gr`
--
ALTER TABLE `ga_gr`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ga_gr_detail`
--
ALTER TABLE `ga_gr_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ga_items`
--
ALTER TABLE `ga_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ga_items_main`
--
ALTER TABLE `ga_items_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ga_organization`
--
ALTER TABLE `ga_organization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ga_projects`
--
ALTER TABLE `ga_projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ga_qty_wh`
--
ALTER TABLE `ga_qty_wh`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ga_warehouses`
--
ALTER TABLE `ga_warehouses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gr`
--
ALTER TABLE `gr`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gr_detail`
--
ALTER TABLE `gr_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `iprice`
--
ALTER TABLE `iprice`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `iprice_cat`
--
ALTER TABLE `iprice_cat`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `items_main`
--
ALTER TABLE `items_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `module_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mr_main`
--
ALTER TABLE `mr_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `new_category`
--
ALTER TABLE `new_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `paper_special`
--
ALTER TABLE `paper_special`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pev`
--
ALTER TABLE `pev`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pev_app`
--
ALTER TABLE `pev_app`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pev_detail`
--
ALTER TABLE `pev_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `po`
--
ALTER TABLE `po`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `po_copy`
--
ALTER TABLE `po_copy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `po_detail`
--
ALTER TABLE `po_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `po_detail_new`
--
ALTER TABLE `po_detail_new`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `po_new`
--
ALTER TABLE `po_new`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `po_old`
--
ALTER TABLE `po_old`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
  MODIFY `position_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pre`
--
ALTER TABLE `pre`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pre_detail`
--
ALTER TABLE `pre_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `projects_save`
--
ALTER TABLE `projects_save`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `qo`
--
ALTER TABLE `qo`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `qo_detail`
--
ALTER TABLE `qo_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `qty_wh`
--
ALTER TABLE `qty_wh`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `report_attach`
--
ALTER TABLE `report_attach`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_so`
--
ALTER TABLE `rfq_so`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rq`
--
ALTER TABLE `rq`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rq_detail`
--
ALTER TABLE `rq_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `se`
--
ALTER TABLE `se`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `se_detail`
--
ALTER TABLE `se_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `so`
--
ALTER TABLE `so`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `so_detail`
--
ALTER TABLE `so_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `su`
--
ALTER TABLE `su`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `su_detail`
--
ALTER TABLE `su_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `warehouses`
--
ALTER TABLE `warehouses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wo`
--
ALTER TABLE `wo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wo_detail`
--
ALTER TABLE `wo_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wo_track`
--
ALTER TABLE `wo_track`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `x_gr`
--
ALTER TABLE `x_gr`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;--
-- Database: `ims_decree`
--
CREATE DATABASE IF NOT EXISTS `ims_decree` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims_decree`;

-- --------------------------------------------------------

--
-- Table structure for table `log_view`
--

CREATE TABLE `log_view` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `location` varchar(400) NOT NULL,
  `log_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(6) NOT NULL,
  `position` int(3) NOT NULL,
  `stage` int(2) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `href` varchar(200) DEFAULT NULL,
  `level` int(3) NOT NULL,
  `division` varchar(200) DEFAULT NULL,
  `lock` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `log_view`
--
ALTER TABLE `log_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `log_view`
--
ALTER TABLE `log_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;--
-- Database: `ims_dirfin`
--
CREATE DATABASE IF NOT EXISTS `ims_dirfin` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims_dirfin`;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(1) NOT NULL,
  `username` varchar(200) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `opt_name` varchar(50) NOT NULL,
  `opt_value` varchar(50) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_counter`
--

CREATE TABLE `doc_counter` (
  `doc_code` varchar(10) NOT NULL,
  `last_num` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='menyimpan informasi nomor dokumen terakhir';

-- --------------------------------------------------------

--
-- Table structure for table `log_view`
--

CREATE TABLE `log_view` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `location` varchar(400) NOT NULL,
  `log_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(6) NOT NULL,
  `position` int(3) NOT NULL,
  `stage` int(2) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `href` varchar(200) DEFAULT NULL,
  `level` int(3) NOT NULL,
  `division` varchar(200) DEFAULT NULL,
  `lock` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `opt_name` (`opt_name`);

--
-- Indexes for table `doc_counter`
--
ALTER TABLE `doc_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_view`
--
ALTER TABLE `log_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_counter`
--
ALTER TABLE `doc_counter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_view`
--
ALTER TABLE `log_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;--
-- Database: `ims_dirops`
--
CREATE DATABASE IF NOT EXISTS `ims_dirops` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims_dirops`;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(1) NOT NULL,
  `username` varchar(200) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `opt_name` varchar(50) NOT NULL,
  `opt_value` varchar(50) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_counter`
--

CREATE TABLE `doc_counter` (
  `doc_code` varchar(10) NOT NULL,
  `last_num` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='menyimpan informasi nomor dokumen terakhir';

-- --------------------------------------------------------

--
-- Table structure for table `log_view`
--

CREATE TABLE `log_view` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `location` varchar(400) NOT NULL,
  `log_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(6) NOT NULL,
  `position` int(3) NOT NULL,
  `stage` int(2) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `href` varchar(200) DEFAULT NULL,
  `level` int(3) NOT NULL,
  `division` varchar(200) DEFAULT NULL,
  `lock` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `opt_name` (`opt_name`);

--
-- Indexes for table `doc_counter`
--
ALTER TABLE `doc_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_view`
--
ALTER TABLE `log_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_counter`
--
ALTER TABLE `doc_counter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_view`
--
ALTER TABLE `log_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;--
-- Database: `ims_dirut`
--
CREATE DATABASE IF NOT EXISTS `ims_dirut` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims_dirut`;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(1) NOT NULL,
  `username` varchar(200) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_counter`
--

CREATE TABLE `doc_counter` (
  `doc_code` varchar(10) NOT NULL,
  `last_num` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='menyimpan informasi nomor dokumen terakhir';

-- --------------------------------------------------------

--
-- Table structure for table `log_view`
--

CREATE TABLE `log_view` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `location` varchar(400) NOT NULL,
  `log_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_view`
--

INSERT INTO `log_view` (`id`, `user`, `location`, `log_time`) VALUES
(1, 'superuser', '/ims99/dirut/?m=emp_join&op=emp_join', '2017-04-18 08:11:39'),
(2, 'superuser', '/ims99/dirut/?m=bank_ru&op=bank_ru', '2017-04-26 06:22:46'),
(3, 'superuser', '/ims99/dirut/?m=bank_ru_detail&op=bank_ru&nama_bank=bri&curr=idr', '2017-04-26 06:22:51'),
(4, 'superuser', '/ims99/dirut/?m=bank_ru_add&op=bank_ru', '2017-04-26 06:22:53'),
(5, 'superuser', '/ims99/dirut/?m=bank_ru_detail&op=bank_ru&nama_bank=bri&curr=idr', '2017-04-26 06:22:58'),
(6, 'superuser', '/ims99/dirut/?m=bank_ru&op=bank_ru', '2017-04-26 06:23:01'),
(7, 'superuser', '/ims99/dirut/?m=bank_ru_main&op=bank_ru', '2017-04-26 06:23:06'),
(8, 'superuser', '/ims99/dirut/?m=bank_ru&op=bank_ru', '2017-04-26 06:23:08'),
(9, 'superuser', '/ims99/dirut/?m=ins_ru&op=ins_ru', '2017-04-26 06:23:19'),
(10, 'superuser', '/ims99/dirut/?m=ins_ru_add&op=ins_ru', '2017-04-26 06:23:22'),
(11, 'superuser', '/ims99/dirut/?m=bank_fm&op=bank_fm', '2017-04-26 06:23:41'),
(12, 'superuser', '/ims99/dirut/?m=bank_fm_detail&nama_bank=bca&curr=idr', '2017-04-26 06:23:45'),
(13, 'superuser', '/ims99/dirut/?m=bank_fm_add', '2017-04-26 06:23:46'),
(14, 'superuser', '/ims99/dirut/?m=bank_fm_add', '2017-04-26 06:23:58'),
(15, 'superuser', '/ims99/dirut/?m=bank_fm_main', '2017-04-26 06:24:00'),
(16, 'superuser', '/ims99/dirut/?m=bank_fm_edit&id=1', '2017-04-26 06:24:03'),
(17, 'superuser', '/ims99/dirut/?m=bank_fm_main', '2017-04-26 06:24:05'),
(18, 'superuser', '/ims99/dirut/?m=bank_fm', '2017-04-26 06:24:06'),
(19, 'superuser', '/ims99/dirut/?m=bank_fm_detail&nama_bank=bca&curr=idr', '2017-04-26 06:24:09'),
(20, 'superuser', '/ims99/dirut/?m=bank_fm', '2017-04-26 06:24:12'),
(21, 'superuser', '/ims99/dirut/?m=bank_fm_detail&nama_bank=bca&curr=idr', '2017-04-26 06:24:13'),
(22, 'superuser', '/ims99/dirut/?m=underumr&op=underumr', '2017-04-26 06:28:15'),
(23, 'superuser', '/ims99/dirut/index.php', '2017-05-12 11:34:14'),
(24, 'superuser', '/ims99/dirut/?m=legal_doc&op=assetlegal&a=LEGAL', '2017-05-15 05:12:49'),
(25, 'superuser', '/ims99/dirut/?m=legal_doc&op=assetlegal&a=LEGAL', '2017-05-15 05:12:57'),
(26, 'superuser', '/ims99/dirut/?m=legal_doc&op=assetlegal&a=CAR', '2017-05-15 05:12:58'),
(27, 'superuser', '/ims99/dirut/?m=legal_doc&op=assetlegal&a=MOTORCYCLE', '2017-05-15 05:12:59'),
(28, 'superuser', '/ims99/dirut/?m=legal_doc_add&op=assetlegal&a=1&c_id=2', '2017-05-15 05:13:00'),
(29, 'superuser', '/ims99/dirut/?m=legal_doc&op=assetlegal&a=MOTORCYCLE', '2017-05-15 05:13:02'),
(30, 'superuser', '/ims99/dirut/?m=legal_doc_add&op=assetlegal&a=1&c_id=2', '2017-05-15 05:15:57'),
(31, 'superuser', '/ims99/dirut/?m=legal_doc&op=assetlegal&a=MOTORCYCLE', '2017-05-15 05:15:59'),
(32, 'superuser', '/ims99/dirut/?m=legal_doc&op=assetlegal&a=MOTORCYCLE', '2017-05-15 05:23:06'),
(33, 'superuser', '/ims99/dirut/?m=certi_id&op=assetlegal', '2017-05-15 05:23:10'),
(34, 'superuser', '/ims99/dirut/?m=certi_id_pr&op=assetlegal&a=e', '2017-05-15 05:23:15'),
(35, 'superuser', '/ims99/dirut/?m=certi_id&op=assetlegal', '2017-05-15 05:23:15'),
(36, 'superuser', '/ims99/dirut/?m=legal_doc&op=assetlegal&a=LEGAL', '2017-05-15 05:24:05'),
(37, 'superuser', '/ims99/dirut/?m=legal_doc_add&op=assetlegal&id=1', '2017-05-15 05:36:29'),
(38, 'superuser', '/ims99/dirut/?m=preference&op=preference', '2017-05-15 08:20:17'),
(39, 'superuser', '/ims99/dirut/?m=preference&op=preference&l=1&lo=dif', '2017-05-15 08:20:22'),
(40, 'superuser', '/ims99/dirut/?m=preference&op=preference', '2017-05-15 08:20:23'),
(41, 'superuser', '/ims99/dirut/?m=preference&op=preference&l=1&lo=dirut', '2017-05-15 08:20:26'),
(42, 'superuser', '/ims99/dirut/?m=preference&op=preference', '2017-05-15 08:20:27'),
(43, 'superuser', '/ims99/dirut/?m=preference&op=preference&l=1&lo=dirut', '2017-05-15 08:20:28'),
(44, 'superuser', '/ims99/dirut/?m=preference&op=preference', '2017-05-15 08:20:29'),
(45, 'superuser', '/ims99/dirut/?m=legal_doc&op=assetlegal&a=LEGAL', '2017-05-15 08:22:16'),
(46, 'superuser', '/ims99/dirut/?m=legal_doc&op=assetlegal&a=LEGAL', '2017-05-15 08:22:23'),
(47, 'superuser', '/ims99/dirut/?m=legal_doc&op=assetlegal&a=LEGAL', '2017-05-15 08:22:26'),
(48, 'superuser', '/ims99/dirut/?m=legal_doc_add&op=assetlegal&id=1', '2017-05-15 08:22:28'),
(49, 'superuser', '/ims99/dirut/?m=legal_doc&op=assetlegal&a=LEGAL', '2017-05-15 08:22:30'),
(50, 'superuser', '/ims99/dirut/?m=certi_id&op=assetlegal', '2017-05-15 08:22:35'),
(51, 'superuser', '/ims99/dirut/?m=certi_id_pr&op=assetlegal&a=e', '2017-05-15 08:51:44'),
(52, 'superuser', '/ims99/dirut/?m=certi_id&op=assetlegal', '2017-05-15 08:51:44'),
(53, 'superuser', '/ims99/dirut/?m=certi_id_pr&op=assetlegal&a=d&certificate_edit_id=2', '2017-05-15 08:51:51'),
(54, 'superuser', '/ims99/dirut/?m=certi_id&op=assetlegal', '2017-05-15 08:51:51'),
(55, 'superuser', '/ims99/dirut/?m=bank_fm&op=bank_fm', '2017-05-17 09:56:43'),
(56, 'superuser', '/ims99/dirut/?m=bank_fm_detail&nama_bank=bri&curr=idr', '2017-05-17 09:56:53'),
(57, 'superuser', '/ims99/dirut/?m=bank_fm_add', '2017-05-17 09:56:54'),
(58, 'superuser', '/ims99/dirut/?m=bank_fm_add', '2017-05-17 09:57:03'),
(59, 'superuser', '/ims99/dirut/?m=bank_fm_main', '2017-05-17 09:57:05'),
(60, 'superuser', '/ims99/dirut/?m=bank_fm_main&del&id=1', '2017-05-17 09:57:28'),
(61, 'superuser', '/ims99/dirut/?m=bank_fm_main', '2017-05-17 09:57:30'),
(62, 'superuser', '/ims99/dirut/?m=bank_fm_main', '2017-05-17 10:14:00'),
(63, 'superuser', '/ims99/dirut/?m=bank_fm_add', '2017-05-17 10:14:01'),
(64, 'superuser', '/ims99/dirut/?m=bank_fm_detail&nama_bank=bri&curr=idr', '2017-05-17 10:14:02'),
(65, 'superuser', '/ims99/dirut/?m=bank_fm&op=bank_fm', '2017-05-17 10:14:02'),
(66, 'superuser', '/ims99/dirut/?m=bank_ru&op=bank_ru', '2017-05-22 02:57:34');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(6) NOT NULL,
  `position` int(3) NOT NULL,
  `stage` int(2) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `href` varchar(200) DEFAULT NULL,
  `level` int(3) NOT NULL,
  `division` varchar(200) DEFAULT NULL,
  `lock` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `position`, `stage`, `name`, `parent`, `href`, `level`, `division`, `lock`) VALUES
(111, 1, 1, 'Employee Join', NULL, '?m=emp_join&op=emp_join', 90, NULL, NULL),
(32, 3, 1, 'Preference', NULL, '?m=preference&op=preference', 90, NULL, NULL),
(110, 99, 1, 'BOD Evaluation', NULL, '?m=appraisal_main&op=appraisal_main', 90, NULL, NULL),
(85, 99, 1, 'Reimburse Report', NULL, '?m=reimburse_recap&t=Reimburse Report&op=reimburse_recap', 90, NULL, NULL),
(46, 96, 2, 'Edit Quantities', 84, '?m=itemchange&op=assetprev', 90, NULL, NULL),
(83, 96, 2, 'Item Delete', 84, '?m=itemdelete&op=assetprev', 90, NULL, NULL),
(84, 96, 5, 'Asset Previleges', NULL, 'op=assetprev', 90, NULL, NULL),
(95, 60, 1, 'Salary List', NULL, '?m=allsalary&op=allsalary', 70, NULL, NULL),
(96, 5, 2, 'Asset Vehicle', 103, '?m=certificate&op=assetlegal', 40, NULL, NULL),
(97, 1, 2, 'Asset Editor', 103, '?m=certi_id&op=assetlegal', 40, NULL, NULL),
(98, 7, 2, 'Asset Property', 103, '?m=asset_property&op=assetlegal', 40, NULL, NULL),
(99, 8, 2, 'Legal Document', 103, '?m=legal_doc&op=assetlegal&a=LEGAL', 40, NULL, NULL),
(102, 50, 1, 'Yearly Employees', NULL, '?m=workyear&op=workyear', 80, NULL, NULL),
(103, 74, 1, 'Assets & Legal', NULL, 'op=assetlegal', 40, NULL, NULL),
(104, 60, 1, 'Documentary', NULL, '?m=video&op=video', 40, NULL, NULL),
(105, 72, 1, 'PO & WO Validation Code', NULL, '?m=paper_permit&op=paper_permit', 90, NULL, NULL),
(106, 85, 1, 'Project Cost', NULL, '?m=profit_loss&t=Project Cost&op=profit_loss', 90, NULL, NULL),
(107, 71, 1, 'Recent Price', NULL, '?m=po_trace&op=po_trace', 0, NULL, NULL),
(108, 10, 1, 'Salary To Do', NULL, '?m=underumr&op=underumr', 90, NULL, NULL),
(116, 1, 1, 'Hospital File', NULL, '?m=scan_show&op=scan_show', 90, NULL, NULL),
(117, 1, 1, 'Bank RU', NULL, '?m=bank_ru&op=bank_ru', 90, NULL, NULL),
(118, 1, 1, 'Insurance RU', NULL, '?m=ins_ru&op=ins_ru', 90, NULL, NULL),
(119, 1, 1, 'Bank FM', NULL, '?m=bank_fm&op=bank_fm', 90, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doc_counter`
--
ALTER TABLE `doc_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_view`
--
ALTER TABLE `log_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_counter`
--
ALTER TABLE `doc_counter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_view`
--
ALTER TABLE `log_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;--
-- Database: `ims_doc`
--
CREATE DATABASE IF NOT EXISTS `ims_doc` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims_doc`;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(11) NOT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `content` text,
  `created_by` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu_item`
--

CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL,
  `title` varchar(75) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `treeview_items`
--

CREATE TABLE `treeview_items` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `parent_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_item`
--
ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `treeview_items`
--
ALTER TABLE `treeview_items`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `treeview_items`
--
ALTER TABLE `treeview_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;--
-- Database: `ims_driver`
--
CREATE DATABASE IF NOT EXISTS `ims_driver` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims_driver`;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(1) NOT NULL,
  `username` varchar(200) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `opt_name` varchar(50) NOT NULL,
  `opt_value` varchar(50) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_counter`
--

CREATE TABLE `doc_counter` (
  `doc_code` varchar(10) NOT NULL,
  `last_num` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='menyimpan informasi nomor dokumen terakhir';

-- --------------------------------------------------------

--
-- Table structure for table `log_view`
--

CREATE TABLE `log_view` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `location` varchar(400) NOT NULL,
  `log_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(6) NOT NULL,
  `position` int(3) NOT NULL,
  `stage` int(2) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `href` varchar(200) DEFAULT NULL,
  `level` int(3) NOT NULL,
  `division` varchar(200) DEFAULT NULL,
  `lock` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `opt_name` (`opt_name`);

--
-- Indexes for table `doc_counter`
--
ALTER TABLE `doc_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_view`
--
ALTER TABLE `log_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_counter`
--
ALTER TABLE `doc_counter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_view`
--
ALTER TABLE `log_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;--
-- Database: `ims_finance`
--
CREATE DATABASE IF NOT EXISTS `ims_finance` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims_finance`;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(1) NOT NULL,
  `username` varchar(200) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bank_loan`
--

CREATE TABLE `bank_loan` (
  `id` int(5) NOT NULL,
  `bank` varchar(200) NOT NULL,
  `value` decimal(20,2) NOT NULL,
  `bunga` decimal(5,2) NOT NULL,
  `start` date NOT NULL,
  `period` int(5) NOT NULL,
  `type` varchar(20) NOT NULL,
  `currency` varchar(200) NOT NULL,
  `cicil_start` int(3) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bank_loan_detail`
--

CREATE TABLE `bank_loan_detail` (
  `id` int(5) NOT NULL,
  `id_loan` int(5) NOT NULL,
  `cicilan` decimal(20,2) NOT NULL,
  `bunga_rate` decimal(5,2) NOT NULL,
  `bunga` decimal(20,2) NOT NULL,
  `status` varchar(10) NOT NULL,
  `n_cicil` int(4) DEFAULT NULL,
  `plan_date` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bank_loan_int`
--

CREATE TABLE `bank_loan_int` (
  `id` int(9) NOT NULL,
  `id_loan` int(9) NOT NULL,
  `id_entry` int(9) NOT NULL,
  `change_date` date NOT NULL,
  `init_rate` decimal(5,2) NOT NULL,
  `change_rate` decimal(5,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bcr`
--

CREATE TABLE `bcr` (
  `id` int(10) NOT NULL,
  `no` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `subject` varchar(200) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `currency` varchar(20) DEFAULT NULL,
  `requestor` varchar(200) DEFAULT NULL,
  `approver` varchar(200) DEFAULT NULL,
  `finance` varchar(200) NOT NULL,
  `status` varchar(200) DEFAULT NULL,
  `divs` varchar(200) DEFAULT NULL,
  `date_app` date DEFAULT NULL,
  `date_m_rec` date DEFAULT NULL,
  `date_bal_app` date DEFAULT NULL,
  `date_bal_rec` date DEFAULT NULL,
  `app1` text,
  `app2` text,
  `date_dir` date DEFAULT NULL,
  `app3` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bcr_config`
--

CREATE TABLE `bcr_config` (
  `dir` int(1) NOT NULL,
  `dirut` int(1) NOT NULL,
  `dif` int(1) NOT NULL,
  `ops` int(1) NOT NULL,
  `mar` int(1) NOT NULL,
  `pro` int(1) NOT NULL,
  `fin` int(1) NOT NULL,
  `it` int(1) NOT NULL,
  `ast` int(1) NOT NULL,
  `bin` int(1) NOT NULL,
  `cil` int(1) NOT NULL,
  `dops` int(1) NOT NULL,
  `gm` int(11) NOT NULL,
  `hrd` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bcr_detail`
--

CREATE TABLE `bcr_detail` (
  `id` int(10) NOT NULL,
  `bcr_id` int(10) NOT NULL,
  `descr` text,
  `amount` decimal(20,2) DEFAULT NULL,
  `remark` text,
  `actual` decimal(20,2) DEFAULT NULL,
  `projects` varchar(50) DEFAULT NULL,
  `category` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cashbond`
--

CREATE TABLE `cashbond` (
  `id` int(11) NOT NULL,
  `subject` varchar(400) NOT NULL,
  `input_date` date NOT NULL,
  `m_approve` varchar(100) DEFAULT NULL,
  `m_approve_time` date DEFAULT NULL,
  `approved_by` varchar(200) DEFAULT NULL,
  `approved_time` date DEFAULT NULL,
  `currency` varchar(8) NOT NULL,
  `sisa` decimal(15,2) DEFAULT NULL,
  `division` varchar(80) DEFAULT NULL,
  `done` date DEFAULT NULL,
  `user` varchar(100) NOT NULL DEFAULT 'open',
  `project` varchar(40) DEFAULT NULL,
  `from` varchar(200) DEFAULT NULL,
  `vehicle` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cashbond`
--

INSERT INTO `cashbond` (`id`, `subject`, `input_date`, `m_approve`, `m_approve_time`, `approved_by`, `approved_time`, `currency`, `sisa`, `division`, `done`, `user`, `project`, `from`, `vehicle`) VALUES
(4, 'wedwed', '2017-05-16', NULL, NULL, NULL, NULL, 'USD', NULL, 'hrd', NULL, 'superuser', '342', NULL, 0),
(3, 'fgf', '2017-04-18', NULL, NULL, NULL, NULL, 'USD', NULL, 'driver', NULL, 'superuser', '342', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cashbond_detail`
--

CREATE TABLE `cashbond_detail` (
  `id` int(11) NOT NULL,
  `id_cashbond` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `no_nota` varchar(400) NOT NULL,
  `deskripsi` text NOT NULL,
  `cashin` decimal(15,2) NOT NULL,
  `cashout` decimal(15,2) NOT NULL,
  `category` varchar(20) DEFAULT NULL,
  `source_string` varchar(100) DEFAULT NULL,
  `source_int` int(4) DEFAULT NULL,
  `fuel_type` varchar(300) DEFAULT NULL,
  `fuel_amount` decimal(15,2) DEFAULT NULL,
  `fuel_km` decimal(15,2) DEFAULT NULL,
  `fuel_id` int(11) DEFAULT NULL,
  `fuel_by` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cashbond_detail`
--

INSERT INTO `cashbond_detail` (`id`, `id_cashbond`, `tanggal`, `no_nota`, `deskripsi`, `cashin`, `cashout`, `category`, `source_string`, `source_int`, `fuel_type`, `fuel_amount`, `fuel_km`, `fuel_id`, `fuel_by`) VALUES
(7, 3, '2017-05-26', 'asCAC', 'CAASCC', '0.00', '23424.00', 'transportasi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 3, '2017-05-19', 'SDF', 'SDF', '0.00', '0.00', 'others', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `opt_name` varchar(50) NOT NULL,
  `opt_value` varchar(50) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_counter`
--

CREATE TABLE `doc_counter` (
  `doc_code` varchar(10) NOT NULL,
  `last_num` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='menyimpan informasi nomor dokumen terakhir';

-- --------------------------------------------------------

--
-- Table structure for table `dps_brankas`
--

CREATE TABLE `dps_brankas` (
  `id` int(11) NOT NULL,
  `dps_name` varchar(200) NOT NULL,
  `dps_owner` varchar(200) NOT NULL,
  `dps_users` varchar(500) DEFAULT NULL,
  `dps_status` int(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dps_history`
--

CREATE TABLE `dps_history` (
  `id` int(11) NOT NULL,
  `id_items` int(11) NOT NULL,
  `remark` text NOT NULL,
  `user` varchar(50) NOT NULL,
  `date_input` date NOT NULL,
  `amount` decimal(16,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dps_items`
--

CREATE TABLE `dps_items` (
  `id` int(11) NOT NULL,
  `brankas_id` int(11) NOT NULL,
  `item_name` text NOT NULL,
  `curr_code` varchar(5) DEFAULT NULL,
  `amount` decimal(16,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forged_proll`
--

CREATE TABLE `forged_proll` (
  `id` int(11) NOT NULL,
  `emp_name` varchar(200) DEFAULT NULL,
  `emp_id` varchar(200) DEFAULT NULL,
  `emp_position` varchar(200) DEFAULT NULL,
  `salary` decimal(15,2) DEFAULT NULL,
  `transport` decimal(15,2) DEFAULT NULL,
  `meal` decimal(15,2) DEFAULT NULL,
  `house` decimal(15,2) DEFAULT NULL,
  `pension` decimal(15,2) DEFAULT NULL,
  `health` decimal(15,2) DEFAULT NULL,
  `health_insurance` decimal(15,2) DEFAULT NULL,
  `regional` decimal(15,2) DEFAULT NULL,
  `jamsostek` decimal(15,2) DEFAULT NULL,
  `overtime` decimal(15,2) DEFAULT NULL,
  `voucher` decimal(15,2) DEFAULT NULL,
  `allowance_office` decimal(15,2) DEFAULT NULL,
  `slip_month` varchar(3) DEFAULT NULL,
  `slip_year` varchar(4) DEFAULT NULL,
  `creator` varchar(200) DEFAULT NULL,
  `purpose` text,
  `forge_date` date DEFAULT NULL,
  `thr` decimal(15,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `instant_sp`
--

CREATE TABLE `instant_sp` (
  `id` int(11) NOT NULL,
  `type_instant` varchar(10) NOT NULL,
  `project_instant` varchar(100) NOT NULL,
  `date_instant` date NOT NULL,
  `document_instant` varchar(100) NOT NULL,
  `amount_instant` decimal(20,2) NOT NULL,
  `description_instant` text NOT NULL,
  `paid` int(3) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instant_sp`
--

INSERT INTO `instant_sp` (`id`, `type_instant`, `project_instant`, `date_instant`, `document_instant`, `amount_instant`, `description_instant`, `paid`) VALUES
(1, '0', '', '2017-06-09', '1', '0.00', '<p>sdcsdc</p>', 0);

-- --------------------------------------------------------

--
-- Table structure for table `inv_out`
--

CREATE TABLE `inv_out` (
  `id_inv` int(11) NOT NULL,
  `no` varchar(60) NOT NULL,
  `title` varchar(400) NOT NULL,
  `start_periode` date DEFAULT NULL,
  `end_periode` date DEFAULT NULL,
  `company` text,
  `value` decimal(15,2) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `input_by` varchar(40) NOT NULL,
  `input_date` date NOT NULL,
  `prj_code` varchar(100) DEFAULT NULL,
  `valueday` decimal(15,2) DEFAULT NULL,
  `prefix` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inv_out_detail`
--

CREATE TABLE `inv_out_detail` (
  `id_inv_d` int(11) NOT NULL,
  `id_inv` int(11) NOT NULL,
  `activity` varchar(400) NOT NULL,
  `date` date NOT NULL,
  `no_inv` varchar(40) NOT NULL,
  `value_d` decimal(15,2) NOT NULL,
  `input_by` varchar(20) NOT NULL,
  `input_time` date NOT NULL,
  `ppn` varchar(10) DEFAULT NULL,
  `pph_23` varchar(10) DEFAULT NULL,
  `ppn_dpp` varchar(10) DEFAULT NULL,
  `pph_4` varchar(10) DEFAULT NULL,
  `pph_22` varchar(10) DEFAULT NULL,
  `pph_29` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inv_out_print`
--

CREATE TABLE `inv_out_print` (
  `id` int(11) NOT NULL,
  `id_inv_out_detail` int(11) DEFAULT NULL,
  `description` text,
  `unit_price` float DEFAULT NULL,
  `qty` float DEFAULT NULL,
  `uom` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_in`
--

CREATE TABLE `invoice_in` (
  `id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `paper_type` varchar(10) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `amount_left` decimal(20,2) NOT NULL,
  `pay_date` date NOT NULL,
  `app_date` date NOT NULL,
  `status` varchar(200) NOT NULL,
  `project` varchar(400) DEFAULT NULL,
  `plan_date` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_in_pay`
--

CREATE TABLE `invoice_in_pay` (
  `id` int(5) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `pay_num` int(3) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `pay_date` date NOT NULL,
  `description` text NOT NULL,
  `paid` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `leasing`
--

CREATE TABLE `leasing` (
  `id` int(5) NOT NULL,
  `subject` varchar(400) NOT NULL,
  `value` decimal(20,2) NOT NULL,
  `bunga` decimal(5,2) NOT NULL,
  `start` date NOT NULL,
  `period` int(5) NOT NULL,
  `vendor` varchar(200) NOT NULL,
  `currency` varchar(200) NOT NULL,
  `cicil_start` int(3) NOT NULL,
  `dp` decimal(20,2) NOT NULL,
  `adm` decimal(20,2) NOT NULL,
  `insurance` decimal(20,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `leasing_detail`
--

CREATE TABLE `leasing_detail` (
  `id` int(5) NOT NULL,
  `id_leasing` int(5) NOT NULL,
  `cicilan` decimal(20,2) NOT NULL,
  `bunga_rate` decimal(5,2) NOT NULL,
  `bunga` decimal(20,2) NOT NULL,
  `status` varchar(10) NOT NULL,
  `n_cicil` int(4) DEFAULT NULL,
  `plan_date` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `leasing_int`
--

CREATE TABLE `leasing_int` (
  `id` int(9) NOT NULL,
  `id_leasing` int(9) NOT NULL,
  `id_entry` int(9) NOT NULL,
  `change_date` date NOT NULL,
  `init_rate` decimal(5,2) NOT NULL,
  `change_rate` decimal(5,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_view`
--

CREATE TABLE `log_view` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `location` varchar(400) NOT NULL,
  `log_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_view`
--

INSERT INTO `log_view` (`id`, `user`, `location`, `log_time`) VALUES
(1, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 05:32:09'),
(2, 'superuser', '/ims99/finance/index2.php?m=cashbond_edit&op=cb', '2017-04-17 05:32:15'),
(3, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 05:32:17'),
(4, 'superuser', '/ims99/finance/index2.php?m=cashbond_edit&op=cb', '2017-04-17 05:40:27'),
(5, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 05:51:43'),
(6, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 05:59:50'),
(7, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 06:16:07'),
(8, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 06:56:23'),
(9, 'superuser', '/ims99/finance/index2.php?m=cashbond_edit&op=cb', '2017-04-17 08:06:49'),
(10, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 08:07:49'),
(11, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=1&who=user', '2017-04-17 08:07:51'),
(12, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 08:07:52'),
(13, 'superuser', '/ims99/finance/index2.php?m=cashbond_edit&op=cb', '2017-04-17 08:07:55'),
(14, 'superuser', '/ims99/finance/index2.php?m=cashbond_add&op=cb', '2017-04-17 08:07:58'),
(15, 'superuser', '/ims99/finance/index2.php?m=cashbond_edit&op=cb', '2017-04-17 08:08:03'),
(16, 'superuser', '/ims99/finance/index2.php?m=cashbond_edit&op=cb', '2017-04-17 08:36:04'),
(17, 'superuser', '/ims99/finance/index2.php?m=cashbond_edit&op=cb', '2017-04-17 08:36:51'),
(18, 'superuser', '/ims99/finance/index2.php?m=cashbond_edit&op=cb', '2017-04-17 08:36:58'),
(19, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 08:36:58'),
(20, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 08:44:39'),
(21, 'superuser', '/ims99/finance/index2.php?m=cashbond_edit&op=cb', '2017-04-17 08:44:41'),
(22, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 08:44:43'),
(23, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 08:50:22'),
(24, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=1', '2017-04-17 09:00:12'),
(25, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=1&cashtype=cashout&cat=consumable', '2017-04-17 09:00:19'),
(26, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=1', '2017-04-17 09:00:21'),
(27, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 09:01:07'),
(28, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb&del=1&q=', '2017-04-17 09:01:09'),
(29, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 09:03:02'),
(30, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashin&curr=EURO', '2017-04-17 09:04:24'),
(31, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashin&curr=EURO', '2017-04-17 09:04:30'),
(32, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 09:04:31'),
(33, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&id=1&ii=2', '2017-04-17 09:04:33'),
(34, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&id=1&ii=2', '2017-04-17 09:04:39'),
(35, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 09:04:39'),
(36, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashin&curr=EURO', '2017-04-17 09:04:44'),
(37, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashin&curr=EURO', '2017-04-17 09:04:50'),
(38, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 09:04:50'),
(39, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&del=2&q=2&bank=0&sum=324.00', '2017-04-17 09:06:17'),
(40, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 09:06:17'),
(41, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&del=1&q=2&bank=0&sum=234.00', '2017-04-17 09:16:33'),
(42, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 09:16:33'),
(43, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashout&cat=consumable', '2017-04-17 09:21:24'),
(44, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashout&cat=consumable', '2017-04-17 09:21:30'),
(45, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 09:21:30'),
(46, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashout&cat=consumable', '2017-04-17 09:21:34'),
(47, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashout&cat=consumable', '2017-04-17 09:21:42'),
(48, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 09:21:42'),
(49, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&del=4&q=2', '2017-04-17 09:21:44'),
(50, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 09:21:44'),
(51, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashin&curr=EURO', '2017-04-17 09:36:45'),
(52, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 09:36:48'),
(53, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashin&curr=EURO', '2017-04-17 09:36:52'),
(54, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 09:36:56'),
(55, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashin&curr=EURO', '2017-04-17 09:37:28'),
(56, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 09:40:26'),
(57, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashout&cat=transportasi', '2017-04-17 09:41:09'),
(58, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashout&cat=transportasi', '2017-04-17 09:57:50'),
(59, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 10:24:53'),
(60, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashout&cat=transportasi', '2017-04-17 10:24:54'),
(61, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashout&cat=transportasi', '2017-04-17 10:25:00'),
(62, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 10:25:00'),
(63, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashout&cat=transportasi', '2017-04-17 10:41:57'),
(64, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 10:41:58'),
(65, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 10:41:59'),
(66, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashout&cat=consumable', '2017-04-17 10:42:01'),
(67, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 10:42:01'),
(68, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashout&cat=consumable', '2017-04-17 10:42:02'),
(69, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 10:42:03'),
(70, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 10:42:04'),
(71, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 10:42:04'),
(72, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashin&curr=EURO', '2017-04-17 10:42:05'),
(73, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 10:42:06'),
(74, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&id=1&ii=2', '2017-04-17 10:42:06'),
(75, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 10:42:07'),
(76, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=2&cashtype=cashin&curr=EURO', '2017-04-17 10:42:07'),
(77, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=2', '2017-04-17 10:42:07'),
(78, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb&del=1&q=', '2017-04-17 10:42:07'),
(79, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 10:42:07'),
(80, 'superuser', '/ims99/finance/index2.php?m=cashbond_edit&op=cb', '2017-04-17 10:42:08'),
(81, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 10:42:08'),
(82, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 10:52:02'),
(83, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 10:54:37'),
(84, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-17 10:58:37'),
(85, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-18 02:43:59'),
(86, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-18 02:44:03'),
(87, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-18 02:47:19'),
(88, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-18 02:47:47'),
(89, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=2&who=user', '2017-04-18 02:47:49'),
(90, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-18 02:50:15'),
(91, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-18 06:50:05'),
(92, 'superuser', '/ims99/finance/index2.php?m=cashbond_edit&op=cb', '2017-04-18 06:50:07'),
(93, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-18 06:50:10'),
(94, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb&del=2&q=', '2017-04-18 06:50:11'),
(95, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-18 09:05:28'),
(96, 'superuser', '/ims99/finance/index2.php?m=cashbond_edit&op=cb', '2017-04-18 09:05:31'),
(97, 'superuser', '/ims99/finance/index2.php?m=cashbond_edit&op=cb', '2017-04-18 09:05:38'),
(98, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-18 09:05:38'),
(99, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:06:56'),
(100, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:07:48'),
(101, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:08:42'),
(102, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:09:06'),
(103, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:09:24'),
(104, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:09:46'),
(105, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:10:18'),
(106, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:10:43'),
(107, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:11:17'),
(108, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:11:25'),
(109, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:11:32'),
(110, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:11:49'),
(111, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:11:58'),
(112, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:12:07'),
(113, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:12:17'),
(114, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:12:48'),
(115, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:13:27'),
(116, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:14:26'),
(117, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:15:05'),
(118, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:15:41'),
(119, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:15:51'),
(120, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:16:02'),
(121, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:16:08'),
(122, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:16:18'),
(123, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:16:30'),
(124, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:17:33'),
(125, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:17:47'),
(126, 'superuser', '/ims99/finance/index2.php?m=cashbond_print_pdf&op=cb&app=3&who=user', '2017-04-18 09:18:37'),
(127, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:18:43'),
(128, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=3&who=user', '2017-04-18 09:20:42'),
(129, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-18 09:20:43'),
(130, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-18 09:20:51'),
(131, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-20 04:23:34'),
(132, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-04-21 03:13:49'),
(133, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-05-15 08:46:08'),
(134, 'superuser', '/ims99/finance/index2.php?m=cashbond_edit&op=cb', '2017-05-15 08:46:12'),
(135, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-05-15 08:46:14'),
(136, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=3', '2017-05-15 08:47:45'),
(137, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=3&cashtype=cashin&curr=USD', '2017-05-15 08:51:23'),
(138, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-05-15 09:02:09'),
(139, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=3', '2017-05-15 09:02:15'),
(140, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-05-15 09:02:19'),
(141, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-05-16 04:31:23'),
(142, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=3', '2017-05-16 04:31:26'),
(143, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-05-16 04:31:29'),
(144, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-05-16 04:44:10'),
(145, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-05-16 04:44:13'),
(146, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-05-16 05:06:19'),
(147, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 05:06:21'),
(148, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-05-16 05:06:23'),
(149, 'superuser', '/ims99/finance/index2.php?m=cashbond&rp=bank&op=cb', '2017-05-16 05:06:25'),
(150, 'superuser', '/ims99/finance/index2.php?m=reimburse&rp=bank&op=cb', '2017-05-16 05:06:45'),
(151, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 05:07:03'),
(152, 'superuser', '/ims99/finance/index2.php?m=reimburse&rp=bank&op=cb', '2017-05-16 05:07:05'),
(153, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 05:07:07'),
(154, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 05:14:26'),
(155, 'superuser', '/ims99/finance/index2.php?m=reimburse_edit&op=cb', '2017-05-16 05:45:13'),
(156, 'superuser', '/ims99/finance/index2.php?m=reimburse_edit&op=cb', '2017-05-16 05:45:22'),
(157, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 05:45:23'),
(158, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=1', '2017-05-16 05:45:25'),
(159, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 05:45:38'),
(160, 'superuser', '/ims99/finance/index2.php?m=reimburse_print&op=cb&app=1&who=user', '2017-05-16 05:45:41'),
(161, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 05:45:43'),
(162, 'superuser', '/ims99/finance/index2.php?m=reimburse_print&op=cb&app=1&who=manager', '2017-05-16 05:45:44'),
(163, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 05:45:47'),
(164, 'superuser', '/ims99/finance/index2.php?m=reimburse_edit&op=cb&id=1', '2017-05-16 05:45:48'),
(165, 'superuser', '/ims99/finance/index2.php?m=reimburse_edit&op=cb&id=1', '2017-05-16 05:45:51'),
(166, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 05:45:51'),
(167, 'superuser', '/ims99/finance/index2.php?m=reimburse_edit&op=cb', '2017-05-16 05:46:05'),
(168, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 05:46:08'),
(169, 'superuser', '/ims99/finance/index2.php?m=reimburse_edit&op=cb', '2017-05-16 05:46:10'),
(170, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 06:07:32'),
(171, 'superuser', '/ims99/finance/index2.php?m=reimburse_print&op=cb&print=1', '2017-05-16 06:10:21'),
(172, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 06:10:23'),
(173, 'superuser', '/ims99/finance/index2.php?m=reimburse_edit&op=cb&id=1', '2017-05-16 06:14:22'),
(174, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 06:14:23'),
(175, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb&del=1&q=', '2017-05-16 06:14:29'),
(176, 'superuser', '/ims99/finance/index2.php?m=reimburse_edit&op=cb', '2017-05-16 06:14:31'),
(177, 'superuser', '/ims99/finance/index2.php?m=reimburse_edit&op=cb', '2017-05-16 06:14:37'),
(178, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 06:14:37'),
(179, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=2', '2017-05-16 06:14:39'),
(180, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 06:14:40'),
(181, 'superuser', '/ims99/finance/index2.php?m=reimburse_print&op=cb&print=2', '2017-05-16 06:14:47'),
(182, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 06:14:49'),
(183, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=2', '2017-05-16 06:14:50'),
(184, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 06:14:52'),
(185, 'superuser', '/ims99/finance/index2.php?m=reimburse_edit&op=cb&id=2', '2017-05-16 06:16:11'),
(186, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 06:22:16'),
(187, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=2', '2017-05-16 06:22:18'),
(188, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 06:22:37'),
(189, 'superuser', '/ims99/finance/index2.php?m=reimburse_edit&op=cb', '2017-05-16 06:22:39'),
(190, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 06:22:40'),
(191, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=2', '2017-05-16 06:44:36'),
(192, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail_edit&op=cb&ii=2&cashtype=cashin&curr=IDR', '2017-05-16 06:44:38'),
(193, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=2', '2017-05-16 06:44:39'),
(194, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail_edit&op=cb&ii=2&cashtype=cashout&cat=consumable', '2017-05-16 06:44:41'),
(195, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=2', '2017-05-16 06:44:42'),
(196, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-05-16 08:21:25'),
(197, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=3', '2017-05-16 08:21:32'),
(198, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=3&cashtype=cashout&cat=others', '2017-05-16 08:21:45'),
(199, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=3&cashtype=cashout&cat=others', '2017-05-16 08:21:52'),
(200, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=3', '2017-05-16 08:21:52'),
(201, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=3&cashtype=cashout&cat=transportasi', '2017-05-16 08:21:59'),
(202, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=3&cashtype=cashout&cat=transportasi', '2017-05-16 08:22:05'),
(203, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail&op=cb&id=3', '2017-05-16 08:22:05'),
(204, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=3&cashtype=cashin&curr=USD', '2017-05-16 08:25:08'),
(205, 'superuser', '/ims99/finance/index2.php?m=cashbond_detail_edit&op=cb&ii=3&cashtype=cashin&curr=USD', '2017-05-16 08:29:29'),
(206, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 08:29:50'),
(207, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=2', '2017-05-16 08:29:54'),
(208, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail_edit&op=cb&ii=2&cashtype=cashin&curr=IDR', '2017-05-16 08:30:00'),
(209, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail_edit&op=cb&ii=2&cashtype=cashin&curr=IDR', '2017-05-16 08:31:10'),
(210, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=2', '2017-05-16 09:00:05'),
(211, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail_edit&op=cb&ii=2&cashtype=cashin&curr=IDR', '2017-05-16 09:00:06'),
(212, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail_edit&op=cb&ii=2&cashtype=cashin&curr=IDR', '2017-05-16 09:00:16'),
(213, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=2', '2017-05-16 09:00:16'),
(214, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=2', '2017-05-16 09:06:24'),
(215, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=2', '2017-05-16 09:06:26'),
(216, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&del=1&q=2&bank=&sum=2323.00', '2017-05-16 09:06:28'),
(217, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=2', '2017-05-16 09:06:28'),
(218, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail_edit&op=cb&ii=2&cashtype=cashin&curr=IDR', '2017-05-16 09:06:30'),
(219, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail_edit&op=cb&ii=2&cashtype=cashin&curr=IDR', '2017-05-16 09:06:41'),
(220, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=2', '2017-05-16 09:06:42'),
(221, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail_edit&op=cb&id=2&ii=2', '2017-05-16 09:06:43'),
(222, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail_edit&op=cb&id=2&ii=2', '2017-05-16 09:06:51'),
(223, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=2', '2017-05-16 09:06:51'),
(224, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail_edit&op=cb&id=2&ii=2', '2017-05-16 09:07:11'),
(225, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail_edit&op=cb&id=2&ii=2', '2017-05-16 09:09:04'),
(226, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=2', '2017-05-16 09:21:54'),
(227, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 09:30:06'),
(228, 'superuser', '/ims99/finance/index2.php?m=reimburse_print&op=cb&app=2&who=user', '2017-05-16 09:30:08'),
(229, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 09:30:11'),
(230, 'superuser', '/ims99/finance/index2.php?m=reimburse_print&op=cb&app=2&who=manager', '2017-05-16 09:30:13'),
(231, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 09:30:14'),
(232, 'superuser', '/ims99/finance/index2.php?m=reimburse_edit&op=cb&id=2', '2017-05-16 09:30:16'),
(233, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 09:30:17'),
(234, 'superuser', '/ims99/finance/index2.php?m=reimburse_print&op=cb&app=2&who=user', '2017-05-16 09:30:26'),
(235, 'superuser', '/ims99/finance/index2.php?m=reimburse_print&op=cb&app=2&who=user', '2017-05-16 09:32:49'),
(236, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 09:32:52'),
(237, 'superuser', '/ims99/finance/index2.php?m=reimburse_print&op=cb&app=2&who=user', '2017-05-16 09:32:54'),
(238, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 09:32:58'),
(239, 'superuser', '/ims99/finance/index2.php?m=reimburse_print&op=cb&app=2&who=manager', '2017-05-16 09:32:59'),
(240, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 10:24:54'),
(241, 'superuser', '/ims99/finance/index2.php?m=reimburse&rp=bank&op=cb', '2017-05-16 10:24:56'),
(242, 'superuser', '/ims99/finance/index2.php?m=reimburse&op=cb', '2017-05-16 10:25:00'),
(243, 'superuser', '/ims99/finance/index2.php?m=reimburse_print&op=cb&app=2&who=user', '2017-05-16 10:25:01'),
(244, 'superuser', '/ims99/finance/index2.php?m=reimburse_print', '2017-05-16 10:25:05'),
(245, 'superuser', '/ims99/finance/index2.php?m=reimburse', '2017-05-16 10:25:05'),
(246, 'superuser', '/ims99/finance/index2.php?m=reimburse_print&op=cb&app=2&who=manager', '2017-05-16 10:25:06'),
(247, 'superuser', '/ims99/finance/index2.php?m=reimburse_print', '2017-05-16 10:25:09'),
(248, 'superuser', '/ims99/finance/index2.php?m=reimburse', '2017-05-16 10:25:10'),
(249, 'superuser', '/ims99/finance/index2.php?m=reimburse_print&op=cb&app=2&who=finance', '2017-05-16 10:25:11'),
(250, 'superuser', '/ims99/finance/index2.php?m=reimburse_print', '2017-05-16 10:25:16'),
(251, 'superuser', '/ims99/finance/index2.php?m=reimburse', '2017-05-16 10:25:16'),
(252, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-05-16 10:25:23'),
(253, 'superuser', '/ims99/finance/index2.php?m=reimburse&rp=bank&op=cb', '2017-05-16 10:25:25'),
(254, 'superuser', '/ims99/finance/index2.php?m=reimburse&rp=bank&op=cb', '2017-05-16 10:28:10'),
(255, 'superuser', '/ims99/finance/index2.php?m=reimburse_detail&op=cb&id=2', '2017-05-16 10:28:12'),
(256, 'superuser', '/ims99/finance/index2.php?m=reimburse&rp=bank&op=cb', '2017-05-16 10:28:16'),
(257, 'superuser', '/ims99/finance/index2.php?m=cashbond&op=cb', '2017-05-16 10:28:39'),
(258, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=4&who=user', '2017-05-16 10:28:41'),
(259, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=4&who=user', '2017-05-16 10:36:06'),
(260, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=4&who=user', '2017-05-16 10:36:09'),
(261, 'superuser', '/ims99/finance/index2.php?m=cashbond_print&op=cb&app=4&who=user', '2017-05-16 10:36:12'),
(262, 'superuser', '/ims99/finance/index2.php?m=paysch_table&op=paysch_table', '2017-05-22 03:00:21'),
(263, 'superuser', '/ims99/finance/index2.php?m=paysch_table&op=paysch_table', '2017-05-22 03:00:26'),
(264, 'superuser', '/ims99/finance/index2.php?m=paysch_table&op=paysch_table', '2017-05-22 03:02:17'),
(265, 'superuser', '/ims99/finance/index2.php?m=paysch_table&op=paysch_table', '2017-05-22 03:02:21'),
(266, 'superuser', '/ims99/finance/index2.php?m=paysch_table&op=paysch_table', '2017-05-22 03:02:39'),
(267, 'superuser', '/ims99/finance/index2.php?m=paysch_table&op=paysch_table&M=2&Y=2017', '2017-05-22 03:07:01');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(6) NOT NULL,
  `position` int(3) NOT NULL,
  `stage` int(2) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `href` varchar(200) DEFAULT NULL,
  `level` int(3) NOT NULL,
  `division` varchar(200) DEFAULT NULL,
  `lock` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `position`, `stage`, `name`, `parent`, `href`, `level`, `division`, `lock`) VALUES
(1, 100, 1, 'Treasury', NULL, '?m=treasure&op=treasure', 0, NULL, NULL),
(2, 95, 1, 'Invoice In', NULL, 'op=invoice_in', 0, NULL, NULL),
(3, 98, 2, 'PO', 2, '?m=invoice_in&t=PO&op=invoice_in', 0, NULL, NULL),
(4, 97, 2, 'WO', 2, '?m=invoice_in&t=WO&op=invoice_in', 0, NULL, NULL),
(5, 50, 2, 'Fix Data SP', 2, NULL, 0, NULL, NULL),
(6, 99, 3, 'PO', 5, '?m=invoice_in&t=PO&r=1&op=invoice_in', 0, NULL, NULL),
(7, 70, 3, 'WO', 5, '?m=invoice_in&t=WO&r=1&op=invoice_in', 0, NULL, NULL),
(8, 99, 1, 'Loan', NULL, '?m=pinjaman&op=pinjaman', 0, NULL, NULL),
(9, 97, 1, 'Leasing', NULL, '?m=leasing&op=leasing', 0, NULL, NULL),
(10, 94, 1, 'Schedule Payment', NULL, '?m=paysch_table&op=paysch_table', 0, NULL, NULL),
(11, 93, 1, '[BR] Budget Request', NULL, 'op=bcr_day', 0, NULL, NULL),
(12, 97, 2, 'Monthly BR', 11, '?m=bcr_day&op=bcr_day', 90, NULL, NULL),
(13, 98, 2, 'Waiting Approval BR', 11, '?m=bcr_day&w=wait&op=bcr_day', 50, NULL, NULL),
(14, 99, 2, 'All BR', 11, '?m=bcr_day&w=all&op=bcr_day', 90, NULL, NULL),
(15, 80, 2, 'Cost Direktur Utama', 11, '?m=bcr&q=dirut&op=bcr_day', 0, 'admin,Secretary', NULL),
(16, 79, 2, 'Cost Sekretaris Dirut', 11, '?m=bcr&q=dir&op=bcr_day', 0, 'admin,Secretary', NULL),
(17, 78, 2, 'Cost Direktur Finance', 11, '?m=bcr&q=dif&op=bcr_day', 0, 'Finance', NULL),
(18, 77, 2, 'Cost Operation', 11, '?m=bcr&q=ops&op=bcr_day', 0, 'Operation', NULL),
(19, 76, 2, 'Cost Direktur Operation', 11, '?m=bcr&q=dops&op=bcr_day', 0, 'Admin,Secretary', NULL),
(20, 75, 2, 'Cost Marketing', 11, '?m=bcr&q=mar&op=bcr_day', 0, 'Marketing', NULL),
(21, 74, 2, 'Cost Procurement', 11, '?m=bcr&q=pro&op=bcr_day', 0, 'Procurement', NULL),
(22, 73, 2, 'Cost Finance', 11, '?m=bcr&q=fin&op=bcr_day', 0, 'Finance', NULL),
(23, 72, 2, 'Cost IT', 11, '?m=bcr&q=it&op=bcr_day', 0, 'IT', NULL),
(24, 71, 2, 'Cost Asset', 11, '?m=bcr&q=ast&op=bcr_day', 0, 'Asset', NULL),
(25, 70, 2, 'Cost WH Bintaro', 11, '?m=bcr&q=bin&op=bcr_day', 0, 'Asset', NULL),
(26, 69, 2, 'Cost WH Cileungsi', 11, '?m=bcr&q=cil&op=bcr_day', 0, 'Asset', NULL),
(27, 92, 1, 'Filing Document', NULL, 'op=fildoc', 0, NULL, NULL),
(28, 99, 2, 'Filing BR', 27, '?m=filing_bcr&op=fildoc', 0, NULL, NULL),
(29, 98, 2, 'Filing Invoice In', 27, '?m=filing_invoice_in&op=fildoc', 0, NULL, NULL),
(79, 5, 1, 'Deposit Box', NULL, '?m=safety_deposit&op=safety_deposit', 0, NULL, NULL),
(36, 96, 1, 'Utilization', NULL, 'op=util', 0, NULL, NULL),
(37, 77, 2, 'Cost General Manager', 11, '?m=bcr&q=gm&op=bcr_day', 0, 'GM', NULL),
(38, 96, 1, 'Salary Financing', NULL, '?m=expense_salary&op=expense_salary', 0, NULL, NULL),
(43, 71, 2, 'Cost HRD', 11, '?m=bcr&q=hrd&op=bcr_day', 0, 'HRD', NULL),
(73, 93, 1, 'Employee Loan', NULL, 'op=emploan', 0, NULL, NULL),
(75, 68, 2, 'Cost Technical Engineering', 11, '?m=bcr&q=technical&op=bcr_day', 0, 'Technical', NULL),
(76, 10, 1, 'Forged Slip', NULL, '?m=f_proll_main&op=f_proll_main', 0, NULL, NULL),
(77, 67, 2, 'Cost Quality Control', 11, '?m=bcr&q=qc&op=bcr_day', 0, 'QC', NULL),
(78, 97, 2, 'Report', 2, '?m=invoice_search&op=invoice_in', 0, NULL, NULL),
(80, 70, 1, 'Bid/Perf Bond', NULL, '?m=bp_tampil_main&op=bp_tampil_main', 0, NULL, NULL),
(81, 100, 2, 'Employee Loan List', 73, '?m=loan&op=emploan', 0, NULL, NULL),
(82, 99, 2, 'Employee Loan Bank', 73, '?m=loan_bank&op=emploan', 0, NULL, NULL),
(83, 99, 2, 'Utilization Main', 36, '?m=util_home&op=util', 0, NULL, NULL),
(84, 98, 2, 'Utilization Criteria', 36, '?m=util_criteria&op=util', 0, NULL, NULL),
(90, 99, 2, 'Reimburse Bank', 88, '?m=reimburse&rp=bank&op=cb', 0, NULL, NULL),
(89, 100, 2, 'Reimburse', 88, '?m=reimburse&op=cb', 0, NULL, NULL),
(88, 99, 1, 'Reimburse', NULL, 'op=cb', 0, NULL, NULL),
(87, 99, 2, 'Cashbond Bank', 85, '?m=cashbond&rp=bank&op=cb', 0, NULL, NULL),
(86, 100, 2, 'Cashbond', 85, '?m=cashbond&op=cb', 0, NULL, NULL),
(85, 100, 1, 'Cashbond', NULL, 'op=cb', 0, NULL, NULL),
(92, 100, 1, 'Cashbond', NULL, 'op=drivercb', 0, NULL, NULL),
(93, 99, 0, 'Reimburse', NULL, 'op=drivercb', 0, NULL, NULL),
(94, 100, 2, 'Cashbond', 92, '?m=ecashbond&op=drivercb', 0, NULL, NULL),
(95, 99, 2, 'Cashbond Bank', 92, '?m=ecashbond&rp=bank&op=drivercb', 0, NULL, NULL),
(96, 100, 0, 'Reimburse', 93, '?m=dreimburse&op=drivercb', 0, NULL, NULL),
(97, 99, 0, 'Reimburse Bank', 93, '?m=dreimburse&rp=bank&op=drivercb', 0, NULL, NULL),
(98, 94, 1, 'Invoice In VFI System', NULL, '?m=invoice_in_vfi_add&op=invoice_in', 0, NULL, NULL),
(99, 100, 1, 'Tax', NULL, '?m=tax_all&op=tax', 0, NULL, NULL),
(100, 99, 2, 'Tax In', 99, '?m=tax_in&op=tax', 0, NULL, NULL),
(101, 98, 2, 'Tax Out', 99, '?m=tax_out&op=tax', 0, NULL, NULL),
(102, 100, 2, 'Tax All', 99, '?m=tax_all&op=tax', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reimburse`
--

CREATE TABLE `reimburse` (
  `id` int(11) NOT NULL,
  `subject` varchar(400) NOT NULL,
  `input_date` date NOT NULL,
  `m_approve` varchar(100) DEFAULT NULL,
  `m_approve_time` date DEFAULT NULL,
  `approved_by` varchar(200) DEFAULT NULL,
  `approved_time` date DEFAULT NULL,
  `currency` varchar(8) NOT NULL,
  `sisa` decimal(15,2) DEFAULT NULL,
  `division` varchar(80) DEFAULT NULL,
  `done` date DEFAULT NULL,
  `user` varchar(100) NOT NULL DEFAULT 'open',
  `project` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reimburse`
--

INSERT INTO `reimburse` (`id`, `subject`, `input_date`, `m_approve`, `m_approve_time`, `approved_by`, `approved_time`, `currency`, `sisa`, `division`, `done`, `user`, `project`) VALUES
(2, 'sdf', '2017-05-16', 'superuser', '2017-05-16', 'superuser', '2017-05-16', 'IDR', '0.00', 'driver', '2017-05-16', 'superuser', '342');

-- --------------------------------------------------------

--
-- Table structure for table `reimburse_detail`
--

CREATE TABLE `reimburse_detail` (
  `id` int(11) NOT NULL,
  `id_reimburse` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `no_nota` varchar(400) NOT NULL,
  `deskripsi` text NOT NULL,
  `cashin` decimal(15,2) NOT NULL,
  `cashout` decimal(15,2) NOT NULL,
  `category` varchar(20) DEFAULT NULL,
  `source_string` varchar(100) DEFAULT NULL,
  `source_int` int(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reimburse_detail`
--

INSERT INTO `reimburse_detail` (`id`, `id_reimburse`, `tanggal`, `no_nota`, `deskripsi`, `cashin`, `cashout`, `category`, `source_string`, `source_int`) VALUES
(2, 2, '2017-05-13', 'dsf', 'dsf sdfff', '123123.00', '0.00', 'PAID', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `schedule_payment`
--

CREATE TABLE `schedule_payment` (
  `id` int(11) NOT NULL,
  `input_time` datetime NOT NULL,
  `sp_date` date NOT NULL,
  `payment_type` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `IDR` decimal(15,2) NOT NULL,
  `USD` decimal(15,2) NOT NULL,
  `PIC` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `treasure`
--

CREATE TABLE `treasure` (
  `id` int(3) NOT NULL,
  `source` varchar(200) NOT NULL,
  `USD` decimal(15,2) NOT NULL,
  `IDR` decimal(15,2) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `br_balance` decimal(15,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `treasure`
--

INSERT INTO `treasure` (`id`, `source`, `USD`, `IDR`, `type`, `br_balance`) VALUES
(1, 'qdqd', '3244.00', '123357.00', NULL, '234.00');

-- --------------------------------------------------------

--
-- Table structure for table `treasure_history`
--

CREATE TABLE `treasure_history` (
  `id` int(11) NOT NULL,
  `id_treasure` int(4) NOT NULL,
  `date_input` date NOT NULL,
  `description` text NOT NULL,
  `IDR` decimal(15,2) NOT NULL,
  `USD` decimal(15,2) NOT NULL,
  `PIC` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `treasure_history`
--

INSERT INTO `treasure_history` (`id`, `id_treasure`, `date_input`, `description`, `IDR`, `USD`, `PIC`) VALUES
(1, 1, '2017-05-16', 'Auto Transferal amount from Reimburse (sdf) ', '123123.00', '0.00', 'superuser');

-- --------------------------------------------------------

--
-- Table structure for table `treasure_insert`
--

CREATE TABLE `treasure_insert` (
  `id` int(11) NOT NULL,
  `id_treasure` int(6) NOT NULL,
  `date_insert` date NOT NULL,
  `description` text NOT NULL,
  `project` varchar(200) NOT NULL,
  `IDR` decimal(15,2) NOT NULL,
  `USD` decimal(15,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `undo_date`
--

CREATE TABLE `undo_date` (
  `id_undo` int(6) NOT NULL,
  `id_item` int(6) NOT NULL,
  `type` varchar(10) NOT NULL,
  `date_ori` date NOT NULL,
  `date_new` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `realname` varchar(50) NOT NULL,
  `can_see` text COMMENT 'staff, manager, director',
  `level` varchar(15) NOT NULL,
  `division` varchar(50) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `util_criteria`
--

CREATE TABLE `util_criteria` (
  `id` int(3) NOT NULL,
  `name` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `author` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `util_instances`
--

CREATE TABLE `util_instances` (
  `id` int(11) NOT NULL,
  `id_master` int(11) NOT NULL,
  `subject` varchar(400) NOT NULL,
  `description` text NOT NULL,
  `pay_date` date NOT NULL,
  `currency` varchar(5) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `amount_back` decimal(15,2) NOT NULL,
  `progress` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `util_master`
--

CREATE TABLE `util_master` (
  `id` int(11) NOT NULL,
  `subject` varchar(400) NOT NULL,
  `description` text NOT NULL,
  `recurrent_date` date NOT NULL,
  `recurrent_type` varchar(40) NOT NULL,
  `type` varchar(40) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `currency` varchar(5) NOT NULL,
  `status` varchar(40) NOT NULL,
  `last_date` date DEFAULT NULL,
  `n_date` int(4) DEFAULT NULL,
  `classification` int(3) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `util_salary`
--

CREATE TABLE `util_salary` (
  `id` int(11) NOT NULL,
  `salary_date` date NOT NULL,
  `currency` varchar(5) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `jamsostek` decimal(15,2) NOT NULL,
  `health_insurance` decimal(15,2) NOT NULL,
  `pension` decimal(15,2) NOT NULL,
  `plan_date` date NOT NULL,
  `status` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_loan`
--
ALTER TABLE `bank_loan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_loan_detail`
--
ALTER TABLE `bank_loan_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_loan_int`
--
ALTER TABLE `bank_loan_int`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bcr`
--
ALTER TABLE `bcr`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bcr_detail`
--
ALTER TABLE `bcr_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cashbond`
--
ALTER TABLE `cashbond`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cashbond_detail`
--
ALTER TABLE `cashbond_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `opt_name` (`opt_name`);

--
-- Indexes for table `doc_counter`
--
ALTER TABLE `doc_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dps_brankas`
--
ALTER TABLE `dps_brankas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dps_history`
--
ALTER TABLE `dps_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dps_items`
--
ALTER TABLE `dps_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forged_proll`
--
ALTER TABLE `forged_proll`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instant_sp`
--
ALTER TABLE `instant_sp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inv_out`
--
ALTER TABLE `inv_out`
  ADD PRIMARY KEY (`id_inv`);

--
-- Indexes for table `inv_out_detail`
--
ALTER TABLE `inv_out_detail`
  ADD PRIMARY KEY (`id_inv_d`);

--
-- Indexes for table `inv_out_print`
--
ALTER TABLE `inv_out_print`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_in`
--
ALTER TABLE `invoice_in`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_in_pay`
--
ALTER TABLE `invoice_in_pay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leasing`
--
ALTER TABLE `leasing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leasing_detail`
--
ALTER TABLE `leasing_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leasing_int`
--
ALTER TABLE `leasing_int`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_view`
--
ALTER TABLE `log_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reimburse`
--
ALTER TABLE `reimburse`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reimburse_detail`
--
ALTER TABLE `reimburse_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule_payment`
--
ALTER TABLE `schedule_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `treasure`
--
ALTER TABLE `treasure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `treasure_history`
--
ALTER TABLE `treasure_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `treasure_insert`
--
ALTER TABLE `treasure_insert`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `undo_date`
--
ALTER TABLE `undo_date`
  ADD PRIMARY KEY (`id_undo`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `util_criteria`
--
ALTER TABLE `util_criteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `util_instances`
--
ALTER TABLE `util_instances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `util_master`
--
ALTER TABLE `util_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `util_salary`
--
ALTER TABLE `util_salary`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bank_loan`
--
ALTER TABLE `bank_loan`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bank_loan_detail`
--
ALTER TABLE `bank_loan_detail`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bank_loan_int`
--
ALTER TABLE `bank_loan_int`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bcr`
--
ALTER TABLE `bcr`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bcr_detail`
--
ALTER TABLE `bcr_detail`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cashbond`
--
ALTER TABLE `cashbond`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cashbond_detail`
--
ALTER TABLE `cashbond_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_counter`
--
ALTER TABLE `doc_counter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dps_brankas`
--
ALTER TABLE `dps_brankas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dps_history`
--
ALTER TABLE `dps_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dps_items`
--
ALTER TABLE `dps_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forged_proll`
--
ALTER TABLE `forged_proll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `instant_sp`
--
ALTER TABLE `instant_sp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `inv_out`
--
ALTER TABLE `inv_out`
  MODIFY `id_inv` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inv_out_detail`
--
ALTER TABLE `inv_out_detail`
  MODIFY `id_inv_d` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inv_out_print`
--
ALTER TABLE `inv_out_print`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice_in`
--
ALTER TABLE `invoice_in`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice_in_pay`
--
ALTER TABLE `invoice_in_pay`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `leasing`
--
ALTER TABLE `leasing`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `leasing_detail`
--
ALTER TABLE `leasing_detail`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `leasing_int`
--
ALTER TABLE `leasing_int`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_view`
--
ALTER TABLE `log_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=268;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `reimburse`
--
ALTER TABLE `reimburse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `reimburse_detail`
--
ALTER TABLE `reimburse_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `schedule_payment`
--
ALTER TABLE `schedule_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `treasure`
--
ALTER TABLE `treasure`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `treasure_history`
--
ALTER TABLE `treasure_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `treasure_insert`
--
ALTER TABLE `treasure_insert`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `undo_date`
--
ALTER TABLE `undo_date`
  MODIFY `id_undo` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `util_criteria`
--
ALTER TABLE `util_criteria`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `util_instances`
--
ALTER TABLE `util_instances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `util_master`
--
ALTER TABLE `util_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `util_salary`
--
ALTER TABLE `util_salary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;--
-- Database: `ims_forum`
--
CREATE DATABASE IF NOT EXISTS `ims_forum` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims_forum`;

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `id_activity` int(3) NOT NULL,
  `id_dar` int(3) NOT NULL,
  `time` time NOT NULL,
  `activity` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(1) NOT NULL,
  `username` varchar(200) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `opt_name` varchar(50) NOT NULL,
  `opt_value` varchar(50) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `engine_report`
--

CREATE TABLE `engine_report` (
  `id_er` int(3) NOT NULL,
  `id_dar` int(3) NOT NULL,
  `description` varchar(50) NOT NULL,
  `status` varchar(25) NOT NULL,
  `RH_prev` int(25) NOT NULL,
  `RH_today` int(25) NOT NULL,
  `total` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `epf_report`
--

CREATE TABLE `epf_report` (
  `id_epf` int(3) NOT NULL,
  `id_dar` int(3) NOT NULL,
  `lamp` varchar(25) NOT NULL,
  `lamp1` varchar(25) NOT NULL,
  `lamp2` varchar(25) NOT NULL,
  `lamp3` varchar(25) NOT NULL,
  `lamp4` varchar(25) NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id_inven` int(3) NOT NULL,
  `id_dar` int(3) NOT NULL,
  `description` varchar(50) NOT NULL,
  `merk` varchar(50) NOT NULL,
  `status` varchar(25) NOT NULL,
  `remark` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_view`
--

CREATE TABLE `log_view` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `location` varchar(400) NOT NULL,
  `log_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_view`
--

INSERT INTO `log_view` (`id`, `user`, `location`, `log_time`) VALUES
(1, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-17 10:47:00'),
(2, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-17 10:47:07'),
(3, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-17 10:49:40'),
(4, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-17 10:49:50'),
(5, 'superuser', '/ims99/forum/?m=policy_category&op=policy', '2017-04-17 10:49:55'),
(6, 'superuser', '/ims99/forum/?m=policy_category&op=policy', '2017-04-17 10:49:58'),
(7, 'superuser', '/ims99/forum/?m=policy_category&op=policy', '2017-04-17 10:49:59'),
(8, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-17 10:50:02'),
(9, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-17 10:50:08'),
(10, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=1', '2017-04-17 10:50:14'),
(11, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=1', '2017-04-17 10:50:46'),
(12, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=1', '2017-04-17 10:50:47'),
(13, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=1', '2017-04-17 10:50:53'),
(14, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=1', '2017-04-17 10:54:28'),
(15, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-17 11:18:19'),
(16, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-17 11:32:29'),
(17, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 02:33:25'),
(18, 'superuser', '/ims99/forum/?m=policy_category&op=policy', '2017-04-18 02:33:36'),
(19, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 02:33:51'),
(20, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 02:50:19'),
(21, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 02:50:26'),
(22, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 03:22:18'),
(23, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 03:22:21'),
(24, 'superuser', '/ims99/forum/?m=policy_wait&op=policy', '2017-04-18 03:22:23'),
(25, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=1', '2017-04-18 04:01:08'),
(26, 'superuser', '/ims99/forum/?m=policy_view&op=policy&m_id=1&d_id=1&act=ack', '2017-04-18 04:01:15'),
(27, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=1', '2017-04-18 04:01:19'),
(28, 'superuser', '/ims99/forum/?m=policy_wait&op=policy', '2017-04-18 04:01:22'),
(29, 'superuser', '/ims99/forum/?m=policy_main&st=bk&op=policy', '2017-04-18 04:01:29'),
(30, 'superuser', '/ims99/forum/?m=policy_main&st=bk&op=policy', '2017-04-18 04:01:33'),
(31, 'superuser', '/ims99/forum/?m=policy_main&st=bk&op=policy', '2017-04-18 04:01:37'),
(32, 'superuser', '/ims99/forum/?m=policy_wait&op=policy', '2017-04-18 04:01:42'),
(33, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2', '2017-04-18 04:01:44'),
(34, 'superuser', '/ims99/forum/?m=policy_wait&op=policy', '2017-04-18 04:01:46'),
(35, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=1', '2017-04-18 04:01:48'),
(36, 'superuser', '/ims99/forum/?m=policy_wait&op=policy', '2017-04-18 04:01:50'),
(37, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2', '2017-04-18 04:01:57'),
(38, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2', '2017-04-18 04:48:36'),
(39, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2', '2017-04-18 04:48:36'),
(40, 'superuser', '/ims99/forum/?m=policy_view&op=policy&m_id=2&d_id=2&act=ack', '2017-04-18 04:48:42'),
(41, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2', '2017-04-18 04:48:44'),
(42, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2', '2017-04-18 04:48:45'),
(43, 'superuser', '/ims99/forum/?m=policy_wait&op=policy', '2017-04-18 04:48:46'),
(44, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 04:48:49'),
(45, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2', '2017-04-18 04:48:51'),
(46, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2&del=2', '2017-04-18 04:48:53'),
(47, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2', '2017-04-18 04:48:53'),
(48, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2', '2017-04-18 04:49:13'),
(49, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2', '2017-04-18 04:49:13'),
(50, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2', '2017-04-18 05:08:32'),
(51, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2&rev=1', '2017-04-18 05:08:35'),
(52, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2&rev=1', '2017-04-18 05:08:39'),
(53, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2', '2017-04-18 05:08:39'),
(54, 'superuser', '/ims99/forum/?m=policy_view&op=policy&m_id=2&d_id=3&act=ack', '2017-04-18 05:08:47'),
(55, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2', '2017-04-18 05:08:49'),
(56, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2&rev=1', '2017-04-18 05:08:50'),
(57, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=2', '2017-04-18 05:11:32'),
(58, 'superuser', '/ims99/forum/?m=policy_edit&op=policy&id_main=2&id_detail=3', '2017-04-18 05:11:34'),
(59, 'superuser', '/ims99/forum/?m=policy_edit&op=policy&id_main=2&id_detail=3', '2017-04-18 05:17:40'),
(60, 'superuser', '/ims99/forum/?m=policy_edit&op=policy&id_main=2&id_detail=3', '2017-04-18 05:17:44'),
(61, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 05:36:02'),
(62, 'superuser', '/ims99/forum/?m=policy_wait&op=policy', '2017-04-18 05:36:06'),
(63, 'superuser', '/ims99/forum/?m=policy_main&st=bk&op=policy', '2017-04-18 05:38:59'),
(64, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 05:39:51'),
(65, 'superuser', '/ims99/forum/?m=policy_category&op=policy', '2017-04-18 05:43:01'),
(66, 'superuser', '/ims99/forum/?m=li_main&op=letter_in', '2017-04-18 06:42:57'),
(67, 'superuser', '/ims99/forum/?m=li_add&op=letter_in', '2017-04-18 06:43:00'),
(68, 'superuser', '/ims99/forum/?m=li_add&op=letter_in', '2017-04-18 06:43:08'),
(69, 'superuser', '/ims99/forum/?m=li_add&op=letter_in', '2017-04-18 06:43:23'),
(70, 'superuser', '/ims99/forum/?m=li_main&op=letter_in', '2017-04-18 06:43:23'),
(71, 'superuser', '/ims99/forum/?m=li_main&op=letter_in&approve&id=1', '2017-04-18 06:43:27'),
(72, 'superuser', '/ims99/forum/?m=li_main&op=letter_in', '2017-04-18 06:43:27'),
(73, 'superuser', '/ims99/forum/?m=li_main&op=letter_in', '2017-04-18 06:43:32'),
(74, 'superuser', '/ims99/forum/?m=li_add&op=letter_in', '2017-04-18 06:49:50'),
(75, 'superuser', '/ims99/forum/?m=li_main&op=letter_in', '2017-04-18 06:49:51'),
(76, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 08:35:17'),
(77, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 08:35:20'),
(78, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 08:35:26'),
(79, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 08:35:28'),
(80, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 08:35:35'),
(81, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 08:35:37'),
(82, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=1', '2017-04-18 08:35:44'),
(83, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 08:35:46'),
(84, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-04-18 08:35:50'),
(85, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=3', '2017-04-18 08:35:52'),
(86, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=3', '2017-04-18 08:35:57'),
(87, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=3', '2017-04-18 08:35:57'),
(88, 'superuser', '/ims99/forum/?m=policy_view&op=policy&m_id=3&d_id=5&act=ack', '2017-04-18 08:36:08'),
(89, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=3', '2017-04-18 08:36:17'),
(90, 'superuser', '/ims99/forum/?m=policy_view&op=policy&m_id=3&d_id=5&act=ack', '2017-04-18 08:36:45'),
(91, 'superuser', '/ims99/forum/?m=policy_view&op=policy&d_id=5&m_id=3&rap=ack', '2017-04-18 08:38:17'),
(92, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=3', '2017-04-18 08:38:18'),
(93, 'superuser', '/ims99/forum/?m=policy_view&op=policy&m_id=3&d_id=5&act=app', '2017-04-18 08:41:12'),
(94, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=3', '2017-04-18 09:05:03'),
(95, 'superuser', '/ims99/forum/?m=policy_view&op=policy&m_id=3&d_id=5&act=app', '2017-04-18 09:05:07'),
(96, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=3', '2017-04-18 09:05:15'),
(97, 'superuser', '/ims99/forum/?m=policy_view&op=policy&m_id=3&d_id=5&act=app', '2017-04-18 09:05:17'),
(98, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=3', '2017-04-18 09:05:19'),
(99, 'superuser', '/ims99/forum/?m=policy_view&op=policy&m_id=3&d_id=5&act=ack', '2017-04-18 09:05:21'),
(100, 'superuser', '/ims99/forum/?m=policy_detail&op=policy&m_id=3', '2017-04-18 09:05:22'),
(101, 'superuser', '/ims99/forum/?op=safetymeeting&m=safe_main', '2017-04-20 02:48:12'),
(102, 'superuser', '/ims99/forum/?op=safetymeeting&m=safe_main', '2017-04-20 02:48:29'),
(103, 'superuser', '/ims99/forum/?m=safe_main', '2017-04-20 02:48:29'),
(104, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=1', '2017-04-20 02:48:33'),
(105, 'superuser', '/ims99/forum/?m=safe_main', '2017-04-20 02:48:35'),
(106, 'superuser', '/ims99/forum/?m=safe_main&app_id=1', '2017-04-20 02:48:37'),
(107, 'superuser', '/ims99/forum/?m=safe_main', '2017-04-20 02:48:41'),
(108, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=1', '2017-04-20 02:50:04'),
(109, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=1', '2017-04-20 02:50:34'),
(110, 'superuser', '/ims99/forum/?m=safe_mom&id=1', '2017-04-20 02:50:36'),
(111, 'superuser', '/ims99/forum/?m=safe_main', '2017-04-20 02:50:43'),
(112, 'superuser', '/ims99/forum/?op=cs&m=cs_main', '2017-04-20 02:50:57'),
(113, 'superuser', '/ims99/forum/?op=mcu&m=mcu_main', '2017-04-20 02:51:15'),
(114, 'superuser', '/ims99/forum/?m=mcu_main&op=mcu&main=1', '2017-04-20 02:51:18'),
(115, 'superuser', '/ims99/forum/?op=cs&m=cs_main', '2017-04-20 02:53:23'),
(116, 'superuser', '/ims99/forum/?op=mcu&m=mcu_main', '2017-04-20 02:53:29'),
(117, 'superuser', '/ims99/forum/?m=mcu_main&op=mcu&main=1', '2017-04-20 02:53:31'),
(118, 'superuser', '/ims99/forum/?m=mcu_main&op=mcu&main=1', '2017-04-20 02:53:33'),
(119, 'superuser', '/ims99/forum/?m=mcu_main&op=mcu', '2017-04-20 02:53:33'),
(120, 'superuser', '/ims99/forum/?m=mcu_log&op=mcu&id=1', '2017-04-20 02:53:35'),
(121, 'superuser', '/ims99/forum/?op=sop&m=sop_main', '2017-04-20 02:54:10'),
(122, 'superuser', '/ims99/forum/?m=sop_category&op=sop', '2017-04-20 02:54:16'),
(123, 'superuser', '/ims99/forum/?m=sop_category&op=sop', '2017-04-20 02:54:22'),
(124, 'superuser', '/ims99/forum/?m=sop_category&op=sop', '2017-04-20 02:54:22'),
(125, 'superuser', '/ims99/forum/?m=sop_main&op=sop', '2017-04-20 02:54:24'),
(126, 'superuser', '/ims99/forum/?m=sop_main&op=sop', '2017-04-20 02:54:28'),
(127, 'superuser', '/ims99/forum/?m=sop_main&op=sop', '2017-04-20 02:54:29'),
(128, 'superuser', '/ims99/forum/?op=safetymeeting&m=safe_main', '2017-04-20 03:14:11'),
(129, 'superuser', '/ims99/forum/?op=meeting&m=mtg_main', '2017-04-20 03:27:48'),
(130, 'superuser', '/ims99/forum/?op=meeting&m=mtg_main', '2017-04-20 03:27:59'),
(131, 'superuser', '/ims99/forum/?m=mtg_main&op=meeting', '2017-04-20 03:27:59'),
(132, 'superuser', '/ims99/forum/?m=mtg_absence&op=meeting&a=invite&id=1', '2017-04-20 03:28:12'),
(133, 'superuser', '/ims99/forum/?m=mtg_main&op=meeting', '2017-04-20 03:28:26'),
(134, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=1', '2017-04-20 03:31:12'),
(135, 'superuser', '/ims99/forum/?m=safe_main', '2017-04-20 03:33:14'),
(136, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=1', '2017-04-20 03:33:21'),
(137, 'superuser', '/ims99/forum/?m=safe_mom&id=1', '2017-04-20 03:33:31'),
(138, 'superuser', '/ims99/forum/?m=safe_mom&id=1', '2017-04-20 03:36:31'),
(139, 'superuser', '/ims99/forum/?m=safe_main', '2017-04-20 03:36:33'),
(140, 'superuser', '/ims99/forum/?m=safe_main&app_id=1', '2017-04-20 03:36:46'),
(141, 'superuser', '/ims99/forum/?m=safe_main', '2017-04-20 03:38:07'),
(142, 'superuser', '/ims99/forum/?m=safe_main', '2017-04-20 03:38:11'),
(143, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=1', '2017-04-20 03:38:16'),
(144, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=1', '2017-04-20 03:44:18'),
(145, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=1', '2017-04-20 03:45:44'),
(146, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=1', '2017-04-20 03:50:34'),
(147, 'superuser', '/ims99/forum/?op=meeting&m=mtg_main', '2017-04-20 04:23:27'),
(148, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=1', '2017-04-20 04:42:44'),
(149, 'superuser', '/ims99/forum/?op=miss&m=miss_main', '2017-04-21 02:58:30'),
(150, 'superuser', '/ims99/forum/?op=miss&m=miss_main', '2017-04-21 02:58:30'),
(151, 'superuser', '/ims99/forum/?m=miss_main&new=1', '2017-04-21 02:58:33'),
(152, 'superuser', '/ims99/forum/?m=miss_main&new=1', '2017-04-21 02:58:46'),
(153, 'superuser', '/ims99/forum/?m=miss_main', '2017-04-21 02:58:46'),
(154, 'superuser', '/ims99/forum/?m=miss_view&d_id=1', '2017-04-21 03:00:08'),
(155, 'superuser', '/ims99/forum/?m=miss_main', '2017-04-21 03:00:12'),
(156, 'superuser', '/ims99/forum/?op=csr&m=csr_main', '2017-04-21 04:02:33'),
(157, 'superuser', '/ims99/forum/?m=csr_main&op=csr&new=1', '2017-04-21 04:02:36'),
(158, 'superuser', '/ims99/forum/?m=li_main&op=letter_in', '2017-04-26 08:10:22'),
(159, 'superuser', '/ims99/forum/?m=li_add&op=letter_in', '2017-04-26 08:10:24'),
(160, 'superuser', '/ims99/forum/?m=li_main&op=letter_in', '2017-04-26 08:10:29'),
(161, 'superuser', '/ims99/forum/?op=managementvisit&m=mv_main', '2017-05-12 11:29:49'),
(162, 'superuser', '/ims99/forum/?op=managementvisit&m=mv_main', '2017-05-12 11:29:50'),
(163, 'superuser', '/ims99/forum/?op=managementvisit&m=mv_main', '2017-05-12 11:29:55'),
(164, 'superuser', '/ims99/forum/?m=mv_main&op=managementvisit', '2017-05-12 11:29:56'),
(165, 'superuser', '/ims99/forum/?m=mv_absence&op=managementvisit&a=emp&id=1', '2017-05-12 11:29:58'),
(166, 'superuser', '/ims99/forum/?m=mv_absence&op=managementvisit&a=emp&id=1', '2017-05-12 11:30:02'),
(167, 'superuser', '/ims99/forum/?m=mv_absence&op=managementvisit&a=guest&id=1', '2017-05-12 11:30:04'),
(168, 'superuser', '/ims99/forum/?m=mv_absence&op=managementvisit&a=guest&id=1', '2017-05-12 11:30:09'),
(169, 'superuser', '/ims99/forum/?m=mv_absence&op=managementvisit&a=guest&id=1', '2017-05-12 11:30:09'),
(170, 'superuser', '/ims99/forum/?m=mv_mom&op=managementvisit&id=1', '2017-05-12 11:31:41'),
(171, 'superuser', '/ims99/forum/?m=mv_mom&op=managementvisit&id=1', '2017-05-12 11:31:49'),
(172, 'superuser', '/ims99/forum/?m=mv_mom&op=managementvisit&id=1', '2017-05-12 11:31:49'),
(173, 'superuser', '/ims99/forum/?m=mv_mom&op=managementvisit&id=1', '2017-05-12 11:31:58'),
(174, 'superuser', '/ims99/forum/?m=mv_mom&op=managementvisit&id=1', '2017-05-12 11:31:58'),
(175, 'superuser', '/ims99/forum/?m=mv_attach&op=managementvisit&id=1', '2017-05-12 11:32:00'),
(176, 'superuser', '/ims99/forum/?m=mv_attach&op=managementvisit&id=1', '2017-05-12 11:32:05'),
(177, 'superuser', '/ims99/forum/?m=mv_attach&op=managementvisit&id=1', '2017-05-12 11:32:05'),
(178, 'superuser', '/ims99/forum/?m=mv_attach&op=managementvisit&id=1&del=1', '2017-05-12 11:32:16'),
(179, 'superuser', '/ims99/forum/?m=mv_attach&op=managementvisit&id=1', '2017-05-12 11:32:16'),
(180, 'superuser', '/ims99/forum/?m=mv_attach&op=managementvisit&id=1', '2017-05-12 11:32:19'),
(181, 'superuser', '/ims99/forum/?m=mv_attach&op=managementvisit&id=1', '2017-05-12 11:32:19'),
(182, 'superuser', '/ims99/forum/?m=mv_attach&op=managementvisit&id=1', '2017-05-12 11:32:30'),
(183, 'superuser', '/ims99/forum/?m=mv_attach&op=managementvisit&id=1', '2017-05-12 11:32:30'),
(184, 'superuser', '/ims99/forum/?m=mv_mom&op=managementvisit&id=1', '2017-05-12 11:33:16'),
(185, 'superuser', '/ims99/forum/?m=mv_absence&op=managementvisit&a=emp&id=1', '2017-05-12 11:33:22'),
(186, 'superuser', '/ims99/forum/?m=mv_absence&op=managementvisit&a=emp&id=1', '2017-05-12 11:33:25'),
(187, 'superuser', '/ims99/forum/?m=mv_absence&op=managementvisit&a=emp&id=1', '2017-05-12 11:33:26'),
(188, 'superuser', '/ims99/forum/?m=mv_mom&op=managementvisit&id=1', '2017-05-12 11:33:27'),
(189, 'superuser', '/ims99/forum/?m=mv_mom&op=managementvisit&id=1', '2017-05-12 11:33:36'),
(190, 'superuser', '/ims99/forum/?m=mv_mom&op=managementvisit&id=1', '2017-05-12 11:33:37'),
(191, 'superuser', '/ims99/forum/?m=mv_main&op=managementvisit', '2017-05-12 11:33:39'),
(192, 'superuser', '/ims99/forum/?m=mv_main&op=managementvisit', '2017-05-12 11:33:43'),
(193, 'superuser', '/ims99/forum/?m=mv_main&op=managementvisit', '2017-05-12 11:33:43'),
(194, 'superuser', '/ims99/forum/?m=mv_main&op=managementvisit&app_id=1', '2017-05-12 11:33:46'),
(195, 'superuser', '/ims99/forum/?m=mv_main&op=managementvisit', '2017-05-12 11:33:51'),
(196, 'superuser', '/ims99/forum/?m=mv_main&op=managementvisit&app_id=2', '2017-05-12 11:33:54'),
(197, 'superuser', '/ims99/forum/?m=mv_main&op=managementvisit', '2017-05-12 11:33:56'),
(198, 'superuser', '/ims99/forum/?m=mv_absence&op=managementvisit&a=emp&id=1', '2017-05-12 11:33:58'),
(199, 'superuser', '/ims99/forum/?m=mv_absence&op=managementvisit&a=emp&id=1', '2017-05-12 11:34:02'),
(200, 'superuser', '/ims99/forum/?m=mv_absence&op=managementvisit&a=emp&id=1', '2017-05-12 11:34:05'),
(201, 'superuser', '/ims99/forum/?m=mv_absence&op=managementvisit&a=emp&id=1', '2017-05-12 11:34:09'),
(202, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-05-15 05:19:16'),
(203, 'superuser', '/ims99/forum/?m=policy_main&st=bk&op=policy', '2017-05-15 05:19:20'),
(204, 'superuser', '/ims99/forum/?m=policy_wait&op=policy', '2017-05-15 05:19:22'),
(205, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-05-15 05:19:24'),
(206, 'superuser', '/ims99/forum/?m=policy_wait&op=policy', '2017-05-15 05:19:27'),
(207, 'superuser', '/ims99/forum/?m=policy_main&op=policy', '2017-05-15 05:19:29'),
(208, 'superuser', '/ims99/forum/?m=user_list&op=user', '2017-05-15 08:20:13'),
(209, 'superuser', '/ims99/forum/?op=troubleshoot&m=troubleshoot_main', '2017-05-17 02:50:15'),
(210, 'superuser', '/ims99/forum/?m=troubleshoot_main&new=1', '2017-05-17 02:50:18'),
(211, 'superuser', '/ims99/forum/?m=troubleshoot_main&new=1', '2017-05-17 02:50:29'),
(212, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 02:50:30'),
(213, 'superuser', '/ims99/forum/?m=troubleshoot_pic&id=1', '2017-05-17 02:50:38'),
(214, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=1&d_id=1', '2017-05-17 02:50:41'),
(215, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 02:50:42'),
(216, 'superuser', '/ims99/forum/?m=troubleshoot_main&follow=1', '2017-05-17 02:50:45'),
(217, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 02:50:50'),
(218, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=2&d_id=1', '2017-05-17 02:50:53'),
(219, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 02:51:10'),
(220, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 02:58:21'),
(221, 'superuser', '/ims99/forum/?m=troubleshoot_main&new=1', '2017-05-17 02:58:38'),
(222, 'superuser', '/ims99/forum/?op=troubleshoot&m=troubleshoot_main', '2017-05-17 02:58:39'),
(223, 'superuser', '/ims99/forum/?op=troubleshoot&m=troubleshoot_main', '2017-05-17 02:58:42'),
(224, 'superuser', '/ims99/forum/?m=troubleshoot_view&d_id=1', '2017-05-17 04:10:35'),
(225, 'superuser', '/ims99/forum/?m=troubleshoot_view&d_id=1', '2017-05-17 04:17:53'),
(226, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:21:31'),
(227, 'superuser', '/ims99/forum/?m=troubleshoot_view&d_id=1', '2017-05-17 04:21:33'),
(228, 'superuser', '/ims99/forum/?m=troubleshoot_view&d_id=1', '2017-05-17 04:30:43'),
(229, 'superuser', '/ims99/forum/?m=troubleshoot_view&d_id=1', '2017-05-17 04:31:21'),
(230, 'superuser', '/ims99/forum/?m=troubleshoot_view&d_id=1', '2017-05-17 04:31:26'),
(231, 'superuser', '/ims99/forum/?m=troubleshoot_view&d_id=1', '2017-05-17 04:31:29'),
(232, 'superuser', '/ims99/forum/?m=troubleshoot_view&d_id=1', '2017-05-17 04:31:35'),
(233, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:35:26'),
(234, 'superuser', '/ims99/forum/?m=troubleshoot_pic&id=1', '2017-05-17 04:35:31'),
(235, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=1&d_id=1', '2017-05-17 04:35:37'),
(236, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=1&d_id=1', '2017-05-17 04:35:41'),
(237, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:35:42'),
(238, 'superuser', '/ims99/forum/?m=troubleshoot_pic&id=1', '2017-05-17 04:35:44'),
(239, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=1&d_id=1', '2017-05-17 04:35:47'),
(240, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=1&d_id=1', '2017-05-17 04:35:51'),
(241, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:35:51'),
(242, 'superuser', '/ims99/forum/?m=troubleshoot_main&follow=1', '2017-05-17 04:35:54'),
(243, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:36:03'),
(244, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=2&d_id=1', '2017-05-17 04:36:07'),
(245, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:36:13'),
(246, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=1', '2017-05-17 04:36:15'),
(247, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:46:28'),
(248, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=1', '2017-05-17 04:46:48'),
(249, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:46:50'),
(250, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:50:31'),
(251, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:50:31'),
(252, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=1', '2017-05-17 04:50:43'),
(253, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=1', '2017-05-17 04:52:04'),
(254, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=1', '2017-05-17 04:52:12'),
(255, 'superuser', '/ims99/forum/?m=troubleshoot_warning', '2017-05-17 04:52:22'),
(256, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:52:27'),
(257, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=1', '2017-05-17 04:52:33'),
(258, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:53:00'),
(259, 'superuser', '/ims99/forum/?m=troubleshoot_main&new=1', '2017-05-17 04:54:46'),
(260, 'superuser', '/ims99/forum/?m=troubleshoot_main&new=1', '2017-05-17 04:54:53'),
(261, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:54:54'),
(262, 'superuser', '/ims99/forum/?m=troubleshoot_view&d_id=1', '2017-05-17 04:55:00'),
(263, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:55:03'),
(264, 'superuser', '/ims99/forum/?m=troubleshoot_main&follow=1', '2017-05-17 04:55:05'),
(265, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:55:08'),
(266, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=1', '2017-05-17 04:55:11'),
(267, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=1', '2017-05-17 04:55:15'),
(268, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:55:20'),
(269, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=1&d_id=1', '2017-05-17 04:55:28'),
(270, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=1&d_id=1', '2017-05-17 04:55:30'),
(271, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=1&d_id=1', '2017-05-17 04:55:35'),
(272, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:55:36'),
(273, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=1', '2017-05-17 04:55:40'),
(274, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=1', '2017-05-17 04:55:47'),
(275, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:55:52'),
(276, 'superuser', '/ims99/forum/?m=troubleshoot_warning_view&d_id=1', '2017-05-17 04:56:01'),
(277, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:56:04'),
(278, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=2&d_id=1', '2017-05-17 04:56:06'),
(279, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=2&d_id=1', '2017-05-17 04:56:10'),
(280, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=2&d_id=1', '2017-05-17 04:56:11'),
(281, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:56:12'),
(282, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=2&d_id=1', '2017-05-17 04:56:14'),
(283, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:56:16'),
(284, 'superuser', '/ims99/forum/?m=troubleshoot_warning_view&d_id=1', '2017-05-17 04:56:18'),
(285, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:56:21'),
(286, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=2&d_id=2', '2017-05-17 04:57:05'),
(287, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:57:07'),
(288, 'superuser', '/ims99/forum/?m=troubleshoot_pic_up&id=1', '2017-05-17 04:57:10'),
(289, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=1&d_id=1', '2017-05-17 04:57:15'),
(290, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=1&d_id=1', '2017-05-17 04:57:19'),
(291, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:57:19'),
(292, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=1&d_id=1', '2017-05-17 04:57:23'),
(293, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=1&d_id=1', '2017-05-17 04:57:28'),
(294, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:57:28'),
(295, 'superuser', '/ims99/forum/?m=troubleshoot_main&new=1', '2017-05-17 04:57:37'),
(296, 'superuser', '/ims99/forum/?m=troubleshoot_main&new=1', '2017-05-17 04:57:46'),
(297, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 04:57:46'),
(298, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=3', '2017-05-17 05:42:29'),
(299, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 05:42:30'),
(300, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=2&d_id=3', '2017-05-17 05:42:32'),
(301, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 05:42:35'),
(302, 'superuser', '/ims99/forum/?m=troubleshoot_main&follow=3', '2017-05-17 05:42:37'),
(303, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 05:42:42'),
(304, 'superuser', '/ims99/forum/?m=troubleshoot_main&follow=2', '2017-05-17 05:42:46'),
(305, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 05:42:49'),
(306, 'superuser', '/ims99/forum/?m=troubleshoot_main&follow=1', '2017-05-17 05:50:53'),
(307, 'superuser', '/ims99/forum/?m=troubleshoot_view&d_id=1', '2017-05-17 08:21:54'),
(308, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 08:22:01'),
(309, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 08:53:26'),
(310, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=3', '2017-05-17 08:53:29'),
(311, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 08:53:31'),
(312, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=2&d_id=1', '2017-05-17 08:53:36'),
(313, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 08:53:39'),
(314, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=3', '2017-05-17 08:53:42'),
(315, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=3', '2017-05-17 08:53:48'),
(316, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=3', '2017-05-17 08:53:51'),
(317, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 08:53:53'),
(318, 'superuser', '/ims99/forum/?m=troubleshoot_pic&id=2', '2017-05-17 08:54:04'),
(319, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=2&d_id=1', '2017-05-17 08:54:17'),
(320, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-17 08:54:19'),
(321, 'superuser', '/ims99/forum/?m=mtg_main&op=meeting', '2017-05-17 08:57:53'),
(322, 'superuser', '/ims99/forum/?m=mtg_main&op=meeting', '2017-05-17 08:58:05'),
(323, 'superuser', '/ims99/forum/?m=mtg_main&op=meeting', '2017-05-17 08:58:06'),
(324, 'superuser', '/ims99/forum/?m=mtg_absence&op=meeting&a=invite&id=1', '2017-05-17 08:58:08'),
(325, 'superuser', '/ims99/forum/?m=mtg_absence&op=meeting&a=emp&id=1', '2017-05-17 08:58:11'),
(326, 'superuser', '/ims99/forum/?m=mtg_absence&op=meeting&a=emp&id=1', '2017-05-17 08:58:15'),
(327, 'superuser', '/ims99/forum/?m=mtg_main&op=meeting', '2017-05-17 08:58:17'),
(328, 'superuser', '/ims99/forum/?m=mtg_main&op=meeting&app_id=1', '2017-05-17 08:58:19'),
(329, 'superuser', '/ims99/forum/?m=mtg_main&op=meeting&app_id=1', '2017-05-17 08:58:21'),
(330, 'superuser', '/ims99/forum/?m=mtg_main&op=meeting&app_id=1', '2017-05-17 08:58:21'),
(331, 'superuser', '/ims99/forum/?m=mtg_main&op=meeting&app_id=1', '2017-05-17 08:58:21'),
(332, 'superuser', '/ims99/forum/?m=mtg_main&op=meeting&app_id=2', '2017-05-17 08:58:22'),
(333, 'superuser', '/ims99/forum/?m=mtg_main&op=meeting&app_id=2', '2017-05-17 08:58:22'),
(334, 'superuser', '/ims99/forum/?m=mtg_absence&op=meeting&a=invite&id=1', '2017-05-17 08:58:23'),
(335, 'superuser', '/ims99/forum/?m=mtg_absence&op=meeting&a=guest&id=1', '2017-05-17 08:58:25'),
(336, 'superuser', '/ims99/forum/?m=mtg_absence&op=meeting&a=guest&id=1', '2017-05-17 08:58:44'),
(337, 'superuser', '/ims99/forum/?m=mtg_absence&op=meeting&a=guest&id=1', '2017-05-17 08:58:45'),
(338, 'superuser', '/ims99/forum/?m=mtg_absence&op=meeting&a=guest&id=1', '2017-05-17 08:59:04'),
(339, 'superuser', '/ims99/forum/?m=mtg_absence&op=meeting&a=guest&id=1', '2017-05-17 08:59:08'),
(340, 'superuser', '/ims99/forum/?m=mtg_absence&op=meeting&a=guest&id=1', '2017-05-17 08:59:32'),
(341, 'superuser', '/ims99/forum/?m=mtg_absence&op=meeting&a=guest&id=1', '2017-05-17 08:59:33'),
(342, 'superuser', '/ims99/forum/?m=mtg_absence&op=meeting&a=guest&id=1', '2017-05-17 09:56:18'),
(343, 'superuser', '/ims99/forum/?m=mtg_main&op=meeting', '2017-05-17 09:56:23'),
(344, 'superuser', '/ims99/forum/?op=troubleshoot&m=troubleshoot_main', '2017-05-18 02:41:21'),
(345, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=2&d_id=1', '2017-05-18 02:41:28'),
(346, 'superuser', '/ims99/forum/?op=troubleshoot&m=troubleshoot_main', '2017-05-18 02:44:36'),
(347, 'superuser', '/ims99/forum/?m=troubleshoot_pic_up&id=1', '2017-05-18 02:44:38'),
(348, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=2&d_id=1', '2017-05-18 02:45:27'),
(349, 'superuser', '/ims99/forum/?op=troubleshoot&m=troubleshoot_main', '2017-05-18 03:38:34'),
(350, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=3', '2017-05-18 03:38:36'),
(351, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=3', '2017-05-18 03:38:48'),
(352, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=3', '2017-05-18 03:39:52'),
(353, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=3', '2017-05-18 03:40:04'),
(354, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=3', '2017-05-18 03:41:51'),
(355, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=3', '2017-05-18 03:42:01'),
(356, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=3', '2017-05-18 03:42:48'),
(357, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=3', '2017-05-18 03:54:43'),
(358, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=3', '2017-05-18 03:54:44'),
(359, 'superuser', '/ims99/forum/?op=troubleshoot&m=troubleshoot_main', '2017-05-18 03:54:45'),
(360, 'superuser', '/ims99/forum/?op=troubleshoot&m=troubleshoot_main', '2017-05-18 03:54:47'),
(361, 'superuser', '/ims99/forum/?m=troubleshoot_warning_view&d_id=3', '2017-05-18 03:54:50'),
(362, 'superuser', '/ims99/forum/?op=troubleshoot&m=troubleshoot_main', '2017-05-18 04:01:28'),
(363, 'superuser', '/ims99/forum/?m=troubleshoot_view&d_id=3', '2017-05-18 04:01:32'),
(364, 'superuser', '/ims99/forum/?op=troubleshoot&m=troubleshoot_main', '2017-05-18 04:14:39'),
(365, 'superuser', '/ims99/forum/?op=safetymeeting&m=safe_main', '2017-05-18 04:15:45'),
(366, 'superuser', '/ims99/forum/?op=safetymeeting&m=safe_main', '2017-05-18 04:18:46'),
(367, 'superuser', '/ims99/forum/?m=safe_main', '2017-05-18 04:18:46'),
(368, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=1', '2017-05-18 04:18:49'),
(369, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=1', '2017-05-18 04:18:55'),
(370, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=1', '2017-05-18 04:19:01'),
(371, 'superuser', '/ims99/forum/?m=safe_mom&id=1', '2017-05-18 04:19:08'),
(372, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=1', '2017-05-18 04:19:10'),
(373, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=1', '2017-05-18 04:22:04'),
(374, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=1', '2017-05-18 04:22:24'),
(375, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=1', '2017-05-18 04:22:24'),
(376, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=1', '2017-05-18 04:23:40'),
(377, 'superuser', '/ims99/forum/?m=safe_mom&id=1', '2017-05-18 04:23:41'),
(378, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=1', '2017-05-18 04:23:48'),
(379, 'superuser', '/ims99/forum/?m=safe_main', '2017-05-18 04:23:49'),
(380, 'superuser', '/ims99/forum/?m=safe_main', '2017-05-18 04:23:53'),
(381, 'superuser', '/ims99/forum/?m=safe_main', '2017-05-18 04:24:25'),
(382, 'superuser', '/ims99/forum/?m=safe_main', '2017-05-18 04:40:45'),
(383, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=2', '2017-05-18 04:55:58'),
(384, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=2', '2017-05-18 04:57:56'),
(385, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=2', '2017-05-18 04:57:58'),
(386, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=2', '2017-05-18 04:58:00'),
(387, 'superuser', '/ims99/forum/?m=safe_mom&id=2', '2017-05-18 04:58:01'),
(388, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=2', '2017-05-18 04:58:05'),
(389, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=2', '2017-05-18 04:58:06'),
(390, 'superuser', '/ims99/forum/?m=safe_mom&id=2', '2017-05-18 04:58:08'),
(391, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=2', '2017-05-18 04:58:15'),
(392, 'superuser', '/ims99/forum/?m=safe_mom&id=2', '2017-05-18 04:58:18'),
(393, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=2', '2017-05-18 05:01:16'),
(394, 'superuser', '/ims99/forum/?m=safe_mom&id=2', '2017-05-18 05:01:21'),
(395, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=2', '2017-05-18 05:01:23'),
(396, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=2', '2017-05-18 05:01:24'),
(397, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=2', '2017-05-18 05:01:25'),
(398, 'superuser', '/ims99/forum/?m=safe_mom&id=2', '2017-05-18 05:01:27'),
(399, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=2', '2017-05-18 05:01:28'),
(400, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=2', '2017-05-18 05:01:30'),
(401, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=2', '2017-05-18 05:41:41'),
(402, 'superuser', '/ims99/forum/?m=safe_mom&id=2', '2017-05-18 05:42:52'),
(403, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=2', '2017-05-18 08:10:30'),
(404, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=2', '2017-05-18 08:10:41'),
(405, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=2', '2017-05-18 08:10:46'),
(406, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=2', '2017-05-18 08:10:51'),
(407, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=2', '2017-05-18 08:10:55'),
(408, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=2', '2017-05-18 08:44:43'),
(409, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=2', '2017-05-18 08:44:46'),
(410, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=2', '2017-05-18 08:44:48'),
(411, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=2', '2017-05-18 08:45:01'),
(412, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=2', '2017-05-18 08:45:01'),
(413, 'superuser', '/ims99/forum/?m=safe_mom&id=2', '2017-05-18 08:53:50'),
(414, 'superuser', '/ims99/forum/?m=safe_mom&id=2', '2017-05-18 08:54:00'),
(415, 'superuser', '/ims99/forum/?op=troubleshoot&m=troubleshoot_main', '2017-05-19 03:20:56'),
(416, 'superuser', '/ims99/forum/?m=troubleshoot_view&d_id=3', '2017-05-19 03:20:59'),
(417, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-19 03:21:02'),
(418, 'superuser', '/ims99/forum/?m=troubleshoot_warning_view&d_id=3', '2017-05-19 03:21:04'),
(419, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-19 03:21:06'),
(420, 'superuser', '/ims99/forum/?m=troubleshoot_warning&d_id=2', '2017-05-19 03:21:09'),
(421, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-19 03:21:12'),
(422, 'superuser', '/ims99/forum/?m=troubleshoot_view&d_id=2', '2017-05-19 03:21:14'),
(423, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-19 03:21:17'),
(424, 'superuser', '/ims99/forum/?op=troubleshoot&m=troubleshoot_main', '2017-05-19 03:21:22'),
(425, 'superuser', '/ims99/forum/?m=troubleshoot_view&d_id=1', '2017-05-19 03:21:24'),
(426, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-19 03:21:27'),
(427, 'superuser', '/ims99/forum/?m=troubleshoot_pic_edit&tipe=1&d_id=3', '2017-05-19 03:21:28'),
(428, 'superuser', '/ims99/forum/?m=troubleshoot_main', '2017-05-19 03:21:31'),
(429, 'superuser', '/ims99/forum/?m=troubleshoot_view&d_id=1', '2017-05-19 03:21:32'),
(430, 'superuser', '/ims99/forum/?op=troubleshoot&m=troubleshoot_main', '2017-05-19 03:21:34'),
(431, 'superuser', '/ims99/forum/?op=safetymeeting&m=safe_main', '2017-05-19 03:21:37'),
(432, 'superuser', '/ims99/forum/?m=safe_absence&a=psi&id=2', '2017-05-19 03:33:02'),
(433, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=2', '2017-05-19 03:33:04'),
(434, 'superuser', '/ims99/forum/?m=safe_absence&a=guest&id=2', '2017-05-19 04:28:49'),
(435, 'superuser', '/ims99/forum/?op=sop&m=sop_main', '2017-05-19 11:25:33'),
(436, 'superuser', '/ims99/forum/?op=sop&m=sop_main', '2017-05-19 11:25:40'),
(437, 'superuser', '/ims99/forum/?m=sop_main&op=sop', '2017-05-19 11:25:41'),
(438, 'superuser', '/ims99/forum/?m=sop_detail&op=sop&m_id=1', '2017-05-19 11:25:43'),
(439, 'superuser', '/ims99/forum/?m=sop_detail&op=sop&m_id=1', '2017-05-19 11:25:47'),
(440, 'superuser', '/ims99/forum/?m=sop_detail&op=sop&m_id=1', '2017-05-19 11:25:47'),
(441, 'superuser', '/ims99/forum/?m=sop_view&op=sop&m_id=1&d_id=1&act=ack', '2017-05-19 11:25:49'),
(442, 'superuser', '/ims99/forum/?m=sop_view&op=sop&d_id=1&m_id=1&rap=ack', '2017-05-19 11:25:52'),
(443, 'superuser', '/ims99/forum/?m=sop_detail&op=sop&m_id=1', '2017-05-19 11:26:35'),
(444, 'superuser', '/ims99/forum/?m=sop_edit&op=sop&id_main=1&id_detail=1', '2017-05-19 11:26:38'),
(445, 'superuser', '/ims99/forum/?m=sop_edit&op=sop&id_main=1&id_detail=1', '2017-05-19 11:27:15'),
(446, 'superuser', '/ims99/forum/?op=ir&m=ir_main', '2017-05-22 02:58:33'),
(447, 'superuser', '/ims99/forum/?m=user_list&op=user', '2017-05-23 04:10:54'),
(448, 'superuser', '/ims99/forum/?m=user_list&op=user', '2017-05-23 04:10:55'),
(449, 'superuser', '/ims99/forum/?m=user_add', '2017-05-23 04:10:57'),
(450, 'superuser', '/ims99/forum/?m=user_list&op=user', '2017-05-23 04:11:02'),
(451, 'superuser', '/ims99/forum/?m=user_add', '2017-05-23 04:16:30'),
(452, 'superuser', '/ims99/forum/?m=user_list&op=user', '2017-05-23 04:16:41'),
(453, 'superuser', '/ims99/forum/?m=user_edit&op=user&id=1', '2017-05-23 04:16:43'),
(454, 'superuser', '/ims99/forum/?m=user_list&op=user', '2017-05-23 06:14:33'),
(455, 'superuser', '/ims99/forum/?m=user_add', '2017-05-23 06:14:49'),
(456, 'superuser', '/ims99/forum/?m=user_add', '2017-05-23 06:14:56'),
(457, 'superuser', '/ims99/forum/?op=troubleshoot&m=troubleshoot_main', '2017-05-24 09:41:40'),
(458, 'superuser', '/ims99/forum/?m=troubleshoot_main&new=1', '2017-05-24 09:41:43'),
(459, 'superuser', '/ims99/forum/?op=troubleshoot&m=troubleshoot_main', '2017-05-24 09:42:56');

-- --------------------------------------------------------

--
-- Table structure for table `main_dar`
--

CREATE TABLE `main_dar` (
  `id_dar` int(3) NOT NULL,
  `proj_name` varchar(25) NOT NULL,
  `proj_location` varchar(25) NOT NULL,
  `weather` varchar(25) NOT NULL,
  `current_op` varchar(25) NOT NULL,
  `planned_op` varchar(25) NOT NULL,
  `date` date NOT NULL,
  `report_no` varchar(25) NOT NULL,
  `time_start` int(4) NOT NULL,
  `time_stop` int(4) NOT NULL,
  `status` enum('FINISHED','UNFINISHED') NOT NULL DEFAULT 'UNFINISHED'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `material_report`
--

CREATE TABLE `material_report` (
  `id_mr` int(11) NOT NULL,
  `id_dar` int(3) NOT NULL,
  `description` varchar(25) NOT NULL,
  `stock` int(11) NOT NULL,
  `cons_today` int(11) NOT NULL,
  `cons_day` int(11) NOT NULL,
  `cons_total` int(11) NOT NULL,
  `remaining_stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(6) NOT NULL,
  `position` int(3) NOT NULL,
  `stage` int(2) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `href` varchar(200) DEFAULT NULL,
  `level` int(3) NOT NULL,
  `division` varchar(200) DEFAULT NULL,
  `lock` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `position`, `stage`, `name`, `parent`, `href`, `level`, `division`, `lock`) VALUES
(113, 98, 1, 'Your Pictures', NULL, '?m=file_picture&op=forum', 0, NULL, NULL),
(114, 97, 1, 'Training Type', NULL, '?m=training_type_add&op=training', 0, NULL, NULL),
(41, 99, 1, 'Forum', NULL, '?m=forum&op=forum', 0, NULL, NULL),
(84, 99, 1, 'PMS', NULL, '?m=item_maintenance&op=maintenance', 0, NULL, NULL),
(85, 99, 1, 'Softwares', NULL, '?m=forms&op=software', 0, NULL, NULL),
(86, 99, 1, 'Meetings', NULL, '?m=mtg_main&op=meeting', 0, NULL, NULL),
(87, 99, 1, 'Management Visit', NULL, '?m=mv_main&op=managementvisit', 0, NULL, NULL),
(88, 99, 1, 'Medical Checkups', NULL, '?m=mcu_main&op=mcu', 0, NULL, NULL),
(89, 99, 1, 'Safety Meeting', NULL, '?m=safe_main&op=safe', 0, NULL, NULL),
(90, 99, 1, 'Near Miss', NULL, '?m=near_miss_main&op=nearmiss', 0, NULL, NULL),
(91, 99, 1, 'Standard Operating Procedures', NULL, '?m=sop_main&op=sop', 0, NULL, NULL),
(92, 99, 1, 'Work Instruction', NULL, '?m=wi_main&op=wi', 0, NULL, NULL),
(93, 99, 1, 'SOP Category', NULL, '?m=sop_category&op=sop', 0, NULL, NULL),
(94, 99, 1, 'Customer Satisfaction', NULL, '?m=cs_main&op=cs', 0, NULL, NULL),
(95, 99, 1, 'Corporate Social Responsibility', NULL, '?m=csr_main&op=csr', 0, NULL, NULL),
(96, 99, 1, 'Accident Report', NULL, '?m=ar_main&op=ar', 0, NULL, NULL),
(97, 99, 1, 'Chat', NULL, '?m=m_forum&op=penhouse', 0, NULL, NULL),
(98, 99, 1, 'Guest Book Cileungsi', NULL, '?m=q_absence&op=guest_cileungsi', 0, NULL, NULL),
(99, 99, 1, 'Field Daily Report', NULL, '?m=project_report_active&op=fdr', 0, NULL, NULL),
(100, 98, 1, 'Archived Report', NULL, '?m=project_report_passive&op=fdr', 0, NULL, NULL),
(101, 99, 1, 'Quiz', NULL, '?m=qz_quiz_main&op=quiz', 0, NULL, NULL),
(102, 99, 1, 'Events', NULL, '?m=ev_main&op=event', 0, NULL, NULL),
(103, 99, 1, 'Ownership Programme', NULL, '?m=op_main&op=op', 0, NULL, NULL),
(104, 99, 1, 'Letter In', NULL, '?m=li_main&op=letter_in', 0, NULL, NULL),
(105, 99, 1, 'Letter Out', NULL, '?m=lo_main&op=letter_out', 0, NULL, NULL),
(106, 99, 1, 'Troubleshoot', NULL, '?m=troubleshoot_main&op=troubleshoot', 0, NULL, NULL),
(107, 99, 1, 'Revision Request', NULL, 'op=revisi', 0, NULL, NULL),
(108, 99, 1, 'History', NULL, '?m=doc_center_main&op=file', 0, NULL, NULL),
(109, 98, 1, 'Documents', NULL, '?m=doc_center_data&op=file', 0, NULL, NULL),
(110, 97, 1, 'Peminjaman', NULL, '?m=doc_center_peminjam&op=file', 0, NULL, NULL),
(111, 96, 1, 'Personal File', NULL, '?m=doc_center_data_pribadi&op=file', 0, NULL, NULL),
(112, 99, 1, 'Miss', NULL, '?m=miss_main&op=miss', 0, NULL, NULL),
(115, 98, 1, 'Training Record', NULL, '?m=training&op=training', 0, NULL, NULL),
(116, 98, 1, 'Equipment', NULL, '?m=equip_main&op=wi', 0, NULL, NULL),
(117, 99, 1, 'Service Ticketing', NULL, '?m=st_main_user&op=sticketing', 0, NULL, NULL),
(118, 2, 2, 'Revision Request List', 107, '?m=prev_main&op=revisi', 0, NULL, NULL),
(119, 2, 2, 'Revision Request Bank', 107, '?m=prev_bank&op=revisi', 0, NULL, NULL),
(120, 2, 2, 'Revision Request Rejected', 107, '?m=prev_reject&op=revisi', 0, NULL, NULL),
(121, 99, 1, 'User', NULL, '?m=user_list&op=user', 0, NULL, NULL),
(122, 99, 1, 'Level', NULL, '?m=level&op=user', 0, NULL, NULL),
(123, 100, 1, 'Customer Inspection', NULL, '?m=ci_main&op=cusinsp', 0, NULL, NULL),
(124, 100, 1, 'Policy', NULL, 'op=policy', 0, NULL, NULL),
(125, 99, 1, 'Policy Category', NULL, '?m=policy_category&op=policy', 0, NULL, NULL),
(127, 98, 2, 'Policy Bank', 124, '?m=policy_main&st=bk&op=policy', 0, NULL, NULL),
(126, 99, 2, 'Policy Waiting', 124, '?m=policy_wait&op=policy', 0, NULL, NULL),
(128, 100, 2, 'Policy All', 124, '?m=policy_main&op=policy', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `op`
--

CREATE TABLE `op` (
  `id_op` int(11) NOT NULL,
  `id_emp` varchar(11) NOT NULL,
  `transmisi` varchar(20) NOT NULL,
  `status` varchar(10) NOT NULL,
  `jenis_kendaraan` varchar(20) NOT NULL,
  `merk` varchar(20) NOT NULL,
  `tipe` varchar(50) NOT NULL,
  `warna` varchar(20) NOT NULL,
  `tahun` int(11) NOT NULL,
  `total_cc` varchar(20) NOT NULL,
  `harga_kendaraan` int(11) NOT NULL,
  `subsidi` int(11) DEFAULT NULL,
  `cicilan_bulan` int(11) DEFAULT NULL,
  `pinjaman_cop_mop` int(11) NOT NULL,
  `path1` text NOT NULL,
  `ukuran1` varchar(100) NOT NULL,
  `path2` text,
  `ukuran2` varchar(100) DEFAULT NULL,
  `n_bulan` int(6) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `personel`
--

CREATE TABLE `personel` (
  `id_personel` int(3) NOT NULL,
  `id_dar` int(3) NOT NULL,
  `chief_op` varchar(25) NOT NULL,
  `morning_shift` varchar(25) NOT NULL,
  `night_shift` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id_activity`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `opt_name` (`opt_name`);

--
-- Indexes for table `engine_report`
--
ALTER TABLE `engine_report`
  ADD PRIMARY KEY (`id_er`);

--
-- Indexes for table `epf_report`
--
ALTER TABLE `epf_report`
  ADD PRIMARY KEY (`id_epf`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id_inven`);

--
-- Indexes for table `log_view`
--
ALTER TABLE `log_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_dar`
--
ALTER TABLE `main_dar`
  ADD PRIMARY KEY (`id_dar`);

--
-- Indexes for table `material_report`
--
ALTER TABLE `material_report`
  ADD PRIMARY KEY (`id_mr`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `op`
--
ALTER TABLE `op`
  ADD PRIMARY KEY (`id_op`);

--
-- Indexes for table `personel`
--
ALTER TABLE `personel`
  ADD PRIMARY KEY (`id_personel`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `id_activity` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `engine_report`
--
ALTER TABLE `engine_report`
  MODIFY `id_er` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `epf_report`
--
ALTER TABLE `epf_report`
  MODIFY `id_epf` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id_inven` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_view`
--
ALTER TABLE `log_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=460;
--
-- AUTO_INCREMENT for table `main_dar`
--
ALTER TABLE `main_dar`
  MODIFY `id_dar` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `material_report`
--
ALTER TABLE `material_report`
  MODIFY `id_mr` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT for table `op`
--
ALTER TABLE `op`
  MODIFY `id_op` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `personel`
--
ALTER TABLE `personel`
  MODIFY `id_personel` int(3) NOT NULL AUTO_INCREMENT;--
-- Database: `ims_gm`
--
CREATE DATABASE IF NOT EXISTS `ims_gm` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims_gm`;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(1) NOT NULL,
  `username` varchar(200) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `opt_name` varchar(50) NOT NULL,
  `opt_value` varchar(50) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_counter`
--

CREATE TABLE `doc_counter` (
  `doc_code` varchar(10) NOT NULL,
  `last_num` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='menyimpan informasi nomor dokumen terakhir';

-- --------------------------------------------------------

--
-- Table structure for table `log_view`
--

CREATE TABLE `log_view` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `location` varchar(400) NOT NULL,
  `log_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(6) NOT NULL,
  `position` int(3) NOT NULL,
  `stage` int(2) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `href` varchar(200) DEFAULT NULL,
  `level` int(3) NOT NULL,
  `division` varchar(200) DEFAULT NULL,
  `lock` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `opt_name` (`opt_name`);

--
-- Indexes for table `doc_counter`
--
ALTER TABLE `doc_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_view`
--
ALTER TABLE `log_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_counter`
--
ALTER TABLE `doc_counter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_view`
--
ALTER TABLE `log_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;--
-- Database: `ims_home`
--
CREATE DATABASE IF NOT EXISTS `ims_home` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims_home`;

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `id` int(100) NOT NULL,
  `redir_id` int(100) DEFAULT NULL,
  `id_topik` int(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `act_type` varchar(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(1) NOT NULL,
  `username` varchar(200) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `applicant`
--

CREATE TABLE `applicant` (
  `app_id` int(100) NOT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `app_key` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `kelamin` varchar(10) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `agama` varchar(10) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `no_ktp` varchar(100) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `no_telp1` varchar(15) DEFAULT NULL,
  `no_telp2` varchar(15) DEFAULT NULL,
  `kp_suis` varchar(100) DEFAULT NULL,
  `anak_1` varchar(100) DEFAULT NULL,
  `anak_2` varchar(100) DEFAULT NULL,
  `anak_3` varchar(100) DEFAULT NULL,
  `kk_ayah` varchar(100) DEFAULT NULL,
  `kk_ibu` varchar(100) DEFAULT NULL,
  `kk_saudara_1` varchar(100) DEFAULT NULL,
  `kk_saudara_2` varchar(100) DEFAULT NULL,
  `kk_saudara_3` varchar(100) DEFAULT NULL,
  `kk_saudara_4` varchar(100) DEFAULT NULL,
  `sakit` varchar(5) DEFAULT NULL,
  `sakit_ket` varchar(100) DEFAULT NULL,
  `sakit_date` date DEFAULT NULL,
  `sakit_pengaruh` varchar(100) DEFAULT NULL,
  `sd_name` varchar(100) DEFAULT NULL,
  `sd_kota` varchar(100) DEFAULT NULL,
  `sd_jur` varchar(100) DEFAULT NULL,
  `sd_date1` varchar(10) DEFAULT NULL,
  `sd_date2` varchar(10) DEFAULT NULL,
  `smp_name` varchar(100) DEFAULT NULL,
  `smp_kota` varchar(100) DEFAULT NULL,
  `smp_jur` varchar(100) DEFAULT NULL,
  `smp_date1` varchar(100) DEFAULT NULL,
  `smp_date2` varchar(100) DEFAULT NULL,
  `sma_name` varchar(100) DEFAULT NULL,
  `sma_kota` varchar(100) DEFAULT NULL,
  `sma_jur` varchar(100) DEFAULT NULL,
  `sma_date1` date DEFAULT NULL,
  `sma_date2` date DEFAULT NULL,
  `d3_name` varchar(100) DEFAULT NULL,
  `d3_kota` varchar(100) DEFAULT NULL,
  `d3_jur` varchar(100) DEFAULT NULL,
  `d3_date1` varchar(15) DEFAULT NULL,
  `d3_date2` varchar(15) DEFAULT NULL,
  `s1_name` varchar(100) DEFAULT NULL,
  `s1_kota` varchar(100) DEFAULT NULL,
  `s1_jur` varchar(100) DEFAULT NULL,
  `s1_date1` varchar(15) DEFAULT NULL,
  `s1_date2` varchar(15) DEFAULT NULL,
  `s2_name` varchar(100) DEFAULT NULL,
  `s2_kota` varchar(100) DEFAULT NULL,
  `s2_jur` varchar(100) DEFAULT NULL,
  `s2_date1` varchar(15) DEFAULT NULL,
  `s2_date2` varchar(15) DEFAULT NULL,
  `other_name` varchar(100) DEFAULT NULL,
  `other_kota` varchar(100) DEFAULT NULL,
  `other_jur` varchar(100) DEFAULT NULL,
  `other_date1` date DEFAULT NULL,
  `other_date2` date DEFAULT NULL,
  `skripsi_name` varchar(100) DEFAULT NULL,
  `skripsi_desc` varchar(100) DEFAULT NULL,
  `skripsi_date` varchar(100) DEFAULT NULL,
  `keg_nama` varchar(100) DEFAULT NULL,
  `keg_jabatan` varchar(100) DEFAULT NULL,
  `keg_date` varchar(100) DEFAULT NULL,
  `keg_desc` varchar(100) DEFAULT NULL,
  `bhs_1_nama` varchar(100) DEFAULT NULL,
  `bhs_1_bicara` varchar(100) DEFAULT NULL,
  `bhs_1_dengar` varchar(100) DEFAULT NULL,
  `bhs_1_tulis` varchar(100) DEFAULT NULL,
  `bhs_2_nama` varchar(100) DEFAULT NULL,
  `bhs_2_bicara` varchar(100) DEFAULT NULL,
  `bhs_2_dengar` varchar(100) DEFAULT NULL,
  `bhs_2_tulis` varchar(100) DEFAULT NULL,
  `hobby` varchar(100) DEFAULT NULL,
  `achievment` varchar(100) DEFAULT NULL,
  `kendaraan` varchar(100) DEFAULT NULL,
  `kerja_1_per` varchar(100) DEFAULT NULL,
  `kerja_1_alamat` varchar(100) DEFAULT NULL,
  `kerja_1_telp` varchar(100) DEFAULT NULL,
  `kerja_1_desc` varchar(100) DEFAULT NULL,
  `kerja_1_jenis` varchar(100) DEFAULT NULL,
  `kerja_1_atasan` varchar(100) DEFAULT NULL,
  `kerja_1_berhenti` varchar(100) DEFAULT NULL,
  `kerja_1_jml_kary` varchar(100) DEFAULT NULL,
  `kerja_1_direktur` varchar(100) DEFAULT NULL,
  `kerja_2_per` varchar(100) DEFAULT NULL,
  `kerja_2_alamat` varchar(100) DEFAULT NULL,
  `kerja_2_telp` varchar(100) DEFAULT NULL,
  `kerja_2_desc` varchar(100) DEFAULT NULL,
  `kerja_2_jenis` varchar(100) DEFAULT NULL,
  `kerja_2_atasan` varchar(100) DEFAULT NULL,
  `kerja_2_berhenti` varchar(100) DEFAULT NULL,
  `kerja_2_jml_kary` varchar(100) DEFAULT NULL,
  `kerja_2_direktur` varchar(100) DEFAULT NULL,
  `con_person` varchar(100) DEFAULT NULL,
  `con_no_person` varchar(100) DEFAULT NULL,
  `con_job_person` varchar(100) DEFAULT NULL,
  `gaji` int(100) DEFAULT NULL,
  `start_work` date DEFAULT NULL,
  `start_work_why` varchar(100) DEFAULT NULL,
  `status_kerja` int(2) DEFAULT NULL,
  `input_date` date DEFAULT NULL,
  `picture` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `appraisal_detail`
--

CREATE TABLE `appraisal_detail` (
  `id` int(11) NOT NULL,
  `id_main` int(11) DEFAULT NULL,
  `id_employee` int(11) DEFAULT NULL,
  `position_period` varchar(30) DEFAULT NULL,
  `work_period` varchar(30) DEFAULT NULL,
  `score` varchar(30) DEFAULT NULL,
  `position` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `appraisal_img`
--

CREATE TABLE `appraisal_img` (
  `id` int(11) NOT NULL,
  `id_main` int(11) NOT NULL,
  `location` text NOT NULL,
  `nama_file` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `status` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `appraisal_main`
--

CREATE TABLE `appraisal_main` (
  `id` int(11) NOT NULL,
  `judul` text,
  `date_input` datetime DEFAULT NULL,
  `input_by` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ar`
--

CREATE TABLE `ar` (
  `ar_id` int(10) NOT NULL,
  `tittle` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `deskripsi` text,
  `o_level` varchar(10) DEFAULT NULL,
  `approved` varchar(250) DEFAULT NULL,
  `close` date DEFAULT NULL,
  `pelapor` varchar(10) DEFAULT NULL,
  `pict` varchar(250) DEFAULT NULL,
  `pelapor_follow_up` varchar(250) DEFAULT NULL,
  `follow_up_task` text,
  `division` varchar(10) NOT NULL,
  `pict_follow` varchar(250) DEFAULT NULL,
  `pelapor_name` varchar(250) DEFAULT NULL,
  `warning_letter` varchar(250) DEFAULT NULL,
  `prj` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `art_files`
--

CREATE TABLE `art_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fullpath` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `controller_in` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `method_in` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `table_reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `table_primary` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attend_log`
--

CREATE TABLE `attend_log` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `logtime` datetime NOT NULL,
  `logtry` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attend_web`
--

CREATE TABLE `attend_web` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `redir_id` int(11) NOT NULL,
  `checktime` datetime NOT NULL,
  `late_time` int(11) DEFAULT NULL,
  `justification` int(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attend_web_new`
--

CREATE TABLE `attend_web_new` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `redir_id` int(11) DEFAULT NULL,
  `check_in` datetime DEFAULT NULL,
  `check_out` datetime DEFAULT NULL,
  `late_time` int(11) DEFAULT NULL,
  `justification` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auto_gather_server`
--

CREATE TABLE `auto_gather_server` (
  `id` int(11) NOT NULL,
  `attendance_checkin` date NOT NULL,
  `attendance_checkout` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bank_fm`
--

CREATE TABLE `bank_fm` (
  `id` int(11) NOT NULL,
  `nama_bank` varchar(50) NOT NULL,
  `keterangan` text,
  `tgl_trans` date DEFAULT NULL,
  `tgl_exp` date DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `jumlah` float NOT NULL,
  `file` varchar(100) DEFAULT NULL,
  `pic` varchar(50) NOT NULL,
  `telp_pic` varchar(12) NOT NULL,
  `status` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_fm`
--

INSERT INTO `bank_fm` (`id`, `nama_bank`, `keterangan`, `tgl_trans`, `tgl_exp`, `currency`, `jumlah`, `file`, `pic`, `telp_pic`, `status`) VALUES
(2, 'fvd', 'dfvv', '0000-00-00', '0000-00-00', 'IDR', 232222000000, NULL, 'dfv', 'df', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bank_fm_detail`
--

CREATE TABLE `bank_fm_detail` (
  `id` int(11) NOT NULL,
  `id_main` int(11) NOT NULL,
  `tgl_upload` datetime DEFAULT NULL,
  `nama_file` varchar(100) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bank_ru`
--

CREATE TABLE `bank_ru` (
  `id` int(11) NOT NULL,
  `nama_bank` varchar(50) NOT NULL,
  `keterangan` text,
  `tgl_trans` date DEFAULT NULL,
  `tgl_exp` date DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `jumlah` decimal(15,2) NOT NULL,
  `file` varchar(100) DEFAULT NULL,
  `pic` varchar(50) NOT NULL,
  `telp_pic` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bank_ru_detail`
--

CREATE TABLE `bank_ru_detail` (
  `id` int(11) NOT NULL,
  `id_main` int(11) NOT NULL,
  `tgl_upload` datetime DEFAULT NULL,
  `nama_file` varchar(100) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `berkas`
--

CREATE TABLE `berkas` (
  `id` int(50) NOT NULL,
  `id_maintenance` int(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `uploader` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `upload_time` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `initial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `regional_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bugs`
--

CREATE TABLE `bugs` (
  `bugs_id` int(10) NOT NULL,
  `tittle` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `deskripsi` text,
  `o_level` varchar(10) DEFAULT NULL,
  `approved` varchar(250) DEFAULT NULL,
  `close` varchar(250) DEFAULT NULL,
  `pelapor` varchar(10) DEFAULT NULL,
  `pict` varchar(250) DEFAULT NULL,
  `pelapor_follow_up` varchar(250) DEFAULT NULL,
  `follow_up_task` text,
  `division` varchar(10) NOT NULL,
  `pict_follow` varchar(250) DEFAULT NULL,
  `pelapor_name` varchar(250) DEFAULT NULL,
  `warning_letter` varchar(250) DEFAULT NULL,
  `prj` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ci_absence`
--

CREATE TABLE `ci_absence` (
  `id_absence` int(11) NOT NULL,
  `id_main` int(100) DEFAULT NULL,
  `emp_name` varchar(100) DEFAULT NULL,
  `sig_address` varchar(250) DEFAULT NULL,
  `redir_id` int(100) DEFAULT NULL,
  `emp_position` varchar(300) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ci_attach`
--

CREATE TABLE `ci_attach` (
  `attach_id` int(11) NOT NULL,
  `id_main` int(10) DEFAULT NULL,
  `attach_pic` varchar(250) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ci_main`
--

CREATE TABLE `ci_main` (
  `id_main` int(50) NOT NULL,
  `topic` varchar(200) NOT NULL,
  `location` varchar(200) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `date_main` datetime NOT NULL,
  `progress` varchar(200) NOT NULL,
  `approved_by` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ci_mom`
--

CREATE TABLE `ci_mom` (
  `id_mom` int(50) NOT NULL,
  `id_main` int(50) NOT NULL,
  `content` text NOT NULL,
  `pic` varchar(100) NOT NULL,
  `deadline` date NOT NULL,
  `input_time` datetime DEFAULT NULL,
  `floor` varchar(300) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_main`
--

CREATE TABLE `client_main` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `company` varchar(200) DEFAULT NULL,
  `address` text,
  `phone` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `religion` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_main`
--

INSERT INTO `client_main` (`id`, `name`, `company`, `address`, `phone`, `email`, `religion`) VALUES
(1, 'fsdfv dsfsdsf', 'sdf', 'dsf', 'sf', 'dsf', 'dsf'),
(2, 'sdf', 'sdf', 'fds', 'dsf', 'dsf', 'sdf');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id_comment` int(100) NOT NULL,
  `id_topik` int(100) NOT NULL,
  `date_comment` datetime NOT NULL,
  `isi_comment` text NOT NULL,
  `emp_id` int(100) NOT NULL,
  `baca` varchar(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacs`
--

CREATE TABLE `contacs` (
  `id` int(10) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `company` varchar(30) DEFAULT NULL,
  `vendor` varchar(30) DEFAULT NULL,
  `alamat` varchar(30) DEFAULT NULL,
  `email` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cs_absence`
--

CREATE TABLE `cs_absence` (
  `id_absence` int(11) NOT NULL,
  `id_main` int(100) DEFAULT NULL,
  `emp_name` varchar(100) DEFAULT NULL,
  `sig_address` varchar(250) DEFAULT NULL,
  `redir_id` int(100) DEFAULT NULL,
  `emp_position` varchar(300) DEFAULT NULL,
  `company` varchar(300) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cs_attach`
--

CREATE TABLE `cs_attach` (
  `attach_id` int(11) NOT NULL,
  `attach_pic` varchar(250) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `id_main` int(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cs_main`
--

CREATE TABLE `cs_main` (
  `id_main` int(50) NOT NULL,
  `topic` varchar(400) NOT NULL,
  `location` varchar(200) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `date_main` datetime NOT NULL,
  `progress` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cs_mom`
--

CREATE TABLE `cs_mom` (
  `id_mom` int(50) NOT NULL,
  `id_main` int(50) NOT NULL,
  `content` text NOT NULL,
  `pic` varchar(100) NOT NULL,
  `deadline` date NOT NULL,
  `input_time` datetime DEFAULT NULL,
  `floor` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `csr_main`
--

CREATE TABLE `csr_main` (
  `csr_id` int(11) NOT NULL,
  `author` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `title` varchar(500) NOT NULL,
  `deskripsi` text NOT NULL,
  `online` int(1) NOT NULL DEFAULT '0',
  `pict1` varchar(200) DEFAULT NULL,
  `pict2` varchar(200) DEFAULT NULL,
  `pict3` varchar(200) DEFAULT NULL,
  `pict4` varchar(200) DEFAULT NULL,
  `pict5` varchar(200) DEFAULT NULL,
  `division` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE `division` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dl_class`
--

CREATE TABLE `dl_class` (
  `id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dl_main`
--

CREATE TABLE `dl_main` (
  `dl_id` int(11) NOT NULL,
  `author` varchar(200) NOT NULL,
  `title` varchar(500) NOT NULL,
  `deskripsi` text,
  `class` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `file_form` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dl_main`
--

INSERT INTO `dl_main` (`dl_id`, `author`, `title`, `deskripsi`, `class`, `date`, `file_form`) VALUES
(1, 'superuser', 'sdf', '<p>sdfsfd</p>', '', '2017-04-26', 'dl_archive/400_FORM.htm');

-- --------------------------------------------------------

--
-- Table structure for table `doc_center_datadoc`
--

CREATE TABLE `doc_center_datadoc` (
  `id_doc` int(11) NOT NULL,
  `nama_doc` varchar(100) NOT NULL,
  `lokasi_document` varchar(110) NOT NULL,
  `status` varchar(100) NOT NULL,
  `nama_manager` varchar(50) NOT NULL,
  `pass_code` varchar(20) NOT NULL,
  `tgl_update` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_center_historyy`
--

CREATE TABLE `doc_center_historyy` (
  `id_history` int(110) NOT NULL,
  `nama_peminjam` varchar(100) NOT NULL,
  `nama_dokumen` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_center_peminjam`
--

CREATE TABLE `doc_center_peminjam` (
  `id_peminjam` int(11) NOT NULL,
  `nama_peminjam` varchar(200) NOT NULL,
  `divisi` varchar(200) NOT NULL,
  `nama_dokumen` varchar(100) NOT NULL,
  `tgl_pinjam` datetime NOT NULL,
  `tgl_wajib_kembali` datetime NOT NULL,
  `tgl_kembali` datetime DEFAULT NULL,
  `kondisi` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_center_pengembalian`
--

CREATE TABLE `doc_center_pengembalian` (
  `id_pengembalian` int(11) NOT NULL,
  `nama_peminjam_kembali` varchar(100) NOT NULL,
  `divisi` varchar(100) NOT NULL,
  `nama_dokumen_kembali` varchar(100) NOT NULL,
  `tgl_wajib_kembali` date NOT NULL,
  `tgl_kembali` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_counter`
--

CREATE TABLE `doc_counter` (
  `doc_code` varchar(10) NOT NULL,
  `last_num` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='menyimpan informasi nomor dokumen terakhir';

-- --------------------------------------------------------

--
-- Table structure for table `doc_rout_history`
--

CREATE TABLE `doc_rout_history` (
  `id` int(11) NOT NULL,
  `doc_code` varchar(20) NOT NULL,
  `sent_by` varchar(50) NOT NULL,
  `receive_by` varchar(50) DEFAULT NULL,
  `sent_date_time` datetime DEFAULT NULL,
  `receive_date_time` datetime DEFAULT NULL,
  `id_main` int(11) NOT NULL,
  `to_by` varchar(50) NOT NULL,
  `sent_by2` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_rout_login`
--

CREATE TABLE `doc_rout_login` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_rout_main`
--

CREATE TABLE `doc_rout_main` (
  `id` int(11) NOT NULL,
  `sent_by` varchar(50) NOT NULL,
  `to_by` varchar(50) NOT NULL,
  `receive_by` varchar(50) DEFAULT NULL,
  `doc_code` varchar(20) NOT NULL,
  `status` varchar(50) NOT NULL,
  `doc_type` varchar(30) NOT NULL,
  `penjelasan` text NOT NULL,
  `sent_by2` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_rout_ob`
--

CREATE TABLE `doc_rout_ob` (
  `id` int(11) NOT NULL,
  `doc_code` varchar(30) NOT NULL,
  `sent_by` varchar(50) NOT NULL,
  `to_by` varchar(50) NOT NULL,
  `receive_by` varchar(50) DEFAULT NULL,
  `id_main` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `file_type` varchar(50) DEFAULT NULL,
  `division` varchar(50) DEFAULT NULL,
  `uploaded_by` varchar(50) DEFAULT NULL,
  `uploaded_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drawing_category`
--

CREATE TABLE `drawing_category` (
  `id` int(50) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drawing_log`
--

CREATE TABLE `drawing_log` (
  `id` int(100) NOT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `keterangan` varchar(500) DEFAULT NULL,
  `id_drawing` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drawing_status`
--

CREATE TABLE `drawing_status` (
  `id` int(100) NOT NULL,
  `id_drawing` int(10) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `keterangan` varchar(500) DEFAULT NULL,
  `qc` varchar(3) DEFAULT NULL,
  `done` varchar(10) DEFAULT NULL,
  `alasan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drawing_sub_category`
--

CREATE TABLE `drawing_sub_category` (
  `id` int(100) NOT NULL,
  `id_category` int(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `initial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supervisor` int(10) UNSIGNED DEFAULT NULL,
  `jobtitle_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ev_alat`
--

CREATE TABLE `ev_alat` (
  `id` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `nama_alat` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ev_anggota`
--

CREATE TABLE `ev_anggota` (
  `id` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `perusahaan` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ev_dokumentasi_acara`
--

CREATE TABLE `ev_dokumentasi_acara` (
  `id` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `path` varchar(200) NOT NULL,
  `ukuran` int(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ev_jadwal`
--

CREATE TABLE `ev_jadwal` (
  `id` int(11) NOT NULL,
  `nama_event` varchar(100) NOT NULL,
  `topik_event` varchar(20) NOT NULL,
  `tanggal_event` date NOT NULL,
  `status` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ev_juara`
--

CREATE TABLE `ev_juara` (
  `id` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `juara` text NOT NULL,
  `score` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ev_kekurangan`
--

CREATE TABLE `ev_kekurangan` (
  `id` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `kekurangan_acara` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ev_perusahaan`
--

CREATE TABLE `ev_perusahaan` (
  `id` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ev_strukturorganisasi`
--

CREATE TABLE `ev_strukturorganisasi` (
  `id` int(11) NOT NULL,
  `nama` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ev_susunan_acara`
--

CREATE TABLE `ev_susunan_acara` (
  `id` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `susunan_acara` text NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_akhir` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `form_maintenance`
--

CREATE TABLE `form_maintenance` (
  `id` int(50) NOT NULL,
  `id_maintenance` int(50) NOT NULL,
  `type_of_ equipment` varchar(300) NOT NULL,
  `type_of_maintenance` varchar(300) NOT NULL,
  `frequency` varchar(300) NOT NULL,
  `action` varchar(300) NOT NULL,
  `last_check` date DEFAULT NULL,
  `next_check` date DEFAULT NULL,
  `action_by` varchar(300) NOT NULL,
  `remark` text NOT NULL,
  `type_maintenance` varchar(100) DEFAULT NULL,
  `date_maintenance` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forum`
--

CREATE TABLE `forum` (
  `id_forum` int(11) NOT NULL,
  `nama_forum` varchar(100) DEFAULT NULL,
  `date_forum` datetime NOT NULL,
  `emp_id` varchar(5) NOT NULL,
  `baca` varchar(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `initial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `division_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ins_ru`
--

CREATE TABLE `ins_ru` (
  `id` int(11) NOT NULL,
  `nama_asuransi` varchar(50) NOT NULL,
  `alamat_asuransi` text,
  `phone_asuransi` varchar(50) DEFAULT NULL,
  `polis` varchar(50) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `jumlah` varchar(15) NOT NULL,
  `angsuran` varchar(100) DEFAULT NULL,
  `cover_ins` text,
  `tgl_trans` date DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `insured` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ins_ru_detail`
--

CREATE TABLE `ins_ru_detail` (
  `id` int(11) NOT NULL,
  `id_main` int(11) NOT NULL,
  `tgl_upload` datetime DEFAULT NULL,
  `nama_file` varchar(100) DEFAULT NULL,
  `type_file` varchar(50) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id` int(11) NOT NULL,
  `code` text NOT NULL,
  `project` varchar(100) NOT NULL,
  `create_by` varchar(30) NOT NULL,
  `create_date` date NOT NULL,
  `holder` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ir_main`
--

CREATE TABLE `ir_main` (
  `ir_id` int(11) NOT NULL,
  `author` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `title` varchar(500) NOT NULL,
  `deskripsi` text NOT NULL,
  `pict1` varchar(200) DEFAULT NULL,
  `pict2` varchar(200) DEFAULT NULL,
  `pict3` varchar(200) DEFAULT NULL,
  `pict4` varchar(200) DEFAULT NULL,
  `pict5` varchar(200) DEFAULT NULL,
  `pict6` varchar(200) DEFAULT NULL,
  `pict7` varchar(200) DEFAULT NULL,
  `asset` varchar(200) DEFAULT NULL,
  `user` varchar(200) DEFAULT NULL,
  `vendor` varchar(200) DEFAULT NULL,
  `ins_location` text,
  `pic` varchar(200) DEFAULT NULL,
  `pic_num` varchar(200) DEFAULT NULL,
  `item_condition` varchar(200) DEFAULT NULL,
  `weight` varchar(200) DEFAULT NULL,
  `dimension` varchar(200) DEFAULT NULL,
  `capacity` varchar(200) DEFAULT NULL,
  `power` varchar(200) DEFAULT NULL,
  `spec` varchar(200) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `certificate` varchar(200) DEFAULT NULL,
  `manual` varchar(200) DEFAULT NULL,
  `sop` varchar(200) DEFAULT NULL,
  `wi` varchar(200) DEFAULT NULL,
  `nameplate` varchar(200) DEFAULT NULL,
  `workenvironment` varchar(200) DEFAULT NULL,
  `paint` varchar(200) DEFAULT NULL,
  `file_form` varchar(200) DEFAULT NULL,
  `po_num` int(11) DEFAULT NULL,
  `item_code` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `it_punish_main`
--

CREATE TABLE `it_punish_main` (
  `it_punish_id` int(10) NOT NULL,
  `id_redir` int(10) NOT NULL,
  `point` int(10) NOT NULL,
  `date_n` date NOT NULL,
  `reason` text NOT NULL,
  `created_id` int(10) NOT NULL,
  `approve_id` varchar(10) NOT NULL,
  `id_bbt_p` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_maintenance`
--

CREATE TABLE `item_maintenance` (
  `id` int(100) NOT NULL,
  `item_code` varchar(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `periodic` varchar(2) DEFAULT NULL,
  `grade` int(2) DEFAULT NULL,
  `manufacture` date DEFAULT NULL,
  `pic` varchar(250) DEFAULT NULL,
  `pnid` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobtitles`
--

CREATE TABLE `jobtitles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `initial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kalender_event`
--

CREATE TABLE `kalender_event` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `acara` text NOT NULL,
  `deskripsi` text NOT NULL,
  `kategori` int(11) NOT NULL,
  `username` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `krt_property`
--

CREATE TABLE `krt_property` (
  `id` int(11) NOT NULL,
  `id_user` varchar(30) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address1` text NOT NULL,
  `address2` text,
  `telp1` varchar(20) NOT NULL,
  `telp2` varchar(20) DEFAULT NULL,
  `mobile_phone` varchar(20) NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `position` varchar(30) NOT NULL,
  `directory` varchar(100) NOT NULL,
  `size` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `li`
--

CREATE TABLE `li` (
  `id_li` int(11) NOT NULL,
  `dari` varchar(100) NOT NULL,
  `untuk` varchar(100) NOT NULL,
  `perihal` text NOT NULL,
  `no_surat` varchar(30) NOT NULL,
  `no_li` int(11) NOT NULL,
  `kategori` varchar(10) NOT NULL,
  `status_surat` varchar(30) DEFAULT NULL,
  `tgl_terima` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `li`
--

INSERT INTO `li` (`id_li`, `dari`, `untuk`, `perihal`, `no_surat`, `no_li`, `kategori`, `status_surat`, `tgl_terima`) VALUES
(1, 'sdc', 'sdc', '<p><strong>dscscdsc</strong></p>', '1/GT/LI/IV/17', 1, 'LI', 'Approve', '2017-04-18');

-- --------------------------------------------------------

--
-- Table structure for table `li_attach`
--

CREATE TABLE `li_attach` (
  `id` int(11) NOT NULL,
  `id_li` int(11) DEFAULT NULL,
  `file_name` text,
  `file_type` varchar(50) DEFAULT NULL,
  `uploaded_by` varchar(50) DEFAULT NULL,
  `uploaded_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lo`
--

CREATE TABLE `lo` (
  `id_lo` int(11) NOT NULL,
  `dari` varchar(100) NOT NULL,
  `untuk` varchar(100) NOT NULL,
  `perihal` text NOT NULL,
  `no_surat` varchar(30) NOT NULL,
  `no_lo` int(11) NOT NULL,
  `kategori` varchar(10) NOT NULL,
  `status_surat` varchar(30) DEFAULT NULL,
  `tgl_terima` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_view`
--

CREATE TABLE `log_view` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `location` varchar(400) NOT NULL,
  `log_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_comment`
--

CREATE TABLE `m_comment` (
  `id_comment` int(100) NOT NULL,
  `id_topik` int(100) NOT NULL,
  `date_comment` datetime NOT NULL,
  `isi_comment` text NOT NULL,
  `emp_id` int(100) NOT NULL,
  `baca` varchar(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_forum`
--

CREATE TABLE `m_forum` (
  `id_forum` int(11) NOT NULL,
  `nama_forum` varchar(100) DEFAULT NULL,
  `date_forum` datetime NOT NULL,
  `emp_id` varchar(5) NOT NULL,
  `baca` varchar(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_topik`
--

CREATE TABLE `m_topik` (
  `id_topik` int(100) NOT NULL,
  `id_forum` int(100) NOT NULL,
  `emp_id` int(100) NOT NULL,
  `nama_topik` varchar(250) NOT NULL,
  `desc_topik` text NOT NULL,
  `date_topik` datetime DEFAULT NULL,
  `baca` varchar(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_logo`
--

CREATE TABLE `master_logo` (
  `id` int(10) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `pict` text,
  `jalan` varchar(100) DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `pos` varchar(10) DEFAULT NULL,
  `prov` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `email_emp` varchar(100) DEFAULT NULL,
  `web` varchar(50) DEFAULT NULL,
  `npwp` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mcu_log`
--

CREATE TABLE `mcu_log` (
  `id` int(50) NOT NULL,
  `mcu_id` int(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `uploader` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `upload_time` date NOT NULL,
  `mcu_date` date DEFAULT NULL,
  `mcu_remark` varchar(250) DEFAULT NULL,
  `mcu_expired` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mcu_main`
--

CREATE TABLE `mcu_main` (
  `mcu_id` int(10) NOT NULL,
  `mcu_date` date DEFAULT NULL,
  `mcu_remark` varchar(250) DEFAULT NULL,
  `emp_id` int(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mcu_main`
--

INSERT INTO `mcu_main` (`mcu_id`, `mcu_date`, `mcu_remark`, `emp_id`) VALUES
(1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(6) NOT NULL,
  `position` int(3) NOT NULL,
  `stage` int(2) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `href` varchar(200) DEFAULT NULL,
  `level` int(3) NOT NULL,
  `division` varchar(200) DEFAULT NULL,
  `lock` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `position`, `stage`, `name`, `parent`, `href`, `level`, `division`, `lock`) VALUES
(1, 100, 1, '[RQ] Request', NULL, 'op=rq', 0, NULL, NULL),
(2, 2, 2, 'RQ Waiting', 1, '?m=fr&op=rq', 0, NULL, NULL),
(3, 2, 2, 'RQ Bank', 1, '?m=fr_bank&op=rq', 0, NULL, NULL),
(5, 99, 1, '[PR] Purchase Request', NULL, 'op=rq', 0, NULL, NULL),
(6, 2, 2, 'PR Waiting', 5, '?m=pre&op=rq', 0, NULL, NULL),
(7, 2, 2, 'PR Bank', 5, '?m=pre_bank&op=rq', 0, NULL, NULL),
(8, 98, 1, '[PE] Purchase Evaluation', NULL, 'op=pe', 0, NULL, NULL),
(9, 2, 2, 'PE Waiting', 8, '?m=pev&op=pe', 0, NULL, NULL),
(10, 2, 2, 'PE Bank', 8, '?m=pev_bank&op=pe', 0, NULL, NULL),
(11, 97, 1, '[PO] Purchase Order', NULL, 'op=po', 0, NULL, NULL),
(12, 2, 2, 'PO Waiting', 11, '?m=po&op=po', 0, NULL, NULL),
(13, 2, 2, 'PO Bank', 11, '?m=po_bank&op=po', 0, NULL, NULL),
(14, 96, 1, 'Tracking', NULL, '?m=fr_tracking&op=rq', 0, NULL, NULL),
(43, 99, 1, '[GR] Good Reciept', NULL, 'op=gr', 0, NULL, NULL),
(15, 100, 1, '[SO] Service Order', NULL, 'op=so', 0, NULL, NULL),
(16, 2, 2, 'SO Waiting', 15, '?m=so&op=so', 0, NULL, NULL),
(17, 2, 2, 'SO Bank', 15, '?m=so_bank&op=so', 0, NULL, NULL),
(19, 99, 1, '[RFQ] Request For Quotation', NULL, 'op=so', 0, NULL, NULL),
(20, 2, 2, 'RFQ Waiting', 19, '?m=rfq_so&op=so', 0, NULL, NULL),
(21, 2, 2, 'RFQ Bank', 19, '?m=rfq_so_bank&op=so', 0, NULL, NULL),
(22, 98, 1, '[SE] Service Evaluation', NULL, 'op=se', 0, NULL, NULL),
(23, 2, 2, 'SE Waiting', 22, '?m=se&op=se', 0, NULL, NULL),
(24, 2, 2, 'SE Bank', 22, '?m=se_bank&op=se', 0, NULL, NULL),
(25, 97, 1, '[WO] Work Order', NULL, 'op=wo', 0, NULL, NULL),
(26, 2, 2, 'WO Waiting', 25, '?m=wo&op=wo', 0, NULL, NULL),
(27, 2, 2, 'WO Bank', 25, '?m=wo_bank&op=wo', 0, NULL, NULL),
(31, 96, 1, 'Tracking', NULL, '?m=so_tracking&op=so', 0, NULL, NULL),
(29, 1, 1, 'PO Instant', 11, '?m=po_instant&op=po', 0, NULL, NULL),
(30, 1, 1, 'WO Instant', 25, '?m=wo_instant&op=wo', 0, NULL, NULL),
(32, 3, 2, 'RQ Need', 1, '?m=fr_add&op=rq', 0, NULL, NULL),
(33, 3, 2, 'SO Need', 15, '?m=so_edit&a=add&op=so', 0, NULL, NULL),
(34, 100, 1, '[DO] Delivery Order', NULL, 'op=do', 0, NULL, NULL),
(35, 99, 2, 'DO Out', 34, '?m=do&psh=all&op=do', 0, NULL, NULL),
(41, 98, 2, 'DO Check', 34, '?m=do_all&op=do', 0, NULL, NULL),
(64, 99, 1, 'Presentation', NULL, '?m=presentation&op=presentation', 0, NULL, NULL),
(37, 99, 1, 'Quotation List', NULL, '?m=quot_list&op=qo', 0, NULL, NULL),
(39, 100, 1, 'Vendor List', NULL, '?m=organizations&op=org', 0, NULL, NULL),
(40, 100, 1, 'Daily Report', NULL, '?m=daily&op=report', 0, NULL, NULL),
(42, 97, 2, 'DO Report', 34, '?m=do_report&op=do', 0, NULL, NULL),
(44, 99, 2, 'GR Waiting', 43, '?m=gr&op=gr', 0, NULL, NULL),
(45, 99, 2, 'GR Bank', 43, '?m=gr_bank&op=gr', 0, NULL, NULL),
(46, 99, 1, 'Cash Bond', NULL, 'op=cb', 0, NULL, NULL),
(47, 99, 2, 'Cash Bond List', 46, '?m=cashbond&op=cb', 0, NULL, NULL),
(48, 98, 2, 'Cash Bond Bank', 46, '?m=cashbond&rp=bank&op=cb', 0, NULL, NULL),
(49, 99, 1, 'Reimburse sdfs', NULL, 'op=cb', 0, NULL, NULL),
(50, 99, 2, 'Reimburse List', 49, '?m=reimburse&op=cb', 0, NULL, NULL),
(51, 98, 2, 'Reimburse Bank', 49, '?m=reimburse&rp=bank&op=cb', 0, NULL, NULL),
(52, 99, 1, 'Item', NULL, '?m=items_details&op=item', 0, NULL, NULL),
(53, 2, 2, 'RQ Rejected', 1, '?m=fr_reject&op=rq', 0, NULL, NULL),
(54, 1, 2, 'PR Rejected', 5, '?m=pre_reject&op=rq', 0, NULL, NULL),
(55, 1, 2, 'PE Rejected', 8, '?m=pev_reject&op=pe', 0, NULL, NULL),
(56, 2, 2, 'PO Reject', 11, '?m=po_reject&op=po', 0, NULL, NULL),
(57, 2, 2, 'SO Rejected', 15, '?m=so_reject&op=so', 0, NULL, NULL),
(58, 2, 2, 'RFQ Rejected', 19, '?m=rfq_so_reject&op=so', 0, NULL, NULL),
(59, 2, 2, 'SE Rejected', 22, '?m=se_reject&op=se', 0, NULL, NULL),
(60, 2, 2, 'WO Rejected', 25, '?m=wo_reject&op=wo', 0, NULL, NULL),
(61, 1, 2, 'PO Report', 11, '?m=report&op=po', 0, NULL, NULL),
(62, 1, 2, 'WO Report', 25, '?m=wo_report&op=wo', 0, NULL, NULL),
(106, 99, 1, 'Policy Category', NULL, '?m=policy_category&op=policy', 0, NULL, NULL),
(65, 100, 1, 'Cold Storage', NULL, 'op=coldstorage', 0, NULL, NULL),
(66, 100, 2, 'Cold Storage Waiting', 65, '?m=cold_storage_list&op=coldstorage', 0, NULL, NULL),
(67, 99, 2, 'Cold Storage Bank', 65, '?m=cold_storage&op=coldstorage', 0, NULL, NULL),
(68, 100, 1, 'Tax', NULL, '../finance/index2.php?m=tax_all&op=tax', 0, NULL, NULL),
(69, 99, 2, 'Tax In', 68, '../finance/index2.php?m=tax_in&op=tax', 0, NULL, NULL),
(70, 98, 2, 'Tax Out', 68, '../finance/index2.php?m=tax_out&op=tax', 0, NULL, NULL),
(71, 100, 2, 'Tax All', 68, '../finance/index2.php?m=tax_all&op=tax', 0, NULL, NULL),
(105, 100, 1, 'Policy', NULL, 'op=policy', 0, NULL, NULL),
(104, 100, 1, 'Documents', NULL, '?m=documents&op=docs', 0, NULL, NULL),
(107, 98, 2, 'Policy Bank', 105, '?m=policy_main&st=bk&op=policy', 0, NULL, NULL),
(108, 99, 2, 'Policy Waiting', 105, '?m=policy_wait&op=policy', 0, NULL, NULL),
(109, 100, 2, 'Policy All', 105, '?m=policy_main&op=policy', 0, NULL, NULL),
(110, 100, 1, 'Letter In', NULL, '?m=li_main&op=letter_in', 0, NULL, NULL),
(111, 100, 1, 'Letter Out', NULL, '?m=lo_main&op=letter_out', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu_role`
--

CREATE TABLE `menu_role` (
  `menu_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menuheader`
--

CREATE TABLE `menuheader` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '	',
  `position` int(11) DEFAULT '0',
  `category` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menuheaders`
--

CREATE TABLE `menuheaders` (
  `menu_id` int(10) UNSIGNED NOT NULL,
  `header_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_adminarea` varchar(255) CHARACTER SET utf8 DEFAULT 'NO',
  `icon` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `path_icon` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `is_link` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `level` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `miss`
--

CREATE TABLE `miss` (
  `miss_id` int(10) NOT NULL,
  `tittle` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `deskripsi` text,
  `o_level` varchar(10) DEFAULT NULL,
  `approved` varchar(250) DEFAULT NULL,
  `close` date DEFAULT NULL,
  `pelapor` varchar(10) DEFAULT NULL,
  `pict` varchar(250) DEFAULT NULL,
  `pelapor_follow_up` varchar(250) DEFAULT NULL,
  `follow_up_task` text,
  `division` varchar(10) NOT NULL,
  `pict_follow` varchar(250) DEFAULT NULL,
  `pelapor_name` varchar(250) DEFAULT NULL,
  `warning_letter` varchar(250) DEFAULT NULL,
  `prj` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `miss`
--

INSERT INTO `miss` (`miss_id`, `tittle`, `date`, `deskripsi`, `o_level`, `approved`, `close`, `pelapor`, `pict`, `pelapor_follow_up`, `follow_up_task`, `division`, `pict_follow`, `pelapor_name`, `warning_letter`, `prj`) VALUES
(1, 'CXZCZXCZXC', '2017-04-21', 'zxczxczc', NULL, NULL, NULL, '1', NULL, NULL, NULL, '', NULL, 'superuser', NULL, '342');

-- --------------------------------------------------------

--
-- Table structure for table `mtg_absence`
--

CREATE TABLE `mtg_absence` (
  `id_absence` int(11) NOT NULL,
  `id_main` int(100) DEFAULT NULL,
  `plan_name` varchar(200) DEFAULT NULL,
  `emp_name` varchar(100) DEFAULT NULL,
  `sig_address` varchar(250) DEFAULT NULL,
  `redir_id` int(100) DEFAULT NULL,
  `emp_position` varchar(300) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mtg_absence`
--

INSERT INTO `mtg_absence` (`id_absence`, `id_main`, `plan_name`, `emp_name`, `sig_address`, `redir_id`, `emp_position`, `phone`, `email`) VALUES
(1, 1, NULL, 'fgbfgfg', NULL, NULL, 'fgb', 'fgb', 'fgbgf'),
(2, 1, NULL, 'fdfgdg', NULL, NULL, 'dfg', 'dfdg', 'husni.mubarok@outlook.com');

-- --------------------------------------------------------

--
-- Table structure for table `mtg_main`
--

CREATE TABLE `mtg_main` (
  `id_main` int(50) NOT NULL,
  `topic` varchar(400) NOT NULL,
  `location` varchar(200) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `date_main` datetime NOT NULL,
  `date_end` datetime DEFAULT NULL,
  `progress` varchar(200) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'created'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mtg_main`
--

INSERT INTO `mtg_main` (`id_main`, `topic`, `location`, `created_by`, `date_main`, `date_end`, `progress`, `status`) VALUES
(1, 'ewr', 'ewr', 'superuser', '2017-04-15 00:00:00', '2017-04-08 00:00:00', '', 'created'),
(2, 'bfb', 'fgb', 'superuser', '2017-05-27 12:00:00', '2017-05-26 02:00:00', '', 'created');

-- --------------------------------------------------------

--
-- Table structure for table `mtg_mom`
--

CREATE TABLE `mtg_mom` (
  `id_mom` int(50) NOT NULL,
  `id_main` int(50) NOT NULL,
  `content` text NOT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `floor` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mv_absence`
--

CREATE TABLE `mv_absence` (
  `id_absence` int(11) NOT NULL,
  `id_main` int(100) DEFAULT NULL,
  `emp_name` varchar(100) DEFAULT NULL,
  `sig_address` varchar(250) DEFAULT NULL,
  `redir_id` int(100) DEFAULT NULL,
  `emp_position` varchar(300) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mv_absence`
--

INSERT INTO `mv_absence` (`id_absence`, `id_main`, `emp_name`, `sig_address`, `redir_id`, `emp_position`, `phone`, `email`) VALUES
(1, 1, 'rtert', 'img/rtert', NULL, 'ertr', 'etr', 'ert');

-- --------------------------------------------------------

--
-- Table structure for table `mv_attach`
--

CREATE TABLE `mv_attach` (
  `attach_id` int(11) NOT NULL,
  `id_main` int(10) DEFAULT NULL,
  `attach_pic` varchar(250) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mv_attach`
--

INSERT INTO `mv_attach` (`attach_id`, `id_main`, `attach_pic`, `type`, `date_time`) VALUES
(2, 1, 'mv_archive/attach_pic_679_uang.docx', 'docx', '2017-05-12 11:32:30');

-- --------------------------------------------------------

--
-- Table structure for table `mv_main`
--

CREATE TABLE `mv_main` (
  `id_main` int(50) NOT NULL,
  `topic` varchar(200) NOT NULL,
  `location` varchar(200) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `date_main` datetime NOT NULL,
  `progress` varchar(200) NOT NULL,
  `approved_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mv_main`
--

INSERT INTO `mv_main` (`id_main`, `topic`, `location`, `created_by`, `date_main`, `progress`, `approved_by`) VALUES
(1, 'erte', 'ertert', 'superuser', '2017-05-12 11:29:55', '', NULL),
(2, 'SDF', 'SDFSDF', 'superuser', '2017-05-12 11:33:43', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mv_mom`
--

CREATE TABLE `mv_mom` (
  `id_mom` int(50) NOT NULL,
  `id_main` int(50) NOT NULL,
  `content` text NOT NULL,
  `pic` varchar(100) NOT NULL,
  `deadline` date NOT NULL,
  `input_time` datetime DEFAULT NULL,
  `floor` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mv_mom`
--

INSERT INTO `mv_mom` (`id_mom`, `id_main`, `content`, `pic`, `deadline`, `input_time`, `floor`) VALUES
(1, 1, '<p>sdfsdf</p>', 'dsfdsf', '2017-05-19', '2017-05-12 11:31:49', 'sdfsdfd'),
(2, 1, '<p>sdfsfd</p>', 'sdfsdf', '2017-05-17', '2017-05-12 11:31:58', 'sdfsdf'),
(3, 1, '<p>SDF</p>', 'SDF', '2017-05-13', '2017-05-12 11:33:36', 'SDF');

-- --------------------------------------------------------

--
-- Table structure for table `near_miss`
--

CREATE TABLE `near_miss` (
  `near_miss_id` int(10) NOT NULL,
  `tittle` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `deskripsi` text,
  `o_level` varchar(10) DEFAULT NULL,
  `approved` varchar(250) DEFAULT NULL,
  `close` date DEFAULT NULL,
  `pelapor` varchar(10) DEFAULT NULL,
  `pict` varchar(250) DEFAULT NULL,
  `pelapor_follow_up` varchar(250) DEFAULT NULL,
  `follow_up_task` text,
  `division` varchar(10) NOT NULL,
  `pict_follow` varchar(250) DEFAULT NULL,
  `pelapor_name` varchar(250) DEFAULT NULL,
  `warning_letter` varchar(250) DEFAULT NULL,
  `prj` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `o_level`
--

CREATE TABLE `o_level` (
  `id` int(10) NOT NULL,
  `level` varchar(7) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paper_permit`
--

CREATE TABLE `paper_permit` (
  `id_paper_permit` int(10) NOT NULL,
  `author` varchar(200) DEFAULT NULL,
  `purpose` text,
  `nama_paper` varchar(10) DEFAULT NULL,
  `kode` varchar(30) DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paper_permit_data`
--

CREATE TABLE `paper_permit_data` (
  `id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `issued_date` date DEFAULT NULL,
  `issued_by` varchar(200) DEFAULT NULL,
  `paper_num` varchar(100) DEFAULT NULL,
  `vendor` varchar(200) DEFAULT NULL,
  `author` varchar(200) DEFAULT NULL,
  `purpose` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pc_report`
--

CREATE TABLE `pc_report` (
  `id` int(11) NOT NULL,
  `report_num` text,
  `report_date` datetime DEFAULT NULL,
  `mgr_div` text,
  `staff` text,
  `po_out_inv_idr` text,
  `po_out_inv_usd` text,
  `po_out_inv_pay` text,
  `po_out_prj_idr` text,
  `po_out_prj_usd` text,
  `po_out_prj_pay` text,
  `po_out_ga_idr` text,
  `po_out_ga_usd` text,
  `po_out_ga_pay` text,
  `wo_out_inv_idr` text,
  `wo_out_inv_usd` text,
  `wo_out_inv_pay` text,
  `wo_out_prj_idr` text,
  `wo_out_prj_usd` text,
  `wo_out_prj_pay` text,
  `wo_out_ga_idr` text,
  `wo_out_ga_usd` text,
  `wo_out_ga_pay` text,
  `po_ev_inv_idr` text,
  `po_ev_inv_usd` text,
  `po_ev_inv_pay` text,
  `po_ev_prj_idr` text,
  `po_ev_prj_usd` text,
  `po_ev_prj_pay` text,
  `po_ev_ga_idr` text,
  `po_ev_ga_usd` text,
  `po_ev_ga_pay` text,
  `wo_ev_inv_idr` text,
  `wo_ev_inv_usd` text,
  `wo_ev_inv_pay` text,
  `wo_ev_prj_idr` text,
  `wo_ev_prj_usd` text,
  `wo_ev_prj_pay` text,
  `wo_ev_ga_idr` text,
  `wo_ev_ga_usd` text,
  `wo_ev_ga_pay` text,
  `po_inv_time` text,
  `po_prj_time` text,
  `po_ga_time` text,
  `wo_inv_time` date DEFAULT NULL,
  `wo_prj_time` date DEFAULT NULL,
  `wo_ga_time` date DEFAULT NULL,
  `wo_inv_prog` text,
  `wo_prj_prog` text,
  `wo_ga_prog` text,
  `powo_val` text,
  `powo_ket` text,
  `black_vendor` text,
  `black_vendor_ket` text,
  `ket` text,
  `po_out_inv_jml` int(10) DEFAULT NULL,
  `po_out_prj_jml` int(10) DEFAULT NULL,
  `po_out_ga_jml` int(10) DEFAULT NULL,
  `wo_out_inv_jml` int(10) DEFAULT NULL,
  `wo_out_prj_jml` int(10) DEFAULT NULL,
  `wo_out_ga_jml` int(10) DEFAULT NULL,
  `po_ev_inv_jml` int(10) DEFAULT NULL,
  `po_ev_prj_jml` int(10) DEFAULT NULL,
  `po_ev_ga_jml` int(10) DEFAULT NULL,
  `wo_ev_inv_jml` int(10) DEFAULT NULL,
  `wo_ev_prj_jml` int(10) DEFAULT NULL,
  `wo_ev_ga_jml` int(10) DEFAULT NULL,
  `po_inv_jml` int(10) DEFAULT NULL,
  `po_prj_jml` int(10) DEFAULT NULL,
  `po_ga_jml` int(10) DEFAULT NULL,
  `wo_inv_jml` int(10) DEFAULT NULL,
  `wo_prj_jml` int(10) DEFAULT NULL,
  `wo_ga_jml` int(10) DEFAULT NULL,
  `po_inv_tot` text,
  `po_prj_tot` text,
  `po_ga_tot` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id` int(5) NOT NULL,
  `creator` varchar(200) NOT NULL,
  `content` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `is_special` tinyint(4) NOT NULL DEFAULT '0',
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `policy_attachment`
--

CREATE TABLE `policy_attachment` (
  `id` int(11) NOT NULL,
  `id_policy_detail` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `policy_attachment`
--

INSERT INTO `policy_attachment` (`id`, `id_policy_detail`, `name`, `type`) VALUES
(1, 1, 'uang.docx', 'application/vnd.openxmlformats-officedocument.word');

-- --------------------------------------------------------

--
-- Table structure for table `policy_category`
--

CREATE TABLE `policy_category` (
  `id_category` int(11) NOT NULL,
  `name_category` varchar(400) NOT NULL DEFAULT 'EMPTY',
  `created_by` varchar(200) DEFAULT NULL,
  `last_update_by` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `policy_category`
--

INSERT INTO `policy_category` (`id_category`, `name_category`, `created_by`, `last_update_by`) VALUES
(1, 'DSADA', 'superuser', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `policy_detail`
--

CREATE TABLE `policy_detail` (
  `id_detail` int(50) NOT NULL,
  `id_policy_main` int(50) NOT NULL,
  `content` text NOT NULL,
  `content_eng` text NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `date_detail` datetime NOT NULL,
  `approved_by` varchar(200) DEFAULT NULL,
  `approved_time` date DEFAULT NULL,
  `acknowledge_by` varchar(200) DEFAULT NULL,
  `acknowledge_date` date DEFAULT NULL,
  `revision` int(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `policy_detail`
--

INSERT INTO `policy_detail` (`id_detail`, `id_policy_main`, `content`, `content_eng`, `created_by`, `date_detail`, `approved_by`, `approved_time`, `acknowledge_by`, `acknowledge_date`, `revision`) VALUES
(1, 1, '<p>asds sadas</p>', '<p>asdads</p>', 'superuser', '2017-04-17 10:50:46', NULL, NULL, NULL, NULL, NULL),
(3, 2, '<p>sdfdf</p>', '<p>sdfsf</p>', 'superuser', '2017-04-18 04:49:13', NULL, NULL, NULL, NULL, NULL),
(4, 2, '<p>sdfdf dsfsdfdsf</p>', '<p>sdfsf</p>', 'superuser', '2017-04-18 05:08:39', NULL, NULL, NULL, NULL, NULL),
(5, 3, '<p>fwe</p>', '<p>fw</p>', 'superuser', '2017-04-18 08:35:57', NULL, NULL, 'superuser', '2017-04-18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `policy_main`
--

CREATE TABLE `policy_main` (
  `id_main` int(50) NOT NULL,
  `topic` varchar(200) NOT NULL,
  `location` varchar(200) NOT NULL,
  `date_main` datetime NOT NULL,
  `category` int(11) NOT NULL DEFAULT '1',
  `main_created_by` varchar(50) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `policy_main`
--

INSERT INTO `policy_main` (`id_main`, `topic`, `location`, `date_main`, `category`, `main_created_by`) VALUES
(1, 'sddasd', 'operation', '2017-04-17 10:50:08', 1, 'superuser'),
(2, 'sd sadad', 'operation', '2017-04-18 02:50:26', 1, 'superuser'),
(3, 'wef', 'operation', '2017-04-18 08:35:50', 1, 'superuser');

-- --------------------------------------------------------

--
-- Table structure for table `presentation`
--

CREATE TABLE `presentation` (
  `id` int(11) NOT NULL,
  `name` varchar(400) NOT NULL,
  `address` varchar(400) NOT NULL,
  `uploader` varchar(400) NOT NULL,
  `upload_time` date NOT NULL,
  `category` varchar(400) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `prev_main`
--

CREATE TABLE `prev_main` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `req_by` varchar(50) NOT NULL,
  `req_date` date NOT NULL,
  `division` varchar(50) DEFAULT NULL,
  `dir_app` date DEFAULT NULL,
  `dir_rjt` date DEFAULT NULL,
  `dir_note` text,
  `dir_name` varchar(10) DEFAULT NULL,
  `ceo_app` date DEFAULT NULL,
  `ceo_rjt` date DEFAULT NULL,
  `ceo_note` text,
  `final_act` text,
  `final_date` date DEFAULT NULL,
  `final_by` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `status_note` text,
  `status_date` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prev_main_old`
--

CREATE TABLE `prev_main_old` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `type` varchar(50) NOT NULL,
  `req_by` varchar(50) NOT NULL,
  `req_date` date NOT NULL,
  `status` date DEFAULT NULL,
  `division` varchar(200) DEFAULT NULL,
  `manager_name` varchar(200) DEFAULT NULL,
  `manager_approve` date DEFAULT NULL,
  `director_name` varchar(200) DEFAULT NULL,
  `director_approve` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prj_web`
--

CREATE TABLE `prj_web` (
  `id` int(11) NOT NULL,
  `prj_name` varchar(300) NOT NULL,
  `location` varchar(300) NOT NULL,
  `deskripsi` text NOT NULL,
  `direktori` varchar(100) NOT NULL,
  `ukuran` int(11) NOT NULL,
  `online` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE `provinces` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pt_dev_category`
--

CREATE TABLE `pt_dev_category` (
  `id_cat` int(11) NOT NULL,
  `name_cat` varchar(200) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `restricted` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pt_dev_comment`
--

CREATE TABLE `pt_dev_comment` (
  `id_comment` int(11) NOT NULL,
  `id_main` int(11) NOT NULL,
  `user` varchar(200) NOT NULL,
  `post_date` date NOT NULL,
  `content` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pt_dev_jobs`
--

CREATE TABLE `pt_dev_jobs` (
  `id_job` int(11) NOT NULL,
  `id_main` int(11) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `important` int(1) NOT NULL DEFAULT '0',
  `pct` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pt_dev_main`
--

CREATE TABLE `pt_dev_main` (
  `id_main` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `category` int(11) DEFAULT NULL,
  `author` varchar(200) NOT NULL,
  `created_date` date NOT NULL,
  `percentage` int(3) NOT NULL,
  `deadline` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `q_absence`
--

CREATE TABLE `q_absence` (
  `id_absence` int(11) NOT NULL,
  `id_main` int(100) DEFAULT NULL,
  `emp_name` varchar(100) DEFAULT NULL,
  `sig_address` varchar(250) DEFAULT NULL,
  `redir_id` int(100) DEFAULT NULL,
  `emp_position` varchar(300) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `tgl` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quiz_detail`
--

CREATE TABLE `quiz_detail` (
  `id` int(11) NOT NULL,
  `id_fk` int(11) NOT NULL,
  `question` varchar(300) NOT NULL,
  `answer` varchar(300) NOT NULL,
  `author` varchar(20) NOT NULL,
  `division` varchar(30) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qz_quiz_main`
--

CREATE TABLE `qz_quiz_main` (
  `id_quiz` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `jadwal` varchar(30) NOT NULL,
  `status` varchar(10) NOT NULL,
  `div1` varchar(20) NOT NULL,
  `div2` varchar(20) NOT NULL,
  `score_div1` int(11) NOT NULL,
  `score_div2` int(11) NOT NULL,
  `jadwal_div1` datetime DEFAULT NULL,
  `jadwal_div2` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qz_user`
--

CREATE TABLE `qz_user` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `division` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `redir`
--

CREATE TABLE `redir` (
  `id` int(5) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `pin` varchar(200) DEFAULT NULL,
  `sig_password` varchar(100) DEFAULT NULL,
  `level` varchar(200) NOT NULL,
  `o_level` varchar(20) NOT NULL,
  `asset_u` varchar(32) DEFAULT NULL,
  `hrd_u` varchar(32) DEFAULT NULL,
  `finance_u` varchar(32) DEFAULT NULL,
  `status` text,
  `status_date` date DEFAULT NULL,
  `asset_level` enum('operator','it','sec','admin','fin','ast_fin','mar','hrd') NOT NULL,
  `asset_division` enum('GM','IT','Finance','Asset','Procurement','Marketing','HRD','QC','Operation','Secretary','admin') DEFAULT NULL,
  `hrd_level` enum('operator','it','sec','admin','fin','ast_fin','mar','hrd') NOT NULL,
  `hrd_division` enum('GM','IT','Finance','Asset','Procurement','Marketing','HRD','QC','Operation','Secretary','admin') NOT NULL,
  `hrd_can_see` text,
  `mkt_level` enum('operator','it','sec','admin') NOT NULL,
  `ops_level` enum('operator','it','sec','admin') NOT NULL,
  `pro_level` enum('operator','it','sec','admin') NOT NULL,
  `tec_level` enum('operator','it','sec','admin') NOT NULL,
  `gm_level` enum('operator','it','sec','admin') DEFAULT NULL,
  `it_level` enum('operator','it','sec','admin') DEFAULT NULL,
  `proll_level` enum('operator','it','sec','admin') DEFAULT NULL,
  `img` varchar(200) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `sig_file` varchar(200) DEFAULT NULL,
  `enable_name` int(1) NOT NULL,
  `doc_pass` varchar(50) DEFAULT NULL,
  `status_user` varchar(50) DEFAULT NULL,
  `ket_status_user` text,
  `last_update` date DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `cond` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `redir`
--

INSERT INTO `redir` (`id`, `emp_id`, `username`, `password`, `pin`, `sig_password`, `level`, `o_level`, `asset_u`, `hrd_u`, `finance_u`, `status`, `status_date`, `asset_level`, `asset_division`, `hrd_level`, `hrd_division`, `hrd_can_see`, `mkt_level`, `ops_level`, `pro_level`, `tec_level`, `gm_level`, `it_level`, `proll_level`, `img`, `email`, `sig_file`, `enable_name`, `doc_pass`, `status_user`, `ket_status_user`, `last_update`, `last_seen`, `last_login`, `cond`) VALUES
(1, 0, 'superuser', '202cb962ac59075b964b07152d234b70', '371554', NULL, 'admin', '1-0-0', 'superuser', 'superuser', 'superuser', 'tower', NULL, 'admin', 'admin', 'admin', 'admin', 'a:9:{i:0;s:5:\"staff\";i:1;s:5:\"field\";i:2;s:5:\"whbin\";i:3;s:5:\"whcil\";i:4;s:7:\"manager\";i:5;s:3:\"bod\";i:6;s:9:\"konsultan\";i:7;s:5:\"local\";i:8;s:9:\"marketing\";}', 'admin', 'admin', 'admin', 'admin', 'admin', 'admin', 'admin', '', 'email@email.com', 'img/3.png', 1, NULL, 'available', NULL, NULL, NULL, NULL, NULL),
(2, 1, 'asdasd', '0aa1ea9a5a04b78d4581dd6d17742627', NULL, NULL, 'user', '1-1-2', 'asdasd', 'asdasd', 'asdasd', 'tower', NULL, '', 'IT', 'operator', '', NULL, 'operator', 'operator', 'operator', 'operator', 'operator', 'it', 'operator', '', 'a', NULL, 1, NULL, 'available', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `redir_changemail`
--

CREATE TABLE `redir_changemail` (
  `id` int(11) NOT NULL,
  `username` varchar(200) DEFAULT NULL,
  `request_by` varchar(200) DEFAULT NULL,
  `request_date` date DEFAULT NULL,
  `old_mail` varchar(200) DEFAULT NULL,
  `new_mail` varchar(200) DEFAULT NULL,
  `approved_by` varchar(200) DEFAULT NULL,
  `approved_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `regionals`
--

CREATE TABLE `regionals` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `initial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `province_id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rent_main`
--

CREATE TABLE `rent_main` (
  `id` int(11) NOT NULL,
  `id_fd` int(11) NOT NULL,
  `peminjam` varchar(30) NOT NULL,
  `approve_by` varchar(30) DEFAULT NULL,
  `approve_date` date DEFAULT NULL,
  `loan_date` date NOT NULL,
  `return_date` date NOT NULL,
  `deskripsi` text NOT NULL,
  `status` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rent_main`
--

INSERT INTO `rent_main` (`id`, `id_fd`, `peminjam`, `approve_by`, `approve_date`, `loan_date`, `return_date`, `deskripsi`, `status`) VALUES
(1, 0, 'superuser', 'superuser', '2017-05-24', '2017-05-20', '2017-05-19', '<p>xzczcx</p>', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `resource`
--

CREATE TABLE `resource` (
  `id` int(11) NOT NULL,
  `id_sub_category` int(10) DEFAULT NULL,
  `name` varchar(400) NOT NULL,
  `address` varchar(400) NOT NULL,
  `uploader` varchar(400) NOT NULL,
  `upload_time` date NOT NULL,
  `category` varchar(400) NOT NULL,
  `thumb_address` varchar(200) DEFAULT NULL,
  `assignment_project` varchar(100) DEFAULT NULL,
  `held_date` date DEFAULT NULL,
  `held_place` varchar(100) DEFAULT NULL,
  `expiration_date` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `resource_log`
--

CREATE TABLE `resource_log` (
  `id` int(100) NOT NULL,
  `assign_time` datetime DEFAULT NULL,
  `assign_by` varchar(100) DEFAULT NULL,
  `project` varchar(100) DEFAULT NULL,
  `item_name` varchar(100) DEFAULT NULL,
  `item_location` varchar(100) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `item_id` int(100) DEFAULT NULL,
  `assign` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `resource_mkt`
--

CREATE TABLE `resource_mkt` (
  `id` int(11) NOT NULL,
  `prj_id` int(11) DEFAULT NULL,
  `name` varchar(400) NOT NULL,
  `address` varchar(400) NOT NULL,
  `uploader` varchar(400) NOT NULL,
  `upload_time` date NOT NULL,
  `category` varchar(400) NOT NULL,
  `approved_by` varchar(50) NOT NULL,
  `approved_date` date NOT NULL,
  `rejected_by` varchar(50) NOT NULL,
  `rejected_date` date NOT NULL,
  `rejected_reason` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ro_detail`
--

CREATE TABLE `ro_detail` (
  `id_detail` int(11) NOT NULL,
  `fid_main` int(11) NOT NULL,
  `item_name` varchar(50) NOT NULL,
  `alasan` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `status_harga` varchar(30) DEFAULT NULL,
  `harga_final` int(11) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ro_item`
--

CREATE TABLE `ro_item` (
  `id_item` int(11) NOT NULL,
  `item_code` varchar(11) NOT NULL,
  `item_name` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ro_main`
--

CREATE TABLE `ro_main` (
  `id_main` int(11) NOT NULL,
  `request_date` date NOT NULL,
  `due_date` date NOT NULL,
  `request_by` varchar(50) NOT NULL,
  `lokasi` text NOT NULL,
  `approval_item_operation` varchar(30) DEFAULT NULL,
  `waktu_approve_ops` varchar(50) DEFAULT NULL,
  `approval_item_finance` varchar(30) DEFAULT NULL,
  `waktu_approve_finance` varchar(50) DEFAULT NULL,
  `approval_item_wakil` varchar(30) DEFAULT NULL,
  `waktu_approve_wakil` varchar(50) DEFAULT NULL,
  `approval_item_dirut` varchar(30) DEFAULT NULL,
  `waktu_approve_dirut` varchar(50) DEFAULT NULL,
  `approval_price_dirut` varchar(30) DEFAULT NULL,
  `waktu_approve_harga` varchar(50) DEFAULT NULL,
  `status_harga` varchar(30) DEFAULT NULL,
  `waktu_input_harga` varchar(50) DEFAULT NULL,
  `Ro_final` varchar(10) DEFAULT NULL,
  `baris` int(11) DEFAULT NULL,
  `status_wod` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ro_price`
--

CREATE TABLE `ro_price` (
  `id` int(11) NOT NULL,
  `fid_main` int(11) NOT NULL,
  `fid_detail` int(11) NOT NULL,
  `harga1` int(11) DEFAULT NULL,
  `vendor1` varchar(50) DEFAULT NULL,
  `harga2` int(11) DEFAULT NULL,
  `vendor2` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rv_absensi`
--

CREATE TABLE `rv_absensi` (
  `id` int(11) NOT NULL,
  `id_topic` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `divisi` varchar(30) DEFAULT NULL,
  `resume` text NOT NULL,
  `kehadiran` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rv_book`
--

CREATE TABLE `rv_book` (
  `id` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `jam_masuk` varchar(5) NOT NULL,
  `jam_keluar` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rv_room`
--

CREATE TABLE `rv_room` (
  `id` int(11) NOT NULL,
  `nama_ruangan` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rv_time_check`
--

CREATE TABLE `rv_time_check` (
  `id` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `id_book` int(11) NOT NULL,
  `id_topic` int(11) DEFAULT NULL,
  `topic_meeting` text,
  `tanggal` date NOT NULL,
  `jam` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rv_topic`
--

CREATE TABLE `rv_topic` (
  `id_topic` int(11) NOT NULL,
  `topic_meeting` text NOT NULL,
  `projek` char(5) NOT NULL,
  `pemimpin_meeting` varchar(50) NOT NULL,
  `notulen` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `id_book` int(11) NOT NULL,
  `kehadiran_pemimpin_meeting` varchar(20) NOT NULL,
  `kehadiran_notulen` varchar(20) NOT NULL,
  `resume_pemimpin_meeting` text NOT NULL,
  `resume_notulen` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `safe_absence`
--

CREATE TABLE `safe_absence` (
  `id_absence` int(11) NOT NULL,
  `id_main` int(100) DEFAULT NULL,
  `emp_name` varchar(100) DEFAULT NULL,
  `sig_address` varchar(250) DEFAULT NULL,
  `redir_id` int(100) DEFAULT NULL,
  `emp_position` varchar(300) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `safe_absence`
--

INSERT INTO `safe_absence` (`id_absence`, `id_main`, `emp_name`, `sig_address`, `redir_id`, `emp_position`, `phone`, `email`) VALUES
(1, 1, 'asc', 'img/authsketch457763.jpg', NULL, 'sac', 'asccs', 'husni.mubarok@outlook.com'),
(2, 2, 'SDFsdf', 'img/SDFsdf0019fecacc1ae87154d85efbc3ac2522.jpg', NULL, 'sdf', '24234', 'husni.mubarok@outlook.com');

-- --------------------------------------------------------

--
-- Table structure for table `safe_main`
--

CREATE TABLE `safe_main` (
  `id_main` int(50) NOT NULL,
  `topic` varchar(200) NOT NULL,
  `location` varchar(200) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `date_main` datetime NOT NULL,
  `progress` varchar(200) NOT NULL,
  `safe_prj` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `safe_main`
--

INSERT INTO `safe_main` (`id_main`, `topic`, `location`, `created_by`, `date_main`, `progress`, `safe_prj`) VALUES
(1, '123', '123', 'superuser', '2017-04-20 02:48:29', '', '342'),
(2, 'asd', 'asd', 'superuser', '2017-05-18 04:18:46', '', '342');

-- --------------------------------------------------------

--
-- Table structure for table `safe_mom`
--

CREATE TABLE `safe_mom` (
  `id_mom` int(50) NOT NULL,
  `id_main` int(50) NOT NULL,
  `content` text NOT NULL,
  `pic` varchar(100) NOT NULL,
  `deadline` date NOT NULL,
  `input_time` datetime DEFAULT NULL,
  `floor` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `safe_mom`
--

INSERT INTO `safe_mom` (`id_mom`, `id_main`, `content`, `pic`, `deadline`, `input_time`, `floor`) VALUES
(1, 1, 'asd', 'asd', '2017-04-20', '2017-04-20 03:36:31', 'asad'),
(2, 2, 'sdfsdf', 'sdfsfd', '2017-06-01', '2017-05-18 08:54:00', 'sdsdfdf');

-- --------------------------------------------------------

--
-- Table structure for table `scan`
--

CREATE TABLE `scan` (
  `id` int(11) NOT NULL,
  `nama_rs` varchar(50) DEFAULT NULL,
  `owner_name` varchar(100) DEFAULT NULL,
  `nama_file` varchar(50) DEFAULT NULL,
  `tgl_file` date DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `doctor_name` varchar(50) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scan_detail`
--

CREATE TABLE `scan_detail` (
  `id` int(11) NOT NULL,
  `id_main` int(11) NOT NULL,
  `location` text NOT NULL,
  `tgl_upload` datetime DEFAULT NULL,
  `nama_file` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service_ticketing_detail`
--

CREATE TABLE `service_ticketing_detail` (
  `id` int(11) NOT NULL,
  `id_st` int(11) NOT NULL,
  `path` text,
  `ukuran` int(11) DEFAULT NULL,
  `pesan` text NOT NULL,
  `username` varchar(100) NOT NULL,
  `date_chat` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service_ticketing_log`
--

CREATE TABLE `service_ticketing_log` (
  `id` int(11) NOT NULL,
  `id_st` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `log_date` datetime NOT NULL,
  `kondisi` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service_ticketing_main`
--

CREATE TABLE `service_ticketing_main` (
  `id` int(11) NOT NULL,
  `jenis_masalah` varchar(30) NOT NULL,
  `kategori_masalah` varchar(50) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `isi_pesan` text NOT NULL,
  `username` varchar(30) NOT NULL,
  `create_date` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `exp_date` datetime DEFAULT NULL,
  `service_type` varchar(50) DEFAULT NULL,
  `app_date` date DEFAULT NULL,
  `status_app` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `software`
--

CREATE TABLE `software` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `software_upload` varchar(100) DEFAULT NULL,
  `uploaded_by` varchar(50) DEFAULT NULL,
  `upload_time` datetime DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sop_category`
--

CREATE TABLE `sop_category` (
  `id_category` int(11) NOT NULL,
  `name_category` varchar(400) NOT NULL DEFAULT 'EMPTY',
  `created_by` varchar(200) DEFAULT NULL,
  `last_update_by` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sop_category`
--

INSERT INTO `sop_category` (`id_category`, `name_category`, `created_by`, `last_update_by`) VALUES
(1, 'KATEGORI 1', 'superuser', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sop_detail`
--

CREATE TABLE `sop_detail` (
  `id_detail` int(50) NOT NULL,
  `id_sop_main` int(50) NOT NULL,
  `content` text NOT NULL,
  `content_eng` text NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `date_detail` datetime NOT NULL,
  `approved_by` varchar(200) DEFAULT NULL,
  `approved_time` date DEFAULT NULL,
  `acknowledge_by` varchar(200) DEFAULT NULL,
  `acknowledge_date` date DEFAULT NULL,
  `revision` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sop_detail`
--

INSERT INTO `sop_detail` (`id_detail`, `id_sop_main`, `content`, `content_eng`, `created_by`, `date_detail`, `approved_by`, `approved_time`, `acknowledge_by`, `acknowledge_date`, `revision`) VALUES
(1, 1, '<p>sdfds fdgfgdfgdf</p>', '<p>sdfsdff</p>', 'superuser', '2017-05-19 11:25:47', NULL, NULL, 'superuser', '2017-05-19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sop_main`
--

CREATE TABLE `sop_main` (
  `id_main` int(50) NOT NULL,
  `topic` varchar(200) NOT NULL,
  `location` varchar(200) NOT NULL,
  `date_main` datetime NOT NULL,
  `category` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sop_main`
--

INSERT INTO `sop_main` (`id_main`, `topic`, `location`, `date_main`, `category`) VALUES
(1, 'asd', 'operation', '2017-04-20 02:54:28', 1),
(2, 'SDFsdf', 'qc', '2017-05-19 11:25:40', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp`
--

CREATE TABLE `sp` (
  `id` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `alasan` text NOT NULL,
  `tanggal_terbit` date NOT NULL,
  `pembuat` varchar(50) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `employee_negotiation` text,
  `sp` varchar(5) DEFAULT NULL,
  `tanggal_approve` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `topik`
--

CREATE TABLE `topik` (
  `id_topik` int(100) NOT NULL,
  `id_forum` int(100) NOT NULL,
  `emp_id` int(100) NOT NULL,
  `nama_topik` varchar(250) NOT NULL,
  `desc_topik` text NOT NULL,
  `date_topik` datetime DEFAULT NULL,
  `baca` varchar(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `training_type`
--

CREATE TABLE `training_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `travel_detail_main`
--

CREATE TABLE `travel_detail_main` (
  `id` int(11) NOT NULL,
  `name` text,
  `nation` varchar(200) DEFAULT NULL,
  `address` text,
  `email` varchar(200) DEFAULT NULL,
  `cost_rate` varchar(200) DEFAULT NULL,
  `airport` varchar(200) DEFAULT NULL,
  `rent_car` varchar(200) DEFAULT NULL,
  `rent_car_phone` varchar(200) DEFAULT NULL,
  `suggestion` text,
  `phone` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `troubleshoot`
--

CREATE TABLE `troubleshoot` (
  `troubleshoot_id` int(10) NOT NULL,
  `tittle` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `deskripsi` text,
  `o_level` varchar(10) DEFAULT NULL,
  `approved` varchar(250) DEFAULT NULL,
  `close` date DEFAULT NULL,
  `pelapor` varchar(10) DEFAULT NULL,
  `pict` varchar(250) DEFAULT NULL,
  `pelapor_follow_up` varchar(250) DEFAULT NULL,
  `follow_up_task` text,
  `division` varchar(10) NOT NULL,
  `pict_follow` varchar(250) DEFAULT NULL,
  `pelapor_name` varchar(250) DEFAULT NULL,
  `warning_letter` varchar(250) DEFAULT NULL,
  `prj` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `troubleshoot`
--

INSERT INTO `troubleshoot` (`troubleshoot_id`, `tittle`, `date`, `deskripsi`, `o_level`, `approved`, `close`, `pelapor`, `pict`, `pelapor_follow_up`, `follow_up_task`, `division`, `pict_follow`, `pelapor_name`, `warning_letter`, `prj`) VALUES
(1, 'ZXX', '2017-05-26', 'ZX', NULL, NULL, NULL, '1', 'troubleshoot_archive/225_PL IMS.png', 'superuser', '', '', 'troubleshoot_archive/594_Picture1.png', 'superuser', 'troubleshoot_archive/WI_662_Picture1.png', '342'),
(2, 'ASD', '2017-05-18', 'asd', NULL, NULL, NULL, '1', 'troubleshoot_archive/625_PL IMS.png', 'superuser', 'sdfdsfdsf', '', NULL, 'superuser', NULL, '342'),
(3, 'AC', '2017-05-12', 'cacas', NULL, NULL, NULL, '1', NULL, 'superuser', 'dfgdgdfg', '', NULL, 'superuser', 'troubleshoot_archive/WI_694_54eb9e61c067c_-_gerbera-daisy-orange-xl.jpg', '342');

-- --------------------------------------------------------

--
-- Table structure for table `twr_dokumen`
--

CREATE TABLE `twr_dokumen` (
  `id` int(11) NOT NULL,
  `nama_dokumen` varchar(50) NOT NULL,
  `direktori` text NOT NULL,
  `ukuran` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `twr_drawtech`
--

CREATE TABLE `twr_drawtech` (
  `id` int(11) NOT NULL,
  `nama_gambar` varchar(50) NOT NULL,
  `direktori` text,
  `ukuran` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `twr_telepon`
--

CREATE TABLE `twr_telepon` (
  `id` int(11) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `lantai` int(11) NOT NULL,
  `telepon` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_employees`
--

CREATE TABLE `user_employees` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_branches`
--

CREATE TABLE `v_branches` (
  `id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `initial` varchar(255) DEFAULT NULL,
  `regional_id` int(10) UNSIGNED DEFAULT NULL,
  `regional_name` varchar(255) DEFAULT NULL,
  `regional_initial` varchar(255) DEFAULT NULL,
  `province_id` int(10) UNSIGNED DEFAULT NULL,
  `company_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_regionals`
--

CREATE TABLE `v_regionals` (
  `id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `initial` varchar(255) DEFAULT NULL,
  `province_id` int(10) UNSIGNED DEFAULT NULL,
  `company_id` int(10) UNSIGNED DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `province_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vacancy`
--

CREATE TABLE `vacancy` (
  `id` int(11) NOT NULL,
  `position` varchar(200) NOT NULL,
  `description` varchar(300) NOT NULL,
  `classification` varchar(100) NOT NULL,
  `start_time` date NOT NULL,
  `end_time` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `id` int(11) NOT NULL,
  `nama_vendor` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `no_telpon` varchar(20) NOT NULL,
  `bank` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE `version` (
  `id` int(11) NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `changelog` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `view_button`
--

CREATE TABLE `view_button` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `path` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `location` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `view_div`
--

CREATE TABLE `view_div` (
  `id_view_div` int(11) NOT NULL,
  `asset_division` varchar(100) DEFAULT NULL,
  `first_level` text,
  `second_level` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wall`
--

CREATE TABLE `wall` (
  `id` int(6) NOT NULL,
  `date` datetime NOT NULL,
  `author` int(5) NOT NULL,
  `content` text NOT NULL,
  `username` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wall_count`
--

CREATE TABLE `wall_count` (
  `wall_count_id` int(11) NOT NULL,
  `wall_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ok` int(11) DEFAULT NULL,
  `bad` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `warehouses`
--

CREATE TABLE `warehouses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `initial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wi_detail`
--

CREATE TABLE `wi_detail` (
  `id_detail` int(50) NOT NULL,
  `id_wi_main` int(50) NOT NULL,
  `content` text NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `date_detail` datetime NOT NULL,
  `approved_by` varchar(200) DEFAULT NULL,
  `approved_time` date DEFAULT NULL,
  `acknowledge_by` varchar(200) DEFAULT NULL,
  `acknowledge_date` date DEFAULT NULL,
  `revision` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wi_main`
--

CREATE TABLE `wi_main` (
  `id_main` int(50) NOT NULL,
  `topic` varchar(200) NOT NULL,
  `location` varchar(200) NOT NULL,
  `date_main` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wod_detail`
--

CREATE TABLE `wod_detail` (
  `id_detail` int(11) NOT NULL,
  `fid_main` int(11) NOT NULL,
  `nama_item` varchar(50) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wod_main`
--

CREATE TABLE `wod_main` (
  `id_main` int(11) NOT NULL,
  `id_ro` int(11) DEFAULT NULL,
  `no_surat` varchar(50) NOT NULL,
  `vendor` varchar(50) NOT NULL,
  `lokasi` varchar(50) NOT NULL,
  `request_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applicant`
--
ALTER TABLE `applicant`
  ADD PRIMARY KEY (`app_id`),
  ADD UNIQUE KEY `nama` (`nama`),
  ADD UNIQUE KEY `nama_2` (`nama`);

--
-- Indexes for table `appraisal_detail`
--
ALTER TABLE `appraisal_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appraisal_img`
--
ALTER TABLE `appraisal_img`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appraisal_main`
--
ALTER TABLE `appraisal_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ar`
--
ALTER TABLE `ar`
  ADD PRIMARY KEY (`ar_id`);

--
-- Indexes for table `attend_log`
--
ALTER TABLE `attend_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attend_web`
--
ALTER TABLE `attend_web`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attend_web_new`
--
ALTER TABLE `attend_web_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auto_gather_server`
--
ALTER TABLE `auto_gather_server`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_fm`
--
ALTER TABLE `bank_fm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_fm_detail`
--
ALTER TABLE `bank_fm_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_ru`
--
ALTER TABLE `bank_ru`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_ru_detail`
--
ALTER TABLE `bank_ru_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `berkas`
--
ALTER TABLE `berkas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bugs`
--
ALTER TABLE `bugs`
  ADD PRIMARY KEY (`bugs_id`);

--
-- Indexes for table `ci_absence`
--
ALTER TABLE `ci_absence`
  ADD PRIMARY KEY (`id_absence`);

--
-- Indexes for table `ci_attach`
--
ALTER TABLE `ci_attach`
  ADD PRIMARY KEY (`attach_id`);

--
-- Indexes for table `ci_main`
--
ALTER TABLE `ci_main`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `ci_mom`
--
ALTER TABLE `ci_mom`
  ADD PRIMARY KEY (`id_mom`);

--
-- Indexes for table `client_main`
--
ALTER TABLE `client_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id_comment`);

--
-- Indexes for table `contacs`
--
ALTER TABLE `contacs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_absence`
--
ALTER TABLE `cs_absence`
  ADD PRIMARY KEY (`id_absence`);

--
-- Indexes for table `cs_attach`
--
ALTER TABLE `cs_attach`
  ADD PRIMARY KEY (`attach_id`);

--
-- Indexes for table `cs_main`
--
ALTER TABLE `cs_main`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `cs_mom`
--
ALTER TABLE `cs_mom`
  ADD PRIMARY KEY (`id_mom`);

--
-- Indexes for table `csr_main`
--
ALTER TABLE `csr_main`
  ADD PRIMARY KEY (`csr_id`);

--
-- Indexes for table `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dl_class`
--
ALTER TABLE `dl_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dl_main`
--
ALTER TABLE `dl_main`
  ADD PRIMARY KEY (`dl_id`);

--
-- Indexes for table `doc_center_datadoc`
--
ALTER TABLE `doc_center_datadoc`
  ADD PRIMARY KEY (`id_doc`);

--
-- Indexes for table `doc_center_historyy`
--
ALTER TABLE `doc_center_historyy`
  ADD PRIMARY KEY (`id_history`);

--
-- Indexes for table `doc_center_peminjam`
--
ALTER TABLE `doc_center_peminjam`
  ADD PRIMARY KEY (`id_peminjam`);

--
-- Indexes for table `doc_center_pengembalian`
--
ALTER TABLE `doc_center_pengembalian`
  ADD PRIMARY KEY (`id_pengembalian`);

--
-- Indexes for table `doc_counter`
--
ALTER TABLE `doc_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doc_rout_history`
--
ALTER TABLE `doc_rout_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doc_rout_login`
--
ALTER TABLE `doc_rout_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doc_rout_main`
--
ALTER TABLE `doc_rout_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doc_rout_ob`
--
ALTER TABLE `doc_rout_ob`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drawing_category`
--
ALTER TABLE `drawing_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drawing_log`
--
ALTER TABLE `drawing_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drawing_status`
--
ALTER TABLE `drawing_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drawing_sub_category`
--
ALTER TABLE `drawing_sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ev_alat`
--
ALTER TABLE `ev_alat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ev_anggota`
--
ALTER TABLE `ev_anggota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ev_dokumentasi_acara`
--
ALTER TABLE `ev_dokumentasi_acara`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ev_jadwal`
--
ALTER TABLE `ev_jadwal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ev_juara`
--
ALTER TABLE `ev_juara`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ev_kekurangan`
--
ALTER TABLE `ev_kekurangan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ev_perusahaan`
--
ALTER TABLE `ev_perusahaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ev_strukturorganisasi`
--
ALTER TABLE `ev_strukturorganisasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ev_susunan_acara`
--
ALTER TABLE `ev_susunan_acara`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_maintenance`
--
ALTER TABLE `form_maintenance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forum`
--
ALTER TABLE `forum`
  ADD PRIMARY KEY (`id_forum`);

--
-- Indexes for table `ins_ru`
--
ALTER TABLE `ins_ru`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ins_ru_detail`
--
ALTER TABLE `ins_ru_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ir_main`
--
ALTER TABLE `ir_main`
  ADD PRIMARY KEY (`ir_id`);

--
-- Indexes for table `it_punish_main`
--
ALTER TABLE `it_punish_main`
  ADD PRIMARY KEY (`it_punish_id`);

--
-- Indexes for table `item_maintenance`
--
ALTER TABLE `item_maintenance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kalender_event`
--
ALTER TABLE `kalender_event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `krt_property`
--
ALTER TABLE `krt_property`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `li`
--
ALTER TABLE `li`
  ADD PRIMARY KEY (`id_li`);

--
-- Indexes for table `li_attach`
--
ALTER TABLE `li_attach`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lo`
--
ALTER TABLE `lo`
  ADD PRIMARY KEY (`id_lo`);

--
-- Indexes for table `log_view`
--
ALTER TABLE `log_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_comment`
--
ALTER TABLE `m_comment`
  ADD PRIMARY KEY (`id_comment`);

--
-- Indexes for table `m_forum`
--
ALTER TABLE `m_forum`
  ADD PRIMARY KEY (`id_forum`);

--
-- Indexes for table `m_topik`
--
ALTER TABLE `m_topik`
  ADD PRIMARY KEY (`id_topik`);

--
-- Indexes for table `master_logo`
--
ALTER TABLE `master_logo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mcu_log`
--
ALTER TABLE `mcu_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mcu_main`
--
ALTER TABLE `mcu_main`
  ADD PRIMARY KEY (`mcu_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `miss`
--
ALTER TABLE `miss`
  ADD PRIMARY KEY (`miss_id`);

--
-- Indexes for table `mtg_absence`
--
ALTER TABLE `mtg_absence`
  ADD PRIMARY KEY (`id_absence`);

--
-- Indexes for table `mtg_main`
--
ALTER TABLE `mtg_main`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `mtg_mom`
--
ALTER TABLE `mtg_mom`
  ADD PRIMARY KEY (`id_mom`);

--
-- Indexes for table `mv_absence`
--
ALTER TABLE `mv_absence`
  ADD PRIMARY KEY (`id_absence`);

--
-- Indexes for table `mv_attach`
--
ALTER TABLE `mv_attach`
  ADD PRIMARY KEY (`attach_id`);

--
-- Indexes for table `mv_main`
--
ALTER TABLE `mv_main`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `mv_mom`
--
ALTER TABLE `mv_mom`
  ADD PRIMARY KEY (`id_mom`);

--
-- Indexes for table `near_miss`
--
ALTER TABLE `near_miss`
  ADD PRIMARY KEY (`near_miss_id`);

--
-- Indexes for table `o_level`
--
ALTER TABLE `o_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paper_permit`
--
ALTER TABLE `paper_permit`
  ADD PRIMARY KEY (`id_paper_permit`);

--
-- Indexes for table `paper_permit_data`
--
ALTER TABLE `paper_permit_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pc_report`
--
ALTER TABLE `pc_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policy_attachment`
--
ALTER TABLE `policy_attachment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policy_category`
--
ALTER TABLE `policy_category`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `policy_detail`
--
ALTER TABLE `policy_detail`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indexes for table `policy_main`
--
ALTER TABLE `policy_main`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `presentation`
--
ALTER TABLE `presentation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prev_main`
--
ALTER TABLE `prev_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prev_main_old`
--
ALTER TABLE `prev_main_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prj_web`
--
ALTER TABLE `prj_web`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pt_dev_category`
--
ALTER TABLE `pt_dev_category`
  ADD PRIMARY KEY (`id_cat`);

--
-- Indexes for table `pt_dev_comment`
--
ALTER TABLE `pt_dev_comment`
  ADD PRIMARY KEY (`id_comment`);

--
-- Indexes for table `pt_dev_jobs`
--
ALTER TABLE `pt_dev_jobs`
  ADD PRIMARY KEY (`id_job`);

--
-- Indexes for table `pt_dev_main`
--
ALTER TABLE `pt_dev_main`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `q_absence`
--
ALTER TABLE `q_absence`
  ADD PRIMARY KEY (`id_absence`);

--
-- Indexes for table `quiz_detail`
--
ALTER TABLE `quiz_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qz_quiz_main`
--
ALTER TABLE `qz_quiz_main`
  ADD PRIMARY KEY (`id_quiz`);

--
-- Indexes for table `qz_user`
--
ALTER TABLE `qz_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `redir`
--
ALTER TABLE `redir`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sig_password` (`sig_password`);

--
-- Indexes for table `redir_changemail`
--
ALTER TABLE `redir_changemail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rent_main`
--
ALTER TABLE `rent_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resource`
--
ALTER TABLE `resource`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resource_log`
--
ALTER TABLE `resource_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resource_mkt`
--
ALTER TABLE `resource_mkt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ro_detail`
--
ALTER TABLE `ro_detail`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indexes for table `ro_item`
--
ALTER TABLE `ro_item`
  ADD PRIMARY KEY (`id_item`);

--
-- Indexes for table `ro_main`
--
ALTER TABLE `ro_main`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `ro_price`
--
ALTER TABLE `ro_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rv_absensi`
--
ALTER TABLE `rv_absensi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rv_book`
--
ALTER TABLE `rv_book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rv_room`
--
ALTER TABLE `rv_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rv_time_check`
--
ALTER TABLE `rv_time_check`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rv_topic`
--
ALTER TABLE `rv_topic`
  ADD PRIMARY KEY (`id_topic`);

--
-- Indexes for table `safe_absence`
--
ALTER TABLE `safe_absence`
  ADD PRIMARY KEY (`id_absence`);

--
-- Indexes for table `safe_main`
--
ALTER TABLE `safe_main`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `safe_mom`
--
ALTER TABLE `safe_mom`
  ADD PRIMARY KEY (`id_mom`);

--
-- Indexes for table `scan`
--
ALTER TABLE `scan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scan_detail`
--
ALTER TABLE `scan_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_ticketing_detail`
--
ALTER TABLE `service_ticketing_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_ticketing_log`
--
ALTER TABLE `service_ticketing_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_ticketing_main`
--
ALTER TABLE `service_ticketing_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `software`
--
ALTER TABLE `software`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sop_category`
--
ALTER TABLE `sop_category`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `sop_detail`
--
ALTER TABLE `sop_detail`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indexes for table `sop_main`
--
ALTER TABLE `sop_main`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `sp`
--
ALTER TABLE `sp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topik`
--
ALTER TABLE `topik`
  ADD PRIMARY KEY (`id_topik`);

--
-- Indexes for table `training_type`
--
ALTER TABLE `training_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `travel_detail_main`
--
ALTER TABLE `travel_detail_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `troubleshoot`
--
ALTER TABLE `troubleshoot`
  ADD PRIMARY KEY (`troubleshoot_id`);

--
-- Indexes for table `twr_dokumen`
--
ALTER TABLE `twr_dokumen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `twr_drawtech`
--
ALTER TABLE `twr_drawtech`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `twr_telepon`
--
ALTER TABLE `twr_telepon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vacancy`
--
ALTER TABLE `vacancy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `view_button`
--
ALTER TABLE `view_button`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `view_div`
--
ALTER TABLE `view_div`
  ADD PRIMARY KEY (`id_view_div`);

--
-- Indexes for table `wall`
--
ALTER TABLE `wall`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wall_count`
--
ALTER TABLE `wall_count`
  ADD PRIMARY KEY (`wall_count_id`);

--
-- Indexes for table `wi_detail`
--
ALTER TABLE `wi_detail`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indexes for table `wi_main`
--
ALTER TABLE `wi_main`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `wod_detail`
--
ALTER TABLE `wod_detail`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indexes for table `wod_main`
--
ALTER TABLE `wod_main`
  ADD PRIMARY KEY (`id_main`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicant`
--
ALTER TABLE `applicant`
  MODIFY `app_id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `appraisal_detail`
--
ALTER TABLE `appraisal_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `appraisal_img`
--
ALTER TABLE `appraisal_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `appraisal_main`
--
ALTER TABLE `appraisal_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ar`
--
ALTER TABLE `ar`
  MODIFY `ar_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attend_log`
--
ALTER TABLE `attend_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attend_web`
--
ALTER TABLE `attend_web`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attend_web_new`
--
ALTER TABLE `attend_web_new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auto_gather_server`
--
ALTER TABLE `auto_gather_server`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bank_fm`
--
ALTER TABLE `bank_fm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `bank_fm_detail`
--
ALTER TABLE `bank_fm_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bank_ru`
--
ALTER TABLE `bank_ru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bank_ru_detail`
--
ALTER TABLE `bank_ru_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `berkas`
--
ALTER TABLE `berkas`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bugs`
--
ALTER TABLE `bugs`
  MODIFY `bugs_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ci_absence`
--
ALTER TABLE `ci_absence`
  MODIFY `id_absence` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ci_attach`
--
ALTER TABLE `ci_attach`
  MODIFY `attach_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ci_main`
--
ALTER TABLE `ci_main`
  MODIFY `id_main` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ci_mom`
--
ALTER TABLE `ci_mom`
  MODIFY `id_mom` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client_main`
--
ALTER TABLE `client_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id_comment` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contacs`
--
ALTER TABLE `contacs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cs_absence`
--
ALTER TABLE `cs_absence`
  MODIFY `id_absence` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cs_attach`
--
ALTER TABLE `cs_attach`
  MODIFY `attach_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cs_main`
--
ALTER TABLE `cs_main`
  MODIFY `id_main` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cs_mom`
--
ALTER TABLE `cs_mom`
  MODIFY `id_mom` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `csr_main`
--
ALTER TABLE `csr_main`
  MODIFY `csr_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `division`
--
ALTER TABLE `division`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dl_class`
--
ALTER TABLE `dl_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dl_main`
--
ALTER TABLE `dl_main`
  MODIFY `dl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `doc_center_datadoc`
--
ALTER TABLE `doc_center_datadoc`
  MODIFY `id_doc` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_center_historyy`
--
ALTER TABLE `doc_center_historyy`
  MODIFY `id_history` int(110) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_center_peminjam`
--
ALTER TABLE `doc_center_peminjam`
  MODIFY `id_peminjam` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_center_pengembalian`
--
ALTER TABLE `doc_center_pengembalian`
  MODIFY `id_pengembalian` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_counter`
--
ALTER TABLE `doc_counter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_rout_history`
--
ALTER TABLE `doc_rout_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_rout_login`
--
ALTER TABLE `doc_rout_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_rout_main`
--
ALTER TABLE `doc_rout_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_rout_ob`
--
ALTER TABLE `doc_rout_ob`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `drawing_category`
--
ALTER TABLE `drawing_category`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `drawing_log`
--
ALTER TABLE `drawing_log`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `drawing_status`
--
ALTER TABLE `drawing_status`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `drawing_sub_category`
--
ALTER TABLE `drawing_sub_category`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ev_alat`
--
ALTER TABLE `ev_alat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ev_anggota`
--
ALTER TABLE `ev_anggota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ev_dokumentasi_acara`
--
ALTER TABLE `ev_dokumentasi_acara`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ev_jadwal`
--
ALTER TABLE `ev_jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ev_juara`
--
ALTER TABLE `ev_juara`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ev_kekurangan`
--
ALTER TABLE `ev_kekurangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ev_perusahaan`
--
ALTER TABLE `ev_perusahaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ev_strukturorganisasi`
--
ALTER TABLE `ev_strukturorganisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ev_susunan_acara`
--
ALTER TABLE `ev_susunan_acara`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `form_maintenance`
--
ALTER TABLE `form_maintenance`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forum`
--
ALTER TABLE `forum`
  MODIFY `id_forum` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ins_ru`
--
ALTER TABLE `ins_ru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ins_ru_detail`
--
ALTER TABLE `ins_ru_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ir_main`
--
ALTER TABLE `ir_main`
  MODIFY `ir_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `it_punish_main`
--
ALTER TABLE `it_punish_main`
  MODIFY `it_punish_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `item_maintenance`
--
ALTER TABLE `item_maintenance`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kalender_event`
--
ALTER TABLE `kalender_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `krt_property`
--
ALTER TABLE `krt_property`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `li`
--
ALTER TABLE `li`
  MODIFY `id_li` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `li_attach`
--
ALTER TABLE `li_attach`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lo`
--
ALTER TABLE `lo`
  MODIFY `id_lo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_view`
--
ALTER TABLE `log_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_comment`
--
ALTER TABLE `m_comment`
  MODIFY `id_comment` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_forum`
--
ALTER TABLE `m_forum`
  MODIFY `id_forum` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_topik`
--
ALTER TABLE `m_topik`
  MODIFY `id_topik` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `master_logo`
--
ALTER TABLE `master_logo`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mcu_log`
--
ALTER TABLE `mcu_log`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mcu_main`
--
ALTER TABLE `mcu_main`
  MODIFY `mcu_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT for table `miss`
--
ALTER TABLE `miss`
  MODIFY `miss_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mtg_absence`
--
ALTER TABLE `mtg_absence`
  MODIFY `id_absence` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mtg_main`
--
ALTER TABLE `mtg_main`
  MODIFY `id_main` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mtg_mom`
--
ALTER TABLE `mtg_mom`
  MODIFY `id_mom` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mv_absence`
--
ALTER TABLE `mv_absence`
  MODIFY `id_absence` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mv_attach`
--
ALTER TABLE `mv_attach`
  MODIFY `attach_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mv_main`
--
ALTER TABLE `mv_main`
  MODIFY `id_main` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mv_mom`
--
ALTER TABLE `mv_mom`
  MODIFY `id_mom` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `near_miss`
--
ALTER TABLE `near_miss`
  MODIFY `near_miss_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `o_level`
--
ALTER TABLE `o_level`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `paper_permit`
--
ALTER TABLE `paper_permit`
  MODIFY `id_paper_permit` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `paper_permit_data`
--
ALTER TABLE `paper_permit_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pc_report`
--
ALTER TABLE `pc_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `policy_attachment`
--
ALTER TABLE `policy_attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `policy_category`
--
ALTER TABLE `policy_category`
  MODIFY `id_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `policy_detail`
--
ALTER TABLE `policy_detail`
  MODIFY `id_detail` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `policy_main`
--
ALTER TABLE `policy_main`
  MODIFY `id_main` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `presentation`
--
ALTER TABLE `presentation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `prev_main`
--
ALTER TABLE `prev_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `prev_main_old`
--
ALTER TABLE `prev_main_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `prj_web`
--
ALTER TABLE `prj_web`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pt_dev_category`
--
ALTER TABLE `pt_dev_category`
  MODIFY `id_cat` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pt_dev_comment`
--
ALTER TABLE `pt_dev_comment`
  MODIFY `id_comment` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pt_dev_jobs`
--
ALTER TABLE `pt_dev_jobs`
  MODIFY `id_job` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pt_dev_main`
--
ALTER TABLE `pt_dev_main`
  MODIFY `id_main` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `q_absence`
--
ALTER TABLE `q_absence`
  MODIFY `id_absence` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quiz_detail`
--
ALTER TABLE `quiz_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `qz_quiz_main`
--
ALTER TABLE `qz_quiz_main`
  MODIFY `id_quiz` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `qz_user`
--
ALTER TABLE `qz_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `redir`
--
ALTER TABLE `redir`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `redir_changemail`
--
ALTER TABLE `redir_changemail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rent_main`
--
ALTER TABLE `rent_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `resource`
--
ALTER TABLE `resource`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `resource_log`
--
ALTER TABLE `resource_log`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `resource_mkt`
--
ALTER TABLE `resource_mkt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ro_detail`
--
ALTER TABLE `ro_detail`
  MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ro_item`
--
ALTER TABLE `ro_item`
  MODIFY `id_item` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ro_main`
--
ALTER TABLE `ro_main`
  MODIFY `id_main` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ro_price`
--
ALTER TABLE `ro_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rv_absensi`
--
ALTER TABLE `rv_absensi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rv_book`
--
ALTER TABLE `rv_book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rv_room`
--
ALTER TABLE `rv_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rv_time_check`
--
ALTER TABLE `rv_time_check`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rv_topic`
--
ALTER TABLE `rv_topic`
  MODIFY `id_topic` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `safe_absence`
--
ALTER TABLE `safe_absence`
  MODIFY `id_absence` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `safe_main`
--
ALTER TABLE `safe_main`
  MODIFY `id_main` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `safe_mom`
--
ALTER TABLE `safe_mom`
  MODIFY `id_mom` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `scan`
--
ALTER TABLE `scan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `scan_detail`
--
ALTER TABLE `scan_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `service_ticketing_detail`
--
ALTER TABLE `service_ticketing_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `service_ticketing_log`
--
ALTER TABLE `service_ticketing_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `service_ticketing_main`
--
ALTER TABLE `service_ticketing_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `software`
--
ALTER TABLE `software`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sop_category`
--
ALTER TABLE `sop_category`
  MODIFY `id_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sop_detail`
--
ALTER TABLE `sop_detail`
  MODIFY `id_detail` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sop_main`
--
ALTER TABLE `sop_main`
  MODIFY `id_main` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sp`
--
ALTER TABLE `sp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `topik`
--
ALTER TABLE `topik`
  MODIFY `id_topik` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `training_type`
--
ALTER TABLE `training_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `travel_detail_main`
--
ALTER TABLE `travel_detail_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `troubleshoot`
--
ALTER TABLE `troubleshoot`
  MODIFY `troubleshoot_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `twr_dokumen`
--
ALTER TABLE `twr_dokumen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `twr_drawtech`
--
ALTER TABLE `twr_drawtech`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `twr_telepon`
--
ALTER TABLE `twr_telepon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vacancy`
--
ALTER TABLE `vacancy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `version`
--
ALTER TABLE `version`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `view_button`
--
ALTER TABLE `view_button`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `view_div`
--
ALTER TABLE `view_div`
  MODIFY `id_view_div` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wall`
--
ALTER TABLE `wall`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wall_count`
--
ALTER TABLE `wall_count`
  MODIFY `wall_count_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wi_detail`
--
ALTER TABLE `wi_detail`
  MODIFY `id_detail` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wi_main`
--
ALTER TABLE `wi_main`
  MODIFY `id_main` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wod_detail`
--
ALTER TABLE `wod_detail`
  MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wod_main`
--
ALTER TABLE `wod_main`
  MODIFY `id_main` int(11) NOT NULL AUTO_INCREMENT;--
-- Database: `ims_hrd`
--
CREATE DATABASE IF NOT EXISTS `ims_hrd` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims_hrd`;

-- --------------------------------------------------------

--
-- Table structure for table `aa_detail`
--

CREATE TABLE `aa_detail` (
  `id` int(11) NOT NULL,
  `id_main` int(11) NOT NULL,
  `nama_peserta` text NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `aa_main`
--

CREATE TABLE `aa_main` (
  `id` int(11) NOT NULL,
  `tanggal_pergi` date NOT NULL,
  `tanggal_pulang` date NOT NULL,
  `award` text NOT NULL,
  `input_by` varchar(30) NOT NULL,
  `tanggal_input` date NOT NULL,
  `path` text,
  `ukuran` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `absen2`
--

CREATE TABLE `absen2` (
  `a_id` int(12) NOT NULL,
  `id` int(10) NOT NULL,
  `date` date NOT NULL,
  `absen_alasan_id` int(12) DEFAULT NULL,
  `approved_by` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `absen_alasan`
--

CREATE TABLE `absen_alasan` (
  `absen_alasan_id` int(11) NOT NULL,
  `alasan` varchar(90) NOT NULL,
  `bobot` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `absen_justification`
--

CREATE TABLE `absen_justification` (
  `id` int(11) NOT NULL,
  `reason` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `emp_id` int(11) DEFAULT NULL,
  `periode` varchar(15) DEFAULT NULL,
  `days_absent` int(11) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `applicant`
--

CREATE TABLE `applicant` (
  `app_id` int(100) NOT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `kelamin` varchar(10) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `agama` varchar(10) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `no_ktp` varchar(100) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `no_telp1` varchar(15) DEFAULT NULL,
  `no_telp2` varchar(15) DEFAULT NULL,
  `kp_suis` varchar(100) DEFAULT NULL,
  `anak_1` varchar(100) DEFAULT NULL,
  `anak_2` varchar(100) DEFAULT NULL,
  `anak_3` varchar(100) DEFAULT NULL,
  `kk_ayah` varchar(100) DEFAULT NULL,
  `kk_ibu` varchar(100) DEFAULT NULL,
  `kk_saudara_1` varchar(100) DEFAULT NULL,
  `kk_saudara_2` varchar(100) DEFAULT NULL,
  `kk_saudara_3` varchar(100) DEFAULT NULL,
  `kk_saudara_4` varchar(100) DEFAULT NULL,
  `sakit` varchar(5) DEFAULT NULL,
  `sakit_ket` varchar(100) DEFAULT NULL,
  `sakit_date` date DEFAULT NULL,
  `sakit_pengaruh` varchar(100) DEFAULT NULL,
  `sd_name` varchar(100) DEFAULT NULL,
  `sd_kota` varchar(100) DEFAULT NULL,
  `sd_jur` varchar(100) DEFAULT NULL,
  `sd_date1` varchar(10) DEFAULT NULL,
  `sd_date2` varchar(10) DEFAULT NULL,
  `smp_name` varchar(100) DEFAULT NULL,
  `smp_kota` varchar(100) DEFAULT NULL,
  `smp_jur` varchar(100) DEFAULT NULL,
  `smp_date1` varchar(100) DEFAULT NULL,
  `smp_date2` varchar(100) DEFAULT NULL,
  `sma_name` varchar(100) DEFAULT NULL,
  `sma_kota` varchar(100) DEFAULT NULL,
  `sma_jur` varchar(100) DEFAULT NULL,
  `sma_date1` date DEFAULT NULL,
  `sma_date2` date DEFAULT NULL,
  `d3_name` varchar(100) DEFAULT NULL,
  `d3_kota` varchar(100) DEFAULT NULL,
  `d3_jur` varchar(100) DEFAULT NULL,
  `d3_date1` varchar(15) DEFAULT NULL,
  `d3_date2` varchar(15) DEFAULT NULL,
  `s1_name` varchar(100) DEFAULT NULL,
  `s1_kota` varchar(100) DEFAULT NULL,
  `s1_jur` varchar(100) DEFAULT NULL,
  `s1_date1` varchar(15) DEFAULT NULL,
  `s1_date2` varchar(15) DEFAULT NULL,
  `s2_name` varchar(100) DEFAULT NULL,
  `s2_kota` varchar(100) DEFAULT NULL,
  `s2_jur` varchar(100) DEFAULT NULL,
  `s2_date1` varchar(15) DEFAULT NULL,
  `s2_date2` varchar(15) DEFAULT NULL,
  `other_name` varchar(100) DEFAULT NULL,
  `other_kota` varchar(100) DEFAULT NULL,
  `other_jur` varchar(100) DEFAULT NULL,
  `other_date1` date DEFAULT NULL,
  `other_date2` date DEFAULT NULL,
  `skripsi_name` varchar(100) DEFAULT NULL,
  `skripsi_desc` varchar(100) DEFAULT NULL,
  `skripsi_date` varchar(100) DEFAULT NULL,
  `keg_nama` varchar(100) DEFAULT NULL,
  `keg_jabatan` varchar(100) DEFAULT NULL,
  `keg_date` varchar(100) DEFAULT NULL,
  `keg_desc` varchar(100) DEFAULT NULL,
  `bhs_1_nama` varchar(100) DEFAULT NULL,
  `bhs_1_bicara` varchar(100) DEFAULT NULL,
  `bhs_1_dengar` varchar(100) DEFAULT NULL,
  `bhs_1_tulis` varchar(100) DEFAULT NULL,
  `bhs_2_nama` varchar(100) DEFAULT NULL,
  `bhs_2_bicara` varchar(100) DEFAULT NULL,
  `bhs_2_dengar` varchar(100) DEFAULT NULL,
  `bhs_2_tulis` varchar(100) DEFAULT NULL,
  `hobby` varchar(100) DEFAULT NULL,
  `achievment` varchar(100) DEFAULT NULL,
  `kendaraan` varchar(100) DEFAULT NULL,
  `kerja_1_per` varchar(100) DEFAULT NULL,
  `kerja_1_alamat` varchar(100) DEFAULT NULL,
  `kerja_1_telp` varchar(100) DEFAULT NULL,
  `kerja_1_desc` varchar(100) DEFAULT NULL,
  `kerja_1_jenis` varchar(100) DEFAULT NULL,
  `kerja_1_atasan` varchar(100) DEFAULT NULL,
  `kerja_1_berhenti` varchar(100) DEFAULT NULL,
  `kerja_1_jml_kary` varchar(100) DEFAULT NULL,
  `kerja_1_direktur` varchar(100) DEFAULT NULL,
  `kerja_2_per` varchar(100) DEFAULT NULL,
  `kerja_2_alamat` varchar(100) DEFAULT NULL,
  `kerja_2_telp` varchar(100) DEFAULT NULL,
  `kerja_2_desc` varchar(100) DEFAULT NULL,
  `kerja_2_jenis` varchar(100) DEFAULT NULL,
  `kerja_2_atasan` varchar(100) DEFAULT NULL,
  `kerja_2_berhenti` varchar(100) DEFAULT NULL,
  `kerja_2_jml_kary` varchar(100) DEFAULT NULL,
  `kerja_2_direktur` varchar(100) DEFAULT NULL,
  `con_person` varchar(100) DEFAULT NULL,
  `con_no_person` varchar(100) DEFAULT NULL,
  `con_job_person` varchar(100) DEFAULT NULL,
  `gaji` int(100) DEFAULT NULL,
  `start_work` date DEFAULT NULL,
  `start_work_why` varchar(100) DEFAULT NULL,
  `status_kerja` int(2) DEFAULT NULL,
  `input_date` date DEFAULT NULL,
  `picture` varchar(250) DEFAULT NULL,
  `app_key` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_test`
--

CREATE TABLE `applicant_test` (
  `id` int(10) NOT NULL,
  `app_id` int(5) DEFAULT NULL,
  `hrd_id` int(10) DEFAULT NULL,
  `end_user_id` int(10) DEFAULT NULL,
  `hrd_1` int(5) DEFAULT NULL,
  `end_user_1` int(5) DEFAULT NULL,
  `hrd_2` int(5) DEFAULT NULL,
  `end_user_2` int(5) DEFAULT NULL,
  `hrd_3` int(5) DEFAULT NULL,
  `end_user_3` int(5) DEFAULT NULL,
  `hrd_4` int(5) DEFAULT NULL,
  `end_user_4` int(5) DEFAULT NULL,
  `keterangan` varchar(250) DEFAULT NULL,
  `bod` varchar(5) DEFAULT NULL,
  `emp_type` varchar(10) DEFAULT NULL,
  `divisi` varchar(20) DEFAULT NULL,
  `gaji` int(20) DEFAULT NULL,
  `a_jabatan` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `approval`
--

CREATE TABLE `approval` (
  `object` varchar(15) DEFAULT NULL COMMENT 'to',
  `action_lvl` int(11) DEFAULT NULL,
  `action_type` varchar(10) DEFAULT NULL COMMENT 'approve, disapprove',
  `action_time` datetime DEFAULT NULL,
  `action_note` text,
  `id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `emp_id` int(11) DEFAULT NULL,
  `att_date` date DEFAULT NULL,
  `time_in` time DEFAULT NULL,
  `time_out` time DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bbt_p`
--

CREATE TABLE `bbt_p` (
  `id` int(10) NOT NULL,
  `id_p` int(10) NOT NULL,
  `id_t` int(10) NOT NULL,
  `tgl_m` date NOT NULL,
  `tgl_a` date NOT NULL,
  `gp` int(10) NOT NULL,
  `bp` int(10) NOT NULL,
  `keterangan` varchar(250) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bbt_p`
--

INSERT INTO `bbt_p` (`id`, `id_p`, `id_t`, `tgl_m`, `tgl_a`, `gp`, `bp`, `keterangan`, `status`) VALUES
(1, 0, 1, '2017-05-18', '2017-05-18', 0, 213, 'aadad', 2),
(2, 0, 1, '2017-05-18', '2017-05-18', 0, 0, 'ASas', 2);

-- --------------------------------------------------------

--
-- Table structure for table `bcr`
--

CREATE TABLE `bcr` (
  `id` int(10) NOT NULL,
  `no` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `subject` varchar(200) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `currency` varchar(20) DEFAULT NULL,
  `requestor` varchar(200) DEFAULT NULL,
  `approver` varchar(200) DEFAULT NULL,
  `finance` varchar(200) DEFAULT NULL,
  `status` varchar(200) DEFAULT NULL,
  `divs` varchar(200) DEFAULT NULL,
  `date_app` date DEFAULT NULL,
  `date_m_rec` date DEFAULT NULL,
  `date_bal_app` date DEFAULT NULL,
  `date_bal_rec` date DEFAULT NULL,
  `app1` text,
  `app2` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bcr_detail`
--

CREATE TABLE `bcr_detail` (
  `id` int(10) NOT NULL,
  `bcr_id` int(10) NOT NULL,
  `descr` text,
  `amount` decimal(20,2) DEFAULT NULL,
  `remark` text,
  `actual` decimal(10,2) DEFAULT NULL,
  `projects` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bonus`
--

CREATE TABLE `bonus` (
  `bonusID` varchar(20) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `date_given` date DEFAULT NULL,
  `bonus_amount` decimal(10,0) DEFAULT NULL,
  `bonus_start` date DEFAULT NULL,
  `bonus_end` date DEFAULT NULL,
  `given_by` varchar(15) DEFAULT NULL,
  `given_time` datetime DEFAULT NULL,
  `notes` text,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bonus_payment`
--

CREATE TABLE `bonus_payment` (
  `bonus_id` int(11) DEFAULT NULL,
  `date_of_payment` date DEFAULT NULL,
  `amount` decimal(10,0) DEFAULT NULL,
  `payment_id` varchar(25) DEFAULT NULL,
  `remark` text,
  `receive_by` varchar(35) DEFAULT NULL,
  `receive_time` datetime DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `certificate`
--

CREATE TABLE `certificate` (
  `certificate_id` int(10) NOT NULL,
  `certificate_name` varchar(120) DEFAULT NULL,
  `view` int(1) DEFAULT NULL,
  `certificate_date` date DEFAULT NULL,
  `certificate_type` varchar(25) DEFAULT NULL,
  `stnk_no` varchar(120) DEFAULT NULL,
  `stnk_holder` varchar(120) DEFAULT NULL,
  `stnk_pic` varchar(250) DEFAULT NULL,
  `bpkb_no` varchar(120) DEFAULT NULL,
  `bpkb_holder` varchar(120) DEFAULT NULL,
  `bpkb_pic` varchar(250) DEFAULT NULL,
  `legal_holder` varchar(120) DEFAULT NULL,
  `certificate_no` varchar(120) DEFAULT NULL,
  `picture` varchar(250) DEFAULT NULL,
  `item_code` varchar(20) DEFAULT NULL,
  `property_no` varchar(250) DEFAULT NULL COMMENT 'harley:tahunbuat/cc/warna',
  `property_holder` varchar(100) DEFAULT NULL COMMENT 'vendor;notelp',
  `property_value` varchar(200) DEFAULT NULL COMMENT 'harga',
  `property_pic` varchar(250) DEFAULT NULL,
  `legal_no` varchar(250) DEFAULT NULL,
  `legal_desc` varchar(250) DEFAULT NULL,
  `legal_owner` varchar(250) DEFAULT NULL,
  `legal_pic` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `certificate`
--

INSERT INTO `certificate` (`certificate_id`, `certificate_name`, `view`, `certificate_date`, `certificate_type`, `stnk_no`, `stnk_holder`, `stnk_pic`, `bpkb_no`, `bpkb_holder`, `bpkb_pic`, `legal_holder`, `certificate_no`, `picture`, `item_code`, `property_no`, `property_holder`, `property_value`, `property_pic`, `legal_no`, `legal_desc`, `legal_owner`, `legal_pic`) VALUES
(1, 'zxc', 1, '2017-04-27', 'CAR', 'ew', 'as', 'stnk_Book1.htm', '', '', NULL, '', 'xzc', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-27'),
(2, 'ad', 1, '2017-04-28', 'MOTORCYCLE', 'sda', 'sad', NULL, 'sad', 'sad', NULL, 'sda', 'asd', '', 'sad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-28'),
(5, 'erff', 1, '2017-05-20', 'LEGAL', NULL, NULL, NULL, NULL, NULL, NULL, 'erf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'erferferf', 'erfrf', 'erf', 'legal_pic_PL IMS.png'),
(6, 'asd sadadds', 1, '2017-05-14', 'LEGAL', NULL, NULL, NULL, NULL, NULL, NULL, 'sdfS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sad', 'asd', 'sad', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `certificate_cost`
--

CREATE TABLE `certificate_cost` (
  `id` int(11) NOT NULL,
  `certificate_id` varchar(11) DEFAULT NULL,
  `input_date` date DEFAULT NULL,
  `input_by` varchar(200) DEFAULT NULL,
  `part_number` varchar(100) DEFAULT NULL,
  `input_desc` text,
  `price` decimal(17,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `certificate_detail`
--

CREATE TABLE `certificate_detail` (
  `id` int(11) NOT NULL,
  `idmain` int(11) NOT NULL,
  `location` text NOT NULL,
  `nama_file` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `certificate_edit`
--

CREATE TABLE `certificate_edit` (
  `certificate_edit_id` int(11) NOT NULL,
  `redir_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `view_type` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `certificate_edit`
--

INSERT INTO `certificate_edit` (`certificate_edit_id`, `redir_id`, `emp_id`, `view_type`) VALUES
(1, 1, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `certificate_paper`
--

CREATE TABLE `certificate_paper` (
  `id` int(11) NOT NULL,
  `police_num` varchar(200) DEFAULT NULL,
  `harley_usage` int(10) DEFAULT NULL,
  `stnk_no` varchar(200) DEFAULT NULL,
  `stnk_date` date DEFAULT NULL,
  `stnk_value` varchar(20) DEFAULT NULL,
  `stnk_owner` varchar(200) DEFAULT NULL,
  `stnk_holder` varchar(200) DEFAULT NULL,
  `stnk_pic` varchar(200) DEFAULT NULL,
  `stnk_specs` text,
  `stnk_yearcccolor` varchar(200) DEFAULT NULL,
  `stnk_vendor` varchar(200) DEFAULT NULL,
  `stnk_vendorhp` varchar(200) DEFAULT NULL,
  `bpkb_no` varchar(200) DEFAULT NULL,
  `bpkb_owner` varchar(200) DEFAULT NULL,
  `bpkb_holder` varchar(200) DEFAULT NULL,
  `bpkb_pic` varchar(200) DEFAULT NULL,
  `expiry` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `id` int(10) UNSIGNED NOT NULL,
  `opt_name` varchar(50) NOT NULL,
  `opt_value` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cuti`
--

CREATE TABLE `cuti` (
  `c_id` int(11) NOT NULL,
  `id_emp` int(2) NOT NULL,
  `request_at` date NOT NULL,
  `division` varchar(50) NOT NULL,
  `awal` date NOT NULL,
  `akhir` date NOT NULL,
  `keterangan` text NOT NULL,
  `status` int(2) NOT NULL,
  `o_level` varchar(8) NOT NULL,
  `div_by` varchar(100) DEFAULT NULL,
  `div_date` date DEFAULT NULL,
  `hrd_by` varchar(100) DEFAULT NULL,
  `hrd_date` date DEFAULT NULL,
  `reject1` text,
  `reject2` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cv`
--

CREATE TABLE `cv` (
  `cv_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `cv_type` varchar(10) NOT NULL,
  `awal_waktu` date NOT NULL,
  `akhir_waktu` date NOT NULL,
  `keterangan` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cv_u`
--

CREATE TABLE `cv_u` (
  `cv_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `cv_address` varchar(250) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `whom` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `destination`
--

CREATE TABLE `destination` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `currency` varchar(50) DEFAULT NULL,
  `initial` varchar(50) DEFAULT NULL,
  `currency_symbol` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_counter`
--

CREATE TABLE `doc_counter` (
  `doc_code` varchar(10) NOT NULL,
  `last_num` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='menyimpan informasi nomor dokumen terakhir';

-- --------------------------------------------------------

--
-- Table structure for table `driver_duty`
--

CREATE TABLE `driver_duty` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `duty_date` date NOT NULL,
  `duty_purpose` varchar(300) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_name` varchar(35) DEFAULT NULL,
  `emp_id` varchar(16) DEFAULT NULL,
  `emp_position` varchar(35) DEFAULT NULL,
  `tax_status` char(3) DEFAULT NULL,
  `salary` varchar(35) NOT NULL DEFAULT '0',
  `emp_type` varchar(15) DEFAULT NULL COMMENT 'office, engineer',
  `emp_level` varchar(15) DEFAULT NULL COMMENT 'staff, manager, director',
  `transport` varchar(35) DEFAULT NULL,
  `meal` varchar(35) DEFAULT NULL,
  `house` varchar(35) DEFAULT NULL,
  `pension` decimal(10,0) DEFAULT NULL,
  `health` varchar(35) DEFAULT NULL,
  `health_insurance` decimal(10,0) DEFAULT NULL,
  `jamsostek` decimal(10,0) DEFAULT NULL,
  `fld_bonus` decimal(10,0) NOT NULL DEFAULT '0',
  `odo_bonus` decimal(10,0) NOT NULL DEFAULT '0',
  `wh_bonus` decimal(10,0) NOT NULL DEFAULT '0',
  `overtime` decimal(10,0) NOT NULL DEFAULT '0',
  `others` decimal(10,0) DEFAULT '0',
  `voucher` decimal(10,0) DEFAULT NULL,
  `allowance_office` decimal(10,0) DEFAULT NULL,
  `max_loan` decimal(10,0) DEFAULT NULL,
  `loan_balance` decimal(10,0) DEFAULT NULL,
  `bank_acct` varchar(100) DEFAULT NULL,
  `bank_code` varchar(5) NOT NULL DEFAULT '002',
  `picture` varchar(250) DEFAULT NULL,
  `religion` varchar(12) NOT NULL,
  `npwp` varchar(30) NOT NULL,
  `yearly_bonus` float DEFAULT NULL,
  `fx_yearly_bonus` decimal(15,2) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `expire` date DEFAULT NULL,
  `ktp` varchar(250) DEFAULT NULL,
  `serti1` varchar(250) DEFAULT NULL,
  `cuti` int(2) DEFAULT NULL,
  `cuti_flag` int(1) NOT NULL,
  `phone2` varchar(50) DEFAULT NULL,
  `phoneh` varchar(24) DEFAULT NULL,
  `thr` decimal(5,2) NOT NULL DEFAULT '0.00',
  `emp_birth` date NOT NULL,
  `dom_meal` decimal(15,2) DEFAULT NULL,
  `dom_spending` decimal(15,2) DEFAULT NULL,
  `dom_overnight` decimal(15,2) DEFAULT NULL,
  `ovs_meal` decimal(15,2) DEFAULT NULL,
  `ovs_spending` decimal(15,2) DEFAULT NULL,
  `ovs_overnight` decimal(15,2) DEFAULT NULL,
  `dom_transport_airport` decimal(15,2) NOT NULL DEFAULT '0.00',
  `dom_transport_train` decimal(15,2) DEFAULT NULL,
  `dom_transport_bus` decimal(15,2) DEFAULT NULL,
  `dom_transport_cil` decimal(15,2) DEFAULT NULL,
  `ovs_transport_airport` decimal(15,2) DEFAULT NULL,
  `ovs_transport_train` decimal(15,2) DEFAULT NULL,
  `ovs_transport_bus` decimal(15,2) DEFAULT NULL,
  `ovs_transport_cil` decimal(15,2) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `emp_lahir` date DEFAULT NULL,
  `expel` date DEFAULT NULL,
  `gender` varchar(3) DEFAULT NULL,
  `mcu_id` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `emp_name`, `emp_id`, `emp_position`, `tax_status`, `salary`, `emp_type`, `emp_level`, `transport`, `meal`, `house`, `pension`, `health`, `health_insurance`, `jamsostek`, `fld_bonus`, `odo_bonus`, `wh_bonus`, `overtime`, `others`, `voucher`, `allowance_office`, `max_loan`, `loan_balance`, `bank_acct`, `bank_code`, `picture`, `religion`, `npwp`, `yearly_bonus`, `fx_yearly_bonus`, `address`, `phone`, `expire`, `ktp`, `serti1`, `cuti`, `cuti_flag`, `phone2`, `phoneh`, `thr`, `emp_birth`, `dom_meal`, `dom_spending`, `dom_overnight`, `ovs_meal`, `ovs_spending`, `ovs_overnight`, `dom_transport_airport`, `dom_transport_train`, `dom_transport_bus`, `dom_transport_cil`, `ovs_transport_airport`, `ovs_transport_train`, `ovs_transport_bus`, `ovs_transport_cil`, `email`, `emp_lahir`, `expel`, `gender`, `mcu_id`) VALUES
(1, 'Husni Mubarack Obama', 'GT-20041701', '123', NULL, 'dhb05284813482879288614da2614284', 'staff', NULL, 'dhb05284758482839285988cc7117284', 'dhb022843614822782865418e2553284', 'dhb082842334825532854136b2740284', '123', 'dhb03284238482952285875cc7693284', '123', '123', '123', '0', '123', '123', NULL, '123', '123', NULL, NULL, '123', '002', NULL, 'Islam proska', '', 123, NULL, 'asd', '123', NULL, NULL, NULL, NULL, 0, '123', NULL, '0.00', '0000-00-00', '55000.00', '100000.00', '400000.00', '15.00', '15.00', '100.00', '200000.00', '200000.00', '200000.00', '100000.00', '15.00', '15.00', '15.00', '10.00', '123', '2017-04-20', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_history`
--

CREATE TABLE `employee_history` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `activity` varchar(10) NOT NULL,
  `act_date` date NOT NULL,
  `act_by` varchar(100) DEFAULT NULL,
  `act_amount` decimal(15,0) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_history`
--

INSERT INTO `employee_history` (`id`, `emp_id`, `activity`, `act_date`, `act_by`, `act_amount`) VALUES
(1, 1, 'in', '2017-04-20', 'superuser', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_history_edit`
--

CREATE TABLE `employee_history_edit` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `req_act_date` date DEFAULT NULL,
  `app_act_date` date DEFAULT NULL,
  `request_by` varchar(50) DEFAULT NULL,
  `app_date` date DEFAULT NULL,
  `status` enum('confirmed','unconfirmed') DEFAULT 'unconfirmed'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `waktu` datetime DEFAULT NULL,
  `tipe` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_in`
--

CREATE TABLE `invoice_in` (
  `id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `paper_type` varchar(10) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `pay_date` date NOT NULL,
  `app_date` date NOT NULL,
  `status` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `konsultan`
--

CREATE TABLE `konsultan` (
  `id` int(11) NOT NULL,
  `menu` varchar(400) NOT NULL,
  `text_contained` varchar(400) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loan`
--

CREATE TABLE `loan` (
  `loanID` varchar(20) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `date_given` date DEFAULT NULL,
  `loan_amount` decimal(10,0) DEFAULT NULL,
  `loan_start` date DEFAULT NULL,
  `loan_end` date DEFAULT NULL,
  `given_by` varchar(15) DEFAULT NULL,
  `given_time` datetime DEFAULT NULL,
  `notes` text,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loan_payment`
--

CREATE TABLE `loan_payment` (
  `loan_id` int(11) DEFAULT NULL,
  `date_of_payment` date DEFAULT NULL,
  `amount` decimal(10,0) DEFAULT NULL,
  `payment_id` varchar(25) DEFAULT NULL,
  `remark` text,
  `receive_by` varchar(35) DEFAULT NULL,
  `receive_time` datetime DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `local`
--

CREATE TABLE `local` (
  `id` int(11) NOT NULL,
  `menu` varchar(400) NOT NULL,
  `text_contained` varchar(400) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_view`
--

CREATE TABLE `log_view` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `location` varchar(400) NOT NULL,
  `log_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mcu_create`
--

CREATE TABLE `mcu_create` (
  `id_mcu` int(11) NOT NULL,
  `id_emp` int(11) NOT NULL,
  `tgl_cek` date NOT NULL,
  `create_by` varchar(200) NOT NULL,
  `path` varchar(200) NOT NULL,
  `create_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(6) NOT NULL,
  `position` int(3) NOT NULL,
  `stage` int(2) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `href` varchar(200) DEFAULT NULL,
  `level` int(3) NOT NULL,
  `division` varchar(200) DEFAULT NULL,
  `lock` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `overtime`
--

CREATE TABLE `overtime` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `ovt_date` date DEFAULT NULL,
  `time_in` time DEFAULT NULL COMMENT 'berisi jumlah waktu overtime sehari',
  `time_out` time DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penalty`
--

CREATE TABLE `penalty` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `penalty_id` int(11) DEFAULT NULL,
  `active_date_from` datetime DEFAULT NULL,
  `active_date_to` datetime DEFAULT NULL,
  `status` enum('enable','disable') DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penalty_type`
--

CREATE TABLE `penalty_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `action` varchar(100) DEFAULT NULL,
  `function` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id` int(11) NOT NULL,
  `judul` text,
  `tgl_terbit` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `created_by` varchar(200) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `approve_by` varchar(200) DEFAULT NULL,
  `approve_time` date DEFAULT NULL,
  `filepath` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `rpt_subject` varchar(255) DEFAULT NULL,
  `rpt_text` text,
  `rpt_time` datetime DEFAULT NULL,
  `approve_by` varchar(35) DEFAULT NULL,
  `approve_time` datetime DEFAULT NULL,
  `approve_notes` text,
  `create_by` varchar(35) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salary_archive`
--

CREATE TABLE `salary_archive` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `archive_period` varchar(10) NOT NULL DEFAULT '00-0000',
  `salary` varchar(35) NOT NULL,
  `ovt_rate` decimal(17,2) NOT NULL,
  `ovt_nom` int(5) NOT NULL,
  `field_rate` decimal(17,2) NOT NULL,
  `field_nom` int(3) NOT NULL,
  `wh_rate` decimal(17,2) NOT NULL,
  `wh_nom` int(3) NOT NULL,
  `odo_rate` decimal(17,2) NOT NULL,
  `odo_nom` int(3) NOT NULL,
  `voucher` decimal(17,2) NOT NULL,
  `deduction` decimal(17,2) NOT NULL,
  `lateness` int(11) NOT NULL,
  `bonus` decimal(17,2) NOT NULL,
  `thr` decimal(17,2) NOT NULL,
  `category` varchar(40) NOT NULL,
  `fld_dgr` decimal(17,2) NOT NULL,
  `fld_swt` decimal(17,2) NOT NULL,
  `odo_dgr` decimal(17,2) NOT NULL,
  `odo_swt` decimal(17,2) NOT NULL,
  `proportional` decimal(17,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salary_history`
--

CREATE TABLE `salary_history` (
  `id` int(11) NOT NULL,
  `user` varchar(100) NOT NULL,
  `target` int(5) NOT NULL,
  `basic` decimal(15,2) DEFAULT NULL,
  `voucher` decimal(15,2) DEFAULT NULL,
  `position` decimal(15,2) DEFAULT NULL,
  `execute_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `to`
--

CREATE TABLE `to` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `doc_num` varchar(30) DEFAULT NULL,
  `doc_date` date DEFAULT NULL,
  `destination` varchar(250) DEFAULT NULL,
  `location` varchar(250) DEFAULT NULL,
  `tolocation` varchar(200) DEFAULT NULL,
  `dest_type` char(4) DEFAULT NULL COMMENT 'wh, fld',
  `travel_type` char(4) DEFAULT NULL COMMENT 'odo, reg',
  `departure_dt` date DEFAULT NULL,
  `return_dt` date DEFAULT NULL,
  `purpose` text,
  `action` varchar(15) DEFAULT NULL COMMENT 'approve, disapprove',
  `action_by` varchar(35) DEFAULT NULL,
  `action_time` datetime DEFAULT NULL,
  `action_notes` text,
  `admin` varchar(200) DEFAULT NULL,
  `admin_time` datetime DEFAULT NULL,
  `create_by` varchar(35) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `paid_by` varchar(40) DEFAULT NULL,
  `paid_time` datetime DEFAULT NULL,
  `project` varchar(400) DEFAULT NULL,
  `sppd_type` varchar(5) NOT NULL,
  `to_meal` decimal(15,2) DEFAULT NULL,
  `to_spending` decimal(15,2) DEFAULT NULL,
  `to_overnight` decimal(15,2) DEFAULT NULL,
  `to_transport` decimal(15,2) DEFAULT NULL,
  `status` int(2) NOT NULL,
  `to_cekmeal` int(1) DEFAULT NULL,
  `to_cekspending` int(1) DEFAULT NULL,
  `to_cekovernight` int(1) DEFAULT NULL,
  `to_cektransport` int(1) DEFAULT NULL,
  `transport` decimal(15,2) NOT NULL DEFAULT '0.00',
  `taxi` decimal(15,2) NOT NULL DEFAULT '0.00',
  `rent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `airtax` decimal(15,2) NOT NULL DEFAULT '0.00',
  `departure_time` varchar(40) DEFAULT NULL,
  `departure_no` varchar(40) DEFAULT NULL,
  `return_time` varchar(40) DEFAULT NULL,
  `return_no` varchar(40) DEFAULT NULL,
  `last_payment` decimal(15,2) DEFAULT NULL,
  `location_rate` varchar(5) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `to_plan`
--

CREATE TABLE `to_plan` (
  `emp_id` int(11) NOT NULL,
  `assign_date` date NOT NULL,
  `project` varchar(10) NOT NULL,
  `remark` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `travel_cost`
--

CREATE TABLE `travel_cost` (
  `id` int(11) NOT NULL,
  `destination_id` int(11) DEFAULT NULL,
  `emp_type` enum('bod','manager','staff','marketing','field','konsultan') DEFAULT NULL,
  `ovs_meal` decimal(11,2) DEFAULT NULL,
  `ovs_spending` decimal(11,2) DEFAULT NULL,
  `ovs_overnight` decimal(11,2) DEFAULT NULL,
  `ovs_transport_airport` decimal(11,2) DEFAULT NULL,
  `ovs_transport_bus` decimal(11,2) DEFAULT NULL,
  `ovs_transport_train` decimal(11,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `realname` varchar(50) NOT NULL,
  `can_see` text COMMENT 'staff, manager, director',
  `level` varchar(15) NOT NULL,
  `division` varchar(50) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aa_detail`
--
ALTER TABLE `aa_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aa_main`
--
ALTER TABLE `aa_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `absen2`
--
ALTER TABLE `absen2`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `absen_alasan`
--
ALTER TABLE `absen_alasan`
  ADD PRIMARY KEY (`absen_alasan_id`);

--
-- Indexes for table `absen_justification`
--
ALTER TABLE `absen_justification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `emp_id` (`emp_id`,`periode`);

--
-- Indexes for table `applicant`
--
ALTER TABLE `applicant`
  ADD PRIMARY KEY (`app_id`);

--
-- Indexes for table `applicant_test`
--
ALTER TABLE `applicant_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `approval`
--
ALTER TABLE `approval`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `emp_date` (`emp_id`,`att_date`);

--
-- Indexes for table `bbt_p`
--
ALTER TABLE `bbt_p`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bcr`
--
ALTER TABLE `bcr`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bcr_detail`
--
ALTER TABLE `bcr_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonus_payment`
--
ALTER TABLE `bonus_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `certificate`
--
ALTER TABLE `certificate`
  ADD PRIMARY KEY (`certificate_id`);

--
-- Indexes for table `certificate_cost`
--
ALTER TABLE `certificate_cost`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `certificate_detail`
--
ALTER TABLE `certificate_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `certificate_edit`
--
ALTER TABLE `certificate_edit`
  ADD PRIMARY KEY (`certificate_edit_id`);

--
-- Indexes for table `certificate_paper`
--
ALTER TABLE `certificate_paper`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `opt_name` (`opt_name`);

--
-- Indexes for table `cuti`
--
ALTER TABLE `cuti`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `cv`
--
ALTER TABLE `cv`
  ADD PRIMARY KEY (`cv_id`);

--
-- Indexes for table `cv_u`
--
ALTER TABLE `cv_u`
  ADD PRIMARY KEY (`cv_id`);

--
-- Indexes for table `destination`
--
ALTER TABLE `destination`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doc_counter`
--
ALTER TABLE `doc_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_duty`
--
ALTER TABLE `driver_duty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `emp_id` (`emp_id`),
  ADD KEY `name` (`emp_name`);

--
-- Indexes for table `employee_history`
--
ALTER TABLE `employee_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_history_edit`
--
ALTER TABLE `employee_history_edit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_in`
--
ALTER TABLE `invoice_in`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `konsultan`
--
ALTER TABLE `konsultan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan`
--
ALTER TABLE `loan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_payment`
--
ALTER TABLE `loan_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `local`
--
ALTER TABLE `local`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_view`
--
ALTER TABLE `log_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mcu_create`
--
ALTER TABLE `mcu_create`
  ADD PRIMARY KEY (`id_mcu`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `overtime`
--
ALTER TABLE `overtime`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `emp_date` (`emp_id`,`ovt_date`);

--
-- Indexes for table `penalty`
--
ALTER TABLE `penalty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penalty_type`
--
ALTER TABLE `penalty_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary_archive`
--
ALTER TABLE `salary_archive`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary_history`
--
ALTER TABLE `salary_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `to`
--
ALTER TABLE `to`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `to_plan`
--
ALTER TABLE `to_plan`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `travel_cost`
--
ALTER TABLE `travel_cost`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aa_detail`
--
ALTER TABLE `aa_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aa_main`
--
ALTER TABLE `aa_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `absen2`
--
ALTER TABLE `absen2`
  MODIFY `a_id` int(12) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `absen_alasan`
--
ALTER TABLE `absen_alasan`
  MODIFY `absen_alasan_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `absen_justification`
--
ALTER TABLE `absen_justification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicant`
--
ALTER TABLE `applicant`
  MODIFY `app_id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicant_test`
--
ALTER TABLE `applicant_test`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `approval`
--
ALTER TABLE `approval`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bbt_p`
--
ALTER TABLE `bbt_p`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `bcr`
--
ALTER TABLE `bcr`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bcr_detail`
--
ALTER TABLE `bcr_detail`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bonus`
--
ALTER TABLE `bonus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bonus_payment`
--
ALTER TABLE `bonus_payment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `certificate`
--
ALTER TABLE `certificate`
  MODIFY `certificate_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `certificate_cost`
--
ALTER TABLE `certificate_cost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `certificate_detail`
--
ALTER TABLE `certificate_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `certificate_edit`
--
ALTER TABLE `certificate_edit`
  MODIFY `certificate_edit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `certificate_paper`
--
ALTER TABLE `certificate_paper`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cuti`
--
ALTER TABLE `cuti`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cv`
--
ALTER TABLE `cv`
  MODIFY `cv_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cv_u`
--
ALTER TABLE `cv_u`
  MODIFY `cv_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `destination`
--
ALTER TABLE `destination`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_counter`
--
ALTER TABLE `doc_counter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `driver_duty`
--
ALTER TABLE `driver_duty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employee_history`
--
ALTER TABLE `employee_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employee_history_edit`
--
ALTER TABLE `employee_history_edit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice_in`
--
ALTER TABLE `invoice_in`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `konsultan`
--
ALTER TABLE `konsultan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `loan`
--
ALTER TABLE `loan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `loan_payment`
--
ALTER TABLE `loan_payment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `local`
--
ALTER TABLE `local`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_view`
--
ALTER TABLE `log_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mcu_create`
--
ALTER TABLE `mcu_create`
  MODIFY `id_mcu` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `overtime`
--
ALTER TABLE `overtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `penalty`
--
ALTER TABLE `penalty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `penalty_type`
--
ALTER TABLE `penalty_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `salary_archive`
--
ALTER TABLE `salary_archive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `salary_history`
--
ALTER TABLE `salary_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `to`
--
ALTER TABLE `to`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `travel_cost`
--
ALTER TABLE `travel_cost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;--
-- Database: `ims_invoice`
--
CREATE DATABASE IF NOT EXISTS `ims_invoice` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims_invoice`;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(1) NOT NULL,
  `username` varchar(200) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `opt_name` varchar(50) NOT NULL,
  `opt_value` varchar(50) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_counter`
--

CREATE TABLE `doc_counter` (
  `doc_code` varchar(10) NOT NULL,
  `last_num` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='menyimpan informasi nomor dokumen terakhir';

-- --------------------------------------------------------

--
-- Table structure for table `log_view`
--

CREATE TABLE `log_view` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `location` varchar(400) NOT NULL,
  `log_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(6) NOT NULL,
  `position` int(3) NOT NULL,
  `stage` int(2) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `href` varchar(200) DEFAULT NULL,
  `level` int(3) NOT NULL,
  `division` varchar(200) DEFAULT NULL,
  `lock` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `opt_name` (`opt_name`);

--
-- Indexes for table `doc_counter`
--
ALTER TABLE `doc_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_view`
--
ALTER TABLE `log_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_counter`
--
ALTER TABLE `doc_counter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_view`
--
ALTER TABLE `log_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;--
-- Database: `ims_iso`
--
CREATE DATABASE IF NOT EXISTS `ims_iso` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims_iso`;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(1) NOT NULL,
  `username` varchar(200) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `opt_name` varchar(50) NOT NULL,
  `opt_value` varchar(50) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_counter`
--

CREATE TABLE `doc_counter` (
  `doc_code` varchar(10) NOT NULL,
  `last_num` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='menyimpan informasi nomor dokumen terakhir';

-- --------------------------------------------------------

--
-- Table structure for table `iaq_detail`
--

CREATE TABLE `iaq_detail` (
  `id` int(11) NOT NULL,
  `id_main` int(11) NOT NULL,
  `judul` varchar(30) NOT NULL,
  `Question` text NOT NULL,
  `Evidence` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iaq_detail`
--

INSERT INTO `iaq_detail` (`id`, `id_main`, `judul`, `Question`, `Evidence`) VALUES
(1, 1, 'sdsdc', '<p>sdsscd</p>', '<p>sdc</p>'),
(2, 1, 'sdsdc', '<p>sdc</p>', '<p>sdc</p>'),
(3, 1, 'sdsdc', '<p>sdc</p>', '<p>sdc</p>'),
(4, 1, 'sdsdc', '<p>sdc</p>', '<p>sdc</p>'),
(5, 1, 'sdsdc', '<p>sdc</p>', '<p>sdc</p>');

-- --------------------------------------------------------

--
-- Table structure for table `iaq_main`
--

CREATE TABLE `iaq_main` (
  `id` int(11) NOT NULL,
  `divisi` varchar(30) DEFAULT NULL,
  `judul` text NOT NULL,
  `creator` varchar(30) DEFAULT NULL,
  `create_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iaq_main`
--

INSERT INTO `iaq_main` (`id`, `divisi`, `judul`, `creator`, `create_date`) VALUES
(1, 'admin', 'sdsdc', 'superuser', '2017-05-22');

-- --------------------------------------------------------

--
-- Table structure for table `isofile`
--

CREATE TABLE `isofile` (
  `id` int(11) NOT NULL,
  `author` varchar(30) DEFAULT NULL,
  `divisi` varchar(30) NOT NULL,
  `no_dokumen` varchar(30) NOT NULL,
  `tanggal_terbit` date NOT NULL,
  `revisi` date DEFAULT NULL,
  `judul` varchar(100) NOT NULL,
  `sasaran` text NOT NULL,
  `ruang_lingkup` text NOT NULL,
  `definisi` text NOT NULL,
  `ukuran_kinerja` text,
  `kondisi_umum` text,
  `klausul_1` text,
  `klausul_2` text,
  `klausul_3` text,
  `catatan` text,
  `lampiran` text,
  `tanggal_revisi_lampiran` date DEFAULT NULL,
  `nama_prosedur` varchar(50) DEFAULT NULL,
  `tanggal_pembuatan_sop` date DEFAULT NULL,
  `tanggal_pengesahan_sop` date DEFAULT NULL,
  `tanggal_revisi_sop` date DEFAULT NULL,
  `dasar_hukum` text,
  `kualifikasi_pelaksana` text,
  `keterkaitan` text,
  `peralatan` text,
  `peringatan` text,
  `pendataan` text,
  `uraian_kegiatan` text,
  `parent_no` varchar(500) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `isofile`
--

INSERT INTO `isofile` (`id`, `author`, `divisi`, `no_dokumen`, `tanggal_terbit`, `revisi`, `judul`, `sasaran`, `ruang_lingkup`, `definisi`, `ukuran_kinerja`, `kondisi_umum`, `klausul_1`, `klausul_2`, `klausul_3`, `catatan`, `lampiran`, `tanggal_revisi_lampiran`, `nama_prosedur`, `tanggal_pembuatan_sop`, `tanggal_pengesahan_sop`, `tanggal_revisi_sop`, `dasar_hukum`, `kualifikasi_pelaksana`, `keterkaitan`, `peralatan`, `peringatan`, `pendataan`, `uraian_kegiatan`, `parent_no`) VALUES
(1, 'sdcsdc', 'FINANCE', 'scd', '2017-05-20', '2017-05-22', 'sdc', '<p>sdc</p>', '<p>sd</p>', '<p>sdc</p>', '<p>sdc</p>', '', '<p>sd</p>', '<p>scd</p>', '', '<p>sdc</p>', '<p>sdc</p>', NULL, 'sdc', NULL, '2017-05-19', NULL, '', '', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kpi_data_detail`
--

CREATE TABLE `kpi_data_detail` (
  `id` int(11) NOT NULL,
  `id_main` int(11) DEFAULT NULL,
  `deskripsi` text,
  `metode_ukur` text,
  `metode_approach` text,
  `target` varchar(30) DEFAULT NULL,
  `comment` text,
  `actual` int(30) DEFAULT NULL,
  `target_uom` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kpi_data_main`
--

CREATE TABLE `kpi_data_main` (
  `id` int(11) NOT NULL,
  `judul` text,
  `date_input` date DEFAULT NULL,
  `divisi` varchar(20) DEFAULT NULL,
  `author` varchar(20) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kpi_master_detail`
--

CREATE TABLE `kpi_master_detail` (
  `id` int(11) NOT NULL,
  `id_main` int(11) DEFAULT NULL,
  `deskripsi` text,
  `metode_ukur` text,
  `metode_approach` text,
  `target` varchar(30) DEFAULT NULL,
  `target_uom` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kpi_master_main`
--

CREATE TABLE `kpi_master_main` (
  `id` int(11) NOT NULL,
  `judul` varchar(400) DEFAULT NULL,
  `divisi` varchar(30) DEFAULT NULL,
  `periode` varchar(30) DEFAULT NULL,
  `pic` varchar(20) DEFAULT NULL,
  `bisnis_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_view`
--

CREATE TABLE `log_view` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `location` varchar(400) NOT NULL,
  `log_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_view`
--

INSERT INTO `log_view` (`id`, `user`, `location`, `log_time`) VALUES
(1, 'superuser', '/ims99/iso/', '2017-04-21 04:02:05'),
(2, 'superuser', '/ims99/iso/', '2017-04-21 04:02:12'),
(3, 'superuser', '/ims99/iso/', '2017-05-22 02:58:03'),
(4, 'superuser', '/ims99/iso/', '2017-05-22 03:07:09'),
(5, 'superuser', '/ims99/iso/', '2017-05-22 03:09:42'),
(6, 'superuser', '/ims99/iso/?m=iaq_main', '2017-05-22 03:09:46'),
(7, 'superuser', '/ims99/iso/?m=iso_main', '2017-05-22 03:09:49'),
(8, 'superuser', '/ims99/iso/?m=iso_divisi', '2017-05-22 03:09:51'),
(9, 'superuser', '/ims99/iso/?m=iso_main', '2017-05-22 03:10:00'),
(10, 'superuser', '/ims99/iso/?m=iaq_main', '2017-05-22 03:10:01'),
(11, 'superuser', '/ims99/iso/?m=iaq_detail', '2017-05-22 03:10:02'),
(12, 'superuser', '/ims99/iso/?m=iaq_detail', '2017-05-22 03:10:13'),
(13, 'superuser', '/ims99/iso/?m=iaq_main', '2017-05-22 03:10:14'),
(14, 'superuser', '/ims99/iso/?m=iaq_list&id=1', '2017-05-22 03:10:15'),
(15, 'superuser', '/ims99/iso/?m=iaq_main', '2017-05-22 03:10:19'),
(16, 'superuser', '/ims99/iso/?m=iaq_list&id=1', '2017-05-22 03:10:22'),
(17, 'superuser', '/ims99/iso/?m=iaq_main', '2017-05-22 03:10:24'),
(18, 'superuser', '/ims99/iso/?m=iso_main', '2017-05-22 03:10:25'),
(19, 'superuser', '/ims99/iso/?m=iso_add', '2017-05-22 03:10:27'),
(20, 'superuser', '/ims99/iso/?m=iso_add', '2017-05-22 03:10:46'),
(21, 'superuser', '/ims99/iso/?m=iso_main', '2017-05-22 03:10:46'),
(22, 'superuser', '/ims99/iso/?m=iso_template&id=1', '2017-05-22 03:10:48'),
(23, 'superuser', '/ims99/iso/?m=iso_main', '2017-05-22 03:10:54'),
(24, 'superuser', '/ims99/iso/?m=iso_sop&id=1', '2017-05-22 03:10:56'),
(25, 'superuser', '/ims99/iso/?m=iso_sop&id=1', '2017-05-22 03:11:04'),
(26, 'superuser', '/ims99/iso/?m=iso_main', '2017-05-22 03:11:04'),
(27, 'superuser', '/ims99/iso/?m=iso_sop&revisi&id=1', '2017-05-22 03:11:06'),
(28, 'superuser', '/ims99/iso/?m=iso_main', '2017-05-22 03:11:09'),
(29, 'superuser', '/ims99/iso/?m=iso_lampiran&id=1', '2017-05-22 03:11:11'),
(30, 'superuser', '/ims99/iso/?m=iso_lampiran&id=1', '2017-05-22 03:11:14'),
(31, 'superuser', '/ims99/iso/?m=iso_main', '2017-05-22 03:11:14'),
(32, 'superuser', '/ims99/iso/?m=iso_edit&id=1', '2017-05-22 03:11:16'),
(33, 'superuser', '/ims99/iso/?m=iso_edit&id=1', '2017-05-22 03:11:18'),
(34, 'superuser', '/ims99/iso/?m=iso_main', '2017-05-22 03:11:19'),
(35, 'superuser', '/ims99/iso/?m=iaq_main', '2017-05-22 03:22:33'),
(36, 'superuser', '/ims99/iso/?m=iso_main', '2017-05-22 03:22:35'),
(37, 'superuser', '/ims99/iso/?m=iso_divisi', '2017-05-22 03:22:37'),
(38, 'superuser', '/ims99/iso/?m=iso_divisi', '2017-05-22 03:25:32'),
(39, 'superuser', '/ims99/iso/?m=iaq_main', '2017-05-22 03:25:34'),
(40, 'superuser', '/ims99/iso/?m=iso_divisi', '2017-05-22 03:25:36'),
(41, 'superuser', '/ims99/iso/?m=iso_main', '2017-05-22 03:25:37'),
(42, 'superuser', '/ims99/iso/?m=iaq_main', '2017-05-22 03:25:39'),
(43, 'superuser', '/ims99/iso/?m=iso_main', '2017-05-22 03:25:40'),
(44, 'superuser', '/ims99/iso/?m=iso_edit&id=1', '2017-05-22 03:25:40'),
(45, 'superuser', '/ims99/iso/?m=iso_main', '2017-05-22 03:25:41'),
(46, 'superuser', '/ims99/iso/?m=iso_lampiran&id=1', '2017-05-22 03:25:42'),
(47, 'superuser', '/ims99/iso/?m=iso_main', '2017-05-22 03:25:42'),
(48, 'superuser', '/ims99/iso/', '2017-05-22 03:25:45'),
(49, 'superuser', '/ims99/iso/?m=iaq_main', '2017-05-22 03:25:59'),
(50, 'superuser', '/ims99/iso/', '2017-05-22 03:26:02');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(6) NOT NULL,
  `position` int(3) NOT NULL,
  `stage` int(2) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `href` varchar(200) DEFAULT NULL,
  `level` int(3) NOT NULL,
  `division` varchar(200) DEFAULT NULL,
  `lock` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `position`, `stage`, `name`, `parent`, `href`, `level`, `division`, `lock`) VALUES
(1, 100, 1, 'Internal Audit', NULL, '?m=iaq_main', 0, NULL, NULL),
(2, 99, 1, 'ISO Manual', NULL, '?m=iso_main', 0, NULL, NULL),
(3, 98, 1, 'Business Process', NULL, '?m=iso_divisi', 0, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `opt_name` (`opt_name`);

--
-- Indexes for table `doc_counter`
--
ALTER TABLE `doc_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iaq_detail`
--
ALTER TABLE `iaq_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iaq_main`
--
ALTER TABLE `iaq_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `isofile`
--
ALTER TABLE `isofile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kpi_data_detail`
--
ALTER TABLE `kpi_data_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kpi_data_main`
--
ALTER TABLE `kpi_data_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kpi_master_detail`
--
ALTER TABLE `kpi_master_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kpi_master_main`
--
ALTER TABLE `kpi_master_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_view`
--
ALTER TABLE `log_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_counter`
--
ALTER TABLE `doc_counter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `iaq_detail`
--
ALTER TABLE `iaq_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `iaq_main`
--
ALTER TABLE `iaq_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `isofile`
--
ALTER TABLE `isofile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kpi_data_detail`
--
ALTER TABLE `kpi_data_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kpi_data_main`
--
ALTER TABLE `kpi_data_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kpi_master_detail`
--
ALTER TABLE `kpi_master_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kpi_master_main`
--
ALTER TABLE `kpi_master_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_view`
--
ALTER TABLE `log_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;--
-- Database: `ims_it`
--
CREATE DATABASE IF NOT EXISTS `ims_it` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims_it`;

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `id_activity` int(3) NOT NULL,
  `id_dar` int(3) NOT NULL,
  `time` time NOT NULL,
  `activity` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(1) NOT NULL,
  `username` varchar(200) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `opt_name` varchar(50) NOT NULL,
  `opt_value` varchar(50) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_counter`
--

CREATE TABLE `doc_counter` (
  `doc_code` varchar(10) NOT NULL,
  `last_num` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='menyimpan informasi nomor dokumen terakhir';

-- --------------------------------------------------------

--
-- Table structure for table `engine_report`
--

CREATE TABLE `engine_report` (
  `id_er` int(3) NOT NULL,
  `id_dar` int(3) NOT NULL,
  `description` varchar(50) NOT NULL,
  `status` varchar(25) NOT NULL,
  `RH_prev` int(25) NOT NULL,
  `RH_today` int(25) NOT NULL,
  `total` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `epf_report`
--

CREATE TABLE `epf_report` (
  `id_epf` int(3) NOT NULL,
  `id_dar` int(3) NOT NULL,
  `lamp` varchar(25) NOT NULL,
  `lamp1` varchar(25) NOT NULL,
  `lamp2` varchar(25) NOT NULL,
  `lamp3` varchar(25) NOT NULL,
  `lamp4` varchar(25) NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id_inven` int(3) NOT NULL,
  `id_dar` int(3) NOT NULL,
  `description` varchar(50) NOT NULL,
  `merk` varchar(50) NOT NULL,
  `status` varchar(25) NOT NULL,
  `remark` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(5) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id_komentar` int(200) NOT NULL,
  `id_topik` int(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `tgl_komentar` date NOT NULL,
  `komentar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_view`
--

CREATE TABLE `log_view` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `location` varchar(400) NOT NULL,
  `log_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `main_dar`
--

CREATE TABLE `main_dar` (
  `id_dar` int(3) NOT NULL,
  `proj_name` varchar(25) NOT NULL,
  `proj_location` varchar(25) NOT NULL,
  `weather` varchar(25) NOT NULL,
  `current_op` varchar(25) NOT NULL,
  `planned_op` varchar(25) NOT NULL,
  `date` date NOT NULL,
  `report_no` varchar(25) NOT NULL,
  `time_start` int(4) NOT NULL,
  `time_stop` int(4) NOT NULL,
  `status` enum('FINISHED','UNFINISHED') NOT NULL DEFAULT 'UNFINISHED'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `material_report`
--

CREATE TABLE `material_report` (
  `id_mr` int(11) NOT NULL,
  `id_dar` int(3) NOT NULL,
  `description` varchar(25) NOT NULL,
  `stock` int(11) NOT NULL,
  `cons_today` int(11) NOT NULL,
  `cons_day` int(11) NOT NULL,
  `cons_total` int(11) NOT NULL,
  `remaining_stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(6) NOT NULL,
  `position` int(3) NOT NULL,
  `stage` int(2) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `href` varchar(200) DEFAULT NULL,
  `level` int(3) NOT NULL,
  `division` varchar(200) DEFAULT NULL,
  `lock` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `personel`
--

CREATE TABLE `personel` (
  `id_personel` int(3) NOT NULL,
  `id_dar` int(3) NOT NULL,
  `chief_op` varchar(25) NOT NULL,
  `morning_shift` varchar(25) NOT NULL,
  `night_shift` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pro_detail`
--

CREATE TABLE `pro_detail` (
  `id` int(10) NOT NULL,
  `id_prj_code` varchar(10) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pro_pic`
--

CREATE TABLE `pro_pic` (
  `id` int(10) NOT NULL,
  `address` varchar(250) DEFAULT NULL,
  `id_pro_detail` int(10) DEFAULT NULL,
  `type` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `topik`
--

CREATE TABLE `topik` (
  `id_topik` int(50) NOT NULL,
  `id_kategori` int(50) NOT NULL,
  `username` varchar(200) NOT NULL,
  `subjek` varchar(200) NOT NULL,
  `isi_topik` text NOT NULL,
  `tgl_topik` date NOT NULL,
  `tgl_tanggapan` date NOT NULL,
  `user` varchar(200) NOT NULL,
  `status` varchar(50) NOT NULL,
  `msg` text NOT NULL,
  `ok_user` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id_activity`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `opt_name` (`opt_name`);

--
-- Indexes for table `doc_counter`
--
ALTER TABLE `doc_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `engine_report`
--
ALTER TABLE `engine_report`
  ADD PRIMARY KEY (`id_er`);

--
-- Indexes for table `epf_report`
--
ALTER TABLE `epf_report`
  ADD PRIMARY KEY (`id_epf`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id_inven`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id_komentar`);

--
-- Indexes for table `log_view`
--
ALTER TABLE `log_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_dar`
--
ALTER TABLE `main_dar`
  ADD PRIMARY KEY (`id_dar`);

--
-- Indexes for table `material_report`
--
ALTER TABLE `material_report`
  ADD PRIMARY KEY (`id_mr`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personel`
--
ALTER TABLE `personel`
  ADD PRIMARY KEY (`id_personel`);

--
-- Indexes for table `pro_detail`
--
ALTER TABLE `pro_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pro_pic`
--
ALTER TABLE `pro_pic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topik`
--
ALTER TABLE `topik`
  ADD PRIMARY KEY (`id_topik`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `id_activity` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_counter`
--
ALTER TABLE `doc_counter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `engine_report`
--
ALTER TABLE `engine_report`
  MODIFY `id_er` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `epf_report`
--
ALTER TABLE `epf_report`
  MODIFY `id_epf` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id_inven` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id_komentar` int(200) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_view`
--
ALTER TABLE `log_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `main_dar`
--
ALTER TABLE `main_dar`
  MODIFY `id_dar` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `material_report`
--
ALTER TABLE `material_report`
  MODIFY `id_mr` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `personel`
--
ALTER TABLE `personel`
  MODIFY `id_personel` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pro_detail`
--
ALTER TABLE `pro_detail`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pro_pic`
--
ALTER TABLE `pro_pic`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `topik`
--
ALTER TABLE `topik`
  MODIFY `id_topik` int(50) NOT NULL AUTO_INCREMENT;--
-- Database: `ims_marketing`
--
CREATE DATABASE IF NOT EXISTS `ims_marketing` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims_marketing`;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(1) NOT NULL,
  `username` varchar(200) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bp_bond_currency`
--

CREATE TABLE `bp_bond_currency` (
  `id` int(11) NOT NULL,
  `currency` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bp_bond_detail`
--

CREATE TABLE `bp_bond_detail` (
  `id_detail` int(11) NOT NULL,
  `id_main` int(11) NOT NULL,
  `prj_code` varchar(5) NOT NULL,
  `item_name` varchar(50) NOT NULL,
  `request_amount` decimal(15,2) DEFAULT NULL,
  `actual_amount` decimal(15,2) NOT NULL,
  `currency` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bp_bond_main`
--

CREATE TABLE `bp_bond_main` (
  `id` int(11) NOT NULL,
  `prj_code` varchar(5) NOT NULL,
  `prj_name` varchar(50) NOT NULL,
  `perusahaan` varchar(50) DEFAULT NULL,
  `no_tender` varchar(50) NOT NULL,
  `no_bond` varchar(50) NOT NULL,
  `type_bond` varchar(20) DEFAULT NULL,
  `submit_date` date NOT NULL,
  `input_date` date NOT NULL,
  `currency` varchar(5) NOT NULL,
  `app_date` date DEFAULT NULL,
  `price_date` date DEFAULT NULL,
  `release_date` date DEFAULT NULL,
  `alasan_approve_operation` text NOT NULL,
  `alasan_reject_operation` text NOT NULL,
  `alasan_approve_finance` text NOT NULL,
  `alasan_reject_finance` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `nama_prj` text NOT NULL,
  `nilai_jaminan` decimal(17,2) NOT NULL,
  `date1` date DEFAULT NULL,
  `date2` date DEFAULT NULL,
  `durasi` int(6) DEFAULT NULL,
  `retrieve_by` varchar(50) DEFAULT NULL,
  `retrieve_date` date DEFAULT NULL,
  `retrieve_to` varchar(50) DEFAULT NULL,
  `receive_by` varchar(50) DEFAULT NULL,
  `receive_date` date DEFAULT NULL,
  `receive_to` varchar(50) DEFAULT NULL,
  `final_by` varchar(50) DEFAULT NULL,
  `final_date` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `c_prognosis`
--

CREATE TABLE `c_prognosis` (
  `id` int(11) NOT NULL,
  `RCTR` varchar(40) NOT NULL,
  `subject` varchar(400) NOT NULL,
  `description` text NOT NULL,
  `category` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `id_project` int(11) NOT NULL,
  `amount` decimal(15,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `opt_name` varchar(50) NOT NULL,
  `opt_value` varchar(50) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_counter`
--

CREATE TABLE `doc_counter` (
  `doc_code` varchar(10) NOT NULL,
  `last_num` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='menyimpan informasi nomor dokumen terakhir';

-- --------------------------------------------------------

--
-- Table structure for table `fix_pl`
--

CREATE TABLE `fix_pl` (
  `id` int(11) NOT NULL,
  `RCTR` varchar(40) NOT NULL,
  `subject` varchar(400) NOT NULL,
  `description` text NOT NULL,
  `category` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `id_project` int(11) NOT NULL,
  `amount` decimal(15,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_view`
--

CREATE TABLE `log_view` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `location` varchar(400) NOT NULL,
  `log_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_view`
--

INSERT INTO `log_view` (`id`, `user`, `location`, `log_time`) VALUES
(1, 'superuser', '/ims99/marketing/index.php?m=inventory_pinjam&op=inventory_pinjam', '2017-05-24 02:46:48'),
(2, 'superuser', '/ims99/marketing/index.php?m=inventory_pinjam&add&op=inventory_pinjam', '2017-05-24 02:46:57'),
(3, 'superuser', '/ims99/marketing/index.php?m=inventory_pinjam&op=inventory_pinjam', '2017-05-24 02:46:57'),
(4, 'superuser', '/ims99/marketing/index.php?m=inventory_approve&op=inventory_pinjam&id=1', '2017-05-24 02:46:59'),
(5, 'superuser', '/ims99/marketing/index.php?m=inventory_approve&op=inventory_pinjam&id=1', '2017-05-24 02:47:03'),
(6, 'superuser', '/ims99/marketing/index.php?m=inventory_pinjam&op=inventory_pinjam', '2017-05-24 02:47:05'),
(7, 'superuser', '/ims99/marketing/index.php?m=inventory_pinjam&op=inventory_pinjam', '2017-05-24 08:20:37'),
(8, 'superuser', '/ims99/marketing/index.php?m=projects&t=Projects&op=projects', '2017-05-24 08:22:53'),
(9, 'superuser', '/ims99/marketing/index.php?m=inventory_pinjam&op=inventory_pinjam', '2017-05-24 08:22:57'),
(10, 'superuser', '/ims99/marketing/index.php?m=inventory_pinjam&op=inventory_pinjam', '2017-05-24 08:24:25'),
(11, 'superuser', '/ims99/marketing/index.php?m=td_main&op=td_main', '2017-05-29 07:51:27'),
(12, 'superuser', '/ims99/marketing/index.php?m=td_main&op=td_main', '2017-05-29 07:51:43'),
(13, 'superuser', '/ims99/marketing/index.php?m=td_main&op=td_main', '2017-05-29 07:51:44'),
(14, 'superuser', '/ims99/marketing/index.php?m=td_edit&op=td_main&id=1', '2017-05-29 07:51:47'),
(15, 'superuser', '/ims99/marketing/index.php?m=td_update_tanggal_pq&op=td_main&id=1', '2017-05-29 07:52:11'),
(16, 'superuser', '/ims99/marketing/index.php?m=td_edit&op=td_main&id=1', '2017-05-29 07:52:13'),
(17, 'superuser', '/ims99/marketing/index.php?m=td_pq_main&op=td_main&id=1', '2017-05-29 07:52:19'),
(18, 'superuser', '/ims99/marketing/index.php?m=td_pq_tambah_data&id=1', '2017-05-29 07:52:21'),
(19, 'superuser', '/ims99/marketing/index.php?m=td_pq_tambah_data', '2017-05-29 07:52:26'),
(20, 'superuser', '/ims99/marketing/index.php?m=td_pq_tambah_data&id=1', '2017-05-29 07:52:28'),
(21, 'superuser', '/ims99/marketing/index.php?m=td_pq_main&op=td_main&id=1', '2017-05-29 07:52:30'),
(22, 'superuser', '/ims99/marketing/index.php?m=td_edit&id=1', '2017-05-29 07:52:34'),
(23, 'superuser', '/ims99/marketing/index.php?m=td_edit&op=td_main&id=1', '2017-05-29 07:52:37'),
(24, 'superuser', '/ims99/marketing/index.php?m=td_edit&id=1', '2017-05-29 07:52:38'),
(25, 'superuser', '/ims99/marketing/index.php?m=td_pq_main&op=td_main&id=1', '2017-05-29 07:52:42'),
(26, 'superuser', '/ims99/marketing/index.php?m=td_edit_pq&id=1', '2017-05-29 07:52:44'),
(27, 'superuser', '/ims99/marketing/index.php?m=td_edit_pq', '2017-05-29 07:52:46'),
(28, 'superuser', '/ims99/marketing/index.php?m=td_pq_main&op=td_main&id=1', '2017-05-29 07:52:48'),
(29, 'superuser', '/ims99/marketing/index.php?m=td_update_status_pq&id=1', '2017-05-29 07:53:06'),
(30, 'superuser', '/ims99/marketing/index.php?m=td_pq_main&op=td_main&id=1', '2017-05-29 07:53:08'),
(31, 'superuser', '/ims99/marketing/index.php?m=td_tambah_peserta&id=1', '2017-05-29 07:53:12'),
(32, 'superuser', '/ims99/marketing/index.php?m=td_pq_tambah_data', '2017-05-29 07:53:15'),
(33, 'superuser', '/ims99/marketing/index.php?m=td_tambah_peserta&id=1', '2017-05-29 07:53:16'),
(34, 'superuser', '/ims99/marketing/index.php?m=td_pq_main&id=1', '2017-05-29 07:53:17'),
(35, 'superuser', '/ims99/marketing/index.php?m=td_edit_tender&id=1', '2017-05-29 07:58:48'),
(36, 'superuser', '/ims99/marketing/index.php?m=td_edit_tender', '2017-05-29 07:58:52'),
(37, 'superuser', '/ims99/marketing/index.php?m=td_pq_main&id=1', '2017-05-29 07:58:53'),
(38, 'superuser', '/ims99/marketing/index.php?m=td_edit_tender&id=2', '2017-05-29 07:58:59'),
(39, 'superuser', '/ims99/marketing/index.php?m=td_main', '2017-05-29 07:59:01');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(6) NOT NULL,
  `position` int(3) NOT NULL,
  `stage` int(2) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `href` varchar(200) DEFAULT NULL,
  `level` int(3) NOT NULL,
  `division` varchar(200) DEFAULT NULL,
  `lock` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paper_pq`
--

CREATE TABLE `paper_pq` (
  `id_pq` int(11) NOT NULL,
  `psirep` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `procuration` varchar(400) DEFAULT NULL,
  `tender` varchar(400) DEFAULT NULL,
  `document` varchar(400) DEFAULT NULL,
  `notender` varchar(400) DEFAULT NULL,
  `related` varchar(400) DEFAULT NULL,
  `time` varchar(400) DEFAULT NULL,
  `company` varchar(400) NOT NULL,
  `bid` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paper_pq_detail`
--

CREATE TABLE `paper_pq_detail` (
  `id_pq_detail` int(11) NOT NULL,
  `id_pq` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `jammasuk` varchar(400) DEFAULT NULL,
  `jamkeluar` varchar(400) DEFAULT NULL,
  `servicecompany` varchar(400) DEFAULT NULL,
  `catatan` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sim`
--

CREATE TABLE `sim` (
  `id_prg` int(11) NOT NULL,
  `no_sim` int(11) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `remark` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subcost`
--

CREATE TABLE `subcost` (
  `id` int(11) NOT NULL,
  `subject` varchar(400) NOT NULL,
  `input_date` date NOT NULL,
  `f_approve` varchar(100) DEFAULT NULL,
  `f_approve_time` date DEFAULT NULL,
  `d_approve` varchar(100) DEFAULT NULL,
  `d_approve_time` date DEFAULT NULL,
  `currency` varchar(8) NOT NULL,
  `sisa` decimal(15,2) DEFAULT NULL,
  `division` varchar(80) DEFAULT NULL,
  `done` date DEFAULT NULL,
  `user` varchar(100) NOT NULL DEFAULT 'open',
  `project` varchar(40) DEFAULT NULL,
  `from` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subcost_detail`
--

CREATE TABLE `subcost_detail` (
  `id` int(11) NOT NULL,
  `id_subcost` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `no_nota` varchar(400) NOT NULL,
  `deskripsi` text NOT NULL,
  `receiver` varchar(200) DEFAULT NULL,
  `deliver` varchar(200) DEFAULT NULL,
  `cashin` decimal(15,2) NOT NULL,
  `cashout` decimal(15,2) NOT NULL,
  `dcashin` decimal(15,2) NOT NULL,
  `dcashout` decimal(15,2) NOT NULL,
  `category` varchar(20) DEFAULT NULL,
  `source_string` varchar(100) DEFAULT NULL,
  `source_int` int(4) DEFAULT NULL,
  `ceo_approve` varchar(40) DEFAULT NULL,
  `ceo_approve_time` date DEFAULT NULL,
  `fin_approve` varchar(40) DEFAULT NULL,
  `fin_approve_time` date DEFAULT NULL,
  `ops_approve` varchar(40) DEFAULT NULL,
  `ops_approve_time` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `td_aw`
--

CREATE TABLE `td_aw` (
  `id` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `tanya_jawab` text NOT NULL,
  `floor` varchar(30) NOT NULL,
  `revisi` text NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `td_document1`
--

CREATE TABLE `td_document1` (
  `id` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `document` text NOT NULL,
  `floor` varchar(30) NOT NULL,
  `revisi` text NOT NULL,
  `status` varchar(10) NOT NULL,
  `tanggal_submit` datetime DEFAULT NULL,
  `sebar_doc1` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `td_document2`
--

CREATE TABLE `td_document2` (
  `id` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `document` text NOT NULL,
  `floor` varchar(30) NOT NULL,
  `revisi` text NOT NULL,
  `status` varchar(10) NOT NULL,
  `tanggal_submit` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `td_jadwal`
--

CREATE TABLE `td_jadwal` (
  `id` int(11) NOT NULL,
  `nama_proyek` text NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `jadwal_pendaftaran` datetime NOT NULL,
  `tipe_pekerjaan` varchar(50) NOT NULL,
  `lokasi_pekerjaan` varchar(50) NOT NULL,
  `tanggal_pq` datetime DEFAULT NULL,
  `tanggal_aw` datetime DEFAULT NULL,
  `tanggal_submit1` datetime DEFAULT NULL,
  `tanggal_submit2` datetime DEFAULT NULL,
  `info` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `kondisi` varchar(10) DEFAULT NULL,
  `alasan_pq` text NOT NULL,
  `alasan_aw` text NOT NULL,
  `alasan_doc1` text NOT NULL,
  `alasan_doc2` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `td_jadwal`
--

INSERT INTO `td_jadwal` (`id`, `nama_proyek`, `nama_perusahaan`, `jadwal_pendaftaran`, `tipe_pekerjaan`, `lokasi_pekerjaan`, `tanggal_pq`, `tanggal_aw`, `tanggal_submit1`, `tanggal_submit2`, `info`, `status`, `kondisi`, `alasan_pq`, `alasan_aw`, `alasan_doc1`, `alasan_doc2`) VALUES
(1, '<p>wed</p>', 'Test', '2017-05-12 12:50:00', 'wd', 'wd', '2017-05-25 11:11:00', '2017-05-18 11:11:00', NULL, NULL, 'ed', 'create', 'aktif', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `td_perusahaan`
--

CREATE TABLE `td_perusahaan` (
  `id` int(11) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `td_perusahaan`
--

INSERT INTO `td_perusahaan` (`id`, `nama_perusahaan`) VALUES
(1, 'Test');

-- --------------------------------------------------------

--
-- Table structure for table `td_peserta_tender`
--

CREATE TABLE `td_peserta_tender` (
  `id` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `kondisi` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `td_peserta_tender`
--

INSERT INTO `td_peserta_tender` (`id`, `id_jadwal`, `nama_perusahaan`, `harga`, `status`, `kondisi`) VALUES
(1, 1, 'PT Putra Sejati Indomakmur', 34534534, 'None', 'Pemenang'),
(2, 1, '1221w', 0, 'PQ', 'aktif');

-- --------------------------------------------------------

--
-- Table structure for table `td_pq`
--

CREATE TABLE `td_pq` (
  `id` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `tanya_jawab` text NOT NULL,
  `floor` varchar(30) NOT NULL,
  `revisi` text NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `td_pq`
--

INSERT INTO `td_pq` (`id`, `id_jadwal`, `tanya_jawab`, `floor`, `revisi`, `status`) VALUES
(1, 1, 'qqewwe', 'qweqwe', 'qweqwe', 'ok');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bp_bond_currency`
--
ALTER TABLE `bp_bond_currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bp_bond_detail`
--
ALTER TABLE `bp_bond_detail`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indexes for table `bp_bond_main`
--
ALTER TABLE `bp_bond_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_prognosis`
--
ALTER TABLE `c_prognosis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `opt_name` (`opt_name`);

--
-- Indexes for table `doc_counter`
--
ALTER TABLE `doc_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fix_pl`
--
ALTER TABLE `fix_pl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_view`
--
ALTER TABLE `log_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paper_pq`
--
ALTER TABLE `paper_pq`
  ADD PRIMARY KEY (`id_pq`);

--
-- Indexes for table `paper_pq_detail`
--
ALTER TABLE `paper_pq_detail`
  ADD PRIMARY KEY (`id_pq_detail`);

--
-- Indexes for table `subcost`
--
ALTER TABLE `subcost`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcost_detail`
--
ALTER TABLE `subcost_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `td_aw`
--
ALTER TABLE `td_aw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `td_document1`
--
ALTER TABLE `td_document1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `td_document2`
--
ALTER TABLE `td_document2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `td_jadwal`
--
ALTER TABLE `td_jadwal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `td_perusahaan`
--
ALTER TABLE `td_perusahaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `td_peserta_tender`
--
ALTER TABLE `td_peserta_tender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `td_pq`
--
ALTER TABLE `td_pq`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bp_bond_currency`
--
ALTER TABLE `bp_bond_currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bp_bond_detail`
--
ALTER TABLE `bp_bond_detail`
  MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bp_bond_main`
--
ALTER TABLE `bp_bond_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `c_prognosis`
--
ALTER TABLE `c_prognosis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_counter`
--
ALTER TABLE `doc_counter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fix_pl`
--
ALTER TABLE `fix_pl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_view`
--
ALTER TABLE `log_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `paper_pq`
--
ALTER TABLE `paper_pq`
  MODIFY `id_pq` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `paper_pq_detail`
--
ALTER TABLE `paper_pq_detail`
  MODIFY `id_pq_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subcost`
--
ALTER TABLE `subcost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subcost_detail`
--
ALTER TABLE `subcost_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `td_aw`
--
ALTER TABLE `td_aw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `td_document1`
--
ALTER TABLE `td_document1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `td_document2`
--
ALTER TABLE `td_document2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `td_jadwal`
--
ALTER TABLE `td_jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `td_perusahaan`
--
ALTER TABLE `td_perusahaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `td_peserta_tender`
--
ALTER TABLE `td_peserta_tender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `td_pq`
--
ALTER TABLE `td_pq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;--
-- Database: `local_ims_rms`
--
CREATE DATABASE IF NOT EXISTS `local_ims_rms` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `local_ims_rms`;

-- --------------------------------------------------------

--
-- Table structure for table `actions`
--

CREATE TABLE `actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `actions`
--

INSERT INTO `actions` (`id`, `name`, `description`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'create', 'add new', NULL, NULL, NULL, '2017-03-10 00:04:08', NULL, NULL),
(2, 'read', 'read function', NULL, NULL, NULL, '2017-03-10 00:57:13', NULL, NULL),
(3, 'update', 'update function', NULL, NULL, NULL, '2017-03-10 00:57:25', NULL, NULL),
(4, 'edit', 'edit function', NULL, NULL, NULL, '2017-03-10 00:57:43', NULL, NULL),
(6, 'delete', 'function for delete data', NULL, NULL, NULL, '2017-03-12 20:22:27', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `default_roles`
--

CREATE TABLE `default_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_jobs` int(10) UNSIGNED NOT NULL,
  `id_modules` int(10) UNSIGNED NOT NULL,
  `id_actions` int(10) UNSIGNED NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_roles`
--

INSERT INTO `default_roles` (`id`, `id_jobs`, `id_modules`, `id_actions`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(10, 4, 3, 1, NULL, NULL, NULL, '2017-05-24 03:00:51', NULL, NULL),
(11, 4, 3, 2, NULL, NULL, NULL, '2017-05-24 03:00:51', NULL, NULL),
(12, 4, 4, 2, NULL, NULL, NULL, '2017-05-24 03:00:51', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `divisions`
--

CREATE TABLE `divisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `divisions`
--

INSERT INTO `divisions` (`id`, `name`, `description`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'IT', 'divisi IT', NULL, NULL, NULL, '2017-03-12 19:58:03', NULL, NULL),
(4, 'HRD', 'divisi HRD', NULL, NULL, NULL, '2017-03-12 19:58:18', NULL, NULL),
(5, 'Finance', 'divisi finance', NULL, NULL, NULL, '2017-03-12 19:58:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_master_employees` int(10) UNSIGNED NOT NULL,
  `id_jobs` int(10) UNSIGNED NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `identities`
--

CREATE TABLE `identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `identities`
--

INSERT INTO `identities` (`id`, `name`, `description`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'saya', 'desct saya', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'job', 'job desc', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_divisions` int(10) UNSIGNED NOT NULL,
  `id_jobtitles` int(10) UNSIGNED NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `id_divisions`, `id_jobtitles`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 3, 3, NULL, NULL, NULL, '2017-03-12 20:01:30', NULL, NULL),
(5, 4, 4, NULL, NULL, NULL, '2017-03-12 20:01:36', NULL, NULL),
(6, 5, 5, NULL, NULL, NULL, '2017-03-12 20:01:43', NULL, NULL),
(11, 4, 1, NULL, NULL, NULL, '2017-05-24 02:15:49', NULL, NULL),
(12, 3, 4, NULL, NULL, NULL, '2017-05-24 05:17:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jobtitles`
--

CREATE TABLE `jobtitles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jobtitles`
--

INSERT INTO `jobtitles` (`id`, `name`, `description`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Direktur', 'Direktur', NULL, NULL, NULL, '2017-05-23 03:52:36', NULL, NULL),
(2, 'Manager', 'Manager', NULL, NULL, NULL, '2017-05-23 03:52:47', NULL, NULL),
(3, 'Supervisor', 'Supervisor', NULL, NULL, NULL, '2017-05-23 03:53:00', NULL, NULL),
(4, 'Staff', '', NULL, NULL, NULL, '2017-05-24 05:16:56', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_employees`
--

CREATE TABLE `master_employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_identities` int(10) UNSIGNED NOT NULL,
  `value_identities` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` enum('1','2') COLLATE utf8_unicode_ci NOT NULL,
  `id_religions` int(10) UNSIGNED NOT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `place_of_birth` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `initial` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `initial`, `description`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'request_waiting', 'rq', 'modules untuk request', NULL, NULL, NULL, '2017-03-12 20:08:02', '2017-03-19 19:59:06', NULL),
(4, 'request', 'emp', 'module employee', NULL, NULL, NULL, '2017-03-12 20:26:08', '2017-03-19 20:00:41', NULL),
(12, 'policy', '', 'policy', NULL, NULL, NULL, '2017-05-23 03:58:29', '2017-05-23 07:13:13', NULL),
(13, 'cashbond', '', 'cashbond', NULL, NULL, NULL, '2017-05-23 03:59:42', '2017-05-23 07:19:38', NULL),
(14, 'policy_category', '', 'policy_category', NULL, NULL, NULL, '2017-05-23 08:28:18', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `religions`
--

CREATE TABLE `religions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `religions`
--

INSERT INTO `religions` (`id`, `name`, `description`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'reli', 'reli desc', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_users` int(10) UNSIGNED NOT NULL,
  `id_default_roles` int(10) UNSIGNED NOT NULL,
  `id_modules` int(10) UNSIGNED NOT NULL,
  `id_actions` int(10) UNSIGNED NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `id_users`, `id_default_roles`, `id_modules`, `id_actions`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(51, 5, 0, 3, 1, NULL, NULL, NULL, '2017-05-23 04:17:39', NULL, NULL),
(52, 5, 0, 4, 2, NULL, NULL, NULL, '2017-05-23 04:17:39', NULL, NULL),
(53, 5, 0, 4, 3, NULL, NULL, NULL, '2017-05-23 04:17:39', NULL, NULL),
(74, 1, 0, 3, 1, NULL, NULL, NULL, '2017-05-23 08:28:31', NULL, NULL),
(75, 1, 0, 3, 2, NULL, NULL, NULL, '2017-05-23 08:28:31', NULL, NULL),
(76, 1, 0, 3, 3, NULL, NULL, NULL, '2017-05-23 08:28:31', NULL, NULL),
(77, 1, 0, 3, 4, NULL, NULL, NULL, '2017-05-23 08:28:31', NULL, NULL),
(78, 1, 0, 3, 6, NULL, NULL, NULL, '2017-05-23 08:28:31', NULL, NULL),
(79, 1, 0, 4, 1, NULL, NULL, NULL, '2017-05-23 08:28:31', NULL, NULL),
(80, 1, 0, 4, 2, NULL, NULL, NULL, '2017-05-23 08:28:31', NULL, NULL),
(81, 1, 0, 4, 3, NULL, NULL, NULL, '2017-05-23 08:28:31', NULL, NULL),
(82, 1, 0, 4, 4, NULL, NULL, NULL, '2017-05-23 08:28:31', NULL, NULL),
(83, 1, 0, 4, 6, NULL, NULL, NULL, '2017-05-23 08:28:31', NULL, NULL),
(84, 1, 0, 12, 1, NULL, NULL, NULL, '2017-05-23 08:28:31', NULL, NULL),
(85, 1, 0, 12, 2, NULL, NULL, NULL, '2017-05-23 08:28:31', NULL, NULL),
(86, 1, 0, 12, 3, NULL, NULL, NULL, '2017-05-23 08:28:31', NULL, NULL),
(87, 1, 0, 12, 4, NULL, NULL, NULL, '2017-05-23 08:28:32', NULL, NULL),
(88, 1, 0, 12, 6, NULL, NULL, NULL, '2017-05-23 08:28:32', NULL, NULL),
(89, 1, 0, 13, 1, NULL, NULL, NULL, '2017-05-23 08:28:32', NULL, NULL),
(90, 1, 0, 13, 2, NULL, NULL, NULL, '2017-05-23 08:28:32', NULL, NULL),
(91, 1, 0, 13, 3, NULL, NULL, NULL, '2017-05-23 08:28:32', NULL, NULL),
(92, 1, 0, 13, 4, NULL, NULL, NULL, '2017-05-23 08:28:32', NULL, NULL),
(93, 1, 0, 13, 6, NULL, NULL, NULL, '2017-05-23 08:28:32', NULL, NULL),
(94, 1, 0, 14, 1, NULL, NULL, NULL, '2017-05-23 08:28:32', NULL, NULL),
(95, 1, 0, 14, 2, NULL, NULL, NULL, '2017-05-23 08:28:32', NULL, NULL),
(96, 1, 0, 14, 3, NULL, NULL, NULL, '2017-05-23 08:28:32', NULL, NULL),
(97, 1, 0, 14, 4, NULL, NULL, NULL, '2017-05-23 08:28:32', NULL, NULL),
(98, 1, 0, 14, 6, NULL, NULL, NULL, '2017-05-23 08:28:32', NULL, NULL),
(99, 8, 0, 3, 1, NULL, NULL, NULL, '2017-05-24 05:15:14', NULL, NULL),
(100, 8, 0, 3, 2, NULL, NULL, NULL, '2017-05-24 05:15:14', NULL, NULL),
(101, 8, 0, 4, 1, NULL, NULL, NULL, '2017-05-24 05:15:14', NULL, NULL),
(102, 8, 0, 4, 2, NULL, NULL, NULL, '2017-05-24 05:15:14', NULL, NULL),
(103, 10, 0, 4, 1, NULL, NULL, NULL, '2017-05-24 05:15:33', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_employees` int(10) UNSIGNED NOT NULL,
  `id_jobs` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_roles`
--
ALTER TABLE `default_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `default_roles_id_jobs_foreign` (`id_jobs`),
  ADD KEY `default_roles_id_modules_foreign` (`id_modules`),
  ADD KEY `default_roles_id_actions_foreign` (`id_actions`);

--
-- Indexes for table `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employees_id_master_employees_foreign` (`id_master_employees`),
  ADD KEY `employees_id_jobs_foreign` (`id_jobs`);

--
-- Indexes for table `identities`
--
ALTER TABLE `identities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_id_divisions_foreign` (`id_divisions`),
  ADD KEY `jobs_id_jobtitles_foreign` (`id_jobtitles`);

--
-- Indexes for table `jobtitles`
--
ALTER TABLE `jobtitles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_employees`
--
ALTER TABLE `master_employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `master_employees_id_identities_foreign` (`id_identities`),
  ADD KEY `master_employees_id_religions_foreign` (`id_religions`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `religions`
--
ALTER TABLE `religions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roles_id_users_foreign` (`id_users`),
  ADD KEY `roles_id_modules_foreign` (`id_modules`),
  ADD KEY `roles_id_actions_foreign` (`id_actions`),
  ADD KEY `id_default_roles` (`id_default_roles`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_id_employees_foreign` (`id_employees`),
  ADD KEY `id_jobs` (`id_jobs`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actions`
--
ALTER TABLE `actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `default_roles`
--
ALTER TABLE `default_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `identities`
--
ALTER TABLE `identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `jobtitles`
--
ALTER TABLE `jobtitles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `master_employees`
--
ALTER TABLE `master_employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `religions`
--
ALTER TABLE `religions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `default_roles`
--
ALTER TABLE `default_roles`
  ADD CONSTRAINT `default_roles_id_actions_foreign` FOREIGN KEY (`id_actions`) REFERENCES `actions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `default_roles_id_jobs_foreign` FOREIGN KEY (`id_jobs`) REFERENCES `jobs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `default_roles_id_modules_foreign` FOREIGN KEY (`id_modules`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_id_jobs_foreign` FOREIGN KEY (`id_jobs`) REFERENCES `jobs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `employees_id_master_employees_foreign` FOREIGN KEY (`id_master_employees`) REFERENCES `master_employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jobs`
--
ALTER TABLE `jobs`
  ADD CONSTRAINT `jobs_id_divisions_foreign` FOREIGN KEY (`id_divisions`) REFERENCES `divisions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jobs_id_jobtitles_foreign` FOREIGN KEY (`id_jobtitles`) REFERENCES `jobtitles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `master_employees`
--
ALTER TABLE `master_employees`
  ADD CONSTRAINT `master_employees_id_identities_foreign` FOREIGN KEY (`id_identities`) REFERENCES `identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `master_employees_id_religions_foreign` FOREIGN KEY (`id_religions`) REFERENCES `religions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_id_actions_foreign` FOREIGN KEY (`id_actions`) REFERENCES `actions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `roles_id_modules_foreign` FOREIGN KEY (`id_modules`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_jobs`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `users_id_employees_foreign` FOREIGN KEY (`id_employees`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
