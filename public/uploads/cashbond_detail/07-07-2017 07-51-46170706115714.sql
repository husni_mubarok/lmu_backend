/*
MySQL Backup
Source Server Version: 10.1.21
Source Database: sumoboo
Date: 7/6/2017 11:57:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
--  Table structure for `vendor_item`
-- ----------------------------
DROP TABLE IF EXISTS `vendor_item`;
CREATE TABLE `vendor_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_type_id` (`vendor_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `vendor_item_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`),
  CONSTRAINT `vendor_item_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records 
-- ----------------------------
INSERT INTO `vendor_item` VALUES ('1','2','15','1',NULL,NULL,'2017-07-06 03:29:29','2017-07-06 03:29:29',NULL), ('2','2','14','1',NULL,NULL,'2017-07-06 03:29:40','2017-07-06 04:10:22','2017-07-06 04:10:22'), ('3','2','15','1',NULL,NULL,'2017-07-06 04:13:18','2017-07-06 04:13:31','2017-07-06 04:13:31'), ('4','2','12','1',NULL,NULL,'2017-07-06 04:13:26','2017-07-06 04:13:26',NULL), ('5','1','15','1',NULL,NULL,'2017-07-06 04:14:04','2017-07-06 04:14:04',NULL), ('6','1','15','1',NULL,NULL,'2017-07-06 04:14:08','2017-07-06 04:14:08',NULL);
